insert into sysuser(password, is_registered, is_disabled, is_system, guuid, password_salt)
values('6c2a16cb55f07b47a14da2777a5ef9da', 1, 0, 1, 'f34b1f37-d976-454d-a3e9-8cbaee7b96c4', 'f34b1f37-d976-454d-a3e9-8cbaee7b96c4');

insert into endpoint(id_sysuser, id_notification_type, notification_id, is_verified, is_enabled, is_default)
values((select id_sysuser from sysuser where guuid = 'f34b1f37-d976-454d-a3e9-8cbaee7b96c4'),
  (select id_notification_type from notification_type where code = 'sms'),
  '79000000001', 1, 0, 1);

insert into endpoint(id_sysuser, id_notification_type, notification_id, is_verified, is_enabled, is_default)
values((select id_sysuser from sysuser where guuid = 'f34b1f37-d976-454d-a3e9-8cbaee7b96c4'),
  (select id_notification_type from notification_type where code = 'email'), 'admin@stemteam.ru', 1, 0, 1);

insert into stem_right_user(guuid) values('f34b1f37-d976-454d-a3e9-8cbaee7b96c4');

insert into stem_right_userrole(id_stem_right_user, id_stem_right_role)
values((select id_stem_right_user from stem_right_user where guuid = 'f34b1f37-d976-454d-a3e9-8cbaee7b96c4'),
       (select id_stem_right_role from stem_right_role where code = 'admin'));
