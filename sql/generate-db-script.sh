#! /bin/sh

TARGET=full-auth-db.sql

CREATE_DB="""
-- AUTHDB - база данных авторизации

-- mkdir -p /pgstore/authdb/authdb_tbs_default
-- chown postgres:postgres /pgstore/authdb -R
-- chmod 700 /pgstore/authdb -R

-- psql -p 5432 -U postgres

-- Cоздаем пользователя - владельца БД
-- CREATE ROLE authadmin WITH PASSWORD 'pass';
-- alter role authadmin login;
-- UPDATE pg_authid SET rolcatupdate=false WHERE rolname='authadmin';
-- COMMENT ON ROLE authadmin IS 'Администратор AUTHDB';

-- Табличное пространство
-- create tablespace authdb_tbs_default owner authadmin location '/pgstore/authdb/authdb_tbs_default';

-- База данных
-- CREATE DATABASE authdb WITH ENCODING='UTF8' OWNER=authadmin CONNECTION LIMIT=-1 TABLESPACE=authdb_tbs_default LC_COLLATE = 'ru_RU.utf8' LC_CTYPE = 'ru_RU.utf8';

"""

echo "$CREATE_DB" > "$TARGET"

iconv -f cp1251 -t utf8 "create_tables.sql" >> "$TARGET"
dos2unix "$TARGET"

#cat create_tables.sql > "$TARGET"

for FILE in $(ls ./procedures/*.sql); do
    echo >> "$TARGET"
    cat "$FILE" >> "$TARGET"
# /opt/postgres/9.1/bin/psql mondb monitoring -f $file;
done

echo >> "$TARGET"

for FILE in $(ls ./data/*.sql); do
    echo >> "$TARGET"
    cat "$FILE" >> "$TARGET"
# /opt/postgres/9.1/bin/psql mondb monitoring -f $file;
done

