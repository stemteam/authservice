-- AUTHDB - база данных авторизации

-- mkdir -p /pgstore/authdb/authdb_tbs_default
-- chown postgres:postgres /pgstore/authdb -R
-- chmod 700 /pgstore/authdb -R

-- /opt/PostgreSQL/9.3/bin/psql -p 5433 -U postgres

-- Cоздаем пользователя - владельца БД
CREATE ROLE authadmin WITH PASSWORD 'pass';
alter role authadmin login;
UPDATE pg_authid SET rolcatupdate=false WHERE rolname='authadmin';
COMMENT ON ROLE authadmin IS 'Администратор AUTHDB';

-- Табличное пространство
create tablespace authdb_tbs_default owner authadmin location '/pgstore/authdb/authdb_tbs_default';

-- База данных
CREATE DATABASE authdb WITH ENCODING='UTF8' OWNER=authadmin CONNECTION LIMIT=-1 TABLESPACE=authdb_tbs_default LC_COLLATE = 'ru_RU.utf8' LC_CTYPE = 'ru_RU.utf8';

-- накатываем схему из модели
/opt/PostgreSQL/9.3/bin/psql -f create_tables.sql authdb authadmin


-- ключ разработчика
insert into stem_right_apikey (strdata, description) values ('ECD55DDF9398A40DFD1741732E95ACE3D97B24F6666E934C161F83CEF211EF24', 'Ключ разработчика');


