/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     14.10.2016 13:33:37                          */
/*==============================================================*/


create sequence G_ID_ABONENTREF;

create sequence G_ID_APIKEY;

create sequence G_ID_ENDPOINT;

create sequence G_ID_LOCALE;

create sequence G_ID_NOTIFICATION;

create sequence G_ID_NOTIFICATION_TYPE;

create sequence G_ID_PNS_TYPE;

create sequence G_ID_REFTYPE;

create sequence G_ID_SHORT_URL;

create sequence G_ID_STEM_RIGHT_APIKEY;

create sequence G_ID_STEM_RIGHT_RIGHT;

create sequence G_ID_STEM_RIGHT_ROLE;

create sequence G_ID_STEM_RIGHT_ROLERIGHT;

create sequence G_ID_STEM_RIGHT_USER;

create sequence G_ID_STEM_RIGHT_USERROLE;

create sequence G_ID_SYSINVITE;

create sequence G_ID_SYSUSER;

create sequence G_ID_USERREF;

create sequence G_ID_USER_REQUEST;

create sequence G_ID_USER_REQUEST_TYPE;

/*==============================================================*/
/* Table: ENDPOINT                                              */
/*==============================================================*/
create table ENDPOINT (
   ID_ENDPOINT          INT4                 not null,
   ID_SYSUSER           INT4                 null,
   ID_NOTIFICATION_TYPE INT4                 not null,
   ID_PNS_TYPE          INT4                 null,
   ID_LOCALE            INT4                 null,
   DEVICE_ID            VARCHAR              null,
   AZURE_ID             VARCHAR              null,
   NOTIFICATION_ID      VARCHAR              not null,
   NAME                 VARCHAR              null,
   MODEL                VARCHAR              null,
   IS_TABLET            INT2                 null default 0
      constraint CKC_IS_TABLET_ENDPOINT check (IS_TABLET is null or (IS_TABLET in (0,1))),
   IS_VERIFIED          INT2                 not null default 0
      constraint CKC_IS_VERIFIED_ENDPOINT check (IS_VERIFIED in (0,1)),
   IS_ENABLED           INT2                 not null default 0
      constraint CKC_IS_ENABLED_ENDPOINT check (IS_ENABLED in (0,1)),
   CREATEDATE           TIMESTAMP(3)         not null,
   LASTDATE             TIMESTAMP(3)         not null,
   IS_DEFAULT           INT2                 not null default 0
      constraint CKC_IS_DEFAULT_ENDPOINT check (IS_DEFAULT in (0,1)),
   APIKEY               VARCHAR              null
);

alter table ENDPOINT
   add constraint PK_ENDPOINT primary key (ID_ENDPOINT);

/*==============================================================*/
/* Index: SI_ENDPOINT_ID_NOTIFICATION_TYP                       */
/*==============================================================*/
create  index SI_ENDPOINT_ID_NOTIFICATION_TYP on ENDPOINT (
ID_NOTIFICATION_TYPE
);

/*==============================================================*/
/* Index: SI_ENDPOINT_ID_SYSUSER                                */
/*==============================================================*/
create  index SI_ENDPOINT_ID_SYSUSER on ENDPOINT (
ID_SYSUSER
);

/*==============================================================*/
/* Index: FI_ENDPOINT_DEVICE_ID                                 */
/*==============================================================*/
create  index FI_ENDPOINT_DEVICE_ID on ENDPOINT (
DEVICE_ID
);

/*==============================================================*/
/* Index: SI_ENDPOINT_NOTIFICATION_ID                           */
/*==============================================================*/
create  index SI_ENDPOINT_NOTIFICATION_ID on ENDPOINT (
NOTIFICATION_ID
);

/*==============================================================*/
/* Index: SI_ENDPOINT_PNS_TYPE_NOTIFICATI                       */
/*==============================================================*/
create unique index SI_ENDPOINT_PNS_TYPE_NOTIFICATI on ENDPOINT (
ID_PNS_TYPE,
NOTIFICATION_ID
);

/*==============================================================*/
/* Index: SI_ENDPOINT_USER_TYPE_ID                              */
/*==============================================================*/
create unique index SI_ENDPOINT_USER_TYPE_ID on ENDPOINT (
ID_NOTIFICATION_TYPE,
NOTIFICATION_ID
);

/*==============================================================*/
/* Table: LOCALE                                                */
/*==============================================================*/
create table LOCALE (
   ID_LOCALE            INT4                 not null,
   CODE                 VARCHAR              not null,
   CREATEDATE           TIMESTAMP(3)         not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP(3)         not null default CURRENT_TIMESTAMP
);

alter table LOCALE
   add constraint PK_LOCALE primary key (ID_LOCALE);

/*==============================================================*/
/* Index: SI_LOCALE                                             */
/*==============================================================*/
create  index SI_LOCALE on LOCALE (
CODE
);

/*==============================================================*/
/* Table: NOTIFICATION                                          */
/*==============================================================*/
create table NOTIFICATION (
   ID_NOTIFICATION      INT8                 not null,
   ID_ENDPOINT          INT4                 not null,
   EXPIRATIONDATE       TIMESTAMP(3)         not null,
   IS_NOTIFIED          INT2                 not null default 0
      constraint CKC_IS_NOTIFIED_NOTIFICA check (IS_NOTIFIED in (0,1)),
   IS_EXPIRED           INT2                 not null default 0
      constraint CKC_IS_EXPIRED_NOTIFICA check (IS_EXPIRED in (0,1)),
   SENDDATE             TIMESTAMP(3)         null,
   TITLE                VARCHAR              null,
   MESSAGE              VARCHAR              null,
   DATA                 VARCHAR              null,
   CREATEDATE           TIMESTAMP(3)         not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP(3)         not null default CURRENT_TIMESTAMP
);

CREATE INDEX i_notification_active ON notification (is_expired) where is_notified = 0;

alter table NOTIFICATION
   add constraint PK_NOTIFICATION primary key (ID_NOTIFICATION);

/*==============================================================*/
/* Index: FI_NOTIFICATION_ENDPOINT                              */
/*==============================================================*/
create  index FI_NOTIFICATION_ENDPOINT on NOTIFICATION (
ID_ENDPOINT
);

/*==============================================================*/
/* Table: NOTIFICATION_TYPE                                     */
/*==============================================================*/
create table NOTIFICATION_TYPE (
   ID_NOTIFICATION_TYPE INT4                 not null,
   CODE                 VARCHAR              not null,
   NAME                 VARCHAR              not null,
   CREATEDATE           TIMESTAMP(3)         not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP(3)         not null default CURRENT_TIMESTAMP
);

insert into notification_type (id_notification_type, code, name) values (nextval('g_id_notification_type'), 'sms', '����������� �� SMS');
insert into notification_type (id_notification_type, code, name) values (nextval('g_id_notification_type'), 'email', '����������� �� EMAIL');
insert into notification_type (id_notification_type, code, name) values (nextval('g_id_notification_type'), 'push', 'Push �����������');

alter table NOTIFICATION_TYPE
   add constraint PK_NOTIFICATION_TYPE primary key (ID_NOTIFICATION_TYPE);

/*==============================================================*/
/* Index: SI_NOTIFICATION_TYPE                                  */
/*==============================================================*/
create unique index SI_NOTIFICATION_TYPE on NOTIFICATION_TYPE (
CODE
);

/*==============================================================*/
/* Table: PNS_TYPE                                              */
/*==============================================================*/
create table PNS_TYPE (
   ID_PNS_TYPE          INT4                 not null,
   CODE                 VARCHAR              not null,
   NAME                 VARCHAR              not null,
   CREATEDATE           TIMESTAMP(3)         not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP(3)         not null default CURRENT_TIMESTAMP
);

insert into PNS_TYPE (id_pns_type, code, name) values (nextval('g_id_pns_type'), 'gcm', 'Android PNS');
insert into PNS_TYPE (id_pns_type, code, name) values (nextval('g_id_pns_type'), 'apns', 'Apple PNS');
insert into PNS_TYPE (id_pns_type, code, name) values (nextval('g_id_pns_type'), 'mpns', 'Windows phone 8');
insert into PNS_TYPE (id_pns_type, code, name) values (nextval('g_id_pns_type'), 'wns', 'Windows store');

alter table PNS_TYPE
   add constraint PK_PNS_TYPE primary key (ID_PNS_TYPE);

/*==============================================================*/
/* Index: SI_PNS_TYPE                                           */
/*==============================================================*/
create unique index SI_PNS_TYPE on PNS_TYPE (
CODE
);

/*==============================================================*/
/* Table: REFTYPE                                               */
/*==============================================================*/
create table REFTYPE (
   ID_REFTYPE           INT4                 not null,
   CODE                 VARCHAR              not null,
   NAME                 VARCHAR              not null,
   CREATEDATE           TIMESTAMP(3)         not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP(3)         not null default CURRENT_TIMESTAMP
);

insert into reftype (id_reftype, code, name) values (nextval('g_id_reftype'), 'child', '�������');

alter table REFTYPE
   add constraint PK_REFTYPE primary key (ID_REFTYPE);

/*==============================================================*/
/* Index: SI_REFTYPE                                            */
/*==============================================================*/
create unique index SI_REFTYPE on REFTYPE (
CODE
);

/*==============================================================*/
/* Table: SHORT_URL                                             */
/*==============================================================*/
create table SHORT_URL (
   ID_SHORT_URL         INT8                 not null,
   URL                  VARCHAR              not null,
   CREATEDATE           TIMESTAMP(3)         not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP(3)         not null default CURRENT_TIMESTAMP
);

alter table SHORT_URL
   add constraint PK_SHORT_URL primary key (ID_SHORT_URL);

/*==============================================================*/
/* Index: SI_SHORT_URL                                          */
/*==============================================================*/
create unique index SI_SHORT_URL on SHORT_URL (
URL
);

/*==============================================================*/
/* Table: STEM_RIGHT_APIKEY                                     */
/*==============================================================*/
create table STEM_RIGHT_APIKEY (
   ID_STEM_RIGHT_APIKEY INT4                 not null,
   STRDATA              VARCHAR              not null,
   DESCRIPTION          VARCHAR              not null,
   CREATEDATE           TIMESTAMP(3)         not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP(3)         not null default CURRENT_TIMESTAMP
);

alter table STEM_RIGHT_APIKEY
   add constraint PK_STEM_RIGHT_APIKEY primary key (ID_STEM_RIGHT_APIKEY);

/*==============================================================*/
/* Index: SI_STEM_RIGHT_APIKEY                                  */
/*==============================================================*/
create unique index SI_STEM_RIGHT_APIKEY on STEM_RIGHT_APIKEY (
STRDATA
);

/*==============================================================*/
/* Table: STEM_RIGHT_RIGHT                                      */
/*==============================================================*/
create table STEM_RIGHT_RIGHT (
   ID_STEM_RIGHT_RIGHT  INT4                 not null,
   CODE                 VARCHAR              not null,
   DESCRIPTION          VARCHAR              not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

alter table STEM_RIGHT_RIGHT
   add constraint PK_STEM_RIGHT_RIGHT primary key (ID_STEM_RIGHT_RIGHT);

/*==============================================================*/
/* Index: SI_STEM_RIGHT_RIGHT                                   */
/*==============================================================*/
create unique index SI_STEM_RIGHT_RIGHT on STEM_RIGHT_RIGHT (
CODE
);

/*==============================================================*/
/* Table: STEM_RIGHT_ROLE                                       */
/*==============================================================*/
create table STEM_RIGHT_ROLE (
   ID_STEM_RIGHT_ROLE   INT4                 not null,
   NAME                 VARCHAR              not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP,
   DESCRIPTION          VARCHAR              not null,
   CODE                 VARCHAR              not null,
   IS_ADMIN             INT2                 not null default 0
      constraint CKC_IS_ADMIN_STEM_RIG check (IS_ADMIN in (0,1))
);

insert into stem_right_role (id_stem_right_role, code, name, description, is_admin) values (1, 'admin', 'admin', '�������������', 1);
alter sequence g_id_stem_right_role restart with 2;

alter table STEM_RIGHT_ROLE
   add constraint PK_STEM_RIGHT_ROLE primary key (ID_STEM_RIGHT_ROLE);

/*==============================================================*/
/* Index: SI_STEM_RIGHT_ROLE                                    */
/*==============================================================*/
create unique index SI_STEM_RIGHT_ROLE on STEM_RIGHT_ROLE (
CODE
);

/*==============================================================*/
/* Table: STEM_RIGHT_ROLERIGHT                                  */
/*==============================================================*/
create table STEM_RIGHT_ROLERIGHT (
   ID_STEM_RIGHT_ROLERIGHT INT4                 not null,
   ID_STEM_RIGHT_ROLE   INT4                 not null,
   ID_STEM_RIGHT_RIGHT  INT4                 not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

alter table STEM_RIGHT_ROLERIGHT
   add constraint PK_STEM_RIGHT_ROLERIGHT primary key (ID_STEM_RIGHT_ROLERIGHT);

/*==============================================================*/
/* Index: FI_STEM_RIGHT_ROLERIGHT_ROLE                          */
/*==============================================================*/
create  index FI_STEM_RIGHT_ROLERIGHT_ROLE on STEM_RIGHT_ROLERIGHT (
ID_STEM_RIGHT_ROLE
);

/*==============================================================*/
/* Index: FI_STEM_RIGHT_ROLERIGHT_RIGHT                         */
/*==============================================================*/
create  index FI_STEM_RIGHT_ROLERIGHT_RIGHT on STEM_RIGHT_ROLERIGHT (
ID_STEM_RIGHT_RIGHT
);

/*==============================================================*/
/* Index: SI_STEM_RIGHT_ROLERIGHT                               */
/*==============================================================*/
create unique index SI_STEM_RIGHT_ROLERIGHT on STEM_RIGHT_ROLERIGHT (
ID_STEM_RIGHT_ROLE,
ID_STEM_RIGHT_RIGHT
);

/*==============================================================*/
/* Table: STEM_RIGHT_USER                                       */
/*==============================================================*/
create table STEM_RIGHT_USER (
   ID_STEM_RIGHT_USER   INT4                 not null,
   GUUID                VARCHAR              not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

alter table STEM_RIGHT_USER
   add constraint PK_STEM_RIGHT_USER primary key (ID_STEM_RIGHT_USER);

/*==============================================================*/
/* Index: SI_STEM_RIGHT_USER                                    */
/*==============================================================*/
create unique index SI_STEM_RIGHT_USER on STEM_RIGHT_USER (
GUUID
);

/*==============================================================*/
/* Table: STEM_RIGHT_USERROLE                                   */
/*==============================================================*/
create table STEM_RIGHT_USERROLE (
   ID_STEM_RIGHT_USERROLE INT4                 not null,
   ID_STEM_RIGHT_ROLE   INT4                 not null,
   ID_STEM_RIGHT_USER   INT4                 not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

alter table STEM_RIGHT_USERROLE
   add constraint PK_STEM_RIGHT_USERROLE primary key (ID_STEM_RIGHT_USERROLE);

/*==============================================================*/
/* Index: FI_STEM_RIGHT_USERROLE_USER                           */
/*==============================================================*/
create  index FI_STEM_RIGHT_USERROLE_USER on STEM_RIGHT_USERROLE (
ID_STEM_RIGHT_USER
);

/*==============================================================*/
/* Index: FI_STEM_RIGHT_USERROLE_ROLE                           */
/*==============================================================*/
create  index FI_STEM_RIGHT_USERROLE_ROLE on STEM_RIGHT_USERROLE (
ID_STEM_RIGHT_ROLE
);

/*==============================================================*/
/* Index: SI_STEM_RIGHT_USERROLE                                */
/*==============================================================*/
create unique index SI_STEM_RIGHT_USERROLE on STEM_RIGHT_USERROLE (
ID_STEM_RIGHT_ROLE,
ID_STEM_RIGHT_USER
);

/*==============================================================*/
/* Table: SYSINVITE                                             */
/*==============================================================*/
create table SYSINVITE (
   ID_SYSINVITE         INT4                 not null,
   ID_SYSUSER           INT4                 null,
   CODE                 VARCHAR              not null,
   CREATEDATE           TIMESTAMP(3)         not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP(3)         not null default CURRENT_TIMESTAMP
);

alter table SYSINVITE
   add constraint PK_SYSINVITE primary key (ID_SYSINVITE);

/*==============================================================*/
/* Index: SI_SYSINVITE                                          */
/*==============================================================*/
create unique index SI_SYSINVITE on SYSINVITE (
CODE
);

/*==============================================================*/
/* Index: FI_SYSINVITE_SYSUSER                                  */
/*==============================================================*/
create  index FI_SYSINVITE_SYSUSER on SYSINVITE (
ID_SYSUSER
);

/*==============================================================*/
/* Table: SYSLOGIN                                              */
/*==============================================================*/
create table SYSLOGIN (
   ID_SYSLOGIN          UUID                 not null,
   ID_SYSUSER           INT4                 not null,
   ID_ENDPOINT          INT4                 null,
   CREATEDATE           TIMESTAMP(3)         not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP(3)         not null default CURRENT_TIMESTAMP,
   EXPIRED_TIME         TIMESTAMP(3)         not null default CURRENT_TIMESTAMP + interval '1 day',
   STATE                VARCHAR              null,
   EXT_ACCESS_TOKEN     VARCHAR              null
);

alter table SYSLOGIN
   add constraint PK_SYSLOGIN primary key (ID_SYSLOGIN);

/*==============================================================*/
/* Index: FI_SYSLOGIN_SYSUSER                                   */
/*==============================================================*/
create  index FI_SYSLOGIN_SYSUSER on SYSLOGIN (
ID_SYSUSER
);

/*==============================================================*/
/* Index: I_SYSLOGIN_LASTDATE                                   */
/*==============================================================*/
create  index I_SYSLOGIN_LASTDATE on SYSLOGIN (
LASTDATE
);

/*==============================================================*/
/* Index: FI_SYSLOGIN_ENDPOINT                                  */
/*==============================================================*/
create  index FI_SYSLOGIN_ENDPOINT on SYSLOGIN (
ID_ENDPOINT
);

/*==============================================================*/
/* Index: I_SYSLOGIN_STATE                                      */
/*==============================================================*/
create  index I_SYSLOGIN_STATE on SYSLOGIN (
ID_ENDPOINT,
STATE
);

/*==============================================================*/
/* Table: SYSUSER                                               */
/*==============================================================*/
create table SYSUSER (
   ID_SYSUSER           INT4                 not null,
   GUUID                UUID                 not null,
   PASSWORD             VARCHAR              not null,
   IS_REGISTERED        INT2                 not null default 0
      constraint CKC_IS_REGISTERED_SYSUSER check (IS_REGISTERED in (0,1)),
   IS_DISABLED          INT2                 not null default 0
      constraint CKC_IS_DISABLED_SYSUSER check (IS_DISABLED in (0,1)),
   CREATEDATE           TIMESTAMP(3)         not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP(3)         not null default CURRENT_TIMESTAMP,
   IS_SYSTEM            INT2                 not null default 0
      constraint CKC_IS_SYSTEM_SYSUSER check (IS_SYSTEM in (0,1)),
   FIRST_NAME           VARCHAR              null,
   LAST_NAME            VARCHAR              null,
   MIDDLE_NAME          VARCHAR              null,
   IMAGE_URL            VARCHAR              null,
   PASSWORD_SALT        VARCHAR              not null,
   BIRTHDAY             TIMESTAMP            null;
);

alter table SYSUSER
   add constraint PK_SYSUSER primary key (ID_SYSUSER);

/*==============================================================*/
/* Index: SI_SYSUSER_GUUID                                      */
/*==============================================================*/
create unique index SI_SYSUSER_GUUID on SYSUSER (
GUUID
);

/*==============================================================*/
/* Index: I_SYSUSER_LASTDATE                                    */
/*==============================================================*/
create  index I_SYSUSER_LASTDATE on SYSUSER (
LASTDATE
);

/*==============================================================*/
/* Table: USERREF                                               */
/*==============================================================*/
create table USERREF (
   ID_USERREF           INT8                 not null,
   ID_SYSUSER           INT4                 not null,
   ID_SYSUSER_REF       INT4                 not null,
   ID_REFTYPE           INT4                 not null,
   CREATEDATE           TIMESTAMP(3)         not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP(3)         not null default CURRENT_TIMESTAMP
);

alter table USERREF
   add constraint PK_USERREF primary key (ID_USERREF);

/*==============================================================*/
/* Index: FI_USERREF_SYSUSER                                    */
/*==============================================================*/
create  index FI_USERREF_SYSUSER on USERREF (
ID_SYSUSER
);

/*==============================================================*/
/* Index: FI_USERREF_SYSUSERREF                                 */
/*==============================================================*/
create  index FI_USERREF_SYSUSERREF on USERREF (
ID_SYSUSER_REF
);

/*==============================================================*/
/* Index: FI_USERREF_REFTYPE                                    */
/*==============================================================*/
create  index FI_USERREF_REFTYPE on USERREF (
ID_REFTYPE
);

/*==============================================================*/
/* Index: SI_USERREF                                            */
/*==============================================================*/
create unique index SI_USERREF on USERREF (
ID_REFTYPE,
ID_SYSUSER,
ID_SYSUSER_REF
);

/*==============================================================*/
/* Table: USER_REQUEST                                          */
/*==============================================================*/
create table USER_REQUEST (
   ID_USER_REQUEST      INT8                 not null,
   ID_SYSUSER           INT4                 not null,
   ID_USER_REQUEST_TYPE INT2                 not null,
   CODE                 VARCHAR              not null,
   EXPIRATION_TIME      TIMESTAMP(3)         not null,
   CREATEDATE           TIMESTAMP(3)         not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP(3)         not null default CURRENT_TIMESTAMP
);

alter table USER_REQUEST
   add constraint PK_USER_REQUEST primary key (ID_USER_REQUEST);

/*==============================================================*/
/* Index: FI_USER_REQUEST_USER_REQUEST_TY                       */
/*==============================================================*/
create  index FI_USER_REQUEST_USER_REQUEST_TY on USER_REQUEST (
ID_USER_REQUEST_TYPE
);

/*==============================================================*/
/* Index: FI_USER_REQUEST_SYSUSER                               */
/*==============================================================*/
create  index FI_USER_REQUEST_SYSUSER on USER_REQUEST (
ID_SYSUSER
);

/*==============================================================*/
/* Index: SI_USER_REQUEST                                       */
/*==============================================================*/
create unique index SI_USER_REQUEST on USER_REQUEST (
ID_SYSUSER,
ID_USER_REQUEST_TYPE
);

/*==============================================================*/
/* Table: USER_REQUEST_TYPE                                     */
/*==============================================================*/
create table USER_REQUEST_TYPE (
   ID_USER_REQUEST_TYPE INT2                 not null,
   CODE                 VARCHAR              not null,
   NAME                 VARCHAR              not null,
   CREATEDATE           TIMESTAMP(3)         not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP(3)         not null default CURRENT_TIMESTAMP
);

insert into USER_REQUEST_TYPE (ID_USER_REQUEST_TYPE, code, name) values (nextval('G_ID_USER_REQUEST_TYPE'), 'register', '�����������');
insert into USER_REQUEST_TYPE (ID_USER_REQUEST_TYPE, code, name) values (nextval('G_ID_USER_REQUEST_TYPE'), 'passrecover', '�������������� ������');
insert into USER_REQUEST_TYPE (ID_USER_REQUEST_TYPE, code, name) values (nextval('G_ID_USER_REQUEST_TYPE'), 'delete', '�������� �������');

alter table USER_REQUEST_TYPE
   add constraint PK_USER_REQUEST_TYPE primary key (ID_USER_REQUEST_TYPE);

/*==============================================================*/
/* Index: SI_USER_REQUEST_TYPE                                  */
/*==============================================================*/
create unique index SI_USER_REQUEST_TYPE on USER_REQUEST_TYPE (
CODE
);

/*==============================================================*/
/* Index: SI_USER_REQUEST_TYPE2                                 */
/*==============================================================*/
create unique index SI_USER_REQUEST_TYPE2 on USER_REQUEST_TYPE (
NAME
);

alter table ENDPOINT
   add constraint FK_ENDPOINT_SYSUSER foreign key (ID_SYSUSER)
      references SYSUSER (ID_SYSUSER)
      on delete cascade on update restrict;

alter table ENDPOINT
   add constraint FK_ENDPOINT_NOTIFICATION_TYPE foreign key (ID_NOTIFICATION_TYPE)
      references NOTIFICATION_TYPE (ID_NOTIFICATION_TYPE)
      on delete restrict on update restrict;

alter table ENDPOINT
   add constraint FK_ENDPOINT_PNS_TYPE foreign key (ID_PNS_TYPE)
      references PNS_TYPE (ID_PNS_TYPE)
      on delete restrict on update restrict;

alter table ENDPOINT
   add constraint FK_ENDPOINT_LOCALE foreign key (ID_LOCALE)
      references LOCALE (ID_LOCALE)
      on delete restrict on update restrict;

alter table NOTIFICATION
   add constraint FK_NOTIFICATION_ENDPOINT foreign key (ID_ENDPOINT)
      references ENDPOINT (ID_ENDPOINT)
      on delete cascade on update restrict;

alter table STEM_RIGHT_ROLERIGHT
   add constraint FK_STEM_RIGHT_ROLERIGHT_STEM_RIGHT_RIGHT foreign key (ID_STEM_RIGHT_RIGHT)
      references STEM_RIGHT_RIGHT (ID_STEM_RIGHT_RIGHT)
      on delete restrict on update restrict;

alter table STEM_RIGHT_ROLERIGHT
   add constraint FK_STEM_RIGHT_ROLERIGHT_STEM_RIGHT_ROLE foreign key (ID_STEM_RIGHT_ROLE)
      references STEM_RIGHT_ROLE (ID_STEM_RIGHT_ROLE)
      on delete restrict on update restrict;

alter table STEM_RIGHT_USERROLE
   add constraint FK_STEM_RIGHT_USERROLE_STEM_RIGHT_ROLE foreign key (ID_STEM_RIGHT_ROLE)
      references STEM_RIGHT_ROLE (ID_STEM_RIGHT_ROLE)
      on delete restrict on update restrict;

alter table STEM_RIGHT_USERROLE
   add constraint FK_STEM_RIGHT_USERROLE_STEM_RIGHT_USER foreign key (ID_STEM_RIGHT_USER)
      references STEM_RIGHT_USER (ID_STEM_RIGHT_USER)
      on delete restrict on update restrict;

alter table SYSINVITE
   add constraint FK_SYSINVITE_SYSUSER foreign key (ID_SYSUSER)
      references SYSUSER (ID_SYSUSER)
      on delete restrict on update restrict;

alter table SYSLOGIN
   add constraint FK_SYSLOGIN_SYSUSER foreign key (ID_SYSUSER)
      references SYSUSER (ID_SYSUSER)
      on delete restrict on update restrict;

alter table SYSLOGIN
   add constraint FK_SYSLOGIN_ENDPOINT foreign key (ID_ENDPOINT)
      references ENDPOINT (ID_ENDPOINT)
      on delete restrict on update restrict;

alter table USERREF
   add constraint FK_USERREF_REFTYPE foreign key (ID_REFTYPE)
      references REFTYPE (ID_REFTYPE)
      on delete restrict on update restrict;

alter table USERREF
   add constraint FK_USERREF_SYSUSER foreign key (ID_SYSUSER)
      references SYSUSER (ID_SYSUSER)
      on delete restrict on update restrict;

alter table USERREF
   add constraint FK_USERREF_SYSUSER_REF foreign key (ID_SYSUSER_REF)
      references SYSUSER (ID_SYSUSER)
      on delete restrict on update restrict;

alter table USER_REQUEST
   add constraint FK_USER_REQUEST_SYSUSER foreign key (ID_SYSUSER)
      references SYSUSER (ID_SYSUSER)
      on delete cascade on update restrict;

alter table USER_REQUEST
   add constraint FK_USER_REQUEST_USER_REQUEST_TYPE foreign key (ID_USER_REQUEST_TYPE)
      references USER_REQUEST_TYPE (ID_USER_REQUEST_TYPE)
      on delete restrict on update restrict;


-- trigger function for INSERT
create or replace function F_TIB_ENDPOINT() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_ENDPOINT is null) then
    new.id_ENDPOINT := nextval('G_ID_ENDPOINT');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_ENDPOINT 
before insert
on ENDPOINT  
for each row
execute procedure F_TIB_ENDPOINT();


-- trigger function for UPDATE
create or replace function F_TUB_ENDPOINT() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_ENDPOINT 
before update
on ENDPOINT 
for each row
execute procedure F_TUB_ENDPOINT();


-- trigger function for INSERT
create or replace function F_TIB_PNS_TYPE() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_LOCALE is null) then
    new.id_LOCALE := nextval('G_ID_LOCALE');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_PNS_TYPE 
before insert
on LOCALE  
for each row
execute procedure F_TIB_PNS_TYPE();


-- trigger function for UPDATE
create or replace function F_TUB_PNS_TYPE() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_PNS_TYPE 
before update
on LOCALE 
for each row
execute procedure F_TUB_PNS_TYPE();


-- trigger function for INSERT
create or replace function F_TIB_NOTIFICATION() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_NOTIFICATION is null) then
    new.id_NOTIFICATION := nextval('G_ID_NOTIFICATION');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_NOTIFICATION 
before insert
on NOTIFICATION  
for each row
execute procedure F_TIB_NOTIFICATION();


-- trigger function for UPDATE
create or replace function F_TUB_NOTIFICATION() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  
  if (old.is_notified = 0 and new.is_notified = 1) then
    new.senddate := current_timestamp;
  end if;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_NOTIFICATION 
before update
on NOTIFICATION 
for each row
execute procedure F_TUB_NOTIFICATION();


-- trigger function for INSERT
create or replace function F_TIB_NOTIFICATION_TYPE() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_NOTIFICATION_TYPE is null) then
    new.id_NOTIFICATION_TYPE := nextval('G_ID_NOTIFICATION_TYPE');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_NOTIFICATION_TYPE 
before insert
on NOTIFICATION_TYPE  
for each row
execute procedure F_TIB_NOTIFICATION_TYPE();


-- trigger function for UPDATE
create or replace function F_TUB_NOTIFICATION_TYPE() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_NOTIFICATION_TYPE 
before update
on NOTIFICATION_TYPE 
for each row
execute procedure F_TUB_NOTIFICATION_TYPE();


-- trigger function for INSERT
create or replace function F_TIB_PNS_TYPE() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_PNS_TYPE is null) then
    new.id_PNS_TYPE := nextval('G_ID_PNS_TYPE');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_PNS_TYPE 
before insert
on PNS_TYPE  
for each row
execute procedure F_TIB_PNS_TYPE();


-- trigger function for UPDATE
create or replace function F_TUB_PNS_TYPE() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_PNS_TYPE 
before update
on PNS_TYPE 
for each row
execute procedure F_TUB_PNS_TYPE();


-- trigger function for INSERT
create or replace function F_TIB_REFTYPE() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_REFTYPE is null) then
    new.id_REFTYPE := nextval('G_ID_REFTYPE');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_REFTYPE 
before insert
on REFTYPE  
for each row
execute procedure F_TIB_REFTYPE();


-- trigger function for UPDATE
create or replace function F_TUB_REFTYPE() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_REFTYPE 
before update
on REFTYPE 
for each row
execute procedure F_TUB_REFTYPE();


-- trigger function for INSERT
create or replace function F_TIB_SHORT_URL() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_SHORT_URL is null) then
    new.id_SHORT_URL := nextval('G_ID_SHORT_URL');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_SHORT_URL 
before insert
on SHORT_URL  
for each row
execute procedure F_TIB_SHORT_URL();


-- trigger function for UPDATE
create or replace function F_TUB_SHORT_URL() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_SHORT_URL 
before update
on SHORT_URL 
for each row
execute procedure F_TUB_SHORT_URL();


-- trigger function for INSERT
create or replace function F_TIB_STEM_RIGHT_APIKEY() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_STEM_RIGHT_APIKEY is null) then
    new.id_STEM_RIGHT_APIKEY := nextval('G_ID_STEM_RIGHT_APIKEY');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_STEM_RIGHT_APIKEY 
before insert
on STEM_RIGHT_APIKEY  
for each row
execute procedure F_TIB_STEM_RIGHT_APIKEY();


-- trigger function for UPDATE
create or replace function F_TUB_STEM_RIGHT_APIKEY() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_STEM_RIGHT_APIKEY 
before update
on STEM_RIGHT_APIKEY 
for each row
execute procedure F_TUB_STEM_RIGHT_APIKEY();


-- trigger function for INSERT
create or replace function F_TIB_STEM_RIGHT_RIGHT() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_STEM_RIGHT_RIGHT is null) then
    new.id_STEM_RIGHT_RIGHT := nextval('G_ID_STEM_RIGHT_RIGHT');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_STEM_RIGHT_RIGHT 
before insert
on STEM_RIGHT_RIGHT  
for each row
execute procedure F_TIB_STEM_RIGHT_RIGHT();


-- trigger function for UPDATE
create or replace function F_TUB_STEM_RIGHT_RIGHT() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_STEM_RIGHT_RIGHT 
before update
on STEM_RIGHT_RIGHT 
for each row
execute procedure F_TUB_STEM_RIGHT_RIGHT();


-- trigger function for INSERT
create or replace function F_TIB_STEM_RIGHT_ROLE() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_STEM_RIGHT_ROLE is null) then
    new.id_STEM_RIGHT_ROLE := nextval('G_ID_STEM_RIGHT_ROLE');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_STEM_RIGHT_ROLE 
before insert
on STEM_RIGHT_ROLE  
for each row
execute procedure F_TIB_STEM_RIGHT_ROLE();


-- trigger function for UPDATE
create or replace function F_TUB_STEM_RIGHT_ROLE() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_STEM_RIGHT_ROLE 
before update
on STEM_RIGHT_ROLE 
for each row
execute procedure F_TUB_STEM_RIGHT_ROLE();


-- trigger function for INSERT
create or replace function F_TIB_STEM_RIGHT_ROLERIGHT() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_STEM_RIGHT_ROLERIGHT is null) then
    new.id_STEM_RIGHT_ROLERIGHT := nextval('G_ID_STEM_RIGHT_ROLERIGHT');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_STEM_RIGHT_ROLERIGHT 
before insert
on STEM_RIGHT_ROLERIGHT  
for each row
execute procedure F_TIB_STEM_RIGHT_ROLERIGHT();


-- trigger function for UPDATE
create or replace function F_TUB_STEM_RIGHT_ROLERIGHT() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_STEM_RIGHT_ROLERIGHT 
before update
on STEM_RIGHT_ROLERIGHT 
for each row
execute procedure F_TUB_STEM_RIGHT_ROLERIGHT();


-- trigger function for INSERT
create or replace function F_TIB_STEM_RIGHT_USER() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_STEM_RIGHT_USER is null) then
    new.id_STEM_RIGHT_USER := nextval('G_ID_STEM_RIGHT_USER');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_STEM_RIGHT_USER 
before insert
on STEM_RIGHT_USER  
for each row
execute procedure F_TIB_STEM_RIGHT_USER();


-- trigger function for UPDATE
create or replace function F_TUB_STEM_RIGHT_USER() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_STEM_RIGHT_USER 
before update
on STEM_RIGHT_USER 
for each row
execute procedure F_TUB_STEM_RIGHT_USER();


-- trigger function for INSERT
create or replace function F_TIB_STEM_RIGHT_USERROLE() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_STEM_RIGHT_USERROLE is null) then
    new.id_STEM_RIGHT_USERROLE := nextval('G_ID_STEM_RIGHT_USERROLE');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_STEM_RIGHT_USERROLE 
before insert
on STEM_RIGHT_USERROLE  
for each row
execute procedure F_TIB_STEM_RIGHT_USERROLE();


-- trigger function for UPDATE
create or replace function F_TUB_STEM_RIGHT_SYSUSERROLE() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_STEM_RIGHT_SYSUSERROLE 
before update
on STEM_RIGHT_USERROLE 
for each row
execute procedure F_TUB_STEM_RIGHT_SYSUSERROLE();


-- trigger function for INSERT
create or replace function F_TIB_SYSINVITE() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_SYSINVITE is null) then
    new.id_SYSINVITE := nextval('G_ID_SYSINVITE');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_SYSINVITE 
before insert
on SYSINVITE  
for each row
execute procedure F_TIB_SYSINVITE();


-- trigger function for UPDATE
create or replace function F_TUB_SYSINVITE() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_SYSINVITE 
before update
on SYSINVITE 
for each row
execute procedure F_TUB_SYSINVITE();


-- trigger function for INSERT
create or replace function F_TIB_SYSLOGIN() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_SYSLOGIN is null) then
    new.id_SYSLOGIN := nextval('G_ID_SYSLOGIN');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_SYSLOGIN 
before insert
on SYSLOGIN  
for each row
execute procedure F_TIB_SYSLOGIN();


-- trigger function for UPDATE
create or replace function F_TUB_SYSLOGIN() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_SYSLOGIN 
before update
on SYSLOGIN 
for each row
execute procedure F_TUB_SYSLOGIN();


-- trigger function for INSERT
create or replace function F_TIB_SYSUSER() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_SYSUSER is null) then
    new.id_SYSUSER := nextval('G_ID_SYSUSER');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_SYSUSER 
before insert
on SYSUSER  
for each row
execute procedure F_TIB_SYSUSER();


-- trigger function for UPDATE
create or replace function F_TUB_SYSUSER() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_SYSUSER 
before update
on SYSUSER 
for each row
execute procedure F_TUB_SYSUSER();


-- trigger function for INSERT
create or replace function F_TIB_USERREF() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_USERREF is null) then
    new.id_USERREF := nextval('G_ID_USERREF');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_USERREF 
before insert
on USERREF  
for each row
execute procedure F_TIB_USERREF();


-- trigger function for UPDATE
create or replace function F_TUB_USERREF() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_USERREF 
before update
on USERREF 
for each row
execute procedure F_TUB_USERREF();


-- trigger function for INSERT
create or replace function F_TIB_USER_REQUEST() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_USER_REQUEST is null) then
    new.id_USER_REQUEST := nextval('G_ID_USER_REQUEST');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_USER_REQUEST 
before insert
on USER_REQUEST  
for each row
execute procedure F_TIB_USER_REQUEST();


-- trigger function for UPDATE
create or replace function F_TUB_USER_REQUEST() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_USER_REQUEST 
before update
on USER_REQUEST 
for each row
execute procedure F_TUB_USER_REQUEST();


-- trigger function for INSERT
create or replace function F_TIB_USER_REQUEST_TYPE() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_USER_REQUEST_TYPE is null) then
    new.id_USER_REQUEST_TYPE := nextval('G_ID_USER_REQUEST_TYPE');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_USER_REQUEST_TYPE 
before insert
on USER_REQUEST_TYPE  
for each row
execute procedure F_TIB_USER_REQUEST_TYPE();


-- trigger function for UPDATE
create or replace function F_TUB_USER_REQUEST_TYPE() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_USER_REQUEST_TYPE 
before update
on USER_REQUEST_TYPE 
for each row
execute procedure F_TUB_USER_REQUEST_TYPE();

