-- guid varchar => guid uuid
create table tmp_syslogin as select id_syslogin, id_sysuser from syslogin;
drop table syslogin;



/*==============================================================*/
/* Table: SYSLOGIN                                              */
/*==============================================================*/
create table SYSLOGIN (
   ID_SYSLOGIN          UUID                 not null,
   ID_SYSUSER           INT4                 not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

alter table SYSLOGIN
   add constraint PK_SYSLOGIN primary key (ID_SYSLOGIN);

/*==============================================================*/
/* Index: FI_SYSLOGIN_SYSUSER                                   */
/*==============================================================*/
create  index FI_SYSLOGIN_SYSUSER on SYSLOGIN (
ID_SYSUSER
);

/*==============================================================*/
/* Index: I_SYSLOGIN_LASTDATE                                   */
/*==============================================================*/
create  index I_SYSLOGIN_LASTDATE on SYSLOGIN (
LASTDATE
);

alter table SYSLOGIN
   add constraint FK_SYSLOGIN_SYSUSER foreign key (ID_SYSUSER)
      references SYSUSER (ID_SYSUSER)
      on delete restrict on update restrict;


-- trigger function for INSERT
create or replace function F_TIB_SYSLOGIN() 
returns trigger as
$body$
declare
begin
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_SYSLOGIN 
before insert
on SYSLOGIN  
for each row
execute procedure F_TIB_SYSLOGIN();


-- trigger function for UPDATE
create or replace function F_TUB_SYSLOGIN() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_SYSLOGIN 
before update
on SYSLOGIN 
for each row
execute procedure F_TUB_SYSLOGIN();

insert into syslogin (id_syslogin, id_sysuser) select id_syslogin::uuid, id_sysuser from tmp_syslogin;
drop table tmp_syslogin;


alter table sysuser add guuid_backup varchar;
update sysuser set guuid_backup = guuid;
drop index SI_SYSUSER_GUUID;
alter table sysuser drop column guuid;
alter table sysuser add guuid uuid;
update sysuser set guuid = guuid_backup::uuid;
alter table sysuser alter guuid set not null;
create unique index SI_SYSUSER_GUUID on SYSUSER (
GUUID
);
alter table sysuser drop column guuid_backup;

alter table sysuser alter lastdate set data type timestamp(3);
alter table sysuser alter createdate set data type timestamp(3);
alter table endpoint alter lastdate set data type timestamp(3);
alter table endpoint alter createdate set data type timestamp(3);
alter table locale alter lastdate set data type timestamp(3);
alter table locale alter createdate set data type timestamp(3);
alter table mobile_app alter createdate set data type timestamp(3);
alter table mobile_app alter createdate set data type timestamp(3);
alter table notification alter createdate set data type timestamp(3);
alter table notification alter createdate set data type timestamp(3);
alter table notification_type alter createdate set data type timestamp(3);
alter table notification_type alter createdate set data type timestamp(3);
alter table pns_type alter createdate set data type timestamp(3);
alter table pns_type alter createdate set data type timestamp(3);
alter table reftype alter createdate set data type timestamp(3);
alter table reftype alter createdate set data type timestamp(3);
alter table stem_right_apikey alter createdate set data type timestamp(3);
alter table stem_right_apikey alter createdate set data type timestamp(3);
alter table sysinvite alter createdate set data type timestamp(3);
alter table sysinvite alter createdate set data type timestamp(3);
alter table syslogin alter createdate set data type timestamp(3);
alter table syslogin alter createdate set data type timestamp(3);
alter table user_request alter createdate set data type timestamp(3);
alter table user_request alter createdate set data type timestamp(3);
alter table user_request_type alter createdate set data type timestamp(3);
alter table user_request_type alter createdate set data type timestamp(3);
alter table userref alter createdate set data type timestamp(3);
alter table userref alter createdate set data type timestamp(3);

alter table notification alter expirationdate set data type timestamp(3);
alter table user_request alter expiration_time set data type timestamp(3);
alter table notification alter senddate set data type timestamp(3);


create index I_SYSUSER_LASTDATE on SYSUSER (LASTDATE);


insert into reftype (code, name) values ('slave', 'Подчиненный. Владелец волен делать с ним что хочет.');


alter table endpoint add apikey varchar;

update endpoint set apikey = '0648D5FC60D031985C98A331F6050F3265FCB61C10B53FAFCA26050D0EB610EB' where id_mobile_app in (select a.id_mobile_app from mobile_app a where a.code = 'heddo');
update endpoint set apikey = 'B176BB4F50F0EB0A12D0A7FA9FEDD2BFDE134A1285B5C702AEB6D4378BF5E414' where id_mobile_app in (select a.id_mobile_app from mobile_app a where a.code = 'medsi');

alter table endpoint drop column ID_MOBILE_APP;
drop table mobile_app;
drop sequence g_id_mobile_app;

drop function auth_upsert_endpoint(
   aid_endpoint in integer,        -- идентификатор точки контакта
   aid_sysuser in integer,         -- идентификатор пользователя
   anotify_type_code in varchar,   -- код типа уведомления
   anotification_id in varchar,    -- ID в системе (email, phone number, PUSH Token)
   adevice_id in varchar,          -- уникальный ID устройства
   aazure_id in varchar,           -- ID полученный от Azure
   apns_type_code in varchar,      -- код типа центра уведомлений
   ais_tablet in integer,          -- является ли планшетом
   alocale in varchar,             -- текущая локаль устройства
   aname in varchar,               -- настраиваемое имя устройства
   amodel in varchar,              -- модель устройства
   amobile_app_code in varchar,    -- код мобильного приложения
   ais_verified in integer,        -- точка контакта подтверждена
   ais_enabled in integer         -- использовать точку контакта
  );


create or replace function auth_upsert_endpoint(
   aid_endpoint in integer,        -- идентификатор точки контакта
   aid_sysuser in integer,         -- идентификатор пользователя
   anotify_type_code in varchar,   -- код типа уведомления
   anotification_id in varchar,    -- ID в системе (email, phone number, PUSH Token)
   adevice_id in varchar,          -- уникальный ID устройства
   aazure_id in varchar,           -- ID полученный от Azure
   apns_type_code in varchar,      -- код типа центра уведомлений
   ais_tablet in integer,          -- является ли планшетом
   alocale in varchar,             -- текущая локаль устройства
   aname in varchar,               -- настраиваемое имя устройства
   amodel in varchar,              -- модель устройства
   aapikey in varchar,    -- ключ апи
   ais_verified in integer,        -- точка контакта подтверждена
   ais_enabled in integer         -- использовать точку контакта
  )
  returns integer as
$body$
declare
  fid_notification_type integer;
  fid_endpoint integer := aid_endpoint;
  fid_pns_type integer := NULL;
  fid_locale integer := NULL;
  fis_default integer := 0;
  fid_mobile_app integer := NULL;
begin
------------------------------------------------------------------------------------------------
-- Добавляет или обновляет информацию о контакте пользователя
------------------------------------------------------------------------------------------------

  select id_notification_type into fid_notification_type
    from notification_type where code = anotify_type_code;
  if (not FOUND) then
    raise exception 'Тип уведомления с кодом % не найден', anotify_type_code;
  end if;

  -- проверка существования контакта по умолчанию для sms/email
  if (aid_sysuser is not null) then
    if (anotify_type_code = 'email' or anotify_type_code = 'sms') then
      if (not exists(select 1 from endpoint
        where id_sysuser = aid_sysuser
          and id_notification_type = fid_notification_type)) then
          fis_default = 1;
      end if;
    end if;
  end if;

  if (anotify_type_code = 'push') then
    select id_pns_type into fid_pns_type
      from pns_type where code = apns_type_code;
    if (not FOUND) then
      raise exception 'Платформа доставки сообщений с кодом % не найдена', apns_type_code;
    end if;
  end if;

  if (alocale is not null) then
    select id_locale into fid_locale
      from locale where code = alocale;
    if (not FOUND) then
      insert into locale(code) values(alocale);
    end if;
  end if;

  if (aid_endpoint is null) then
    insert into endpoint(id_sysuser, id_notification_type, notification_id,
      id_pns_type, id_locale, device_id, azure_id, name, model,
      is_tablet, is_verified, is_enabled, is_default, apikey)
    values(aid_sysuser, fid_notification_type, anotification_id,
      fid_pns_type, fid_locale, adevice_id, aazure_id, aname, amodel, 
      ais_tablet, ais_verified, ais_enabled, fis_default, aapikey)
      returning id_endpoint into fid_endpoint;
  else
    update endpoint set
      id_sysuser = aid_sysuser,
      id_notification_type = fid_notification_type,
      notification_id = anotification_id,
      id_pns_type = fid_pns_type,
      id_locale = fid_locale,
      device_id = adevice_id,
      azure_id = aazure_id,
      name = aname,
      model = amodel,
      is_tablet = ais_tablet,
      is_verified = ais_verified,
      is_enabled = ais_enabled,
      is_default = fis_default,
      apikey = aapikey
    where id_endpoint = aid_endpoint;
  end if;

  return fid_endpoint;
end
$body$
language 'plpgsql' VOLATILE;

drop index SI_ENDPOINT_DEVICE_ID;
create  index FI_ENDPOINT_DEVICE_ID on ENDPOINT (
DEVICE_ID
);



alter table notification add SMS_SIGN varchar;

