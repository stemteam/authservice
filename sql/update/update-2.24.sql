alter table sysuser add PASSWORD_SALT VARCHAR;
update sysuser set password_salt = guuid;
update sysuser u set password_salt = (select e.notification_id from endpoint e join notification_type nt on e.id_notification_type = nt.id_notification_type where e.id_sysuser = u.id_sysuser and nt.code = 'sms')
where exists (select 1 from endpoint e join notification_type nt on e.id_notification_type = nt.id_notification_type where e.id_sysuser = u.id_sysuser and nt.code = 'sms');
alter table sysuser alter PASSWORD_SALT set not null;

alter table syslogin add EXPIRED_TIME   TIMESTAMP(3)         not null default CURRENT_TIMESTAMP + interval '1 day';
alter table syslogin add    STATE                VARCHAR;
alter table syslogin add EXT_ACCESS_TOKEN VARCHAR;

insert into notification_type (code, name) values ('vk', 'Вконтакте');
insert into notification_type (code, name) values ('ok', 'Одноклассники');
insert into notification_type (code, name) values ('facebook', 'Facebook');
insert into notification_type (code, name) values ('esia', 'ЕСИА');


alter table syslogin add ID_ENDPOINT int;
alter table SYSLOGIN
   add constraint FK_SYSLOGIN_ENDPOINT foreign key (ID_ENDPOINT)
      references ENDPOINT (ID_ENDPOINT)
      on delete restrict on update restrict;

create  index FI_SYSLOGIN_ENDPOINT on SYSLOGIN (
ID_ENDPOINT
);

create  index I_SYSLOGIN_STATE on SYSLOGIN (
ID_ENDPOINT,
STATE
);


