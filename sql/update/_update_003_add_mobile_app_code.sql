begin;

alter table endpoint add column id_mobile_app int4 null;

/*==============================================================*/

create sequence G_ID_MOBILE_APP;

/*==============================================================*/
/* Table: MOBILE_APP                                            */
/*==============================================================*/
create table MOBILE_APP (
   ID_MOBILE_APP        INT4                 not null,
   CODE                 VARCHAR              not null,
   NAME                 VARCHAR              not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

insert into MOBILE_APP (id_mobile_app, code, name) values (nextval('g_id_mobile_app'), 'heddo', 'Приложение Heddo');

-- обновляем существующие данные
update endpoint set id_mobile_app = (select id_mobile_app from mobile_app where code = 'heddo')
where id_notification_type = (select nt.id_notification_type from notification_type nt where nt.code = 'push');

alter table MOBILE_APP
   add constraint PK_MOBILE_APP primary key (ID_MOBILE_APP);

alter table ENDPOINT
   add constraint FK_ENDPOINT_MOBILE_APP foreign key (ID_MOBILE_APP)
      references MOBILE_APP (ID_MOBILE_APP)
      on delete restrict on update restrict;

/*==============================================================*/
/* Index: SI_MOBILE_APP                                         */
/*==============================================================*/
create unique index SI_MOBILE_APP on MOBILE_APP (
CODE
);


-- trigger function for INSERT
create or replace function F_TIB_MOBILE_APP()
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_MOBILE_APP is null) then
    new.id_MOBILE_APP := nextval('G_ID_MOBILE_APP');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker;
-- trigger for INSERT
create trigger TIB_MOBILE_APP
before insert
on MOBILE_APP
for each row
execute procedure F_TIB_MOBILE_APP();


-- trigger function for UPDATE
create or replace function F_TUB_MOBILE_APP()
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker;
-- trigger for UPDATE
create trigger TUB_MOBILE_APP
before update
on MOBILE_APP
for each row
execute procedure F_TUB_MOBILE_APP();


create or replace function auth_upsert_endpoint(
   aid_endpoint in integer,        -- идентификатор точки контакта
   aid_sysuser in integer,         -- идентификатор пользователя
   anotify_type_code in varchar,   -- код типа уведомления
   anotification_id in varchar,    -- ID в системе (email, phone number, PUSH Token)
   adevice_id in varchar,          -- уникальный ID устройства
   aazure_id in varchar,           -- ID полученный от Azure
   apns_type_code in varchar,      -- код типа центра уведомлений
   ais_tablet in integer,          -- является ли планшетом
   alocale in varchar,             -- текущая локаль устройства
   aname in varchar,               -- настраиваемое имя устройства
   amodel in varchar,              -- модель устройства
   amobile_app_code in varchar,    -- код мобильного приложения
   ais_verified in integer,        -- точка контакта подтверждена
   ais_enabled in integer          -- использовать точку контакта
  )
  returns integer as
$body$
declare
  fid_notification_type integer;
  fid_endpoint integer := aid_endpoint;
  fid_pns_type integer := NULL;
  fid_locale integer := NULL;
  fis_default integer := 0;
  fid_mobile_app integer := NULL;
begin
------------------------------------------------------------------------------------------------
-- Добавляет или обновляет информацию о контакте пользователя
------------------------------------------------------------------------------------------------

  select id_notification_type into fid_notification_type
    from notification_type where code = anotify_type_code;
  if (not FOUND) then
    raise exception 'Тип уведомления с кодом % не найден', anotify_type_code;
  end if;

  -- проверка существования контакта по умолчанию для sms/email
  if (aid_sysuser is not null) then
    if (anotify_type_code = 'email' or anotify_type_code = 'sms') then
      if (not exists(select 1 from endpoint
        where id_sysuser = aid_sysuser
          and id_notification_type = fid_notification_type)) then
          fis_default = 1;
      end if;
    end if;
  end if;

  if (anotify_type_code = 'push') then
    select id_pns_type into fid_pns_type
      from pns_type where code = apns_type_code;
    if (not FOUND) then
      raise exception 'Платформа доставки сообщений с кодом % не найдена', apns_type_code;
    end if;

    select id_mobile_app into fid_mobile_app
      from mobile_app where code = amobile_app_code;
    if (not FOUND) then
      raise exception 'Мобильное приложение с кодом % не зарегистрировано', amobile_app_code;
    end if;
  end if;

  if (alocale is not null) then
    select id_locale into fid_locale
      from locale where code = alocale;
    if (not FOUND) then
      insert into locale(code) values(alocale);
    end if;
  end if;

  if (aid_endpoint is null) then
    insert into endpoint(id_sysuser, id_notification_type, notification_id,
      id_pns_type, id_locale, device_id, azure_id, name, model, id_mobile_app,
      is_tablet, is_verified, is_enabled, is_default)
    values(aid_sysuser, fid_notification_type, anotification_id,
      fid_pns_type, fid_locale, adevice_id, aazure_id, aname, amodel, fid_mobile_app,
      ais_tablet, ais_verified, ais_enabled, fis_default)
      returning id_endpoint into fid_endpoint;
  else
    update endpoint set
      id_sysuser = aid_sysuser,
      id_notification_type = fid_notification_type,
      notification_id = anotification_id,
      id_pns_type = fid_pns_type,
      id_locale = fid_locale,
      device_id = adevice_id,
      azure_id = aazure_id,
      name = aname,
      model = amodel,
      id_mobile_app = fid_mobile_app,
      is_tablet = ais_tablet,
      is_verified = ais_verified,
      is_enabled = ais_enabled,
      is_default = fis_default
    where id_endpoint = aid_endpoint;
  end if;

  return fid_endpoint;
end
$body$
language 'plpgsql' VOLATILE;

commit;
