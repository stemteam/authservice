begin;

alter table sysuser add column is_system int2 not null default 0
      constraint CKC_IS_SYSTEM_SYSUSER check (IS_SYSTEM in (0,1));

insert into notification_type (id_notification_type, code, name) values (nextval('g_id_notification_type'), 'push', 'Push уведомления');

create sequence G_ID_PNS_TYPE;

/*==============================================================*/
/* Table: PNS_TYPE                                              */
/*==============================================================*/
create table PNS_TYPE (
   ID_PNS_TYPE          INT4                 not null,
   CODE                 VARCHAR              not null,
   NAME                 VARCHAR              not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

insert into PNS_TYPE (id_pns_type, code, name) values (nextval('g_id_pns_type'), 'gcm', 'Android PNS');
insert into PNS_TYPE (id_pns_type, code, name) values (nextval('g_id_pns_type'), 'apns', 'Apple PNS');
insert into PNS_TYPE (id_pns_type, code, name) values (nextval('g_id_pns_type'), 'mpns', 'Windows phone 8');
insert into PNS_TYPE (id_pns_type, code, name) values (nextval('g_id_pns_type'), 'wns', 'Windows store');

alter table PNS_TYPE
   add constraint PK_PNS_TYPE primary key (ID_PNS_TYPE);

/*==============================================================*/
/* Index: SI_PNS_TYPE                                           */
/*==============================================================*/
create unique index SI_PNS_TYPE on PNS_TYPE (
CODE
);


-- trigger function for INSERT
create or replace function F_TIB_PNS_TYPE()
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_PNS_TYPE is null) then
    new.id_PNS_TYPE := nextval('G_ID_PNS_TYPE');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker;
-- trigger for INSERT
create trigger TIB_PNS_TYPE
before insert
on PNS_TYPE
for each row
execute procedure F_TIB_PNS_TYPE();


-- trigger function for UPDATE
create or replace function F_TUB_PNS_TYPE()
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker;
-- trigger for UPDATE
create trigger TUB_PNS_TYPE
before update
on PNS_TYPE
for each row
execute procedure F_TUB_PNS_TYPE();

/*==============================================================*/

create sequence G_ID_LOCALE;

/*==============================================================*/
/* Table: LOCALE                                                */
/*==============================================================*/
create table LOCALE (
   ID_LOCALE            INT4                 not null,
   CODE                 VARCHAR              not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

alter table LOCALE
   add constraint PK_LOCALE primary key (ID_LOCALE);

/*==============================================================*/
/* Index: SI_LOCALE                                             */
/*==============================================================*/
create  index SI_LOCALE on LOCALE (
CODE
);


-- trigger function for INSERT
create or replace function F_TIB_PNS_TYPE()
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_LOCALE is null) then
    new.id_LOCALE := nextval('G_ID_LOCALE');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker;
-- trigger for INSERT
create trigger TIB_PNS_TYPE
before insert
on LOCALE
for each row
execute procedure F_TIB_PNS_TYPE();


-- trigger function for UPDATE
create or replace function F_TUB_PNS_TYPE()
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker;
-- trigger for UPDATE
create trigger TUB_PNS_TYPE
before update
on LOCALE
for each row
execute procedure F_TUB_PNS_TYPE();

/*==============================================================*/
create sequence G_ID_ENDPOINT;

/*==============================================================*/
/* Table: ENDPOINT                                              */
/*==============================================================*/
create table ENDPOINT (
   ID_ENDPOINT          INT4                 not null,
   ID_SYSUSER           INT4                 null,
   ID_NOTIFICATION_TYPE INT4                 not null,
   NOTIFICATION_ID      VARCHAR              not null,
   ID_PNS_TYPE          INT4                 null,
   ID_LOCALE            INT4                 null,
   DEVICE_ID            VARCHAR              null,
   AZURE_ID             VARCHAR              null,
   NAME                 VARCHAR              null,
   MODEL                VARCHAR              null,
   IS_TABLET            INT2                 null default 0
      constraint CKC_IS_TABLET_ENDPOINT check (IS_TABLET is null or (IS_TABLET in (0,1))),
   IS_VERIFIED          INT2                 not null default 0
      constraint CKC_IS_VERIFIED_ENDPOINT check (IS_VERIFIED in (0,1)),
   IS_ENABLED           INT2                 not null default 0
      constraint CKC_IS_ENABLED_ENDPOINT check (IS_ENABLED in (0,1)),
   IS_DEFAULT           INT2                 not null default 0
      constraint CKC_IS_DEFAULT_ENDPOINT check (IS_DEFAULT in (0,1)),
   CREATEDATE           TIMESTAMP            not null,
   LASTDATE             TIMESTAMP            not null
);

alter table ENDPOINT
   add constraint PK_ENDPOINT primary key (ID_ENDPOINT);

/*==============================================================*/
/* Index: SI_ENDPOINT_ID_NOTIFICATION_TYP                       */
/*==============================================================*/
create  index SI_ENDPOINT_ID_NOTIFICATION_TYP on ENDPOINT (
ID_NOTIFICATION_TYPE
);

/*==============================================================*/
/* Index: SI_ENDPOINT_ID_SYSUSER                                */
/*==============================================================*/
create  index SI_ENDPOINT_ID_SYSUSER on ENDPOINT (
ID_SYSUSER
);

/*==============================================================*/
/* Index: SI_ENDPOINT_DEVICE_ID                                 */
/*==============================================================*/
create  index SI_ENDPOINT_DEVICE_ID on ENDPOINT (
DEVICE_ID
);

/*==============================================================*/
/* Index: SI_ENDPOINT_NOTIFICATION_ID                           */
/*==============================================================*/
create  index SI_ENDPOINT_NOTIFICATION_ID on ENDPOINT (
NOTIFICATION_ID
);

/*==============================================================*/
/* Index: SI_ENDPOINT_PNS_TYPE_NOTIFICATI                       */
/*==============================================================*/
create unique index SI_ENDPOINT_PNS_TYPE_NOTIFICATI on ENDPOINT (
ID_PNS_TYPE,
NOTIFICATION_ID
);

alter table ENDPOINT
   add constraint FK_ENDPOINT_SYSUSER foreign key (ID_SYSUSER)
      references SYSUSER (ID_SYSUSER)
      on delete cascade on update restrict;

alter table ENDPOINT
   add constraint FK_ENDPOINT_NOTIFICATION_TYPE foreign key (ID_NOTIFICATION_TYPE)
      references NOTIFICATION_TYPE (ID_NOTIFICATION_TYPE)
      on delete restrict on update restrict;

alter table ENDPOINT
   add constraint FK_ENDPOINT_PNS_TYPE foreign key (ID_PNS_TYPE)
      references PNS_TYPE (ID_PNS_TYPE)
      on delete restrict on update restrict;

alter table ENDPOINT
   add constraint FK_ENDPOINT_LOCALE foreign key (ID_LOCALE)
      references LOCALE (ID_LOCALE)
      on delete restrict on update restrict;


-- trigger function for INSERT
create or replace function F_TIB_ENDPOINT()
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_ENDPOINT is null) then
    new.id_ENDPOINT := nextval('G_ID_ENDPOINT');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker;
-- trigger for INSERT
create trigger TIB_ENDPOINT
before insert
on ENDPOINT
for each row
execute procedure F_TIB_ENDPOINT();


-- trigger function for UPDATE
create or replace function F_TUB_ENDPOINT()
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker;
-- trigger for UPDATE
create trigger TUB_ENDPOINT
before update
on ENDPOINT
for each row
execute procedure F_TUB_ENDPOINT();

/*==============================================================*/

create or replace function auth_upsert_endpoint(
   aid_endpoint in integer,        -- идентификатор точки контакта
   aid_sysuser in integer,         -- идентификатор пользователя
   anotify_type_code in varchar,   -- код типа уведомления
   anotification_id in varchar,    -- ID в системе (email, phone number, PUSH Token)
   adevice_id in varchar,          -- уникальный ID устройства
   aazure_id in varchar,           -- ID полученный от Azure
   apns_type_code in varchar,      -- код типа центра уведомлений
   ais_tablet in integer,          -- является ли планшетом
   alocale in varchar,             -- текущая локаль устройства
   aname in varchar,               -- настраеваемое имя устройства
   amodel in varchar,              -- модель устройства
   ais_verified in integer,        -- точка контакта подтверждена
   ais_enabled in integer         -- использовать точку контакта
  )
  returns integer as
$body$
declare
  fid_notification_type integer;
  fid_endpoint integer := aid_endpoint;
  fid_pns_type integer := NULL;
  fid_locale integer := NULL;
  fis_default integer := 0;
begin
------------------------------------------------------------------------------------------------
-- Добавляет или обновляет информацию о контакте пользователя
------------------------------------------------------------------------------------------------

  select id_notification_type into fid_notification_type
    from notification_type where code = anotify_type_code;
  if (not FOUND) then
    raise exception 'Тип уведомления с кодом % не найден', anotify_type_code;
  end if;

  -- проверка существования контакта по умолчанию для sms/email
  if (aid_sysuser is not null) then
    if (anotify_type_code = 'email' or anotify_type_code = 'sms') then
      if (not exists(select 1 from endpoint
        where id_sysuser = aid_sysuser
          and id_notification_type = fid_notification_type)) then
          fis_default = 1;
      end if;
    end if;
  end if;

  if (anotify_type_code = 'push') then
    select id_pns_type into fid_pns_type
      from pns_type where code = apns_type_code;
    if (not FOUND) then
      raise exception 'Платформа доставки сообщений с кодом % не найдена', apns_type_code;
    end if;
  end if;

  if (alocale is not null) then
    select id_locale into fid_locale
      from locale where code = alocale;
    if (not FOUND) then
      insert into locale(code) values(alocale);
    end if;
  end if;

  if (aid_endpoint is null) then
    insert into endpoint(id_sysuser, id_notification_type, notification_id,
      id_pns_type, id_locale, device_id, azure_id, name, model,
      is_tablet, is_verified, is_enabled, is_default)
    values(aid_sysuser, fid_notification_type, anotification_id,
      fid_pns_type, fid_locale, adevice_id, aazure_id, aname, amodel,
      ais_tablet, ais_verified, ais_enabled, fis_default)
      returning id_endpoint into fid_endpoint;
  else
    update endpoint set
      id_sysuser = aid_sysuser,
      id_notification_type = fid_notification_type,
      notification_id = anotification_id,
      id_pns_type = fid_pns_type,
      id_locale = fid_locale,
      device_id = adevice_id,
      azure_id = aazure_id,
      name = aname,
      model = amodel,
      is_tablet = ais_tablet,
      is_verified = ais_verified,
      is_enabled = ais_enabled,
      is_default = fis_default
    where id_endpoint = aid_endpoint;
  end if;

  return fid_endpoint;
end
$body$
language 'plpgsql' VOLATILE;

/*==============================================================*/

-- перенос SMS
insert into endpoint(id_sysuser, id_notification_type, notification_id, is_verified, is_enabled, is_default)
(select id_sysuser, 1, mobile_phone, 0, (id_notification_type = 1)::integer, 1 from sysuser
where mobile_phone is not null);

-- перенос email
insert into endpoint(id_sysuser, id_notification_type, notification_id, is_verified, is_enabled, is_default)
(select id_sysuser, 2, email, 0, (id_notification_type = 2)::integer, 1 from sysuser
where email is not null);

alter table sysuser drop column email;
alter table sysuser drop column mobile_phone;
alter table sysuser drop column id_notification_type;

commit;
