-- url shortener
create sequence G_ID_SHORT_URL;

/*==============================================================*/
/* Table: SHORT_URL                                             */
/*==============================================================*/
create table SHORT_URL (
   ID_SHORT_URL         INT8                 not null,
   URL                  VARCHAR              not null,
   CREATEDATE           TIMESTAMP(3)         not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP(3)         not null default CURRENT_TIMESTAMP
);

alter table SHORT_URL
   add constraint PK_SHORT_URL primary key (ID_SHORT_URL);


-- trigger function for INSERT
create or replace function F_TIB_SHORT_URL() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_SHORT_URL is null) then
    new.id_SHORT_URL := nextval('G_ID_SHORT_URL');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_SHORT_URL 
before insert
on SHORT_URL  
for each row
execute procedure F_TIB_SHORT_URL();


-- trigger function for UPDATE
create or replace function F_TUB_SHORT_URL() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_SHORT_URL 
before update
on SHORT_URL 
for each row
execute procedure F_TUB_SHORT_URL();


create unique index SI_SHORT_URL on SHORT_URL (
URL
);


alter table notification add TITLE                VARCHAR;
alter table notification add MESSAGE              VARCHAR;
alter table notification add DATA                 VARCHAR;

update notification set data = sms_sign, message = sms_message
where id_endpoint in (select e.id_endpoint from endpoint e join notification_type t on e.id_notification_type = t.id_notification_type where t.code = 'sms');

update notification set title = email_title, message = email_message
where id_endpoint in (select e.id_endpoint from endpoint e join notification_type t on e.id_notification_type = t.id_notification_type where t.code = 'email');

update notification set title = push_title, message = push_message, data = push_data
where id_endpoint in (select e.id_endpoint from endpoint e join notification_type t on e.id_notification_type = t.id_notification_type where t.code = 'push');

alter table notification drop column SMS_MESSAGE;
alter table notification drop column PUSH_MESSAGE;
alter table notification drop column EMAIL_MESSAGE;
alter table notification drop column PUSH_TITLE;
alter table notification drop column EMAIL_TITLE;
alter table notification drop column PUSH_TYPE;
alter table notification drop column PUSH_DATA;
alter table notification drop column SMS_SIGN;

create  index FI_NOTIFICATION_ENDPOINT on NOTIFICATION (
ID_ENDPOINT
);

-- создаем более эффективный индекс
drop index i_notification_is_notified;
CREATE INDEX i_notification_active ON notification (is_expired) where is_notified = 0;


