create sequence G_ID_USER_REQUEST;
create sequence G_ID_USER_REQUEST_TYPE;

/*==============================================================*/
/* Table: USER_REQUEST_TYPE                                     */
/*==============================================================*/
create table USER_REQUEST_TYPE (
   ID_USER_REQUEST_TYPE INT2                 not null,
   CODE                 VARCHAR              not null,
   NAME                 VARCHAR              not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

insert into USER_REQUEST_TYPE (ID_USER_REQUEST_TYPE, code, name) values (nextval('G_ID_USER_REQUEST_TYPE'), 'register', 'Регистрация');
insert into USER_REQUEST_TYPE (ID_USER_REQUEST_TYPE, code, name) values (nextval('G_ID_USER_REQUEST_TYPE'), 'passrecover', 'Восстановление пароля');
insert into USER_REQUEST_TYPE (ID_USER_REQUEST_TYPE, code, name) values (nextval('G_ID_USER_REQUEST_TYPE'), 'delete', 'Удаление профиля');

alter table USER_REQUEST_TYPE
   add constraint PK_USER_REQUEST_TYPE primary key (ID_USER_REQUEST_TYPE);

/*==============================================================*/
/* Index: SI_USER_REQUEST_TYPE                                  */
/*==============================================================*/
create unique index SI_USER_REQUEST_TYPE on USER_REQUEST_TYPE (
CODE
);

/*==============================================================*/
/* Index: SI_USER_REQUEST_TYPE2                                 */
/*==============================================================*/
create unique index SI_USER_REQUEST_TYPE2 on USER_REQUEST_TYPE (
NAME
);


-- trigger function for INSERT
create or replace function F_TIB_USER_REQUEST_TYPE() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_USER_REQUEST_TYPE is null) then
    new.id_USER_REQUEST_TYPE := nextval('G_ID_USER_REQUEST_TYPE');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_USER_REQUEST_TYPE 
before insert
on USER_REQUEST_TYPE  
for each row
execute procedure F_TIB_USER_REQUEST_TYPE();


-- trigger function for UPDATE
create or replace function F_TUB_USER_REQUEST_TYPE() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_USER_REQUEST_TYPE 
before update
on USER_REQUEST_TYPE 
for each row
execute procedure F_TUB_USER_REQUEST_TYPE();


/*==============================================================*/
/* Table: USER_REQUEST                                          */
/*==============================================================*/
create table USER_REQUEST (
   ID_USER_REQUEST      INT8                 not null,
   ID_SYSUSER           INT4                 not null,
   ID_USER_REQUEST_TYPE INT2                 not null,
   CODE                 VARCHAR              not null,
   EXPIRATION_TIME      TIMESTAMP            not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

alter table USER_REQUEST
   add constraint PK_USER_REQUEST primary key (ID_USER_REQUEST);

/*==============================================================*/
/* Index: FI_USER_REQUEST_USER_REQUEST_TY                       */
/*==============================================================*/
create  index FI_USER_REQUEST_USER_REQUEST_TY on USER_REQUEST (
ID_USER_REQUEST_TYPE
);

/*==============================================================*/
/* Index: FI_USER_REQUEST_SYSUSER                               */
/*==============================================================*/
create  index FI_USER_REQUEST_SYSUSER on USER_REQUEST (
ID_SYSUSER
);

/*==============================================================*/
/* Index: SI_USER_REQUEST                                       */
/*==============================================================*/
create unique index SI_USER_REQUEST on USER_REQUEST (
ID_SYSUSER,
ID_USER_REQUEST_TYPE
);

alter table USER_REQUEST
   add constraint FK_USER_REQUEST_SYSUSER foreign key (ID_SYSUSER)
      references SYSUSER (ID_SYSUSER)
      on delete restrict on update restrict;

alter table USER_REQUEST
   add constraint FK_USER_REQUEST_USER_REQUEST_TYPE foreign key (ID_USER_REQUEST_TYPE)
      references USER_REQUEST_TYPE (ID_USER_REQUEST_TYPE)
      on delete restrict on update restrict;


-- trigger function for INSERT
create or replace function F_TIB_USER_REQUEST() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_USER_REQUEST is null) then
    new.id_USER_REQUEST := nextval('G_ID_USER_REQUEST');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_USER_REQUEST 
before insert
on USER_REQUEST  
for each row
execute procedure F_TIB_USER_REQUEST();


-- trigger function for UPDATE
create or replace function F_TUB_USER_REQUEST() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_USER_REQUEST 
before update
on USER_REQUEST 
for each row
execute procedure F_TUB_USER_REQUEST();


alter table USER_REQUEST
   drop constraint FK_USER_REQUEST_SYSUSER;
 alter table USER_REQUEST
   add constraint FK_USER_REQUEST_SYSUSER foreign key (ID_SYSUSER)
      references SYSUSER (ID_SYSUSER)
      on delete cascade on update restrict;


alter table sysuser drop column ACTIVATION_CODE;
