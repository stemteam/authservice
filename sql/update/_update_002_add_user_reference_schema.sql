begin;

create sequence G_ID_REFTYPE;

/*==============================================================*/
/* Table: REFTYPE                                               */
/*==============================================================*/
create table REFTYPE (
   ID_REFTYPE           INT4                 not null,
   CODE                 VARCHAR              not null,
   NAME                 VARCHAR              not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

insert into reftype (id_reftype, code, name) values (nextval('g_id_reftype'), 'child', 'Ребенок');

alter table REFTYPE
   add constraint PK_REFTYPE primary key (ID_REFTYPE);

/*==============================================================*/
/* Index: SI_REFTYPE                                            */
/*==============================================================*/
create unique index SI_REFTYPE on REFTYPE (
CODE
);


-- trigger function for INSERT
create or replace function F_TIB_REFTYPE()
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_REFTYPE is null) then
    new.id_REFTYPE := nextval('G_ID_REFTYPE');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker;
-- trigger for INSERT
create trigger TIB_REFTYPE
before insert
on REFTYPE
for each row
execute procedure F_TIB_REFTYPE();


-- trigger function for UPDATE
create or replace function F_TUB_REFTYPE()
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker;
-- trigger for UPDATE
create trigger TUB_REFTYPE
before update
on REFTYPE
for each row
execute procedure F_TUB_REFTYPE();


/*==============================================================*/
/* Table: USERREF                                               */
/*==============================================================*/

create sequence G_ID_USERREF;

create table USERREF (
   ID_USERREF           INT8                 not null,
   ID_SYSUSER           INT4                 not null,
   ID_SYSUSER_REF       INT4                 not null,
   ID_REFTYPE           INT4                 not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

alter table USERREF
   add constraint PK_USERREF primary key (ID_USERREF);

/*==============================================================*/
/* Index: FI_USERREF_SYSUSER                                    */
/*==============================================================*/
create  index FI_USERREF_SYSUSER on USERREF (
ID_SYSUSER
);

/*==============================================================*/
/* Index: FI_USERREF_SYSUSERREF                                 */
/*==============================================================*/
create  index FI_USERREF_SYSUSERREF on USERREF (
ID_SYSUSER_REF
);

/*==============================================================*/
/* Index: FI_USERREF_REFTYPE                                    */
/*==============================================================*/
create  index FI_USERREF_REFTYPE on USERREF (
ID_REFTYPE
);

/*==============================================================*/
/* Index: SI_USERREF                                            */
/*==============================================================*/
create unique index SI_USERREF on USERREF (
ID_REFTYPE,
ID_SYSUSER,
ID_SYSUSER_REF
);

alter table USERREF
   add constraint FK_USERREF_REFTYPE foreign key (ID_REFTYPE)
      references REFTYPE (ID_REFTYPE)
      on delete restrict on update restrict;

alter table USERREF
   add constraint FK_USERREF_SYSUSER foreign key (ID_SYSUSER)
      references SYSUSER (ID_SYSUSER)
      on delete restrict on update restrict;

alter table USERREF
   add constraint FK_USERREF_SYSUSER_REF foreign key (ID_SYSUSER_REF)
      references SYSUSER (ID_SYSUSER)
      on delete restrict on update restrict;


-- trigger function for INSERT
create or replace function F_TIB_USERREF()
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_USERREF is null) then
    new.id_USERREF := nextval('G_ID_USERREF');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker;
-- trigger for INSERT
create trigger TIB_USERREF
before insert
on USERREF
for each row
execute procedure F_TIB_USERREF();


-- trigger function for UPDATE
create or replace function F_TUB_USERREF()
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker;
-- trigger for UPDATE
create trigger TUB_USERREF
before update
on USERREF
for each row
execute procedure F_TUB_USERREF();

commit;
