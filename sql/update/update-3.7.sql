drop index if exists SI_ENDPOINT_USER_TYPE_ID;
create unique index SI_ENDPOINT_USER_TYPE_ID on ENDPOINT (
  ID_NOTIFICATION_TYPE,
  NOTIFICATION_ID
);


alter table sysuser add birthday timestamp;
