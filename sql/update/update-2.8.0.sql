create sequence G_ID_NOTIFICATION;

/*==============================================================*/
/* Table: NOTIFICATION                                          */
/*==============================================================*/
create table NOTIFICATION (
   ID_NOTIFICATION      INT8                 not null,
   ID_ENDPOINT          INT4                 not null,
   SMS_MESSAGE          VARCHAR              null,
   PUSH_MESSAGE         VARCHAR              null,
   EMAIL_MESSAGE        VARCHAR              null,
   PUSH_TITLE           VARCHAR              null,
   EMAIL_TITLE          VARCHAR              null,
   PUSH_TYPE            VARCHAR              null,
   PUSH_DATA            VARCHAR              null,
   EXPIRATIONDATE       TIMESTAMP            not null,
   IS_NOTIFIED          INT2                 not null default 0
      constraint CKC_IS_NOTIFIED_NOTIFICA check (IS_NOTIFIED in (0,1)),
   IS_EXPIRED           INT2                 not null default 0
      constraint CKC_IS_EXPIRED_NOTIFICA check (IS_EXPIRED in (0,1)),
   SENDDATE             TIMESTAMP            null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

alter table NOTIFICATION
   add constraint PK_NOTIFICATION primary key (ID_NOTIFICATION);

alter table NOTIFICATION
   add constraint FK_NOTIFICATION_ENDPOINT foreign key (ID_ENDPOINT)
      references ENDPOINT (ID_ENDPOINT)
      on delete restrict on update restrict;

create index i_notification_is_notified on notification(is_notified);

-- trigger function for INSERT
create or replace function F_TIB_NOTIFICATION()
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_NOTIFICATION is null) then
    new.id_NOTIFICATION := nextval('G_ID_NOTIFICATION');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker;
-- trigger for INSERT
create trigger TIB_NOTIFICATION
before insert
on NOTIFICATION
for each row
execute procedure F_TIB_NOTIFICATION();

-- trigger function for UPDATE
create or replace function F_TUB_NOTIFICATION()
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;

  if (old.is_notified = 0 and new.is_notified = 1) then
    new.senddate := current_timestamp;
  end if;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker;

