-- обновление 2.9.0

alter table NOTIFICATION
   drop constraint FK_NOTIFICATION_ENDPOINT;

alter table NOTIFICATION
   add constraint FK_NOTIFICATION_ENDPOINT foreign key (ID_ENDPOINT)
      references ENDPOINT (ID_ENDPOINT)
      on delete cascade on update restrict;
