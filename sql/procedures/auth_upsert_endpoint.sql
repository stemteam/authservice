create or replace function auth_upsert_endpoint(
   aid_endpoint in integer,        -- идентификатор точки контакта
   aid_sysuser in integer,         -- идентификатор пользователя
   anotify_type_code in varchar,   -- код типа уведомления
   anotification_id in varchar,    -- ID в системе (email, phone number, PUSH Token)
   adevice_id in varchar,          -- уникальный ID устройства
   aazure_id in varchar,           -- ID полученный от Azure
   apns_type_code in varchar,      -- код типа центра уведомлений
   ais_tablet in integer,          -- является ли планшетом
   alocale in varchar,             -- текущая локаль устройства
   aname in varchar,               -- настраиваемое имя устройства
   amodel in varchar,              -- модель устройства
   aapikey in varchar,    -- ключ апи
   ais_verified in integer,        -- точка контакта подтверждена
   ais_enabled in integer         -- использовать точку контакта
  )
  returns integer as
$body$
declare
  fid_notification_type integer;
  fid_endpoint integer := aid_endpoint;
  fid_pns_type integer := NULL;
  fid_locale integer := NULL;
  fis_default integer := 0;
  fid_mobile_app integer := NULL;
begin
------------------------------------------------------------------------------------------------
-- Добавляет или обновляет информацию о контакте пользователя
------------------------------------------------------------------------------------------------

  select id_notification_type into fid_notification_type
    from notification_type where code = anotify_type_code;
  if (not FOUND) then
    raise exception 'Тип уведомления с кодом % не найден', anotify_type_code;
  end if;

  -- проверка существования контакта по умолчанию для sms/email
  if (aid_sysuser is not null) then
    if (anotify_type_code = 'email' or anotify_type_code = 'sms') then
      if (not exists(select 1 from endpoint
        where id_sysuser = aid_sysuser
          and id_notification_type = fid_notification_type)) then
          fis_default = 1;
      end if;
    end if;
  end if;

  if (anotify_type_code = 'push') then
    select id_pns_type into fid_pns_type
      from pns_type where code = apns_type_code;
    if (not FOUND) then
      raise exception 'Платформа доставки сообщений с кодом % не найдена', apns_type_code;
    end if;
  end if;

  if (alocale is not null) then
    select id_locale into fid_locale
      from locale where code = alocale;
    if (not FOUND) then
      insert into locale(code) values(alocale);
    end if;
  end if;

  if (aid_endpoint is null) then
    insert into endpoint(id_sysuser, id_notification_type, notification_id,
      id_pns_type, id_locale, device_id, azure_id, name, model,
      is_tablet, is_verified, is_enabled, is_default, apikey)
    values(aid_sysuser, fid_notification_type, anotification_id,
      fid_pns_type, fid_locale, adevice_id, aazure_id, aname, amodel, 
      ais_tablet, ais_verified, ais_enabled, fis_default, aapikey)
      returning id_endpoint into fid_endpoint;
  else
    update endpoint set
      id_sysuser = aid_sysuser,
      id_notification_type = fid_notification_type,
      notification_id = anotification_id,
      id_pns_type = fid_pns_type,
      id_locale = fid_locale,
      device_id = adevice_id,
      azure_id = aazure_id,
      name = aname,
      model = amodel,
      is_tablet = ais_tablet,
      is_verified = ais_verified,
      is_enabled = ais_enabled,
      is_default = fis_default,
      apikey = aapikey
    where id_endpoint = aid_endpoint;
  end if;

  return fid_endpoint;
end
$body$
language 'plpgsql' VOLATILE;
