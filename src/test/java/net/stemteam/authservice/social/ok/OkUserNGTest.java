/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.social.ok;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class OkUserNGTest {
    
    public OkUserNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testUnmarshal() {
        String json = "{\"uid\":\"575065201120\",\"birthday\":\"1981-04-30\",\"age\":35,\"name\":\"Андрей Николаев\",\"locale\":\"ru\",\"gender\":\"male\",\"location\":{\"city\":\"\",\"country\":\"RUSSIAN_FEDERATION\",\"countryCode\":\"RU\",\"countryName\":\"Россия\"},\"online\":\"web\",\"first_name\":\"Андрей\",\"last_name\":\"Николаев\",\"has_email\":true,\"pic_1\":\"https://i508.mycdn.me/res/stub_50x50.gif\",\"pic_2\":\"https://usd1.mycdn.me/res/stub_128x96.gif\",\"pic_3\":\"https://i504.mycdn.me/res/stub_128x96.gif\"}";
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        OkUser user = gson.fromJson(json, OkUser.class);
        assertEquals(user.getFirstName(), "Андрей");
        assertEquals(user.getLastName(), "Николаев");
        assertEquals(user.getUid(), "575065201120");
        assertEquals(user.getLocation().getCountryName(), "Россия");
    }
    
}
