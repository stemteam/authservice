/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.social.vk;

import net.stemteam.authservice.oauth.OAuthRequest;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.Assert;

/**
 * Эти тесты не работают в автоматическом режиме и предназначены только для тестирования разработки
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class ITVkClient {

    VkClient client;

    /**
     * Четкий метод инициализации набора
     */
    @BeforeSuite
    public void before() {
        client = new VkClient();
        client.setApplicationId("5521020");
        client.setApplicationSecureKey("LbkRS0hDevW5Uo9wah2r");
        client.setApplicationOAuthRedirectUrl("https://test.heddo.ru/oauth");

    }

    @AfterSuite
    public void after() {
        // здесь нужно вернуть все к начальному виду
    }

    @Test(priority = 10)
    public void testPrepareRequest() throws Exception {
        System.out.println("testPrepareRequest");
        OAuthRequest req = client.prepareRequest();
        System.out.println("URL: " + req.getUrl());
        Assert.assertNotNull(req);
    }

//    @Test(priority = 20)
//    public void testAuthorize() throws Exception {
//        System.out.println("testAuthorize");
//        SysLogin login = client.authorize("6253756c5bd4d1dc88", "5cda0b01-b54d-41c5-8f49-24f6550e9ebf");
//        System.out.println(login);
//    }

//    @Test(priority = 30)
//    public void testGetMe() throws Exception {
//        System.out.println("testGetMe");
//        VkAccessToken token = new VkAccessToken();
//        token.setAccessToken("cdb673cc9e8a40f89c7d29483aadce9acdf00e5f5d123867eeebcdb0f379d7a32daefa6d9c7e86ed051e5");
//        VkUser u = client.getMe(token);
//        System.out.println(u);
//    }

}
