/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.service;

import java.lang.reflect.InvocationTargetException;
import java.security.InvalidParameterException;
import java.sql.DriverManager;
import java.text.MessageFormat;
import java.util.List;
import java.util.Properties;
import javax.sql.DataSource;
import net.stemteam.authservice.common.NotificationManager;
import net.stemteam.authservice.cron.DeleteExpiredLoginsJob;
import net.stemteam.authservice.cron.DeleteExpiredUserRequestsJob;
import net.stemteam.authservice.cron.UserNotificationJob;
import net.stemteam.authservice.dao.AuthRepository;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.handler.Activate;
import net.stemteam.authservice.handler.ChangePassword;
import net.stemteam.authservice.handler.CheckToken;
import net.stemteam.authservice.handler.ForgotPassword;
import net.stemteam.authservice.handler.ForgotPasswordConfirm;
import net.stemteam.authservice.handler.Login;
import net.stemteam.authservice.handler.RefUsersGet;
import net.stemteam.authservice.handler.Register;
import net.stemteam.authservice.handler.RemoveOtherTokens;
import net.stemteam.authservice.handler.UserAdd;
import net.stemteam.authservice.handler.UserDelete;
import net.stemteam.authservice.handler.UserDeleteConfirm;
import net.stemteam.authservice.handler.UserDeleteRequest;
import net.stemteam.authservice.handler.UserDisabledSet;
import net.stemteam.authservice.handler.UserEdit;
import net.stemteam.authservice.handler.UserGet;
import net.stemteam.authservice.handler.UserListGet;
import net.stemteam.authservice.handler.UserRequestAdd;
import net.stemteam.authservice.notification.AzureHub;
import net.stemteam.authservice.notification.handler.EndpointAdd;
import net.stemteam.authservice.notification.handler.EndpointListGet;
import net.stemteam.authservice.notification.handler.Notify;
import net.stemteam.authservice.notification.handler.NotifyForce;
import net.stemteam.authservice.notification.handler.PushRegistrationEdit;
import net.stemteam.authservice.notification.handler.ToggleEndpoint;
import net.stemteam.authservice.oauth.handler.Facebook;
import net.stemteam.authservice.oauth.handler.GetTokenHandler;
import net.stemteam.authservice.oauth.handler.Ok;
import net.stemteam.authservice.oauth.handler.PrepareRequestHandler;
import net.stemteam.authservice.oauth.handler.Vk;
import net.stemteam.authservice.service.impl.AuthServiceHttp;
import net.stemteam.authservice.service.impl.AuthServiceNative;
import net.stemteam.authservice.social.facebook.FacebookClient;
import net.stemteam.authservice.social.ok.OkClient;
import net.stemteam.authservice.social.vk.VkClient;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.authservice.urlshortener.handler.ShortUrlGet;
import net.stemteam.authservice.urlshortener.handler.ShortUrlOpen;
import net.stemteam.dao.jdbc.JdbcDaoFactory;
import net.stemteam.jaxis.Jaxis;
import net.stemteam.jaxis.common.SmsAero;
import net.stemteam.jaxis.common.UrlHelper;
import net.stemteam.jaxis.db.AbstractDbPool;
import net.stemteam.jaxis.db.DbConfig;
import net.stemteam.jaxis.mail.MailConfig;
import net.stemteam.jaxis.mail.MailConnectionSecurity;
import net.stemteam.rightservice.handler.RoleListGet;
import net.stemteam.rightservice.handler.UserRoleAdd;
import net.stemteam.rightservice.handler.UserRoleDelete;
import net.stemteam.rightservice.handler.UserRoleListGet;
import net.stemteam.rightservice.impl.client.RightClientJava;
import net.stemteam.rightservice.impl.dao.jdbc.JdbcDaoFactoryImpl;
import net.stemteam.rightservice.service.RightService;
import net.stemteam.rightservice.service.ServiceAPIType;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Фабрика. Создает экземпляры AuthService
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class AuthServiceFactory {

    private static final String CONFIG_PROPERTY_NAME = "stemteam.authservice.configuration";
    private static final String DEFAULT_CONFIG_NAME = "file:authservice.xml";
    private XMLConfiguration config;
    private Scheduler scheduler;
    private AuthServiceNative firstNativeService; // сылка на первый созданный экземпляр, при необходимости регистрации хендлеров они будут регистрироваться в него
    private Logger logger = LoggerFactory.getLogger(AuthServiceFactory.class);

    public static final Class[] HANDLER_CLASSES = {
        Login.class,
        CheckToken.class,
        Register.class,
        Activate.class,
        ForgotPassword.class,
        ForgotPasswordConfirm.class,
        ChangePassword.class,
        UserGet.class,
        UserAdd.class,
        PushRegistrationEdit.class,
        Notify.class,
        NotifyForce.class,
        EndpointListGet.class,
        EndpointAdd.class,
        RefUsersGet.class,
        ToggleEndpoint.class,
        UserDeleteRequest.class,
        UserDeleteConfirm.class,
        UserListGet.class,
        UserEdit.class,
        UserDelete.class,
        ShortUrlGet.class,
        ShortUrlOpen.class,
        UserRequestAdd.class,
        UserDisabledSet.class,
        RemoveOtherTokens.class
    };

    public static final Class[] RIGHT_HANDLER_CLASSES = {
        RoleListGet.class,
        net.stemteam.rightservice.handler.RightUserDelete.class,
        UserRoleAdd.class,
        UserRoleDelete.class,
        UserRoleListGet.class,};

    /**
     * Внимание! Требуются разные наборы настроек для создания Http и Native реализаций. Настройки ищутся в
     * System.getProperty("stemteam.authservice.configuration"), в формате
     *
     * @throws org.apache.commons.configuration.ConfigurationException
     */
    public AuthServiceFactory() throws ConfigurationException {
        // читаем расположение файла настроек
        String configName = System.getProperty(CONFIG_PROPERTY_NAME, DEFAULT_CONFIG_NAME);
        // подключаем XML конфиг
        if (configName.startsWith("file:")) {
            config = new XMLConfiguration(configName.substring(5));
        } else {
            throw new InvalidParameterException(MessageFormat.format("Invalid value {0} for key {1}!", configName,
                    CONFIG_PROPERTY_NAME));
        }
    }

    /**
     * Создается экземпляр сервиса атворизации.
     *
     * При создании нативного сервиса ServiceAPIType.JAVA_NATIVE автоматически запускаются кроны.
     *
     * Внимание! Требуются разные наборы настроек для создания Http и Native реализаций. Настройки ищутся в
     * System.getProperty("stemteam.authservice.configuration")
     *
     * @param type
     * @return
     */
    public AuthService getAuthService(ServiceAPIType type) throws SchedulerException {
        switch (type) {
            case JAVA_NATIVE:
                // TODO сделать 
                return getNativeService();
            case HTTP_REQUEST:
                return new AuthServiceHttp(config.getString("client.http.url"), config.getString("client.http.apikey"));
            default:
                throw new UnsupportedOperationException("Unsupported service type: " + type);
        }
    }

    private AuthServiceNative getNativeService() throws SchedulerException {
        AuthServiceNative service = new AuthServiceNative();

        // читаем из конфига настройки подключения к бд
        String salt1 = config.getString("secure.salt1"); // соль 1
        String salt2 = config.getString("secure.salt2"); // соль 2
        User.setSalt1(salt1);
        User.setSalt2(salt2);

        service.setShortUrlGetAddress(config.getString("urlshortener.shorturlget"));

        // настройки отправлялки почты
        MailConfig mailConfig = new MailConfig();
        mailConfig.setName(config.getString("mail.name"));
        mailConfig.setHost(config.getString("mail.host"));
        mailConfig.setPort(config.getInt("mail.port"));
        mailConfig.setLogin(config.getString("mail.login"));
        mailConfig.setPassword(config.getString("mail.password"));
        mailConfig.setReplyTo(config.getString("mail.replyto"));
        mailConfig.setSecurity(MailConnectionSecurity.getByCode(config.getString("mail.security", "NONE")));
        service.setMailConfig(mailConfig);

        // настройки отправлялки SMS (TODO избавиться от статического объекта)
        SmsAero.setUsername(config.getString("sms.username"));
        SmsAero.setPassword(config.getString("sms.password"));
        SmsAero.setSignature(config.getString("sms.signature"));

        // уведомления
        String is_push_enabled = config.getString("push.enabled");
        // при совпадении всех условий считаем конфигурацию отключеной
        if (!(is_push_enabled != null && !is_push_enabled.isEmpty() && is_push_enabled.equals("0"))) {
            List<HierarchicalConfiguration> hubs = config.configurationsAt("push.azure.hub");
            for (HierarchicalConfiguration hub : hubs) {
                String apiKey = hub.getString("apikey");
                String url = hub.getString("url");
                String name = hub.getString("name");
                AzureHub azureHub = new AzureHub(apiKey, url, name);
                service.getAzureHubs().put(apiKey, azureHub);
            }
        }

        // БД
        DbConfig dbConfig = new DbConfig(config.getString("db.Name"), config.getString("db.Url"), config.getInt(
                "db.MinIdle"),
                config.getInt("db.MaxIdle"), config.getInt("db.MaxTotal"), config.getInt("db.ConnectionWaitTimeout"));
        DataSource dataSource = new AbstractDbPool(dbConfig).getDataSource();
        AuthRepository authRepository = new AuthRepository();
        authRepository.setDataSource(dataSource);
        service.setAuthRepository(authRepository);

        // Вадим подкинул псевдонезависимый сервис
        JdbcDaoFactory fa = new JdbcDaoFactoryImpl(dataSource);
        RightService rightService = new RightClientJava(fa);
        authRepository.setRightService(rightService);

        // TODO магический суперсинглтон
        NotificationManager.getInstance().setDataSource(dataSource);
        NotificationManager.getInstance().setService(service);

        // загружаем клиенты OAuth
        VkClient vk = new VkClient();
        vk.setAuthRepository(authRepository);
        vk.setApplicationId(config.getString("vk.applicationId"));
        vk.setApplicationSecureKey(config.getString("vk.applicationSecureKey"));
        vk.setApplicationOAuthRedirectUrl(config.getString("vk.applicationOAuthRedirectUrl"));
        service.addOAuthClient(vk);

        FacebookClient facebook = new FacebookClient();
        facebook.setAuthRepository(authRepository);
        facebook.setApplicationId(config.getString("facebook.applicationId"));
        facebook.setApplicationSecureKey(config.getString("facebook.applicationSecureKey"));
        facebook.setApplicationOAuthRedirectUrl(config.getString("facebook.applicationOAuthRedirectUrl"));
        service.addOAuthClient(facebook);

        OkClient ok = new OkClient();
        ok.setAuthRepository(authRepository);
        ok.setApplicationId(config.getString("ok.applicationId"));
        ok.setApplicationPublicKey(config.getString("ok.applicationPublicKey"));
        ok.setApplicationSecureKey(config.getString("ok.applicationSecureKey"));
        ok.setApplicationOAuthRedirectUrl(config.getString("ok.applicationOAuthRedirectUrl"));
        service.addOAuthClient(ok);

        // создаем и запускаем планировщик (если такового еще не создано)
        if (scheduler == null) {
            Properties props = new Properties();
            props.setProperty("org.quartz.scheduler.instanceName", "auth-scheduler");
            props.setProperty("org.quartz.threadPool.threadCount", "3");
            props.setProperty("org.quartz.jobStore.class", "org.quartz.simpl.RAMJobStore");
            SchedulerFactory factory = new StdSchedulerFactory(props);
            scheduler = factory.getScheduler();

            JobDataMap jobData = new JobDataMap();
            // передаем источник данных в джоб
            jobData.put("DataSource", dataSource);
            jobData.put("AuthServiceNative", service);

            // создаем задание - отзыв о враче
            JobDetail job = JobBuilder.newJob(UserNotificationJob.class)
                    .withIdentity("UserNotificationJob", "authservice")
                    .usingJobData(jobData)
                    .build();

            Trigger trigger = TriggerBuilder.newTrigger()
                    .withIdentity("UserNotificationJobTrigger", "authservice")
                    .startNow()
                    .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                            .withIntervalInSeconds(5) // 5 секунд
                            .repeatForever())
                    .build();
            scheduler.scheduleJob(job, trigger);

            JobDetail job2 = JobBuilder.newJob(DeleteExpiredUserRequestsJob.class)
                    .withIdentity("DeleteExpiredUserRequestsJob", "authservice")
                    .usingJobData(jobData)
                    .build();

            Trigger trigger2 = TriggerBuilder.newTrigger()
                    .withIdentity("DeleteExpiredUserRequestsJobTrigger", "authservice")
                    .startNow()
                    .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                            .withIntervalInSeconds(60) // 1 минута
                            .repeatForever())
                    .build();
            scheduler.scheduleJob(job2, trigger2);

            JobDetail job3 = JobBuilder.newJob(DeleteExpiredLoginsJob.class)
                    .withIdentity("DeleteExpiredLoginsJob", "authservice")
                    .usingJobData(jobData)
                    .build();
            Trigger trigger3 = TriggerBuilder.newTrigger()
                    .withIdentity("DeleteExpiredLoginsJobTrigger", "authservice")
                    .startNow()
                    .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                            .withIntervalInSeconds(1800)
                            .repeatForever())
                    .build();
            scheduler.scheduleJob(job3, trigger3);

            scheduler.start();
        }

        // сылка на первый созданный экземпляр, при необходимости регистрации хендлеров они будут регистрироваться в него
        if (firstNativeService == null) {
            firstNativeService = service;
        }

        return service;
    }

    /**
     * Регистрирует HTTP хэндлеры в целевом веб-сервере
     *
     * @param jaxis
     * @throws SchedulerException
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws InstantiationException
     */
    public void registerHandlers(Jaxis jaxis) throws SchedulerException, NoSuchMethodException, IllegalAccessException, InvocationTargetException,
            InstantiationException {
        // ежели у нас нет ни одного нативного клиента - создадим одного и опубликуем в него
        if (firstNativeService == null) {
            firstNativeService = getNativeService();
        }

        // публикуем хэндлеры
        String defaultNamespace = config.getString("namespace.default");
        String[] auth = defaultNamespace.split(" ");
        for (String namespace : auth) {
            namespace = UrlHelper.correctNamespace(namespace);
            logger.debug("DEPLOY DEFAULT CONTEXTS FOR: {}", namespace);

            // хэндлеры
            for (Class c : HANDLER_CLASSES) {
                createContext(firstNativeService, jaxis, c, namespace + "/" + c.getSimpleName());
            }

            // oauth
            createContext(firstNativeService, jaxis, PrepareRequestHandler.class, namespace + "/oauth/prepareRequest");
            createContext(firstNativeService, jaxis, GetTokenHandler.class, namespace + "/oauth/getToken");
            createContext(firstNativeService, jaxis, Vk.class, namespace + "/oauth/vk");
            createContext(firstNativeService, jaxis, Facebook.class, namespace + "/oauth/facebook");
            createContext(firstNativeService, jaxis, Ok.class, namespace + "/oauth/ok");

            for (Class c : RIGHT_HANDLER_CLASSES) {
                createContext(firstNativeService, jaxis, c, namespace + "/" + c.getSimpleName());
            }
        }
    }

    /**
     * Регистрация обработчика с тем же именем как и класс
     *
     * @param authService
     * @param jaxis где регистрировать
     * @param hClass класс обработчик
     * @param url урл для публикации
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws java.lang.reflect.InvocationTargetException
     * @throws InstantiationException
     */
    private void createContext(AuthServiceNative authService, Jaxis jaxis, Class<? extends DataSetHandler> hClass,
            String url) throws
            NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        DataSetHandler handler = hClass.getConstructor().newInstance();
        handler.setAuthService(authService);
        handler.setDataSource(authService.getAuthRepository().getDataSource()); // TODO ерунда, убрать
        jaxis.createContext(url, handler);
    }

    public static void main(String[] args) throws Exception {
        DriverManager.registerDriver(new org.postgresql.Driver());

        AuthServiceFactory factory = new AuthServiceFactory();

        // HttpClient
        AuthService httpClient = factory.getAuthService(ServiceAPIType.HTTP_REQUEST);
        User u = httpClient.getUser("d6bfe0d2-2d58-4007-9ad0-a50df3fdc373");
        System.out.println("User: " + u.getGuuid());

        // Нативный клиент
        AuthService service = factory.getAuthService(ServiceAPIType.JAVA_NATIVE);
        User uu = service.getUser("d6bfe0d2-2d58-4007-9ad0-a50df3fdc373");
        System.out.println("User native: " + uu.getGuuid());

        // публикация хэндлеров
        Jaxis jaxis = new Jaxis();
        jaxis.init(8007, 50, 200, 300, 1000);
        factory.registerHandlers(jaxis);
    }

}
