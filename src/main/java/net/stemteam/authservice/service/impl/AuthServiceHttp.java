/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.service.impl;

import net.stemteam.authservice.service.AuthService;
import net.stemteam.authservice.entity.Endpoint;
import net.stemteam.authservice.entity.enums.NotificationTypeCode;
import net.stemteam.authservice.common.UserRequestType;
import net.stemteam.authservice.entity.RefType;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.handler.UserRequestAdd;
import net.stemteam.authservice.urlshortener.handler.ShortUrlGet;
import net.stemteam.datatransport.exception.ColumnNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataRecord;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.common.HttpHelper;
import net.stemteam.jaxis.common.PNSType;
import net.stemteam.jaxis.common.UrlHelper;
import net.stemteam.jaxis.common.Version;
import net.stemteam.jaxis.common.WebRequestData;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.jaxis.err.StemException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.stemteam.authservice.api.support.DataSetProvider;
import net.stemteam.authservice.api.support.ParamHelper;
import net.stemteam.authservice.api.support.UserRequestPropogation;
import net.stemteam.authservice.exception.PermissionDenyException;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.handler.RemoveOtherTokens;
import net.stemteam.authservice.handler.UserGet;
import net.stemteam.rightservice.entity.Role;
import net.stemteam.rightservice.handler.RightUserDelete;
import net.stemteam.rightservice.handler.RoleListGet;
import net.stemteam.rightservice.handler.UserRoleAdd;
import net.stemteam.rightservice.handler.UserRoleDelete;
import net.stemteam.rightservice.handler.UserRoleListGet;

/**
 * Клиент сервиса авторизации, взаимодействие посредством HTTP.
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class AuthServiceHttp implements AuthService {

    // поддерживаемая версия сервиса авторизации (управляется только из кода)
    // TODO читать из манифеста вместо явной инициализации (она глупая)
    private static final Version AUTH_SERVICE_VERSION = new Version("3.2");

    Logger logger = LoggerFactory.getLogger(this.getClass());

    private String url;
    private String apiKey;
    private int connectTimeoutMs = 30000;
    private int readTimeoutMs = 30000;

    /**
     * Клиент сервиса авторизации, взаимодействие посредством HTTP.
     *
     * @param url URL подключения к сервису авторизации
     * @param apiKey ключ API для работы с сервисом авторизации
     */
    public AuthServiceHttp(String url, String apiKey) {
        this.url = url;
        this.apiKey = apiKey;
    }

    public String getUrl() {
        return url;
    }

    public String getApiKey() {
        return apiKey;
    }

    /**
     * Возвращает таймаут подключения к ресурсу, установленный для методов хэлпера (милисекунды)
     *
     * @return
     */
    public int getConnectTimeoutMs() {
        return connectTimeoutMs;
    }

    /**
     * Устанавливает таймаут подключения к ресурсу, установленный для методов хэлпера (милисекунды)
     *
     * @param connectTimeoutMs
     */
    public void setConnectTimeoutMs(int connectTimeoutMs) {
        this.connectTimeoutMs = connectTimeoutMs;
    }

    /**
     * Возвращает таймаут чтения данных из ресурса, установленный для методов хэлпера (милисекунды)
     *
     * @return
     */
    public int getReadTimeoutMs() {
        return readTimeoutMs;
    }

    /**
     * Устанавливает таймаут подключения к ресурсу, установленный для методов хэлпера (милисекунды)
     *
     * @param readTimeoutMs
     */
    public void setReadTimeoutMs(int readTimeoutMs) {
        this.readTimeoutMs = readTimeoutMs;
    }

    @Override
    public String shortUrlGet(String longUrl) throws HttpInternalException, HttpException {
        final String method = "/" + ShortUrlGet.class.getSimpleName() + ".json";

        // формируем параметры запроса
        final HashMap<String, String> params = new HashMap<>();
        params.put(ShortUrlGet.PARAM_VERSION, "10");
        params.put(ShortUrlGet.PARAM_APIKEY, apiKey);
        params.put(ShortUrlGet.PARAM_URL, longUrl);
        HttpEntity entity;
        try {
            entity = buildEntity(params, null);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        }
        HttpPost request = new HttpPost(this.url + method);
        request.setEntity(entity);

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpResponse response = httpClient.execute(request);
            HttpEntity respEntity = response.getEntity();
            String responseString = EntityUtils.toString(respEntity, "UTF-8");

            if (response.getStatusLine().getStatusCode() != HttpURLConnection.HTTP_OK) {
                try {
                    DataSet ds = DataSet.fromJson(responseString);
                    throw new HttpException(response.getStatusLine().getStatusCode(), ds);
                } catch (DataSetException | HttpException ex) {
                    throw new HttpException(response.getStatusLine().getStatusCode(), responseString);
                }
            }

            // парсим ответ
            try {
                DataSet ds = DataSet.fromJson(responseString);
                return ds.getRecords().firstElement().getString(ShortUrlGet.FIELD_URL);
            } catch (Exception ex) {
                throw new HttpException(HttpURLConnection.HTTP_INTERNAL_ERROR, ex, responseString);
            }
        } catch (IOException ex) {
            throw new HttpInternalException(ex);
        }
    }

    /**
     * Создает заготовку HTTP запроса
     *
     * @param params карта HTTP параметров (Имя параметра - Значение)
     * @param files карта файлов для загрузки (Имя параметра - Путь к файлу в файловой системе)
     * @return
     */
    private static HttpEntity buildEntity(HashMap<String, String> params, HashMap<String, String> files) throws
            UnsupportedEncodingException {
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        if (params != null) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();

                if ((key != null) && (value != null)) {
                    builder.addTextBody(key, value, ContentType.create("text/plain", "utf-8"));
                }
            }
        }

        if (files != null) {
            for (Map.Entry<String, String> entry : files.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                File f = new File(value);
                builder.addBinaryBody(key, f, ContentType.create("application/octet-stream", "utf-8"), f.getName());
            }
        }
        HttpEntity entity = builder.build();
        return entity;
    }

    /**
     * Проверяет корректно ли сконфигурирован этот хелпер
     *
     * @throws NotConfiguredException
     */
    private void checkConfigured() throws HttpInternalException {
        if ((url == null) || (apiKey == null)) {
            throw new HttpInternalException("Auth client url | apiKey is empty");
        }
    }

//    /**
//     * Возвращает пользователя по токену авторизации
//     *
//     * @param token токен авторизации
//     * @param withReferedUsers нужно ли загружать связи
//     * @param withEndpoints нужно ли возвращать связанные с пользователем эндпоинты
//     * @return
//     * @throws net.stemteam.jaxis.err.HttpUnauthorizedException
//     * @throws net.stemteam.jaxis.err.HttpInternalException
//     */
//    @Override
//    public User getUser(String token, boolean withReferedUsers, boolean withEndpoints) throws HttpUnauthorizedException,
//            HttpInternalException {
//        return getUser(token, withReferedUsers, withEndpoints, false);
//    }
//    
//    /**
//     * Возвращает пользователя по токену авторизации
//     *
//     * @param token токен авторизации
//     * @param withReferedUsers нужно ли загружать связи
//     * @param withEndpoints нужно ли возвращать связанные с пользователем эндпоинты
//     * @param withRoles нужно ли получить список родей
//     * @return
//     * @throws net.stemteam.jaxis.err.StemException
//     */
//    public User getUser(String token, boolean withReferedUsers, boolean withEndpoints, boolean withRoles) throws HttpInternalException, HttpUnauthorizedException {
//        // загрузка пользователя
//        User u = checkToken27(token);
//        // если нужно - загрузка связей
//        if (withReferedUsers) {
//            loadRefUsers(u, null);
//        }
//        // если нужна загрузка эндпоинтов
//        if (withEndpoints) {
//            loadEndpoints(u);
//        }
//        // 
//        return u;
//    }
    private StringBuilder appendParam(StringBuilder sb, String param, String value) throws UnsupportedEncodingException {
        if (value != null) {
            sb.append("&").append(param).append("=").append(UrlHelper.Encode(value));
        }
        return sb;
    }

    private User checkToken27(String token) throws HttpInternalException, HttpUnauthorizedException {
        checkConfigured();
        try {
            StringBuilder sbUrl = new StringBuilder();
            sbUrl.append(url).append("/CheckToken.jaxis?Version=")
                    .append(UrlHelper.Encode(AUTH_SERVICE_VERSION.get()));
            appendParam(sbUrl, "Token", token);
            appendParam(sbUrl, "ApiKey", apiKey);

            URL u = new URL(sbUrl.toString());
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            try {
                c.setRequestMethod("GET");
                c.setRequestProperty("Content-length", "0");
                c.setUseCaches(false);
                c.setAllowUserInteraction(false);
                c.setConnectTimeout(connectTimeoutMs);
                c.setReadTimeout(readTimeoutMs);
                c.connect();
                int status = c.getResponseCode();
                if (status == HttpURLConnection.HTTP_OK) {
                    StringBuilder sb;
                    try ( // если пользователь авторизован - нам нужно получить его GUUID
                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }

                    // получаем дата сет
                    try {
                        DataSet ds = new DataSet();
                        ds.UnPackData(sb.toString());
                        DataRecord row = ds.getRecords().firstElement();
                        String resToken = row.getString("Token");
                        String guuid = row.getString("Id");

                        // не позволяем подменить токен
                        if (!token.equals(resToken)) {
                            throw new HttpUnauthorizedException("Пользователь не авторизован");
                        }
                        return new User(token, guuid);
                    } catch (Exception ex) {
                        throw new HttpInternalException(ex);
                    }
                } else if (status == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    // если пользователь не авторизован
                    throw new HttpUnauthorizedException("Пользователь не авторизован");
                } else {
                    StringBuilder sb;
                    try ( // некая внутренняя ошибка сервера
                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getErrorStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }

                    // логируем
                    logger.error("checkToken error from remote server {} message {}", url, sb.toString());

                    // троваем, в зависимости от исключения
                    throw new HttpInternalException("Ошибка проверки токена.");
                }

            } finally {
                c.disconnect();
            }
        } catch (MalformedURLException ex) {
            throw new HttpInternalException(ex);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        } catch (java.io.IOException ex) {
            throw new HttpInternalException(ex);
        }
    }

    /**
     * Загрузка связанных пользователей
     *
     * @param user пользователь (используется токен)
     * @param refType тип связи
     * @throws net.stemteam.jaxis.err.HttpInternalException
     */
    @Override
    public void loadRefUsers(User user, RefType refType) throws HttpInternalException {
        // спросим детей у сервиса авторизации
        StringBuilder request = new StringBuilder();
        try {
            request
                    .append(getUrl())
                    .append("/RefUsersGet.jaxis")
                    .append("?Version=").append(UrlHelper.Encode(AUTH_SERVICE_VERSION.get()))
                    .append("&Token=").append(UrlHelper.Encode(user.getToken()))
                    .append("&ApiKey=").append(UrlHelper.Encode(getApiKey()));
            if (refType != null) {
                request.append("&RefType=").append(UrlHelper.Encode(refType.getCode()));
            }
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        }

        DataSet refUsers = new DataSet();
        try {
            WebRequestData responce = HttpHelper.requestGet(request.toString(), null, null, connectTimeoutMs,
                    readTimeoutMs);
            if (responce.getHttpCode() != HttpURLConnection.HTTP_OK) {
                try {
                    // тровнем с распакованным датасетом
                    refUsers.UnPackData(responce.getResponceMessage());
                    throw new HttpException(responce.getHttpCode(), refUsers);
                } catch (DataSetException ex) {
                    // если не датасет - тровнем так
                    throw new HttpException(responce.getHttpCode(), responce.getResponceMessage());
                }
            } else {
                // распаковка ответа
                refUsers.UnPackData(responce.getResponceMessage());
            }
        } catch (Exception ex) {
            logger.error("", ex);
            throw new HttpInternalException(ex);
        }

        for (DataRecord row : refUsers.getRecords()) {
            try {
                String guuid = row.getString("Guuid");
                RefType rt = RefType.getByCode(row.getString("RefType"));
                // создаем пользователя
                User u = new User(null, guuid);
                // связываем с текущими
                user.addReference(u, rt);
            } catch (ColumnNotFoundException ex) {
                throw new HttpInternalException(ex);
            }
        }
    }

//    /**
//     * Выполняет запрос к сервису авторизации с целью получения данных пользователя по его токену/логину
//     *
//     * @param token токен авторизации
//     * @param login логин (телефон или мыло)
//     * @return DataSet с инфой
//     * @throws net.stemteam.jaxis.err.HttpInternalException
//     * @throws net.stemteam.jaxis.err.HttpUnauthorizedException
//     * @throws net.stemteam.jaxis.err.HttpNotFoundException
//     * @deprecated неудобно работать с датасетом
//     */
//    @Deprecated
//    @Override
//    public DataSet getUserInfo(String token, String login) throws HttpInternalException, HttpUnauthorizedException,
//            HttpNotFoundException {
//        checkConfigured();
//
//        try {
//            StringBuilder sbUrl = new StringBuilder();
//            sbUrl.append(url).append("/UserInfoGet.jaxis?Version=1&ApiKey=").append(UrlHelper.Encode(apiKey));
//            if ((token != null) && (!"".equals(token))) {
//                sbUrl.append("&Token=").append(UrlHelper.Encode(token));
//            }
//            if ((login != null) && (!"".equals(login))) {
//                sbUrl.append("&Login=").append(UrlHelper.Encode(login));
//            }
//
//            URL u = new URL(sbUrl.toString());
//            HttpURLConnection c = (HttpURLConnection) u.openConnection();
//            try {
//                c.setRequestMethod("GET");
//                c.setRequestProperty("Content-length", "0");
//                c.setUseCaches(false);
//                c.setAllowUserInteraction(false);
//                c.setConnectTimeout(connectTimeoutMs);
//                c.setReadTimeout(readTimeoutMs);
//                c.connect();
//                int status = c.getResponseCode();
//                if (status == HttpURLConnection.HTTP_OK) {
//                    StringBuilder sb;
//                    try ( // если пользователь авторизован - нам нужно получить его GUUID
//                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8")) // UTF-8 обязательно
//                            ) {
//                        sb = new StringBuilder();
//                        String line;
//                        while ((line = br.readLine()) != null) {
//                            sb.append(line);
//                            sb.append("\n");
//                        }
//                    }
//
//                    // получаем дата сет
//                    try {
//                        DataSet ds = new DataSet();
//                        ds.UnPackData(sb.toString());
//                        return ds;
//                    } catch (Exception ex) {
//                        throw new HttpInternalException(ex);
//                    }
//                } else if (status == HttpURLConnection.HTTP_UNAUTHORIZED) {
//                    // если пользователь не авторизован
//                    throw new HttpUnauthorizedException("Пользователь не авторизован");
//                } else if (status == HttpURLConnection.HTTP_NOT_FOUND) {
//                    // если пользователь не авторизован
//                    throw new HttpNotFoundException("Пользователь не найден");
//                } else {
//                    StringBuilder sb;
//                    try ( // некая внутренняя ошибка сервера
//                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getErrorStream(), "UTF-8")) // UTF-8 обязательно
//                            ) {
//                        sb = new StringBuilder();
//                        String line;
//                        while ((line = br.readLine()) != null) {
//                            sb.append(line);
//                            sb.append("\n");
//                        }
//                    }
//
//                    // логируем
//                    logger.error("checkToken error from remote server {} message {}", url, sb.toString());
//
//                    // троваем, в зависимости от исключения
//                    throw new HttpInternalException("Ошибка проверки токена.");
//                }
//
//            } finally {
//                c.disconnect();
//            }
//        } catch (MalformedURLException ex) {
//            throw new HttpInternalException(ex);
//        } catch (UnsupportedEncodingException ex) {
//            throw new HttpInternalException(ex);
//        } catch (java.io.IOException ex) {
//            throw new HttpInternalException(ex);
//        }
//    }
    /**
     * Вход пользователя в систему посредством сервиса авторизации
     *
     * @param login
     * @param password
     * @return
     * @throws HttpException
     */
    @Override
    public User login(String login, String password) throws HttpException {
        checkConfigured();

        try {
            StringBuilder sbUrl = new StringBuilder();
            sbUrl.append(url).append("/Login.jaxis?Version=1&Login=").append(UrlHelper.Encode(login)).append(
                    "&Password=").append(UrlHelper.Encode(password)).append("&ApiKey=").append(UrlHelper.Encode(apiKey));

            URL u = new URL(sbUrl.toString());
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            try {
                c.setRequestMethod("GET");
                c.setRequestProperty("Content-length", "0");
                c.setUseCaches(false);
                c.setAllowUserInteraction(false);
                c.setConnectTimeout(connectTimeoutMs);
                c.setReadTimeout(readTimeoutMs);
                c.connect();
                int status = c.getResponseCode();
                if (status == HttpURLConnection.HTTP_OK) {
                    StringBuilder sb;
                    try ( // если пользователь авторизован - нам нужно получить его GUUID
                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }

                    // получаем дата сет
                    try {
                        DataSet ds = new DataSet();
                        ds.UnPackData(sb.toString());
                        DataRecord row = ds.getRecords().firstElement();
                        String resToken = row.getString("Token");
                        String resId = row.getString("Id");
                        User user = new User(resToken, resId);
                        return user;
                    } catch (Exception ex) {
                        throw new HttpInternalException(ex);
                    }
                } else if (status == HttpURLConnection.HTTP_NOT_FOUND) {
                    // если пользователь не авторизован
                    throw new HttpNotFoundException("Пользователь с таким логином и паролем не найден");
                } else {
                    StringBuilder sb;
                    try ( // некая внутренняя ошибка сервера
                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getErrorStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }

                    // логируем
                    logger.error("login error from remote server {} message {}", url, sb.toString());

                    // получаем датасет
                    DataSet ds = new DataSet();
                    try {
                        ds.UnPackData(sb.toString());
                    } catch (Exception ex) {
                        throw new HttpInternalException(ex);
                    }

                    throw new HttpException(status, ds);
                }

            } finally {
                c.disconnect();
            }
        } catch (MalformedURLException ex) {
            throw new HttpInternalException(ex);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        } catch (java.io.IOException ex) {
            throw new HttpInternalException(ex);
        }
    }

    /**
     * Регистрация пользователя в системе авторизации
     *
     * @param phone
     * @param email
     * @param password
     * @param serviceName название сервиса (используется в оповещениях)
     * @param host хост (используется в оповещениях)
     * @param notificationType метод валидации (phone, email)
     * @param firstName
     * @param lastName
     * @param middleName
     * @param image
     * @param smsSign подпись СМС (отправитель)
     * @return Guuid добавленного но неподтвержденного пользователя
     * @throws HttpException
     */
    @Override
    public String register(String phone, String email, String password, String serviceName, String host,
            String notificationType, String firstName, String lastName, String middleName, String image, String smsSign)
            throws HttpException {
        checkConfigured();

        try {
            StringBuilder sbUrl = new StringBuilder();
            sbUrl
                    .append(url)
                    .append("/Register.jaxis?Version=1&Phone=").append(UrlHelper.Encode(phone))
                    .append("&ApiKey=").append(UrlHelper.Encode(apiKey))
                    .append("&Email=").append(UrlHelper.Encode(email))
                    .append("&Password=").append(UrlHelper.Encode(password))
                    .append("&ServiceName=").append(UrlHelper.Encode(serviceName))
                    .append("&Host=").append(UrlHelper.Encode(host))
                    .append("&NotificationType=").append(UrlHelper.Encode(notificationType))
                    .append("&FirstName=").append(UrlHelper.Encode(firstName))
                    .append("&LastName=").append(UrlHelper.Encode(lastName))
                    .append("&MiddleName=").append(UrlHelper.Encode(middleName))
                    .append("&Image=").append(UrlHelper.Encode(image))
                    .append("&SmsSign=").append(UrlHelper.Encode(smsSign));
            URL u = new URL(sbUrl.toString());
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            try {
                c.setRequestMethod("GET");
                c.setRequestProperty("Content-length", "0");
                c.setUseCaches(false);
                c.setAllowUserInteraction(false);
                c.setConnectTimeout(connectTimeoutMs);
                c.setReadTimeout(readTimeoutMs);
                c.connect();
                int status = c.getResponseCode();
                if (status == HttpURLConnection.HTTP_OK) {
                    StringBuilder sb;
                    try ( // парсим GUUID из ответа
                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }

                    // получаем дата сет
                    try {
                        DataSet ds = new DataSet();
                        ds.UnPackData(sb.toString());
                        DataRecord row = ds.getRecords().firstElement();
                        String guuid = row.getString("Id");
                        return guuid;
                    } catch (Exception ex) {
                        throw new HttpInternalException(ex);
                    }

                } else { // некая внутренняя ошибка сервера
                    StringBuilder sb;
                    try (
                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getErrorStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }

                    // логируем
                    logger.error("register error from remote server {} message {}", url, sb.toString());

                    // получаем датасет
                    DataSet ds = new DataSet();
                    try {
                        ds.UnPackData(sb.toString());
                    } catch (Exception ex) {
                        throw new HttpInternalException(ex);
                    }

                    throw new HttpException(status, ds);
                }

            } finally {
                c.disconnect();
            }
        } catch (MalformedURLException ex) {
            throw new HttpInternalException(ex);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        } catch (java.io.IOException ex) {
            throw new HttpInternalException(ex);
        }
    }

    /**
     * Активация пользователя (завершение регистрации)
     *
     * @param login
     * @param activationCode
     * @return GUUID
     * @throws HttpInternalException
     * @throws HttpNotFoundException
     * @throws HttpBadRequestException
     */
    @Override
    public String activate(String login, String activationCode) throws HttpException {
        checkConfigured();

        try {
            StringBuilder sbUrl = new StringBuilder();
            sbUrl.append(url).append("/Activate.jaxis?Version=1&Login=").append(UrlHelper.Encode(login)).append(
                    "&ApiKey=").append(UrlHelper.Encode(apiKey)).append("&ActivationCode=").append(UrlHelper.Encode(
                                    activationCode));

            URL u = new URL(sbUrl.toString());
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            try {
                c.setRequestMethod("GET");
                c.setRequestProperty("Content-length", "0");
                c.setUseCaches(false);
                c.setAllowUserInteraction(false);
                c.setConnectTimeout(connectTimeoutMs);
                c.setReadTimeout(readTimeoutMs);
                c.connect();
                int status = c.getResponseCode();
                if (status == HttpURLConnection.HTTP_OK) {
                    StringBuilder sb;
                    try ( // парсим GUUID из ответа
                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }

                    // получаем дата сет
                    try {
                        DataSet ds = new DataSet();
                        ds.UnPackData(sb.toString());
                        DataRecord row = ds.getRecords().firstElement();
                        String guuid = row.getString("Id");
                        return guuid;
                    } catch (Exception ex) {
                        throw new HttpInternalException(ex);
                    }

                } else if (status == HttpURLConnection.HTTP_NOT_FOUND) {
                    throw new HttpInternalException("Пользователь не найден");
                } else {
                    StringBuilder sb;
                    try ( // некая внутренняя ошибка сервера
                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getErrorStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }

                    // логируем
                    logger.error("activate error from remote server {} message {}", url, sb.toString());

                    // получаем датасет
                    DataSet ds = new DataSet();
                    try {
                        ds.UnPackData(sb.toString());
                    } catch (Exception ex) {
                        throw new HttpInternalException(ex);
                    }

                    throw new HttpException(status, ds);
                }

            } finally {
                c.disconnect();
            }
        } catch (MalformedURLException ex) {
            throw new HttpInternalException(ex);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        } catch (java.io.IOException ex) {
            throw new HttpInternalException(ex);
        }
    }

    /**
     * Забыл пароль - процедура восстановления. В итоге должно отправиться оповещение с кодом восстановления
     *
     * @param login
     * @param serviceName
     * @param host
     * @param notificationType
     * @param smsSign подпись СМС (отправитель)
     * @throws HttpException
     */
    @Override
    public void forgotPassword(String login, String serviceName, String host, String notificationType, String smsSign)
            throws HttpException {
        checkConfigured();

        try {
            StringBuilder sbUrl = new StringBuilder();
            sbUrl.append(url).append("/ForgotPassword.jaxis?Version=1&Login=").append(UrlHelper.Encode(login)).append(
                    "&ApiKey=").append(UrlHelper.Encode(apiKey)).append("&ServiceName=").append(UrlHelper.Encode(
                                    serviceName)).append("&Host=").append(UrlHelper.Encode(host))
                    .append("&NotificationType=").append(UrlHelper.Encode(notificationType))
                    .append("&SmsSign=").append(UrlHelper.Encode(smsSign));

            URL u = new URL(sbUrl.toString());
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            try {
                c.setRequestMethod("GET");
                c.setRequestProperty("Content-length", "0");
                c.setUseCaches(false);
                c.setAllowUserInteraction(false);
                c.setConnectTimeout(connectTimeoutMs);
                c.setReadTimeout(readTimeoutMs);
                c.connect();
                int status = c.getResponseCode();
                if (status == HttpURLConnection.HTTP_OK) {
                } else if (status == HttpURLConnection.HTTP_NOT_FOUND) {
                    throw new HttpNotFoundException("Пользователь не найден");
                } else {
                    StringBuilder sb;
                    try ( // некая внутренняя ошибка сервера
                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getErrorStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }

                    // логируем
                    logger.error("forgot password error from remote server {} message {}", url, sb.toString());

                    // получаем датасет
                    DataSet ds = new DataSet();
                    try {
                        ds.UnPackData(sb.toString());
                    } catch (Exception ex) {
                        throw new HttpInternalException(ex);
                    }

                    throw new HttpException(status, ds);
                }

            } finally {
                c.disconnect();
            }
        } catch (MalformedURLException ex) {
            throw new HttpInternalException(ex);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        } catch (java.io.IOException ex) {
            throw new HttpInternalException(ex);
        }
    }

    /**
     * Забыл пароль. Подтверждение кодом и смена на новый.
     *
     * @param login
     * @param code
     * @param newPassword
     * @throws HttpException
     */
    @Override
    public void forgotPasswordConfirm(String login, String code, String newPassword) throws HttpException {
        checkConfigured();

        try {
            StringBuilder sbUrl = new StringBuilder();
            sbUrl.append(url).append("/ForgotPasswordConfirm.jaxis?Version=1&Login=").append(UrlHelper.Encode(login)).
                    append("&ApiKey=").append(UrlHelper.Encode(apiKey)).append("&Code=").append(UrlHelper.Encode(code)).
                    append("&NewPassword=").append(UrlHelper.Encode(newPassword));

            URL u = new URL(sbUrl.toString());
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            try {
                c.setRequestMethod("GET");
                c.setRequestProperty("Content-length", "0");
                c.setUseCaches(false);
                c.setAllowUserInteraction(false);
                c.setConnectTimeout(connectTimeoutMs);
                c.setReadTimeout(readTimeoutMs);
                c.connect();
                int status = c.getResponseCode();
                if (status == HttpURLConnection.HTTP_OK) {
                } else if (status == HttpURLConnection.HTTP_NOT_FOUND) {
                    throw new HttpNotFoundException("Пользователь не найден");
                } else {
                    StringBuilder sb;
                    try ( // некая внутренняя ошибка сервера
                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getErrorStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }

                    // логируем
                    logger.error("forgot password confirm error from remote server {} message {}", url, sb.toString());

                    // получаем датасет
                    DataSet ds = new DataSet();
                    try {
                        ds.UnPackData(sb.toString());
                    } catch (Exception ex) {
                        throw new HttpInternalException(ex);
                    }

                    throw new HttpException(status, ds);
                }

            } finally {
                c.disconnect();
            }
        } catch (MalformedURLException ex) {
            throw new HttpInternalException(ex);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        } catch (java.io.IOException ex) {
            throw new HttpInternalException(ex);
        }
    }

    /**
     * Изменение собственного пароля пользователем
     *
     * @param token
     * @param currentPassword
     * @param newPassword
     * @param newPasswordConfirm
     * @throws HttpInternalException
     * @throws HttpNotFoundException
     * @throws HttpBadRequestException
     */
    @Override
    public void changePassword(String token, String currentPassword, String newPassword, String newPasswordConfirm)
            throws HttpException {
        checkConfigured();

        try {
            StringBuilder sbUrl = new StringBuilder();
            sbUrl.append(url).append("/ChangePassword.jaxis?Version=1&Token=").append(UrlHelper.Encode(token)).append(
                    "&ApiKey=").append(UrlHelper.Encode(apiKey)).append("&CurrentPassword=").append(UrlHelper.Encode(
                                    currentPassword)).append("&NewPassword=").append(UrlHelper.Encode(newPassword)).
                    append("&NewPasswordConfirm=").append(UrlHelper.Encode(newPasswordConfirm));

            URL u = new URL(sbUrl.toString());
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            try {
                c.setRequestMethod("GET");
                c.setRequestProperty("Content-length", "0");
                c.setUseCaches(false);
                c.setAllowUserInteraction(false);
                c.setConnectTimeout(connectTimeoutMs);
                c.setReadTimeout(readTimeoutMs);
                c.connect();
                int status = c.getResponseCode();
                if (status == HttpURLConnection.HTTP_OK) {
                } else if (status == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    throw new HttpNotFoundException("Пользователь не авторизован");
                } else {
                    StringBuilder sb;
                    try ( // некая внутренняя ошибка сервера
                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getErrorStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }

                    // логируем
                    logger.error("change password error from remote server {} message {}", url, sb.toString());

                    // получаем датасет
                    DataSet ds = new DataSet();
                    try {
                        ds.UnPackData(sb.toString());
                    } catch (Exception ex) {
                        throw new HttpInternalException(ex);
                    }

                    throw new HttpException(status, ds);
                }

            } finally {
                c.disconnect();
            }
        } catch (MalformedURLException ex) {
            throw new HttpInternalException(ex);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        } catch (java.io.IOException ex) {
            throw new HttpInternalException(ex);
        }
    }

    /**
     * Добавление пользователя в систему
     *
     * @param token
     * @param phone
     * @param email
     * @param password
     * @param serviceName
     * @param host
     * @param notificationType
     * @param firstName
     * @param lastName
     * @param middleName
     * @param image
     * @param refType
     * @return
     * @throws HttpException
     */
    @Override
    public String addUser(String token, String phone, String email, String password, String serviceName, String host,
            String notificationType, String firstName, String lastName, String middleName, String image, RefType refType, Timestamp birthday)
            throws HttpException {
        checkConfigured();

        try {
            StringBuilder sbUrl = new StringBuilder();
            sbUrl
                    .append(url)
                    .append("/UserAdd.jaxis?Version=1&Phone=")
                    .append(UrlHelper.Encode(phone))
                    .append("&Token=").append(UrlHelper.Encode(token))
                    .append("&ApiKey=").append(UrlHelper.Encode(apiKey))
                    .append("&Email=").append(UrlHelper.Encode(email))
                    .append("&Password=").append(UrlHelper.Encode(password))
                    .append("&ServiceName=").append(UrlHelper.Encode(serviceName))
                    .append("&Host=").append(UrlHelper.Encode(host))
                    .append("&NotificationType=").append(UrlHelper.Encode(notificationType))
                    .append("&FirstName=").append(UrlHelper.Encode(firstName))
                    .append("&LastName=").append(UrlHelper.Encode(lastName))
                    .append("&MiddleName=").append(UrlHelper.Encode(middleName))
                    .append("&Image=").append(UrlHelper.Encode(image));
            if (birthday != null) {
                sbUrl.append("&Birthday=").append(String.format("%1$tY%1$tm%1$td%1$tH%1$tM%1$tS.%1$tL", birthday));
            }

            if (refType != null) {
                sbUrl.append("&RefType=").append(refType.getCode());
            }

            URL u = new URL(sbUrl.toString());
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            try {
                c.setRequestMethod("GET");
                c.setRequestProperty("Content-length", "0");
                c.setUseCaches(false);
                c.setAllowUserInteraction(false);
                c.setConnectTimeout(connectTimeoutMs);
                c.setReadTimeout(readTimeoutMs);
                c.connect();
                int status = c.getResponseCode();
                if (status == HttpURLConnection.HTTP_OK) {
                    StringBuilder sb;
                    try ( // парсим GUUID из ответа
                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }

                    // получаем дата сет
                    try {
                        DataSet ds = new DataSet();
                        ds.UnPackData(sb.toString());
                        DataRecord row = ds.getRecords().firstElement();
                        String guuid = row.getString("Id");
                        return guuid;
                    } catch (Exception ex) {
                        throw new HttpInternalException(ex);
                    }

                } else { // некая внутренняя ошибка сервера
                    StringBuilder sb;
                    try (
                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getErrorStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }

                    // логируем
                    logger.error("add user error from remote server {} message {}", url, sb.toString());

                    // получаем датасет
                    DataSet ds = new DataSet();
                    try {
                        ds.UnPackData(sb.toString());
                    } catch (Exception ex) {
                        throw new HttpInternalException(ex);
                    }

                    throw new HttpException(status, ds);
                }

            } finally {
                c.disconnect();
            }
        } catch (MalformedURLException ex) {
            throw new HttpInternalException(ex);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        } catch (java.io.IOException ex) {
            throw new HttpInternalException(ex);
        }
    }

    /**
     * Отправить сообщение пользователю
     *
     * @param senderToken Токен отправителя
     * @param receiverGUUID GUUID получателя
     * @param smallMessage сообщение для SMS|PUSH
     * @param emailTitle заголовок для email
     * @param emailMessage тело письма сообщения email
     * @param endpointApiKey ключ апи для таргетирования брендов
     * @param notificationType тип уведомления email|sms|push (если null то отправка на все доступные)
     * @param verifiedOnly отправлять только на проверенные контакты
     * @param pushTitle заголовок PUSH сообщения
     * @param pushType тип PUSH уведомления, это внутренний код. Сейчас это (url|text)
     * @param pushData данные PUSH уведомления. Это url если тип url. или null если text
     * @param smsSign подпись в СМС (отправитель)
     * @throws HttpBadRequestException
     * @throws HttpNotFoundException
     * @throws HttpInternalException
     * @throws HttpUnauthorizedException
     */
    @Override
    public void notify(String senderToken, String receiverGUUID, String smallMessage, String emailTitle,
            String emailMessage, String endpointApiKey, String notificationType,
            Boolean verifiedOnly, String pushTitle, String pushType, String pushData, String smsSign) throws
            HttpException {
        checkConfigured();

        try {
            Integer vOnly = verifiedOnly ? 1 : 0;

            StringBuilder sbUrl = new StringBuilder();
            sbUrl.append(url).append("/Notify.jaxis?Version=")
                    .append(UrlHelper.Encode(AUTH_SERVICE_VERSION.get()));
            appendParam(sbUrl, "Token", senderToken);
            appendParam(sbUrl, "Guuid", receiverGUUID);
            appendParam(sbUrl, "ApiKey", apiKey);
            appendParam(sbUrl, "SmallMessage", smallMessage);
            appendParam(sbUrl, "EmailTitle", emailTitle);
            appendParam(sbUrl, "EmailMessage", emailMessage);
            appendParam(sbUrl, "EndpointApiKey", endpointApiKey);
            appendParam(sbUrl, "VerifiedOnly", vOnly.toString());
            appendParam(sbUrl, "PushTitle", pushTitle);
            appendParam(sbUrl, "PushType", pushType);
            appendParam(sbUrl, "PushData", pushData);
            appendParam(sbUrl, "NotificationType", notificationType);
            appendParam(sbUrl, "SmsSign", smsSign);

            URL u = new URL(sbUrl.toString());
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            try {
                c.setRequestMethod("GET");
                c.setRequestProperty("Content-length", "0");
                c.setUseCaches(false);
                c.setAllowUserInteraction(false);
                c.setConnectTimeout(connectTimeoutMs);
                c.setReadTimeout(readTimeoutMs);
                c.connect();
                int status = c.getResponseCode();
                if (status == HttpURLConnection.HTTP_OK) {
                } else if (status == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    throw new HttpUnauthorizedException("Пользователь не авторизован");
                } else {
                    StringBuilder sb;
                    try ( // некая внутренняя ошибка сервера
                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getErrorStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }

                    // логируем
                    logger.error("Send notification error from remote server {} message {}", url, sb.toString());

                    // получаем датасет
                    DataSet ds = new DataSet();
                    try {
                        ds.UnPackData(sb.toString());
                    } catch (Exception ex) {
                        throw new HttpInternalException(ex);
                    }

                    throw new HttpException(status, ds);
                }

            } finally {
                c.disconnect();
            }
        } catch (MalformedURLException ex) {
            throw new HttpInternalException(ex);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        } catch (java.io.IOException ex) {
            throw new HttpInternalException(ex);
        }
    }

    /**
     * Отправить сообщение пользователю на основные учетные данные, принудительно.
     *
     * Игнорирует настройки пользователя о получении сообщений. Используется для системных действий.
     *
     * @param senderToken Токен отправителя
     * @param Guuid GUUID получателя
     * @param smallMessage сообщение для SMS|PUSH
     * @param emailTitle заголовок для email
     * @param emailMessage тело письма сообщения email
     * @param smsSign СМС подпись (отправитель)
     * @throws HttpException
     */
    @Override
    public void notifyForce(String senderToken, String Guuid, String smallMessage, String emailTitle,
            String emailMessage, String smsSign) throws HttpException {
        checkConfigured();

        try {
            StringBuilder sbUrl = new StringBuilder();
            sbUrl.append(url).append("/NotifyForce.jaxis?Version=")
                    .append(UrlHelper.Encode(AUTH_SERVICE_VERSION.get()));
            appendParam(sbUrl, "Token", senderToken);
            appendParam(sbUrl, "Guuid", Guuid);
            appendParam(sbUrl, "ApiKey", apiKey);
            appendParam(sbUrl, "SmallMessage", smallMessage);
            appendParam(sbUrl, "EmailTitle", emailTitle);
            appendParam(sbUrl, "EmailMessage", emailMessage);
            appendParam(sbUrl, "SmsSign", smsSign);

            URL u = new URL(sbUrl.toString());
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            try {
                c.setRequestMethod("GET");
                c.setRequestProperty("Content-length", "0");
                c.setUseCaches(false);
                c.setAllowUserInteraction(false);
                c.setConnectTimeout(connectTimeoutMs);
                c.setReadTimeout(readTimeoutMs);
                c.connect();
                int status = c.getResponseCode();
                if (status == HttpURLConnection.HTTP_OK) {
                } else if (status == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    throw new HttpUnauthorizedException("Пользователь не авторизован");
                } else {
                    StringBuilder sb;
                    // некая внутренняя ошибка сервера
                    try (BufferedReader br = new BufferedReader(new InputStreamReader(c.getErrorStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }

                    // логируем
                    logger.error("Send forced notification error from remote server {} message {}", url, sb.toString());

                    // получаем датасет
                    DataSet ds = new DataSet();
                    try {
                        ds.UnPackData(sb.toString());
                    } catch (Exception ex) {
                        throw new HttpInternalException(ex);
                    }

                    throw new HttpException(status, ds);
                }

            } finally {
                c.disconnect();
            }
        } catch (MalformedURLException ex) {
            throw new HttpInternalException(ex);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        } catch (java.io.IOException ex) {
            throw new HttpInternalException(ex);
        }
    }

    /**
     * Запрос удаления пользователя в сервисе авторизации.
     *
     * Возвращает код для подтверждения.
     *
     * @param token
     * @param guuid
     * @return
     * @throws HttpException
     */
    @Override
    public String userDeleteRequest(String token, String guuid) throws HttpException {
        checkConfigured();

        try {
            StringBuilder sbUrl = new StringBuilder();
            sbUrl.append(url).append("/UserDeleteRequest.jaxis?Version=")
                    .append(UrlHelper.Encode(AUTH_SERVICE_VERSION.get()));
            appendParam(sbUrl, "Token", token);
            appendParam(sbUrl, "Guuid", guuid);
            appendParam(sbUrl, "ApiKey", apiKey);

            URL u = new URL(sbUrl.toString());
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            try {
                c.setRequestMethod("GET");
                c.setRequestProperty("Content-length", "0");
                c.setUseCaches(false);
                c.setAllowUserInteraction(false);
                c.setConnectTimeout(connectTimeoutMs);
                c.setReadTimeout(readTimeoutMs);
                c.connect();
                int status = c.getResponseCode();
                if (status == HttpURLConnection.HTTP_OK) {
                    StringBuilder sb;
                    try (BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }

                    // получаем дата сет
                    try {
                        DataSet ds = new DataSet();
                        ds.UnPackData(sb.toString());
                        DataRecord row = ds.getRecords().firstElement();
                        String code = row.getString("Code");
                        return code;
                    } catch (Exception ex) {
                        throw new HttpInternalException(ex);
                    }
                } else if (status == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    throw new HttpUnauthorizedException("Пользователь не авторизован");
                } else {
                    StringBuilder sb;
                    // некая внутренняя ошибка сервера
                    try (BufferedReader br = new BufferedReader(new InputStreamReader(c.getErrorStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }

                    // логируем
                    logger.error("UserDeleteRequest error from remote server {} message {}", url, sb.toString());

                    // получаем датасет
                    DataSet ds = new DataSet();
                    try {
                        ds.UnPackData(sb.toString());
                    } catch (Exception ex) {
                        throw new HttpInternalException(ex);
                    }

                    throw new HttpException(status, ds);
                }

            } finally {
                c.disconnect();
            }
        } catch (IOException ex) {
            throw new HttpInternalException(ex);
        }
    }

    /**
     * Подтверждение удаления пользователя в сервисе авторизации.
     *
     * @param token
     * @param guuid
     * @param code проверочный код
     * @throws HttpException
     */
    @Override
    public void userDeleteConfirm(String token, String guuid, String code) throws HttpException {
        checkConfigured();

        try {
            StringBuilder sbUrl = new StringBuilder();
            sbUrl.append(url).append("/UserDeleteConfirm.jaxis?Version=")
                    .append(UrlHelper.Encode(AUTH_SERVICE_VERSION.get()));
            appendParam(sbUrl, "Token", token);
            appendParam(sbUrl, "Guuid", guuid);
            appendParam(sbUrl, "ApiKey", apiKey);
            appendParam(sbUrl, "Code", code);

            URL u = new URL(sbUrl.toString());
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            try {
                c.setRequestMethod("GET");
                c.setRequestProperty("Content-length", "0");
                c.setUseCaches(false);
                c.setAllowUserInteraction(false);
                c.setConnectTimeout(connectTimeoutMs);
                c.setReadTimeout(readTimeoutMs);
                c.connect();
                int status = c.getResponseCode();
                if (status == HttpURLConnection.HTTP_OK) {
                    return;
                } else if (status == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    throw new HttpUnauthorizedException("Пользователь не авторизован");
                } else {
                    StringBuilder sb;
                    // некая внутренняя ошибка сервера
                    try (BufferedReader br = new BufferedReader(new InputStreamReader(c.getErrorStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }

                    // логируем
                    logger.error("UserDeleteConfirm error from remote server {} message {}", url, sb.toString());

                    // получаем датасет
                    DataSet ds = new DataSet();
                    try {
                        ds.UnPackData(sb.toString());
                    } catch (Exception ex) {
                        throw new HttpInternalException(ex);
                    }

                    throw new HttpException(status, ds);
                }

            } finally {
                c.disconnect();
            }
        } catch (IOException ex) {
            throw new HttpInternalException(ex);
        }
    }

    /**
     * Возвращает список пользователей GUUID, ФИО, картинка. Необходимо быть IsSystem пользователем.
     *
     * @param token токен запрашивающего пользователя
     * @param guuid гуид для ограничения выборки (если не null - возвратятся данные только этого пользователя)
     * @param lastDate вернуть только данные, которые были изменены после lastDate (для обновления кэша). Если lastDate = null, то параметр
     * игнорируется
     * @return
     * @throws HttpInternalException
     * @throws HttpUnauthorizedException
     * @throws HttpNotFoundException
     */
    @Override
    public ArrayList<User> userListGet(String token, String guuid, Timestamp lastDate) throws HttpInternalException,
            HttpUnauthorizedException, HttpNotFoundException, HttpException {
        final String method = "/UserListGet.jaxis";

        // формируем параметры запроса
        final HashMap<String, String> params = new HashMap<>();
        params.put("Version", AUTH_SERVICE_VERSION.get());
        params.put("Token", token);
        params.put("ApiKey", apiKey);
        if (guuid != null) {
            params.put("Guuid", guuid);
        }
        if (lastDate != null) {
            params.put("Lastdate", String.format("%1$tY%1$tm%1$td%1$tH%1$tM%1$tS.%1$tL", lastDate));
        }
        params.put(ParamHelper.PARAM_USER_WITH_LAST_ACTION, "1");
        params.put(ParamHelper.PARAM_USER_WITH_ENDPOINTS, "1");
        params.put(ParamHelper.PARAM_USER_WITH_ROLES, "1");

        HttpEntity entity;
        try {
            entity = buildEntity(params, null);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        }
        HttpPost request = new HttpPost(url + method);
        request.setEntity(entity);

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpResponse response = httpClient.execute(request);
            HttpEntity respEntity = response.getEntity();
            String responseString = EntityUtils.toString(respEntity, "UTF-8");

            if (response.getStatusLine().getStatusCode() != HttpURLConnection.HTTP_OK) {
                try {
                    DataSet ds = new DataSet();
                    ds.UnPackData(responseString);
                    throw new HttpException(response.getStatusLine().getStatusCode(), ds);
                } catch (Exception ex) {
                    throw new HttpException(response.getStatusLine().getStatusCode(), responseString);
                }
            }

            // парсим ответ
            try {
                DataSet ds = new DataSet();
                ds.UnPackData(responseString);

                ArrayList<User> users = new ArrayList();
                for (DataRecord row : ds.getRecords()) {
                    User u = new User(null, row.getString("Guuid"));
                    u.setFirstName(row.getString("FirstName"));
                    u.setLastName(row.getString("LastName"));
                    u.setMiddleName(row.getString("MiddleName"));
                    u.setImage(row.getString("Image"));
                    u.setLastdate(row.getTimestamp("Lastdate"));
                    u.setRegistered(row.getInteger("IsRegistered") == 1);
                    u.setDisabled(row.getInteger("IsDisabled") == 1);
                    u.setLastActionDate(row.getTimestamp("LastActionDate"));

                    DataSet phones = row.getDataSet("Phones");
                    ArrayList<String> arr = new ArrayList();
                    if (phones != null) {
                        for (DataRecord r : phones.getRecords()) {
                            arr.add(r.getString(0));
                        }
                    }
                    u.setPhones(arr);

                    DataSet emails = row.getDataSet("Emails");
                    ArrayList<String> arrEmails = new ArrayList();
                    if (emails != null) {
                        for (DataRecord r : emails.getRecords()) {
                            arrEmails.add(r.getString(0));
                        }
                    }
                    u.setEmails(arrEmails);

                    DataSet roles = row.getDataSet("Roles");
                    List<Role> arrRoles = new ArrayList();
                    if (roles != null) {
                        for (DataRecord r : roles.getRecords()) {
                            Role rr = new Role();
                            rr.setCode(r.getString(0));
                            arrRoles.add(rr);
                        }
                    }
                    u.setRoles(arrRoles);

                    users.add(u);
                }
                return users;

            } catch (Exception ex) {
                logger.error("", ex);
                throw new HttpInternalException(ex);
            }
        } catch (IOException ex) {
            logger.error("", ex);
            throw new HttpInternalException(ex);
        }
    }

    /**
     * Редактирование собственных данных пользователя
     *
     * @param token токен авторизации
     * @param firstName имя
     * @param lastName фамилия
     * @param middleName отчетсво
     * @param image картинка
     * @param guuid
     * @throws HttpInternalException
     * @throws HttpUnauthorizedException
     * @throws HttpNotFoundException
     */
    @Override
    public void userEdit(String token, String firstName, String lastName, String middleName, String image, String guuid, Timestamp birthday)
            throws HttpInternalException,
            HttpUnauthorizedException, HttpNotFoundException, HttpException {
        final String method = "/UserEdit.json";

        // формируем параметры запроса
        final HashMap<String, String> params = new HashMap<>();
        params.put("Version", AUTH_SERVICE_VERSION.get());
        params.put("Token", token);
        params.put("ApiKey", apiKey);
        params.put("FirstName", firstName);
        params.put("LastName", lastName);
        params.put("MiddleName", middleName);
        params.put("Image", image);
        params.put("Guuid", guuid);

        if (birthday != null) {
            params.put("Birthday", String.format("%1$tY%1$tm%1$td%1$tH%1$tM%1$tS.%1$tL", birthday));
        }

        HttpEntity entity;
        try {
            entity = buildEntity(params, null);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        }
        HttpPost request = new HttpPost(url + method);
        request.setEntity(entity);

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpResponse response = httpClient.execute(request);
            HttpEntity respEntity = response.getEntity();
            String responseString = EntityUtils.toString(respEntity, "UTF-8");

            if (response.getStatusLine().getStatusCode() != HttpURLConnection.HTTP_OK) {
                try {
                    logger.error("DATASET {} DATASET", responseString);
                    throw new HttpException(response.getStatusLine().getStatusCode(), DataSet.fromJson(responseString));
                } catch (DataSetException ex) {
                    throw new HttpException(response.getStatusLine().getStatusCode(), "MESSAGE: " + responseString);
                }
            }
        } catch (IOException ex) {
            logger.error("", ex);
        }
    }

    /**
     * Удаление пользователя в базе авторизаци по Guuid. Нужно быть системным пользователем для возможности выполнения этого действия.
     *
     * @param token
     * @param guuid
     * @throws NotConfiguredException
     * @throws HttpException
     */
    @Override
    public void deleteUser(String token, String guuid) throws NotConfiguredException, HttpException {
        checkConfigured();

        try {
            StringBuilder sbUrl = new StringBuilder();
            sbUrl
                    .append(url)
                    .append("/UserDelete.jaxis?Version=1")
                    .append("&Token=").append(UrlHelper.Encode(token))
                    .append("&ApiKey=").append(UrlHelper.Encode(apiKey))
                    .append("&Guuid=").append(UrlHelper.Encode(guuid));
            URL u = new URL(sbUrl.toString());
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            try {
                c.setRequestMethod("GET");
                c.setRequestProperty("Content-length", "0");
                c.setUseCaches(false);
                c.setAllowUserInteraction(false);
                c.setConnectTimeout(connectTimeoutMs);
                c.setReadTimeout(readTimeoutMs);
                c.connect();
                int status = c.getResponseCode();
                if (status == HttpURLConnection.HTTP_OK) {
                    StringBuilder sb;
                    try ( // парсим GUUID из ответа
                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }
                } else { // некая внутренняя ошибка сервера
                    StringBuilder sb;
                    try (
                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getErrorStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }

                    // логируем
                    logger.error("add user error from remote server {} message {}", url, sb.toString());

                    // получаем датасет
                    DataSet ds = new DataSet();
                    try {
                        ds.UnPackData(sb.toString());
                    } catch (Exception ex) {
                        throw new HttpInternalException(ex);
                    }

                    throw new HttpException(status, ds);
                }

            } finally {
                c.disconnect();
            }
        } catch (MalformedURLException ex) {
            throw new HttpInternalException(ex);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        } catch (java.io.IOException ex) {
            throw new HttpInternalException(ex);
        }
    }

    /**
     * Регистрация/обновление регистрационных данных мобильного устройства в сервисе отправки сообщений
     *
     * @param token токен авторизации
     * @param deviceId уникальный id устройства
     * @param PNSToken token полученный от PNS
     * @param PNSCode тип PNS (Platform Notification Service)
     * @param isTablet является ли устройство планшетом, 1 - является
     * @param locale локаль устройства
     * @param model модель устройства
     * @param name настраиваемое имя устройства
     * @param endpointApiKey ключ апи приложения
     * @throws HttpInternalException
     * @throws HttpUnauthorizedException
     * @throws HttpNotFoundException
     * @throws HttpException
     */
    @Override
    public void pushRegistrationEdit(String token, String deviceId, String PNSToken, String PNSCode, String isTablet,
            String locale, String model, String name, String endpointApiKey)
            throws HttpInternalException,
            HttpUnauthorizedException, HttpNotFoundException, HttpException {
        final String method = "/PushRegistrationEdit.json";

        // формируем параметры запроса
        final HashMap<String, String> params = new HashMap<>();
        params.put("Version", AUTH_SERVICE_VERSION.get());
        params.put("Token", token);
        params.put("ApiKey", apiKey);
        params.put("DeviceId", deviceId);
        params.put("PNSToken", PNSToken);
        params.put("PNSCode", PNSCode);
        params.put("IsTablet", isTablet);
        params.put("Locale", locale);
        params.put("Model", model);
        params.put("Name", name);
        params.put("EndpointApiKey", endpointApiKey);

        HttpEntity entity;
        try {
            entity = buildEntity(params, null);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        }
        HttpPost request = new HttpPost(url + method);
        request.setEntity(entity);

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpResponse response = httpClient.execute(request);
            HttpEntity respEntity = response.getEntity();
            String responseString = EntityUtils.toString(respEntity, "UTF-8");

            if (response.getStatusLine().getStatusCode() != HttpURLConnection.HTTP_OK) {
                try {
                    logger.error("DATASET {} DATASET", responseString);
                    throw new HttpException(response.getStatusLine().getStatusCode(), DataSet.fromJson(responseString));
                } catch (DataSetException ex) {
                    throw new HttpException(response.getStatusLine().getStatusCode(), "MESSAGE: " + responseString);
                }
            }
        } catch (IOException ex) {
            logger.error("", ex);
        }
    }

    /**
     * Создает пользовательский запрос (сменить пароль, забыл пароль, активация и т.д.), возвращает код. Ограничение: необходимо быть системным
     * пользователем.
     *
     * @param token токен пользователя, выполняющего действие (должен быть системным)
     * @param login логин изменяемого пользователя
     * @param type тип пользовательского запроса
     * @param expirationTimeSec время жизни запроса (секунд)
     * @return код созданного запроса
     * @throws HttpInternalException
     */
    @Override
    public String userRequestAdd(String token, String login, UserRequestType type, int expirationTimeSec) throws
            HttpInternalException,
            HttpException {
        final String method = "/UserRequestAdd.json";

        // формируем параметры запроса
        final HashMap<String, String> params = new HashMap<>();
        params.put(UserRequestAdd.PARAM_VERSION, AUTH_SERVICE_VERSION.get());
        params.put(UserRequestAdd.PARAM_TOKEN, token);
        params.put(UserRequestAdd.PARAM_APIKEY, apiKey);
        params.put(UserRequestAdd.PARAM_LOGIN, login);
        params.put(UserRequestAdd.PARAM_TYPE, type.getCode());
        params.put(UserRequestAdd.PARAM_EXPIRATION_TIME_SEC, String.valueOf(expirationTimeSec));

        HttpEntity entity;
        try {
            entity = buildEntity(params, null);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        }
        HttpPost request = new HttpPost(url + method);
        request.setEntity(entity);

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpResponse response = httpClient.execute(request);
            HttpEntity respEntity = response.getEntity();
            String responseString = EntityUtils.toString(respEntity, "UTF-8");

            if (response.getStatusLine().getStatusCode() != HttpURLConnection.HTTP_OK) {
                try {
                    DataSet ds = DataSet.fromJson(responseString);
                    throw new HttpException(response.getStatusLine().getStatusCode(), ds);
                } catch (Exception ex) {
                    throw new HttpException(response.getStatusLine().getStatusCode(), responseString);
                }
            }

            // парсим ответ
            try {
                DataSet ds = DataSet.fromJson(responseString);
                return ds.getRecords().firstElement().getString(UserRequestAdd.FIELD_CODE);
            } catch (Exception ex) {
                logger.error("", ex);
                throw new HttpInternalException(ex);
            }
        } catch (IOException ex) {
            logger.error("", ex);
            throw new HttpInternalException(ex);
        }
    }

    /**
     * Возвращает список контактов пользователя. Необходимо быть IsSystem пользователем для получения чужих контактов.
     *
     * @param token токен запрашивающего пользователя
     * @param guuid гуид для ограничения выборки (если null - возвратятся данные вызывающего пользователя)
     * @return
     * @throws net.stemteam.jaxis.err.StemException
     */
    @Override
    public List<Endpoint> getEndpointList(String token, String guuid) throws StemException {
        final String method = "/EndpointListGet.jaxis";

        // формируем параметры запроса
        final HashMap<String, String> params = new HashMap<>();
        params.put("Version", AUTH_SERVICE_VERSION.get());
        params.put("Token", token);
        params.put("ApiKey", apiKey);
        if (guuid != null) {
            params.put("Guuid", guuid);
        }

        HttpEntity entity;
        try {
            entity = buildEntity(params, null);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        }
        HttpPost request = new HttpPost(url + method);
        request.setEntity(entity);

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpResponse response = httpClient.execute(request);
            HttpEntity respEntity = response.getEntity();
            String responseString = EntityUtils.toString(respEntity, "UTF-8");

            if (response.getStatusLine().getStatusCode() != HttpURLConnection.HTTP_OK) {
                try {
                    DataSet ds = new DataSet();
                    ds.UnPackData(responseString);
                    throw new HttpException(response.getStatusLine().getStatusCode(), ds);
                } catch (Exception ex) {
                    throw new HttpException(response.getStatusLine().getStatusCode(), responseString);
                }
            }

            // парсим ответ
            try {
                DataSet ds = new DataSet();
                ds.UnPackData(responseString);

                List<Endpoint> epList = new ArrayList<>();
                for (DataRecord rec : ds.getRecords()) {

                    NotificationTypeCode nType = NotificationTypeCode.valueOf(rec.getString("NotificationCode"));

                    Endpoint.PushNotificationAttributes pna = null;

                    if (nType == NotificationTypeCode.push) {
                        pna = new Endpoint.PushNotificationAttributes(
                                rec.getString("AzureId"),
                                rec.getString("DeviceId"),
                                PNSType.getByCode(rec.getString("PnsType")),
                                rec.getBoolean("IsTabled"),
                                rec.getString("Locale"),
                                rec.getString("Name"),
                                rec.getString("Model"),
                                rec.getString("Apikey"));
                    }

                    Endpoint ep = new Endpoint(
                            rec.getInteger("EndpointId"),
                            nType,
                            rec.getString("NotificationId"),
                            pna,
                            rec.getBoolean("IsEnabled"),
                            rec.getBoolean("IsVerified"),
                            rec.getBoolean("IsDefault"));

                    epList.add(ep);
                }
                return epList;

            } catch (Exception ex) {
                logger.error("", ex);
                throw new HttpInternalException(ex);
            }
        } catch (IOException ex) {
            logger.error("", ex);
            throw new HttpInternalException(ex);
        }
    }

    @Override
    public void setUserDisabled(String token, String guuid, boolean isDisabled) throws StemException {
        checkConfigured();

        try {
            StringBuilder sbUrl = new StringBuilder();
            sbUrl
                    .append(url)
                    .append("/UserDisabledSet.jaxis?Version=1")
                    .append("&Token=").append(UrlHelper.Encode(token))
                    .append("&ApiKey=").append(UrlHelper.Encode(apiKey))
                    .append("&Guuid=").append(UrlHelper.Encode(guuid))
                    .append("&IsDisabled=").append(UrlHelper.Encode(isDisabled ? 1 : 0));
            URL u = new URL(sbUrl.toString());
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            try {
                c.setRequestMethod("GET");
                c.setRequestProperty("Content-length", "0");
                c.setUseCaches(false);
                c.setAllowUserInteraction(false);
                c.setConnectTimeout(connectTimeoutMs);
                c.setReadTimeout(readTimeoutMs);
                c.connect();
                int status = c.getResponseCode();
                if (status == HttpURLConnection.HTTP_OK) {
                    StringBuilder sb;
                    try ( // парсим GUUID из ответа
                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }
                } else { // некая внутренняя ошибка сервера
                    StringBuilder sb;
                    try (
                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getErrorStream(), "UTF-8")) // UTF-8 обязательно
                            ) {
                        sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                    }

                    // логируем
                    logger.error("UserDisabledSet error from remote server {} message {}", url, sb.toString());

                    // получаем датасет
                    DataSet ds = new DataSet();
                    try {
                        ds.UnPackData(sb.toString());
                    } catch (Exception ex) {
                        throw new HttpInternalException(ex);
                    }

                    throw new HttpException(status, ds);
                }

            } finally {
                c.disconnect();
            }
        } catch (MalformedURLException ex) {
            throw new HttpInternalException(ex);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        } catch (java.io.IOException ex) {
            throw new HttpInternalException(ex);
        }
    }

    /**
     * Получить список доступных ролей
     *
     * @param callerToken вызывающий пользователь
     * @return
     * @throws net.stemteam.jaxis.err.StemException
     * @throws net.stemteam.authservice.exception.UserNotAuthorizedException
     * @throws net.stemteam.authservice.exception.PermissionDenyException
     */
    @Override
    public List<Role> getRoleList(String callerToken) throws StemException,
            UserNotAuthorizedException, PermissionDenyException {

        final String method = "/RoleListGet.json";

        // формируем параметры запроса
        final HashMap<String, String> params = new HashMap<>();
        params.put(RoleListGet.PARAM_VERSION, AUTH_SERVICE_VERSION.get());
        params.put(RoleListGet.PARAM_TOKEN, callerToken);
        params.put(RoleListGet.PARAM_API_KEY, apiKey);

        HttpEntity entity;
        try {
            entity = buildEntity(params, null);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        }
        HttpPost request = new HttpPost(url + method);
        request.setEntity(entity);

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpResponse response = httpClient.execute(request);
            HttpEntity respEntity = response.getEntity();
            String responseString = EntityUtils.toString(respEntity, "UTF-8");

            if (response.getStatusLine().getStatusCode() != HttpURLConnection.HTTP_OK) {
                try {
                    DataSet ds = DataSet.fromJson(responseString);
                    throw new HttpException(response.getStatusLine().getStatusCode(), ds);
                } catch (Exception ex) {
                    throw new HttpException(response.getStatusLine().getStatusCode(), responseString);
                }
            }

            // парсим ответ
            try {
                DataSet ds = DataSet.fromJson(responseString);

                DataSetProvider dsp = new DataSetProvider();
                List<Role> roles = dsp.getRolesFromDataSet(ds);
                return roles;
            } catch (Exception ex) {
                logger.error("", ex);
                throw new HttpInternalException(ex);
            }
        } catch (IOException ex) {
            logger.error("", ex);
            throw new HttpInternalException(ex);
        }
    }

    /**
     * Получить список ролей пользователя
     *
     * @param callerToken вызывающий пользователь
     * @param guuid идентификатор пользователя
     * @return
     * @throws net.stemteam.jaxis.err.StemException
     * @throws net.stemteam.authservice.exception.UserNotAuthorizedException
     * @throws net.stemteam.authservice.exception.PermissionDenyException
     */
    @Override
    public List<Role> getUserRoleList(String callerToken, String guuid) throws StemException, UserNotAuthorizedException,
            PermissionDenyException {
        final String method = "/UserRoleListGet.json";

        // формируем параметры запроса
        final HashMap<String, String> params = new HashMap<>();
        params.put(UserRoleListGet.PARAM_VERSION, AUTH_SERVICE_VERSION.get());
        params.put(UserRoleListGet.PARAM_TOKEN, callerToken);
        params.put(UserRoleListGet.PARAM_API_KEY, apiKey);
        params.put(UserRoleListGet.PARAM_GUUID, guuid);

        HttpEntity entity;
        try {
            entity = buildEntity(params, null);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        }
        HttpPost request = new HttpPost(url + method);
        request.setEntity(entity);

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpResponse response = httpClient.execute(request);
            HttpEntity respEntity = response.getEntity();
            String responseString = EntityUtils.toString(respEntity, "UTF-8");

            if (response.getStatusLine().getStatusCode() != HttpURLConnection.HTTP_OK) {
                try {
                    DataSet ds = DataSet.fromJson(responseString);
                    throw new HttpException(response.getStatusLine().getStatusCode(), ds);
                } catch (Exception ex) {
                    throw new HttpException(response.getStatusLine().getStatusCode(), responseString);
                }
            }

            // парсим ответ
            try {
                DataSet ds = DataSet.fromJson(responseString);

                DataSetProvider dsp = new DataSetProvider();
                List<Role> roles = dsp.getRolesFromDataSet(ds);
                return roles;
            } catch (Exception ex) {
                logger.error("", ex);
                throw new HttpInternalException(ex);
            }
        } catch (IOException ex) {
            logger.error("", ex);
            throw new HttpInternalException(ex);
        }
    }

    /**
     * Добавить пользователю роль с заданным идентификатором
     *
     * @param callerToken вызывающий пользователь
     * @param guuid идентификатор пользователя
     * @param roleId идентификатор добавляемой роли
     * @throws net.stemteam.authservice.exception.PermissionDenyException
     * @throws net.stemteam.authservice.exception.UserNotAuthorizedException
     */
    @Override
    public void addUserRole(String callerToken, String guuid, Integer roleId) throws StemException, UserNotAuthorizedException,
            PermissionDenyException {
        final String method = "/UserRoleAdd.json";

        // формируем параметры запроса
        final HashMap<String, String> params = new HashMap<>();
        params.put(UserRoleAdd.PARAM_VERSION, AUTH_SERVICE_VERSION.get());
        params.put(UserRoleAdd.PARAM_TOKEN, callerToken);
        params.put(UserRoleAdd.PARAM_API_KEY, apiKey);
        params.put(UserRoleAdd.PARAM_GUUID, guuid);
        params.put(UserRoleAdd.PARAM_ROLE_ID, roleId.toString());

        HttpEntity entity;
        try {
            entity = buildEntity(params, null);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        }
        HttpPost request = new HttpPost(url + method);
        request.setEntity(entity);

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpResponse response = httpClient.execute(request);
            HttpEntity respEntity = response.getEntity();
            String responseString = EntityUtils.toString(respEntity, "UTF-8");

            if (response.getStatusLine().getStatusCode() != HttpURLConnection.HTTP_OK) {
                try {
                    DataSet ds = DataSet.fromJson(responseString);
                    throw new HttpException(response.getStatusLine().getStatusCode(), ds);
                } catch (Exception ex) {
                    throw new HttpException(response.getStatusLine().getStatusCode(), responseString);
                }
            }
        } catch (IOException ex) {
            logger.error("", ex);
            throw new HttpInternalException(ex);
        }
    }

    /**
     * Забрать у пользователя роль с заданным идентификатором
     *
     * @param callerToken вызывающий пользователь
     * @param guuid идентификатор пользователя
     * @param roleId идентификатор забираемой роли
     * @throws net.stemteam.jaxis.err.StemException
     * @throws net.stemteam.authservice.exception.UserNotAuthorizedException
     * @throws net.stemteam.authservice.exception.PermissionDenyException
     */
    @Override
    public void deleteUserRole(String callerToken, String guuid, Integer roleId) throws StemException,
            UserNotAuthorizedException,
            PermissionDenyException {
        final String method = "/UserRoleDelete.json";

        // формируем параметры запроса
        final HashMap<String, String> params = new HashMap<>();
        params.put(UserRoleDelete.PARAM_VERSION, AUTH_SERVICE_VERSION.get());
        params.put(UserRoleDelete.PARAM_TOKEN, callerToken);
        params.put(UserRoleDelete.PARAM_API_KEY, apiKey);
        params.put(UserRoleDelete.PARAM_GUUID, guuid);
        params.put(UserRoleDelete.PARAM_ROLE_ID, roleId.toString());

        HttpEntity entity;
        try {
            entity = buildEntity(params, null);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        }
        HttpPost request = new HttpPost(url + method);
        request.setEntity(entity);

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpResponse response = httpClient.execute(request);
            HttpEntity respEntity = response.getEntity();
            String responseString = EntityUtils.toString(respEntity, "UTF-8");

            if (response.getStatusLine().getStatusCode() != HttpURLConnection.HTTP_OK) {
                try {
                    DataSet ds = DataSet.fromJson(responseString);
                    throw new HttpException(response.getStatusLine().getStatusCode(), ds);
                } catch (Exception ex) {
                    throw new HttpException(response.getStatusLine().getStatusCode(), responseString);
                }
            }
        } catch (IOException ex) {
            logger.error("", ex);
            throw new HttpInternalException(ex);
        }
    }

    /**
     * Удалить пользователя из системы прав
     *
     * @param callerToken вызывающий пользователь
     * @param guuid идентификатор пользователя
     * @throws StemException
     * @throws net.stemteam.authservice.exception.UserNotAuthorizedException
     * @throws net.stemteam.authservice.exception.PermissionDenyException
     */
    @Override
    public void deleteRightUser(String callerToken, String guuid) throws StemException, UserNotAuthorizedException,
            PermissionDenyException {
        final String method = "/RightUserDelete.json";

        // формируем параметры запроса
        final HashMap<String, String> params = new HashMap<>();
        params.put(RightUserDelete.PARAM_VERSION, AUTH_SERVICE_VERSION.get());
        params.put(RightUserDelete.PARAM_TOKEN, callerToken);
        params.put(RightUserDelete.PARAM_API_KEY, apiKey);
        params.put(RightUserDelete.PARAM_GUUID, guuid);

        HttpEntity entity;
        try {
            entity = buildEntity(params, null);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        }
        HttpPost request = new HttpPost(url + method);
        request.setEntity(entity);

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpResponse response = httpClient.execute(request);
            HttpEntity respEntity = response.getEntity();
            String responseString = EntityUtils.toString(respEntity, "UTF-8");

            if (response.getStatusLine().getStatusCode() != HttpURLConnection.HTTP_OK) {
                try {
                    DataSet ds = DataSet.fromJson(responseString);
                    throw new HttpException(response.getStatusLine().getStatusCode(), ds);
                } catch (Exception ex) {
                    throw new HttpException(response.getStatusLine().getStatusCode(), responseString);
                }
            }
        } catch (IOException ex) {
            logger.error("", ex);
            throw new HttpInternalException(ex);
        }
    }

    @Override
    public User getUser(String token, UserRequestPropogation... requestPropogations) throws StemException {
        return getUser(token, null, null, requestPropogations);
    }

    @Override
    public User getUserByGuuid(String callerToken, String guuid, UserRequestPropogation... requestPropogations) throws
            StemException {
        return getUser(callerToken, guuid, null, requestPropogations);
    }

    @Override
    public User getUserByLogin(String callerToken, String login, UserRequestPropogation... requestPropogations) throws
            StemException {
        return getUser(callerToken, null, login, requestPropogations);
    }

    private User getUser(String token, String guuid, String login, UserRequestPropogation... requestPropogations) throws
            HttpInternalException, HttpException {
        checkConfigured();

        final String method = "/UserGet.json";

        // формируем параметры запроса
        final HashMap<String, String> params = new HashMap<>();
        params.put(ParamHelper.PARAM_VERSION, AUTH_SERVICE_VERSION.get());
        params.put(ParamHelper.PARAM_APIKEY, apiKey);
        params.put(ParamHelper.PARAM_TOKEN, token);
        if (guuid != null) {
            params.put(ParamHelper.PARAM_GUUID, guuid);
        }
        if (login != null) {
            params.put(ParamHelper.PARAM_LOGIN, login);
        }
        if (requestPropogations != null) {
            for (UserRequestPropogation prop : requestPropogations) {
                switch (prop) {
                    case LastAction:
                        params.put(ParamHelper.PARAM_USER_WITH_LAST_ACTION, "1");
                        break;
                    case Endpoints:
                        params.put(ParamHelper.PARAM_USER_WITH_ENDPOINTS, "1");
                        break;
                    case ReferedUsers:
                        params.put(ParamHelper.PARAM_USER_WITH_REFERED_USERS, "1");
                        break;
                    case Roles:
                        params.put(ParamHelper.PARAM_USER_WITH_ROLES, "1");
                        break;
                    case Requests:
                        params.put(ParamHelper.PARAM_USER_WITH_REQUESTS, "1");
                        break;
                    default:
                        throw new IllegalArgumentException("Unsupported UserRequestPropogation: " + prop);
                }
            }
        }

        HttpEntity entity;
        try {
            entity = buildEntity(params, null);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        }
        HttpPost request = new HttpPost(url + method);
        request.setEntity(entity);

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpResponse response = httpClient.execute(request);
            HttpEntity respEntity = response.getEntity();
            String responseString = EntityUtils.toString(respEntity, "UTF-8");

            if (response.getStatusLine().getStatusCode() != HttpURLConnection.HTTP_OK) {
                if (response.getStatusLine().getStatusCode() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    throw new HttpUnauthorizedException("Пользователь не авторизован");
                }
                try {
                    DataSet ds = DataSet.fromJson(responseString);
                    throw new HttpException(response.getStatusLine().getStatusCode(), ds);
                } catch (DataSetException | HttpException ex) {
                    throw new HttpException(response.getStatusLine().getStatusCode(), responseString);
                }
            }

            // парсим ответ
            try {
                DataSet ds = DataSet.fromJson(responseString);
                DataRecord row = ds.getRecords().firstElement();
                DataSetProvider prov = new DataSetProvider();
                User u = prov.getUserFromDataRecord(row);
                return u;
            } catch (Exception ex) {
                logger.error("", ex);
                throw new HttpInternalException(ex);
            }
        } catch (IOException ex) {
            logger.error("", ex);
            throw new HttpInternalException(ex);
        }
    }

    @Override
    public void addUserRole(String callerToken, String guuid, String roleCode) throws HttpInternalException, HttpException {
        final String method = "/UserRoleAdd.json";

        // формируем параметры запроса
        final HashMap<String, String> params = new HashMap<>();
        params.put(UserRoleAdd.PARAM_VERSION, AUTH_SERVICE_VERSION.get());
        params.put(UserRoleAdd.PARAM_TOKEN, callerToken);
        params.put(UserRoleAdd.PARAM_API_KEY, apiKey);
        params.put(UserRoleAdd.PARAM_GUUID, guuid);
        params.put(UserRoleAdd.PARAM_ROLE_CODE, roleCode);

        HttpEntity entity;
        try {
            entity = buildEntity(params, null);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        }
        HttpPost request = new HttpPost(url + method);
        request.setEntity(entity);

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpResponse response = httpClient.execute(request);
            HttpEntity respEntity = response.getEntity();
            String responseString = EntityUtils.toString(respEntity, "UTF-8");

            if (response.getStatusLine().getStatusCode() != HttpURLConnection.HTTP_OK) {
                try {
                    DataSet ds = DataSet.fromJson(responseString);
                    throw new HttpException(response.getStatusLine().getStatusCode(), ds);
                } catch (Exception ex) {
                    throw new HttpException(response.getStatusLine().getStatusCode(), responseString);
                }
            }
        } catch (IOException ex) {
            logger.error("", ex);
            throw new HttpInternalException(ex);
        }
    }

    @Override
    public void deleteUserRole(String callerToken, String guuid, String roleCode) throws StemException,
            UserNotAuthorizedException,
            PermissionDenyException {
        final String method = "/UserRoleDelete.json";

        // формируем параметры запроса
        final HashMap<String, String> params = new HashMap<>();
        params.put(UserRoleDelete.PARAM_VERSION, AUTH_SERVICE_VERSION.get());
        params.put(UserRoleDelete.PARAM_TOKEN, callerToken);
        params.put(UserRoleDelete.PARAM_API_KEY, apiKey);
        params.put(UserRoleDelete.PARAM_GUUID, guuid);
        params.put(UserRoleDelete.PARAM_ROLE_CODE, roleCode);

        HttpEntity entity;
        try {
            entity = buildEntity(params, null);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        }
        HttpPost request = new HttpPost(url + method);
        request.setEntity(entity);

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpResponse response = httpClient.execute(request);
            HttpEntity respEntity = response.getEntity();
            String responseString = EntityUtils.toString(respEntity, "UTF-8");

            if (response.getStatusLine().getStatusCode() != HttpURLConnection.HTTP_OK) {
                try {
                    DataSet ds = DataSet.fromJson(responseString);
                    throw new HttpException(response.getStatusLine().getStatusCode(), ds);
                } catch (Exception ex) {
                    throw new HttpException(response.getStatusLine().getStatusCode(), responseString);
                }
            }
        } catch (IOException ex) {
            logger.error("", ex);
            throw new HttpInternalException(ex);
        }
    }

    @Override
    public List<User> getUsers(String token, Timestamp lastDate, Long limit, Long offset, UserRequestPropogation... requestPropogations) throws
            StemException {
        final String method = "/UserListGet.json";

        // формируем параметры запроса
        final HashMap<String, String> params = new HashMap<>();
        params.put("Version", AUTH_SERVICE_VERSION.get());
        params.put("Token", token);
        params.put("ApiKey", apiKey);
        if (lastDate != null) {
            params.put("Lastdate", String.format("%1$tY%1$tm%1$td%1$tH%1$tM%1$tS.%1$tL", lastDate));
        }
        if (limit != null) {
            params.put("Limit", limit.toString());
        }
        if (offset != null) {
            params.put("Offset", offset.toString());
        }

        if (requestPropogations != null) {
            for (UserRequestPropogation prop : requestPropogations) {
                switch (prop) {
                    case LastAction:
                        params.put(ParamHelper.PARAM_USER_WITH_LAST_ACTION, "1");
                        break;
                    case Endpoints:
                        params.put(ParamHelper.PARAM_USER_WITH_ENDPOINTS, "1");
                        break;
                    case ReferedUsers:
                        params.put(ParamHelper.PARAM_USER_WITH_REFERED_USERS, "1");
                        break;
                    case Roles:
                        params.put(ParamHelper.PARAM_USER_WITH_ROLES, "1");
                        break;
                    case Requests:
                        params.put(ParamHelper.PARAM_USER_WITH_REQUESTS, "1");
                        break;
                    default:
                        throw new IllegalArgumentException("Unsupported UserRequestPropogation: " + prop);
                }
            }
        }

        HttpEntity entity;
        try {
            entity = buildEntity(params, null);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        }
        HttpPost request = new HttpPost(url + method);
        request.setEntity(entity);

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpResponse response = httpClient.execute(request);
            HttpEntity respEntity = response.getEntity();
            String responseString = EntityUtils.toString(respEntity, "UTF-8");

            if (response.getStatusLine().getStatusCode() != HttpURLConnection.HTTP_OK) {
                try {
                    DataSet ds = DataSet.fromJson(responseString);
                    throw new HttpException(response.getStatusLine().getStatusCode(), ds);
                } catch (Exception ex) {
                    throw new HttpException(response.getStatusLine().getStatusCode(), responseString);
                }
            }

            // парсим ответ
            try {
                DataSet ds = DataSet.fromJson(responseString);

                ArrayList<User> users = new ArrayList();
                for (DataRecord row : ds.getRecords()) {
                    User u = new User(null, row.getString("Guuid"));
                    u.setFirstName(row.getString("FirstName"));
                    u.setLastName(row.getString("LastName"));
                    u.setMiddleName(row.getString("MiddleName"));
                    u.setImage(row.getString("Image"));
                    u.setLastdate(row.getTimestamp("Lastdate"));
                    u.setRegistered(row.getInteger("IsRegistered") == 1);
                    u.setDisabled(row.getInteger("IsDisabled") == 1);
                    u.setLastActionDate(row.getTimestamp("LastActionDate"));
                    u.setBirthday(row.getTimestamp("Birthday"));

                    List<Endpoint> ep = new ArrayList<>();
                    DataSet phones = row.getDataSet("Phones");
                    DataSet emails = row.getDataSet("Emails");
                    if (phones != null) {
                        for (DataRecord r : phones.getRecords()) {
                            Endpoint e = new Endpoint(null, NotificationTypeCode.sms, r.getString(0));
                            ep.add(e);
                        }
                    }
                    if (emails != null) {
                        for (DataRecord r : emails.getRecords()) {
                            Endpoint e = new Endpoint(null, NotificationTypeCode.email, r.getString(0));
                            ep.add(e);
                        }
                    }
                    if (!ep.isEmpty()) {
                        u.setEndpoints(ep);
                    }

                    DataSet roles = row.getDataSet("Roles");
                    List<Role> arrRoles = new ArrayList<>();
                    if (roles != null) {
                        for (DataRecord r : roles.getRecords()) {
                            Role rr = new Role();
                            rr.setCode(r.getString(0));
                            arrRoles.add(rr);
                        }
                    }
                    if (!arrRoles.isEmpty()) {
                        u.setRoles(arrRoles);
                    }
                    users.add(u);
                }
                return users;

            } catch (Exception ex) {
                logger.error("", ex);
                throw new HttpInternalException(ex);
            }
        } catch (IOException ex) {
            logger.error("", ex);
            throw new HttpInternalException(ex);
        }
    }
    
    @Override
    public void removeOtherTokens(String token) throws StemException {
        final String method = "/RemoveOtherTokens.json";

        // формируем параметры запроса
        final HashMap<String, String> params = new HashMap<>();
        params.put(RemoveOtherTokens.PARAM_VERSION, AUTH_SERVICE_VERSION.get());
        params.put(RemoveOtherTokens.PARAM_TOKEN, token);
        params.put(RemoveOtherTokens.PARAM_API_KEY, apiKey);

        HttpEntity entity;
        try {
            entity = buildEntity(params, null);
        } catch (UnsupportedEncodingException ex) {
            throw new HttpInternalException(ex);
        }
        HttpPost request = new HttpPost(url + method);
        request.setEntity(entity);

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpResponse response = httpClient.execute(request);
            HttpEntity respEntity = response.getEntity();
            String responseString = EntityUtils.toString(respEntity, "UTF-8");

            if (response.getStatusLine().getStatusCode() != HttpURLConnection.HTTP_OK) {
                try {
                    DataSet ds = DataSet.fromJson(responseString);
                    throw new HttpException(response.getStatusLine().getStatusCode(), ds);
                } catch (Exception ex) {
                    throw new HttpException(response.getStatusLine().getStatusCode(), responseString);
                }
            }
        } catch (IOException ex) {
            logger.error("", ex);
            throw new HttpInternalException(ex);
        }
    }

}
