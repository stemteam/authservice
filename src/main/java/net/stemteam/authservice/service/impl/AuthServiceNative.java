/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.service.impl;

import java.sql.SQLException;
import java.sql.Timestamp;
import net.stemteam.authservice.entity.enums.NotificationTypeCode;
import net.stemteam.authservice.dao.AuthRepository;
import net.stemteam.authservice.oauth.OAuthClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.stemteam.authservice.entity.Endpoint;
import net.stemteam.authservice.common.UserRequestType;
import net.stemteam.authservice.entity.RefType;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.api.support.UserRequestPropogation;
import net.stemteam.authservice.exception.PermissionDenyException;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.exception.UserNotFoundException;
import net.stemteam.authservice.notification.AzureHub;
import net.stemteam.authservice.service.AuthService;
import net.stemteam.jaxis.err.StemException;
import net.stemteam.jaxis.mail.MailConfig;
import net.stemteam.rightservice.entity.Role;
import net.stemteam.rightservice.exception.RightServiceException;

/**
 * Сервис авторизации (нативный)
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class AuthServiceNative implements AuthService {

    private AuthRepository authRepository;
    private List<OAuthClient> ouathClients = new ArrayList<>();
    private MailConfig mailConfig;
    private HashMap<String, AzureHub> azureHubs = new HashMap<>();
    private String shortUrlGetAddress;

    /**
     * Урл для получения содержимого короткой ссылки. Обычно для получения данных короткой ссылки используют короткий
     * домен вида http://short.url/a23dsd, где сам идентификатор короткого урла добавляется в конец. В это свойство
     * можно передать http://short.url/
     *
     * @return
     */
    public String getShortUrlGetAddress() {
        return shortUrlGetAddress;
    }

    /**
     * Урл для получения содержимого короткой ссылки. Обычно для получения данных короткой ссылки используют короткий
     * домен вида http://short.url/a23dsd, где сам идентификатор короткого урла добавляется в конец. В это свойство
     * можно передать http://short.url/
     *
     * @param shortUrlGetAddress
     */
    public void setShortUrlGetAddress(String shortUrlGetAddress) {
        this.shortUrlGetAddress = shortUrlGetAddress;
    }

    /**
     * Настройки отправки почты
     *
     * @return
     */
    public MailConfig getMailConfig() {
        return mailConfig;
    }

    /**
     * Настройки отправки почты
     *
     * @param mailConfig
     */
    public void setMailConfig(MailConfig mailConfig) {
        this.mailConfig = mailConfig;
    }

    /**
     * Отправка PUSH
     *
     * @return
     */
    public HashMap<String, AzureHub> getAzureHubs() {
        return azureHubs;
    }

    /**
     * Отправка PUSH
     *
     * @param azureHubs
     */
    public void setAzureHubs(HashMap<String, AzureHub> azureHubs) {
        this.azureHubs = azureHubs;
    }

    /**
     * Добавить интерфес клиента приложения, поддерживающего внешнюю авторизацию посредством OAuth
     *
     * @param client
     */
    public void addOAuthClient(OAuthClient client) {
        ouathClients.add(client);
    }

    /**
     * Получить интерфес клиента приложения, поддерживающего внешнюю авторизацию посредством OAuth по коду
     *
     * @param code
     * @return
     */
    public OAuthClient getOAuthClient(NotificationTypeCode code) {
        for (OAuthClient client : ouathClients) {
            if (client.getApplicationCode().equals(code)) {
                return client;
            }
        }
        throw new IllegalArgumentException("Приложение с кодом " + code + " не зарегистрировано в сервисе");
    }

    /**
     * Репозиторий для работы с БД
     *
     * @return
     */
    public AuthRepository getAuthRepository() {
        return authRepository;
    }

    /**
     * Репозиторий для работы с БД
     *
     * @param authRepository
     */
    public void setAuthRepository(AuthRepository authRepository) {
        this.authRepository = authRepository;
    }

    @Override
    public String shortUrlGet(String url) throws StemException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User login(String login, String password) throws StemException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String register(String phone, String email, String password, String serviceName, String host,
            String notificationType, String firstName, String lastName, String middleName, String image, String smsSign)
            throws StemException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String activate(String login, String activationCode) throws StemException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void forgotPassword(String login, String serviceName, String host, String notificationType, String smsSign)
            throws StemException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void forgotPasswordConfirm(String login, String code, String newPassword) throws StemException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void changePassword(String token, String currentPassword, String newPassword, String newPasswordConfirm)
            throws StemException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String addUser(String token, String phone, String email, String password, String serviceName, String host,
            String notificationType, String firstName, String lastName, String middleName, String image, RefType refType,
            Timestamp birthday)
            throws StemException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRefUsers(User user, RefType refType) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void notify(String senderToken, String receiverGUUID, String smallMessage, String emailTitle,
            String emailMessage, String endpointApiKey, String notificationType, Boolean verifiedOnly, String pushTitle,
            String pushType, String pushData, String smsSign) throws StemException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void notifyForce(String senderToken, String Guuid, String smallMessage, String emailTitle,
            String emailMessage, String smsSign) throws StemException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String userDeleteRequest(String token, String guuid) throws StemException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void userDeleteConfirm(String token, String guuid, String code) throws StemException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<User> userListGet(String token, String guuid, Timestamp lastDate) throws StemException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void userEdit(String token, String firstName, String lastName, String middleName, String image, String guuid,
            Timestamp birthday)
            throws StemException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteUser(String token, String guuid) throws StemException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void pushRegistrationEdit(String token, String deviceId, String PNSToken, String PNSCode, String isTablet,
            String locale, String model, String name, String endpointApiKey) throws StemException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String userRequestAdd(String token, String login, UserRequestType type, int expirationTimeSec) throws
            StemException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Endpoint> getEndpointList(String token, String guuid) throws StemException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setUserDisabled(String token, String guuid, boolean isDisabled) throws StemException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Получить список доступных ролей
     *
     * @param callerToken пользователь, вызвавший апи
     * @return
     * @throws net.stemteam.jaxis.err.StemException
     * @throws net.stemteam.authservice.exception.UserNotAuthorizedException
     * @throws net.stemteam.authservice.exception.PermissionDenyException
     */
    @Override
    public List<Role> getRoleList(String callerToken) throws StemException,
            UserNotAuthorizedException, PermissionDenyException {
        try {
            User u = authRepository.getUser(new User(callerToken, null), UserRequestPropogation.Roles);
            if (!u.isSystem()) {
                throw new PermissionDenyException("Пользователь не является администратором, действие запрещено");
            }
            return authRepository.getRightService().getRoleList();
        } catch (SQLException ex) {
            throw new StemException(ex);
        }
    }

    /**
     * Получить список ролей пользователя
     *
     * @param callerToken пользователь, вызвавший апи
     * @param guuid идентификатор пользователя
     * @return
     * @throws net.stemteam.jaxis.err.StemException
     * @throws net.stemteam.authservice.exception.UserNotAuthorizedException
     * @throws net.stemteam.authservice.exception.PermissionDenyException
     */
    @Override
    public List<Role> getUserRoleList(String callerToken, String guuid) throws StemException, UserNotAuthorizedException,
            PermissionDenyException {
        try {
            User u = authRepository.getUser(new User(callerToken, null), UserRequestPropogation.Roles);
            if (!u.isSystem()) {
                throw new PermissionDenyException("Пользователь не является администратором, действие запрещено");
            }
            return authRepository.getRightService().getUserRoleList(guuid);
        } catch (SQLException ex) {
            throw new StemException(ex);
        }
    }

    /**
     * Добавить пользователю роль с заданным идентификатором
     *
     * @param callerToken пользователь, вызвавший апи
     * @param guuid идентификатор пользователя
     * @param roleId идентификатор добавляемой роли
     * @throws net.stemteam.authservice.exception.PermissionDenyException
     * @throws net.stemteam.authservice.exception.UserNotAuthorizedException
     */
    @Override
    public void addUserRole(String callerToken, String guuid, Integer roleId) throws StemException,
            UserNotAuthorizedException,
            PermissionDenyException {
        try {
            User u = authRepository.getUser(new User(callerToken, null), UserRequestPropogation.Roles);
            if (!u.isSystem()) {
                throw new PermissionDenyException("Пользователь не является администратором, действие запрещено");
            }
            authRepository.getRightService().addUserRole(guuid, roleId);
        } catch (SQLException ex) {
            throw new StemException(ex);
        }
    }

    /**
     * Забрать у пользователя роль с заданным идентификатором
     *
     * @param callerToken пользователь, вызвавший апи
     * @param guuid идентификатор пользователя
     * @param roleId идентификатор забираемой роли
     * @throws net.stemteam.jaxis.err.StemException
     * @throws net.stemteam.authservice.exception.UserNotAuthorizedException
     * @throws net.stemteam.authservice.exception.PermissionDenyException
     */
    @Override
    public void deleteUserRole(String callerToken, String guuid, Integer roleId) throws StemException,
            UserNotAuthorizedException,
            PermissionDenyException {
        try {
            User u = authRepository.getUser(new User(callerToken, null), UserRequestPropogation.Roles);
            if (!u.isSystem()) {
                throw new PermissionDenyException("Пользователь не является администратором, действие запрещено");
            }
            authRepository.getRightService().deleteUserRole(guuid, roleId);
        } catch (SQLException ex) {
            throw new StemException(ex);
        }
    }

    /**
     * Удалить пользователя из системы прав
     *
     * @param callerToken пользователь, вызвавший апи
     * @param guuid идентификатор пользователя
     * @throws StemException
     * @throws net.stemteam.authservice.exception.UserNotAuthorizedException
     * @throws net.stemteam.authservice.exception.PermissionDenyException
     */
    @Override
    public void deleteRightUser(String callerToken, String guuid) throws StemException, UserNotAuthorizedException,
            PermissionDenyException {
        try {
            User u = authRepository.getUser(new User(callerToken, null), UserRequestPropogation.Roles);
            if (!u.isSystem()) {
                throw new PermissionDenyException("Пользователь не является администратором, действие запрещено");
            }
            authRepository.getRightService().deleteRightUser(guuid);
        } catch (SQLException ex) {
            throw new StemException(ex);
        }

    }

    /**
     * Возвращает данные пользователя
     *
     * @param token
     * @param requestPropogations
     * @return
     * @throws net.stemteam.jaxis.err.StemException
     * @throws net.stemteam.rightservice.exception.RightServiceException
     * @throws net.stemteam.authservice.exception.UserNotAuthorizedException
     * @throws net.stemteam.authservice.exception.UserNotFoundException
     */
    @Override
    public User getUser(String token, UserRequestPropogation... requestPropogations) throws StemException,
            RightServiceException, UserNotAuthorizedException, UserNotFoundException {
        try {
            return authRepository.getUser(new User(token, null), requestPropogations);
        } catch (SQLException ex) {
            throw new StemException(ex);
        }
    }

    /**
     * Возвращает данные пользователя
     *
     * @param callerToken вызывающий пользователь
     * @param guuid
     * @param requestPropogations
     * @return
     * @throws StemException
     * @throws net.stemteam.rightservice.exception.RightServiceException
     * @throws net.stemteam.authservice.exception.UserNotAuthorizedException
     * @throws net.stemteam.authservice.exception.UserNotFoundException
     */
    @Override
    public User getUserByGuuid(String callerToken, String guuid, UserRequestPropogation... requestPropogations) throws
            StemException,
            RightServiceException, UserNotAuthorizedException, UserNotFoundException {
        try {
            User caller = authRepository.getUser(new User(callerToken, null), UserRequestPropogation.Roles);
            if (!caller.isSystem()) {
                throw new PermissionDenyException("Для выполнения действие нужно быть суперпользователем");
            }
            return authRepository.getUser(new User(guuid), requestPropogations);
        } catch (SQLException ex) {
            throw new StemException(ex);
        }
    }

    /**
     * Возвращает данные пользователя
     *
     * @param login
     * @param requestPropogations
     * @return
     * @throws StemException
     * @throws net.stemteam.authservice.exception.UserNotAuthorizedException
     * @throws net.stemteam.authservice.exception.UserNotFoundException
     */
    @Override
    public User getUserByLogin(String callerToken, String login, UserRequestPropogation... requestPropogations) throws
            StemException,
            UserNotAuthorizedException, UserNotFoundException {
        try {
            User caller = authRepository.getUser(new User(callerToken, null), UserRequestPropogation.Roles);
            if (!caller.isSystem()) {
                throw new PermissionDenyException("Для выполнения действие нужно быть суперпользователем");
            }
            return authRepository.getUserByLogin(login, requestPropogations);
        } catch (SQLException ex) {
            throw new StemException(ex);
        }
    }

    @Override
    public void addUserRole(String callerToken, String guuid, String roleCode) throws RightServiceException,
            UserNotAuthorizedException, UserNotFoundException, PermissionDenyException, StemException {
        try {
            User u = authRepository.getUser(new User(callerToken, null), UserRequestPropogation.Roles);
            if (!u.isSystem()) {
                throw new PermissionDenyException("Пользователь не является администратором, действие запрещено");
            }
            Role r = authRepository.getRightService().getRoleByCode(roleCode);
            authRepository.getRightService().addUserRole(guuid, r.getId());
        } catch (SQLException ex) {
            throw new StemException(ex);
        }
    }

    @Override
    public void deleteUserRole(String callerToken, String guuid, String roleCode) throws StemException,
            UserNotAuthorizedException,
            PermissionDenyException {
        try {
            User u = authRepository.getUser(new User(callerToken, null), UserRequestPropogation.Roles);
            if (!u.isSystem()) {
                throw new PermissionDenyException("Пользователь не является администратором, действие запрещено");
            }
            Role r = authRepository.getRightService().getRoleByCode(roleCode);
            authRepository.getRightService().deleteUserRole(guuid, r.getId());
        } catch (SQLException ex) {
            throw new StemException(ex);
        }
    }

    @Override
    public List<User> getUsers(String token, Timestamp lastDate, Long limit, Long offset,
            UserRequestPropogation... requestPropogations) throws
            StemException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void removeOtherTokens(String token) throws StemException, RightServiceException, UserNotAuthorizedException,
            UserNotFoundException {
        try {
            authRepository.removeAnotherTokens(token);
        } catch (SQLException ex) {
            throw new StemException(ex);
        }
    }
}
