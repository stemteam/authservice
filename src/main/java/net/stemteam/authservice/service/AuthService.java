/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.service;

import net.stemteam.authservice.entity.Endpoint;
import net.stemteam.authservice.common.UserRequestType;
import net.stemteam.authservice.entity.RefType;
import net.stemteam.authservice.entity.User;
import net.stemteam.jaxis.err.StemException;

import java.sql.Timestamp;
import java.util.List;
import net.stemteam.authservice.api.support.UserRequestPropogation;
import net.stemteam.rightservice.entity.Role;

/**
 * Клиент сервиса авторизации.
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public interface AuthService {

    /**
     * Получить идентификатор короткой ссылки (пример AsDfg6).
     *
     * @param url длинная ссылка
     * @return
     * @throws StemException
     */
    public String shortUrlGet(String url) throws StemException;

    /**
     * Вход пользователя в систему посредством сервиса авторизации
     *
     * @param login
     * @param password
     * @return
     * @throws StemException
     */
    public User login(String login, String password) throws StemException;

    /**
     * Регистрация пользователя в системе авторизации
     *
     * @param phone
     * @param email
     * @param password
     * @param serviceName название сервиса (используется в оповещениях)
     * @param host хост (используется в оповещениях)
     * @param notificationType метод валидации (phone, email)
     * @param firstName
     * @param lastName
     * @param middleName
     * @param image
     * @param smsSign подпись СМС (отправитель)
     * @return Guuid добавленного но неподтвержденного пользователя
     * @throws StemException
     */
    public String register(String phone, String email, String password, String serviceName, String host,
            String notificationType, String firstName, String lastName, String middleName, String image, String smsSign)
            throws StemException;

    /**
     * Активация пользователя (завершение регистрации)
     *
     * @param login
     * @param activationCode
     * @return GUUID
     * @throws StemException
     */
    public String activate(String login, String activationCode) throws StemException;

    /**
     * Забыл пароль - процедура восстановления. В итоге должно отправиться оповещение с кодом восстановления
     *
     * @param login
     * @param serviceName
     * @param host
     * @param notificationType
     * @param smsSign подпись СМС (отправитель)
     * @throws StemException
     */
    public void forgotPassword(String login, String serviceName, String host, String notificationType, String smsSign)
            throws StemException;

    /**
     * Забыл пароль. Подтверждение кодом и смена на новый.
     *
     * @param login
     * @param code
     * @param newPassword
     * @throws StemException
     */
    public void forgotPasswordConfirm(String login, String code, String newPassword) throws StemException;

    /**
     * Изменение собственного пароля пользователем
     *
     * @param token
     * @param currentPassword
     * @param newPassword
     * @param newPasswordConfirm
     * @throws StemException
     */
    public void changePassword(String token, String currentPassword, String newPassword, String newPasswordConfirm)
            throws StemException;

    /**
     * Добавление пользователя в систему
     *
     * @param token
     * @param phone
     * @param email
     * @param password
     * @param serviceName
     * @param host
     * @param notificationType
     * @param firstName
     * @param lastName
     * @param middleName
     * @param image
     * @param refType
     * @param birthday дата рождения
     * @return
     * @throws StemException
     */
    public String addUser(String token, String phone, String email, String password, String serviceName, String host,
            String notificationType, String firstName, String lastName, String middleName, String image, RefType refType, Timestamp birthday)
            throws StemException;

    /**
     * Загрузка связанных пользователей
     *
     * @param user пользователь (используется токен)
     * @param refType тип связи
     * @throws java.lang.Exception
     */
    public void loadRefUsers(User user, RefType refType) throws Exception;

    /**
     * Отправить сообщение пользователю
     *
     * @param senderToken Токен отправителя
     * @param receiverGUUID GUUID получателя
     * @param smallMessage сообщение для SMS|PUSH
     * @param emailTitle заголовок для email
     * @param emailMessage тело письма сообщения email
     * @param endpointApiKey ключ апи для таргетирования брендов
     * @param notificationType тип уведомления email|sms|push (если null то отправка на все доступные)
     * @param verifiedOnly отправлять только на проверенные контакты
     * @param pushTitle заголовок PUSH сообщения
     * @param pushType тип PUSH уведомления, это внутренний код. Сейчас это (url|text)
     * @param pushData данные PUSH уведомления. Это url если тип url. или null если text
     * @param smsSign подпись в СМС (отправитель)
     * @throws StemException
     */
    public void notify(String senderToken, String receiverGUUID, String smallMessage, String emailTitle,
            String emailMessage, String endpointApiKey, String notificationType,
            Boolean verifiedOnly, String pushTitle, String pushType, String pushData, String smsSign) throws
            StemException;

    /**
     * Отправить сообщение пользователю на основные учетные данные, принудительно.
     *
     * Игнорирует настройки пользователя о получении сообщений. Используется для системных действий.
     *
     * @param senderToken Токен отправителя
     * @param Guuid GUUID получателя
     * @param smallMessage сообщение для SMS|PUSH
     * @param emailTitle заголовок для email
     * @param emailMessage тело письма сообщения email
     * @param smsSign СМС подпись (отправитель)
     * @throws StemException
     */
    public void notifyForce(String senderToken, String Guuid, String smallMessage, String emailTitle,
            String emailMessage, String smsSign) throws StemException;

    /**
     * Запрос удаления пользователя в сервисе авторизации.
     *
     * Возвращает код для подтверждения.
     *
     * @param token
     * @param guuid
     * @return
     * @throws StemException
     */
    public String userDeleteRequest(String token, String guuid) throws StemException;

    /**
     * Подтверждение удаления пользователя в сервисе авторизации.
     *
     * @param token
     * @param guuid
     * @param code проверочный код
     * @throws StemException
     */
    public void userDeleteConfirm(String token, String guuid, String code) throws StemException;

    /**
     * Возвращает список пользователей GUUID, ФИО, картинка. Необходимо быть IsSystem пользователем.
     *
     * @param token токен запрашивающего пользователя
     * @param guuid гуид для ограничения выборки (если не null - возвратятся данные только этого пользователя)
     * @param lastDate вернуть только данные, которые были изменены после lastDate (для обновления кэша). Если lastDate = null, то параметр
     * игнорируется
     * @return
     * @throws StemException
     * @deprecated используйте getUsers вместо этого
     */
    @Deprecated
    public List<User> userListGet(String token, String guuid, Timestamp lastDate) throws StemException;

    /**
     * Редактирование собственных данных пользователя
     *
     * @param token токен авторизации
     * @param firstName имя
     * @param lastName фамилия
     * @param middleName отчетсво
     * @param image картинка
     * @param guuid
     * @param birthday
     * @throws StemException
     */
    public void userEdit(String token, String firstName, String lastName, String middleName, String image,
            String guuid, Timestamp birthday) throws StemException;

    /**
     * Удаление пользователя в базе авторизаци по Guuid. Нужно быть системным пользователем для возможности выполнения этого действия.
     *
     * @param token
     * @param guuid
     * @throws StemException
     */
    public void deleteUser(String token, String guuid) throws StemException;

    /**
     * Регистрация/обновление регистрационных данных мобильного устройства в сервисе отправки сообщений
     *
     * @param token токен авторизации
     * @param deviceId уникальный id устройства
     * @param PNSToken token полученный от PNS
     * @param PNSCode тип PNS (Platform Notification Service)
     * @param isTablet является ли устройство планшетом, 1 - является
     * @param locale локаль устройства
     * @param model модель устройства
     * @param name настраиваемое имя устройства
     * @param endpointApiKey ключ апи приложения
     * @throws StemException
     */
    public void pushRegistrationEdit(String token, String deviceId, String PNSToken, String PNSCode, String isTablet,
            String locale, String model, String name, String endpointApiKey) throws StemException;

    /**
     * Создает пользовательский запрос (сменить пароль, забыл пароль, активация и т.д.), возвращает код. Ограничение: необходимо быть системным
     * пользователем.
     *
     * @param token токен пользователя, выполняющего действие (должен быть системным)
     * @param login логин изменяемого пользователя
     * @param type тип пользовательского запроса
     * @param expirationTimeSec время жизни запроса (секунд)
     * @return код созданного запроса
     * @throws StemException
     */
    public String userRequestAdd(String token, String login, UserRequestType type, int expirationTimeSec) throws
            StemException;

    /**
     * Получение контактов пользователя по Guuid. Нужно быть системным пользователем для возможности получения чужих контактов
     *
     * @param token
     * @param guuid
     * @return
     * @throws StemException
     */
    public List<Endpoint> getEndpointList(String token, String guuid) throws StemException;

    /**
     * Включить/отключить возможность входа пользователю
     *
     * @param token токен пользователя, выполняющего действие (должен быть системным)
     * @param guuid идентификатор пользователя, которого нужно изменить
     * @param isDisabled если false - то пользователь включен, если true - вход в систему запрещен
     * @throws StemException
     */
    public void setUserDisabled(String token, String guuid, boolean isDisabled) throws StemException;

    /**
     * Получить список доступных ролей
     *
     * @param callerToken вызывающий пользователь
     * @return
     * @throws net.stemteam.jaxis.err.StemException
     * @throws net.stemteam.authservice.exception.UserNotAuthorizedException
     * @throws net.stemteam.authservice.exception.PermissionDenyException
     */
    public List<Role> getRoleList(String callerToken) throws StemException;

    /**
     * Получить список ролей пользователя
     *
     * @param callerToken вызывающий пользователь
     * @param guuid идентификатор пользователя
     * @return
     * @throws net.stemteam.jaxis.err.StemException
     * @throws net.stemteam.authservice.exception.UserNotAuthorizedException
     * @throws net.stemteam.authservice.exception.PermissionDenyException
     */
    public List<Role> getUserRoleList(String callerToken, String guuid) throws StemException;

    /**
     * Добавить пользователю роль с заданным идентификатором
     *
     * @param callerToken вызывающий пользователь
     * @param guuid идентификатор пользователя
     * @param roleId идентификатор добавляемой роли
     * @throws net.stemteam.authservice.exception.PermissionDenyException
     * @throws net.stemteam.authservice.exception.UserNotAuthorizedException
     */
    public void addUserRole(String callerToken, String guuid, Integer roleId) throws StemException;

    /**
     * Забрать у пользователя роль с заданным идентификатором
     *
     * @param callerToken вызывающий пользователь
     * @param guuid идентификатор пользователя
     * @param roleId идентификатор забираемой роли
     * @throws net.stemteam.jaxis.err.StemException
     * @throws net.stemteam.authservice.exception.UserNotAuthorizedException
     * @throws net.stemteam.authservice.exception.PermissionDenyException
     */
    public void deleteUserRole(String callerToken, String guuid, Integer roleId) throws StemException;

    /**
     * Удалить пользователя из системы прав
     *
     * @param callerToken вызывающий пользователь
     * @param guuid идентификатор пользователя
     * @throws StemException
     * @throws net.stemteam.authservice.exception.UserNotAuthorizedException
     * @throws net.stemteam.authservice.exception.PermissionDenyException
     */
    public void deleteRightUser(String callerToken, String guuid) throws StemException;

    /**
     * Возвращает данные пользователя
     *
     * @param token
     * @param requestPropogations
     * @return
     * @throws net.stemteam.jaxis.err.StemException
     */
    public User getUser(String token, UserRequestPropogation... requestPropogations) throws StemException;

    /**
     * Возвращает данные пользователя
     *
     * @param callerToken вызывающий пользователь
     * @param guuid
     * @param requestPropogations
     * @return
     * @throws StemException
     */
    public User getUserByGuuid(String callerToken, String guuid, UserRequestPropogation... requestPropogations) throws
            StemException;

    /**
     * Возвращает данные пользователя
     *
     * @param callerToken вызывающий пользователь
     * @param login
     * @param requestPropogations
     * @return
     * @throws StemException
     */
    public User getUserByLogin(String callerToken, String login, UserRequestPropogation... requestPropogations) throws
            StemException;

    /**
     * Назначение роли пользователю
     *
     * @param callerToken
     * @param guuid
     * @param roleCode
     * @throws StemException
     */
    public void addUserRole(String callerToken, String guuid, String roleCode) throws StemException;

    /**
     * Удаление роли пользователя
     *
     * @param callerToken
     * @param guuid
     * @param roleCode
     * @throws StemException
     */
    public void deleteUserRole(String callerToken, String guuid, String roleCode) throws StemException;

    /**
     * Получение списка пользователей (внезапно)
     *
     * @param token токен запрашивающего пользователя (должен быть системным пользователем)
     * @param lastDate дата последних изменений данных пользователей (если не null - будут возвращены пользователи, чьи данные были изменены позже
     * переданной даты-времени)
     * @param limit ограничение на количество возвращаемых записей (пагинация)
     * @param offset отступ от первой записи (пагинация)
     * @param requestPropogations насколько наполнеными должны быть возвращаемые даные
     * @return
     * @throws StemException
     */
    public List<User> getUsers(String token, Timestamp lastDate, Long limit, Long offset, UserRequestPropogation... requestPropogations) throws
            StemException;
    
    /**
     * Удалить все токены авторизации пользователя, отличные от текущего (разлогинить остальных)
     * @param token
     * @throws StemException 
     */
    public void removeOtherTokens(String token) throws StemException;

}
