/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.entity.RefType;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.entity.UserReference;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.ValidationErrorItem;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.exception.RightServiceException;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import net.stemteam.authservice.api.support.DataSetProvider;
import net.stemteam.authservice.api.support.UserRequestPropogation;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.exception.UserNotFoundException;

/**
 * Возвращает связанных с пользователем пользователей
 *
 * @deprecated следует использовать getUser с опцией UserRequestPropogation.ReferedUsers
 */
@Deprecated
public class RefUsersGet extends DataSetHandler {

    private static final String PARAM_REF_TYPE = "RefType";

    @Override
    public DataSet getData(HashMap params, JaxisContentType jct) throws ConnectionPoolNotFoundException,
            DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpBadRequestException, HttpInternalException,
            HttpUnauthorizedException, NotConfiguredException, HttpNotFoundException, RightServiceException,
            UserNotAuthorizedException, UserNotFoundException {
        String Version = getParamValidator().getString(params, "Version", true); // версия протокола
        String Token = getParamValidator().getString(params, "Token", true); // токен авторизации пользователя
        String ApiKey = getParamValidator().getString(params, "ApiKey", true); // ключ апи
        String refTypeStr = getParamValidator().getString(params, PARAM_REF_TYPE); // тип отношений создателя с этим пользователем
        getParamValidator().postValidate();

        RefType refType = null;
        if (refTypeStr != null) {
            try {
                refType = RefType.getByCode(refTypeStr);
            } catch (IllegalArgumentException ex) {
                getParamValidator().add(new ValidationErrorItem("UnsupportedRefType", PARAM_REF_TYPE,
                        "Unsupported RefType = " + refTypeStr));
                getParamValidator().postValidate();
            }
        }

        // проверка апи-ключа
        ApiKeyHelper.getInstance().check(getDataSource(), ApiKey);

        // получение пользователя по токену + проверка авторизации
        User user = getAuthService().getAuthRepository().getUser(new User(Token, null),
                UserRequestPropogation.ReferedUsers);

        List<UserReference> refList;
        if (refType != null) {
            refList = user.getReferences(refType);
        } else {
            refList = user.getReferences();
        }

        DataSetProvider prov = new DataSetProvider();
        return prov.getReferedUsersDataSet(refList);
    }
}
