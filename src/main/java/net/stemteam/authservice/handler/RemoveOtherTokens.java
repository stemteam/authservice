/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.annotation.Method;
import net.stemteam.jaxis.annotation.Param;
import net.stemteam.jaxis.annotation.Return;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.exception.RightServiceException;

import java.sql.SQLException;
import java.util.HashMap;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.jaxis.err.StemException;

/**
 * Удалить все токены авторизации пользователя, отличные от текущего (разлогинить остальных)
 */
public class RemoveOtherTokens extends DataSetHandler {

    @Method(desc = "Удалить все токены авторизации пользователя, отличные от текущего (разлогинить остальных)",
            params = {
                @Param(required = true, name = PARAM_VERSION, desc = "Версия протокола"),
                @Param(required = true, name = PARAM_TOKEN, desc = "токен авторизации"),
                @Param(required = true, name = PARAM_API_KEY, desc = "Ключ API")},
            returns = @Return(desc = "Возвращает: HTTP OK", items = {}))
    @Override
    public DataSet getData(HashMap params, JaxisContentType jct) throws ConnectionPoolNotFoundException,
            DataSetException, CloseDbConnectionException, SQLException, HttpForbiddenException, NotConfiguredException,
            HttpInternalException, HttpUnauthorizedException, RightServiceException, StemException {
        String Version = getParamValidator().getString(params, PARAM_VERSION, true);
        String ApiKey = getParamValidator().getString(params, PARAM_API_KEY, true);
        String token = getParamValidator().getString(params, PARAM_TOKEN, true);
        getParamValidator().postValidate();

        // проверка API-ключа
        ApiKeyHelper.getInstance().check(getDataSource(), ApiKey);

        getAuthService().removeOtherTokens(token);
        return null;
    }
    public static final String PARAM_TOKEN = "Token";
    public static final String PARAM_API_KEY = "ApiKey";
    public static final String PARAM_VERSION = "Version";

}
