/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataColumnType;
import net.stemteam.datatransport.transport.DataRecord;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.NotConfiguredException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;
import net.stemteam.authservice.api.support.ParamHelper;
import net.stemteam.authservice.api.support.UserRequestPropogation;
import net.stemteam.jaxis.annotation.Method;
import net.stemteam.jaxis.annotation.Param;
import net.stemteam.jaxis.annotation.Return;

/**
 * Авторизация пользователя в системе (по номеру мобильного либо емэйлу)
 */
public class Login extends DataSetHandler {

    @Method(desc = "Авторизация пользователя в системе (по номеру мобильного либо емэйлу)",
            params = {
                @Param(name = ParamHelper.PARAM_VERSION, desc = "версия протокола", type = DataColumnType.STRING),
                @Param(name = ParamHelper.PARAM_APIKEY, desc = "ключ API"),
                @Param(name = "Login", desc = "email либо мобильный телефон пользователя"),
                @Param(name = "Password", desc = "хэш пароля пользователя")
            },
            returns = @Return(desc = "Результат залогинивания",
                    items = {
                        @Param(name = "Id", desc = "уникальный идентификатор пользователя (GUUID)"),
                        @Param(name = "Token", desc = "токен авторизации")}))
    @Override
    public DataSet getData(HashMap params, JaxisContentType jct) throws 
            ConnectionPoolNotFoundException, DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpNotFoundException,
            HttpInternalException, NotConfiguredException, Exception {
        String Version = getParamValidator().getString(params, "Version", true);
        String Login = getParamValidator().getString(params, "Login", true);
        String Passwd = getParamValidator().getString(params, "Password", true);
        String ApiKey = getParamValidator().getString(params, "ApiKey", true);
        getParamValidator().postValidate();

        // проверяем валидность API-KEY
        ApiKeyHelper.getInstance().check(getDataSource(), ApiKey);

        User user = getAuthService().getAuthRepository().getUserByLogin(Login, UserRequestPropogation.Roles);

        if (!user.checkPassword(Passwd)) {
            throw new HttpNotFoundException("Пользователь с указанным логином и паролем не найден");
        }

        if (!user.isRegistered()) {
            throw new HttpForbiddenException("Пользователь не завершил регистрацию");
        }

        if (user.isDisabled()) {
            throw new HttpForbiddenException("Пользователь заблокирован");
        }

        // генерируем токен
        String token = UUID.randomUUID().toString();

        try (Connection conn = getConnection()) {

            // записываем его в базку
            PreparedStatement ps2 = conn.prepareStatement(
                    "insert into syslogin (id_syslogin, id_sysuser) values (?::uuid, ?) ");
            ps2.setObject(1, token);
            ps2.setInt(2, user.getId());
            ps2.execute();

            conn.commit();

            DataSet ds = new DataSet();
            ds.createColumn("Token", DataColumnType.STRING);
            ds.createColumn("Id", DataColumnType.STRING);
            ds.createColumn("IsSystem", DataColumnType.INT32);
            DataRecord row = ds.createRecord();
            row.setString("Token", token);
            row.setString("Id", user.getGuuid());
            row.setInt("IsSystem", user.isSystem() ? 1 : 0);
            ds.setAsJson2Array(false);
            return ds;

        }
    }

}
