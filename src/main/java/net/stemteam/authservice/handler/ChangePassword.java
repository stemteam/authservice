/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.common.PasswordValidator;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.ValidationErrorItem;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.exception.RightServiceException;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import net.stemteam.jaxis.annotation.Method;
import net.stemteam.jaxis.annotation.Param;
import net.stemteam.jaxis.annotation.Return;
import net.stemteam.jaxis.err.StemException;

/**
 * Изменение собственного пароля пользователем.
 */
public class ChangePassword extends DataSetHandler {

    @Method(desc = "Изменение собственного пароля пользователем.",
            params = {
                @Param(required = true, name = "Version", desc = "Версия протокола"),
                @Param(required = true, name = "ApiKey", desc = "Ключ API"),
                @Param(required = true, name = "Token", desc = "Токен авторизации"),
                @Param(required = true, name = "CurrentPassword", desc = "Текущий пароль (хэш)"),
                @Param(required = true, name = "NewPassword", desc = "Новый пароль (хэш)"),
                @Param(required = true, name = "NewPasswordConfirm", desc = "Новый пароль - подтверждение (хэш)")},
            returns = @Return(items = {}))
    @Override
    public DataSet getData(HashMap params, JaxisContentType jct) throws
            ConnectionPoolNotFoundException, DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpBadRequestException,
            HttpInternalException, HttpUnauthorizedException, NotConfiguredException, NoSuchAlgorithmException,
            RightServiceException, StemException {
        String version = getParamValidator().getString(params, "Version", true); // версия протокола
        String token = getParamValidator().getString(params, "Token", true); // токен авторизации пользователя
        String currentPassword = getParamValidator().getString(params, "CurrentPassword", true); // текущий пароль
        String newPassword = getParamValidator().getString(params, "NewPassword", true); // новый пароль
        String newPasswordConfirm = getParamValidator().getString(params, "NewPasswordConfirm", true); // новый пароль (подтверждение)
        String apiKey = getParamValidator().getString(params, "ApiKey", true);
        getParamValidator().postValidate();

        PasswordValidator pv = new PasswordValidator();
        if (!pv.validate(newPassword)) {
            getParamValidator().
                    add(new ValidationErrorItem("BadPassword", "NewPassword", pv.getValidationMessage()));
            getParamValidator().postValidate();
        }

        // проверка ключа API
        ApiKeyHelper.getInstance().check(getDataSource(), apiKey);

        // проверка токена
        User user = getAuthService().getUser(token);

        // проверяем что ноый пароль подтвержден верно
        if (!newPassword.equals(newPasswordConfirm)) {
            throw new HttpBadRequestException("Новый пароль и его подтверждение не совпадают");
        }

        // проверяем текущий пароль
        if (!user.checkPassword(currentPassword)) {
            getParamValidator().
                    add(new ValidationErrorItem("Forbidden", "CurrentPassword", "Текущий пароль указан неверно."));
            throw new HttpForbiddenException(getParamValidator().toDataSet());
        }

        // обновляем
        String passwdHashNew = User.getPasswdHash(user.getPasswordSalt(), newPassword);

        try (Connection conn = getConnection()) {

            // обновляем пароль
            PreparedStatement ps = conn.prepareStatement("update sysuser set password = ? where id_sysuser = ?");
            ps.setString(1, passwdHashNew);
            ps.setInt(2, user.getId());
            ps.execute();
            conn.commit();
        }

        return null;
    }
}
