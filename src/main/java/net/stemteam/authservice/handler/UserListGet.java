/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.db.ParamBinder;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.common.Version;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.exception.RightServiceException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import net.stemteam.authservice.api.support.ParamHelper;
import net.stemteam.authservice.api.support.UserRequestPropogation;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.exception.UserNotFoundException;
import net.stemteam.jaxis.annotation.Method;
import net.stemteam.jaxis.annotation.Param;
import net.stemteam.jaxis.annotation.Return;

/**
 * Возвращает список данных пользователей (доступно только системному пользователю)
 */
public class UserListGet extends DataSetHandler {

    @Method(desc = "Возвращает список данных пользователей (доступно только системному пользователю)",
            params = {
                @Param(required = true, name = ParamHelper.PARAM_VERSION, desc = "Версия протокола"),
                @Param(required = true, name = ParamHelper.PARAM_APIKEY, desc = "Ключ API"),
                @Param(required = true, name = ParamHelper.PARAM_TOKEN, desc = "токен авторизации"),
                @Param(name = ParamHelper.PARAM_LASTDATE, desc
                        = "дата/время последнего изменения данных о пользователе (если передана - вернутся пользователи, "
                        + "в данных которых были произведены изменения позднее указанной даты/времени)"),
                @Param(name = ParamHelper.PARAM_USER_WITH_REFERED_USERS, desc
                        = "вернуть ли связанных пользователей, членов семьи и т.д. (0 - нет, 1 - да)"),
                @Param(name = ParamHelper.PARAM_USER_WITH_ENDPOINTS, desc = "вернуть ли список логинов (0 - нет, 1 - да)"),
                @Param(name = ParamHelper.PARAM_USER_WITH_ROLES, desc = "вернуть ли роли (0 - нет, 1 - да)"),
                @Param(name = ParamHelper.PARAM_USER_WITH_LAST_ACTION, desc = "вернуть ли дату последней активности (0 - нет, 1 - да)"),
                @Param(name = ParamHelper.PARAM_USER_WITH_REQUESTS, desc
                        = "вернуть ли открытые запросы пользователя, на удаление/изменение пароля и т.д. (0 - нет, 1 - да)")
            },
            returns = @Return(items = {}))
    @Override
    public DataSet getData(HashMap params, JaxisContentType jct) throws
            ConnectionPoolNotFoundException, DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpBadRequestException,
            HttpInternalException, HttpUnauthorizedException, NotConfiguredException, HttpNotFoundException,
            RightServiceException, UserNotAuthorizedException, UserNotFoundException {
        Version version = getParamValidator().getVersion(params, ParamHelper.PARAM_VERSION, true);
        String token = getParamValidator().getString(params, ParamHelper.PARAM_TOKEN, true);
        String apiKey = getParamValidator().getString(params, ParamHelper.PARAM_APIKEY, true);
        Timestamp lastDate = getParamValidator().getTimestamp(params, ParamHelper.PARAM_LASTDATE);
        Integer withReferedUsers = getParamValidator().getInt(params, ParamHelper.PARAM_USER_WITH_REFERED_USERS);
        Integer withEndpoints = getParamValidator().getInt(params, ParamHelper.PARAM_USER_WITH_ENDPOINTS);
        Integer withRoles = getParamValidator().getInt(params, ParamHelper.PARAM_USER_WITH_ROLES);
        Integer withLastAction = getParamValidator().getInt(params, ParamHelper.PARAM_USER_WITH_LAST_ACTION);
        Integer withRequests = getParamValidator().getInt(params, ParamHelper.PARAM_USER_WITH_REQUESTS);
        Long limit = getParamValidator().getLong(params, ParamHelper.PARAM_LIMIT);
        Long offset = getParamValidator().getLong(params, ParamHelper.PARAM_OFFSET);
        getParamValidator().postValidate();

        withReferedUsers = withReferedUsers == null ? 0 : withReferedUsers;
        withEndpoints = withEndpoints == null ? 0 : withEndpoints;
        withRoles = withRoles == null ? 0 : withRoles;
        withLastAction = withLastAction == null ? 0 : withLastAction;
        withRequests = withRequests == null ? 0 : withRequests;

        ApiKeyHelper.getInstance().check(getDataSource(), apiKey);

        User user = getAuthService().getAuthRepository().getUser(new User(token, null), UserRequestPropogation.Roles);
        if (!user.isSystem()) {
            throw new HttpForbiddenException(
                    "Только системные пользователи могут вызывать этот метод");
        }

        try (Connection conn = getConnection()) {
            // TODO можно реализовать разное поведение, в зависимости от версии постгреса, использовать мат.представление
            // DatabaseMetaData m = conn.getMetaData();
            // m.getDatabaseProductName(); 
            // m.getDatabaseProductVersion();

            String sql = "select u.guuid, u.first_name, u.last_name, u.middle_name, u.image_url as image, u.lastdate,"
                    + "   u.is_registered, u.is_disabled, u.birthday ";
            if (withEndpoints == 1) {
                sql += "  ,(select array_agg(e.notification_id) \n"
                        + "   from endpoint e \n"
                        + "   join notification_type t on e.id_notification_type = t.id_notification_type and t.code = 'sms'\n"
                        + "   where e.id_sysuser = u.id_sysuser) as phones,\n"
                        + "  (select array_agg(e.notification_id) \n"
                        + "   from endpoint e \n"
                        + "   join notification_type t on e.id_notification_type = t.id_notification_type and t.code = 'email'\n"
                        + "   where e.id_sysuser = u.id_sysuser) as emails\n";
            } else {
                sql += " ,null::varchar[] as phones, null::varchar[] as emails ";
            }
            if (withLastAction == 1) {
                sql += " ,(select max(s.lastdate) from syslogin s where s.id_sysuser = u.id_sysuser) as last_action_date ";
            } else {
                sql += " ,null::timestamp as last_action_date ";
            }
            if (withRoles == 1) {
                sql += "  ,(select array_agg(r.code) \n"
                        + "  from stem_right_userrole ur  \n"
                        + "  join stem_right_role r on ur.id_stem_right_role = r.id_stem_right_role \n"
                        + "  join stem_right_user uu on uu.id_stem_right_user = ur.id_stem_right_user\n"
                        + "  where uu.guuid = u.guuid::varchar) as roles ";
            } else {
                sql += " ,null::varchar[] as roles ";
            }
            sql += " from sysuser u "
                    + " where ((?::timestamp is null) or (u.lastdate > ?::timestamp)) ";
            if (limit != null) {
                sql += " limit ? ";
            }
            if (offset != null) {
                sql += " offset ? ";
            }

            try (PreparedStatement ps = conn.prepareStatement(sql)) {
                int num = 1;
                ParamBinder.setTimestamp(ps, num++, lastDate);
                ParamBinder.setTimestamp(ps, num++, lastDate);
                if (limit != null) {
                    ParamBinder.setLong(ps, num++, limit);
                }
                if (offset != null) {
                    ParamBinder.setLong(ps, num++, offset);
                }
                try (ResultSet rs = ps.executeQuery()) {
                    DataSet ds = new DataSet();
                    ds.pack(rs, null);
                    ds.doCamelColumnNames(true);
                    return ds;
                }
            }

        }
    }
}
