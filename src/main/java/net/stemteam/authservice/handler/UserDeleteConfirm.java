/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.common.UserRequestHelper;
import net.stemteam.authservice.common.UserRequestType;
import net.stemteam.authservice.entity.RefType;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.ValidationErrorItem;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.exception.RightServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import net.stemteam.authservice.api.support.UserRequestPropogation;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.exception.UserNotFoundException;
import net.stemteam.jaxis.annotation.Method;
import net.stemteam.jaxis.annotation.Param;
import net.stemteam.jaxis.annotation.Return;
import net.stemteam.jaxis.err.StemException;

/**
 * Подтверждение запроса удаления пользователем собственной учетной записи
 */
public class UserDeleteConfirm extends DataSetHandler {

    private final Logger logger = LoggerFactory.getLogger(UserDeleteConfirm.class);

    @Method(desc = "Удаление пользователя из системы системным пользователем",
            params = {
                @Param(required = true, name = "Version", desc = "Версия протокола"),
                @Param(required = true, name = "ApiKey", desc = "Ключ API"),
                @Param(required = true, name = "Token", desc = "токен авторизации"),
                @Param(name = "Guuid", desc = "удаляемый пользователь или член его семьи (если не задан - удаляем себя)"),
                @Param(required = true, name = "Code", desc = "код подтверждения")
            },
            returns = @Return(items = {}))
    @Override
    public DataSet getData(HashMap params, JaxisContentType jct) throws
            ConnectionPoolNotFoundException, DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpBadRequestException,
            HttpInternalException, HttpUnauthorizedException, NotConfiguredException, HttpNotFoundException,
            RightServiceException, UserNotAuthorizedException, UserNotFoundException, StemException {
        String version = getParamValidator().getString(params, "Version", true);
        String apiKey = getParamValidator().getString(params, "ApiKey", true);
        String token = getParamValidator().getString(params, "Token", true);
        String guuid = getParamValidator().getString(params, "Guuid"); // удаляемый пользователь (если не задан - удаляем себя)
        String code = getParamValidator().getString(params, "Code", true); // код подтверждения
        getParamValidator().postValidate();

        // проверяем валидность API-KEY
        ApiKeyHelper.getInstance().check(getDataSource(), apiKey);

        // проверка токена
        User user = getAuthService().getUser(token, UserRequestPropogation.ReferedUsers);

        if (guuid == null) {
            guuid = user.getGuuid();
        }

        // здесь нам нужно проверить что пользователь удаляет себя, либо одного 
        // из своих подчиненных, затем выслать ему оповещение с неким кодом для 
        // подтверждения действия.
        try (Connection conn = getConnection()) {
            // если кого-то из подчиненных
            if (!user.getGuuid().equals(guuid)) {
                // проверим, связан ли подчиненный
                if (!user.isRefered(new User(guuid), RefType.child)) {
                    throw new HttpForbiddenException("Пользователь не связан с удаляемым");
                }
            }

            // проверка корректности кода запроса пользователя
            if (!UserRequestHelper.codeExists(conn, UserRequestType.DELETE, user.getGuuid(), code)) {
                getParamValidator().add(new ValidationErrorItem("IncorrectCode", "Code",
                        "Проверочный код указан неверно"));
                getParamValidator().postValidate();
            }

            try {
                PreparedStatement ps = conn.prepareStatement(
                        "delete from syslogin where id_sysuser in "
                        + " (select a.id_sysuser from sysuser a where a.guuid = ?::uuid) ");
                ps.setString(1, guuid);
                ps.execute();

                PreparedStatement ps2 = conn.prepareStatement(
                        "delete from userref where id_sysuser in "
                        + "(select a.id_sysuser from sysuser a where a.guuid = ?::uuid) "
                        + "or id_sysuser_ref in (select a.id_sysuser from sysuser a where a.guuid = ?::uuid) ");
                ps2.setString(1, guuid);
                ps2.setString(2, guuid);
                ps2.execute();

                PreparedStatement ps5 = conn.prepareStatement(
                        "delete from notification where id_endpoint in "
                        + "(select e.id_endpoint from endpoint e join sysuser u on e.id_sysuser = u.id_sysuser "
                        + "where u.guuid = ?::uuid) ");
                ps5.setString(1, guuid);
                ps5.execute();

                PreparedStatement ps3 = conn.prepareStatement("delete from sysuser where guuid = ?::uuid ");
                ps3.setString(1, guuid);
                ps3.execute();

                conn.commit();
            } catch (SQLException ex) {
                conn.rollback();
                throw ex;
            }

            return null;
        }
    }
}
