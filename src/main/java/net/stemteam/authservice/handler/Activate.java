/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.common.UserRequestHelper;
import net.stemteam.authservice.common.UserRequestType;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataColumnType;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.ValidationErrorItem;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.exception.RightServiceException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.exception.UserNotFoundException;
import net.stemteam.jaxis.annotation.Method;
import net.stemteam.jaxis.annotation.Param;
import net.stemteam.jaxis.annotation.Return;
import net.stemteam.jaxis.err.HttpUnauthorizedException;

/**
 * Активация зарегистрированного пользователя в системе
 */
public class Activate extends DataSetHandler {

    @Method(desc = "Завершение регистрации, активация учетной записи клиента.",
            params = {
                @Param(required = true, name = "Version", desc = "Версия протокола"),
                @Param(required = true, name = "ApiKey", desc = "Ключ API"),
                @Param(required = true, name = "Login",
                        desc = "номер мобильного телефона в формате 71234567890 либо email"),
                @Param(required = true, name = "ActivationCode", desc = "код активации (присланный в SMS)")},
            returns = @Return(items = {}))
    @Override
    public DataSet getData(HashMap params, JaxisContentType jct) throws ConnectionPoolNotFoundException,
            DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpNotFoundException,
            NotConfiguredException, RightServiceException, UserNotAuthorizedException, UserNotFoundException, HttpUnauthorizedException {
        String Version = getParamValidator().getString(params, "Version", true);
        String Login = getParamValidator().getString(params, "Login", true);
        String activationCode = getParamValidator().getString(params, "ActivationCode", true);
        String ApiKey = getParamValidator().getString(params, "ApiKey", true);
        getParamValidator().postValidate();

        // проверяем валидность API-KEY
        ApiKeyHelper.getInstance().check(getDataSource(), ApiKey);

        Login = Login.toLowerCase();
        Login = Login.trim();

        // запросим пользователя, и если таковой имеется в неактивированном виде - активируем его
        User user = getAuthService().getAuthRepository().getUserByLogin(Login);

        if (user.isRegistered()) {
            throw new HttpForbiddenException(
                    "Пользователь с указанным логином уже подтвердил свою регистрацию в системе");
        }

        try (Connection conn = getConnection()) {
            // проверка корректности кода запроса пользователя
            if (!UserRequestHelper.codeExists(conn, UserRequestType.REGISTER, user.getGuuid(), activationCode)) {
                getParamValidator().add(new ValidationErrorItem("IncorrectCode", "ActivationCode",
                        "Проверочный код указан неверно"));
                getParamValidator().postValidate();
            }
            // активируем
            PreparedStatement ps = conn.prepareStatement("update sysuser set is_registered = 1 where id_sysuser = ?");
            ps.setInt(1, user.getId());
            ps.execute();

            // подтверждаем адреса регистрации с которых была произведена регистрация
            PreparedStatement ps3 = conn.prepareStatement("update endpoint set is_verified = 1 where id_sysuser = ? ");
            ps3.setInt(1, user.getId());
            ps3.execute();
            conn.commit();

            // возвращаем гуид
            DataSet ds = new DataSet();
            ds.createColumn("Id", DataColumnType.STRING);
            ds.createRecord().setString("Id", user.getGuuid());
            ds.setAsJson2Array(false);
            return ds;
        }
    }
}
