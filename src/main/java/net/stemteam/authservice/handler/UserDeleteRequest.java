/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.common.UserRequestHelper;
import net.stemteam.authservice.common.UserRequestType;
import net.stemteam.authservice.entity.RefType;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataColumnType;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.exception.RightServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import net.stemteam.authservice.api.support.UserRequestPropogation;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.exception.UserNotFoundException;
import net.stemteam.jaxis.annotation.Method;
import net.stemteam.jaxis.annotation.Param;
import net.stemteam.jaxis.annotation.Return;
import net.stemteam.jaxis.err.StemException;

/**
 * Запроса пользователем об удаления собственной учетной записи (согласно ФЗ). Здесь происходит формирование и отправка кода подтверждения.
 */
public class UserDeleteRequest extends DataSetHandler {

    private final Logger logger = LoggerFactory.getLogger(UserDeleteRequest.class);

    @Method(desc = "Запроса пользователем об удаления собственной учетной записи (согласно ФЗ). "
            + "Здесь происходит формирование и отправка кода подтверждения.",
            params = {
                @Param(required = true, name = "Version", desc = "Версия протокола"),
                @Param(required = true, name = "ApiKey", desc = "Ключ API"),
                @Param(required = true, name = "Token", desc = "токен авторизации"),
                @Param(name = "Guuid", desc = "удаляемый пользователь или член его семьи (если не задан - удаляем себя)")
            },
            returns = @Return(items = {}))
    @Override
    public DataSet getData(HashMap params, JaxisContentType jct) throws
            ConnectionPoolNotFoundException, DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpBadRequestException,
            HttpInternalException, HttpUnauthorizedException, NotConfiguredException, HttpNotFoundException,
            RightServiceException, UserNotAuthorizedException, UserNotFoundException, StemException {
        String version = getParamValidator().getString(params, "Version", true);
        String apiKey = getParamValidator().getString(params, "ApiKey", true);
        String token = getParamValidator().getString(params, "Token", true);
        String guuid = getParamValidator().getString(params, "Guuid");  // удаляемый пользователь (если не задан - удаляем себя)
        getParamValidator().postValidate();

        // проверяем валидность API-KEY
        ApiKeyHelper.getInstance().check(getDataSource(), apiKey);

        // проверка токена
        User user = getAuthService().getUser(token, UserRequestPropogation.ReferedUsers);

        if (guuid == null) {
            guuid = user.getGuuid();
        }

        // здесь нам нужно проверить что пользователь удаляет себя, либо одного 
        // из своих подчиненных, затем выслать ему оповещение с неким кодом для 
        // подтверждения действия.
        try (Connection conn = getConnection()) {
            // если кого-то из подчиненных
            if (!user.getGuuid().equals(guuid)) {
                // проверим, связан ли подчиненный
                //boolean refered = getAuthService().getAuthRepository().isRefered(user.getGuuid(), guuid, RefType.child);
                if (!user.isRefered(new User(guuid), RefType.child)) {
                    throw new HttpForbiddenException("Пользователь не связан с удаляемым");
                }
            }

            // поищем, нет ли уже неподтвержденного проверочного кода
            String code = UserRequestHelper.getCode(conn, UserRequestType.DELETE, user.getGuuid());

            // генерируем проверочный код, пишем его в базу, высылаем пользователю для продолжения
            if (code == null) {
                code = UserRequestHelper.addUserRequest(conn, UserRequestType.DELETE, user.getGuuid(), 300);
                conn.commit();
            }

            DataSet ds = new DataSet();
            ds.createColumn("Code", DataColumnType.STRING);
            ds.setAsJson2Array(false);
            ds.createRecord().setString("Code", code);
            return ds;
        }
    }
}
