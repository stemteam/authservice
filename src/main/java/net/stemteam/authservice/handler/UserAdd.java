/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.entity.Endpoint;
import net.stemteam.authservice.entity.enums.NotificationTypeCode;
import net.stemteam.authservice.entity.RefType;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataColumnType;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.ValidationErrorItem;
import net.stemteam.jaxis.common.Version;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.exception.RightServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import static net.stemteam.authservice.entity.User.prepareToken;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.exception.UserNotFoundException;
import net.stemteam.jaxis.annotation.Method;
import net.stemteam.jaxis.annotation.Param;
import net.stemteam.jaxis.annotation.Return;

/**
 * Добавление пользователя в систему другим пользователем (системным)
 */
public class UserAdd extends DataSetHandler {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private static final String PARAM_REF_TYPE = "RefType";
    private static final String PARAM_EMAIL = "Email";
    private static final String PARAM_PHONE = "Phone";
    private static final String PARAM_PASSWORD = "Password";

    @Method(desc = "Добавление пользователя в систему другим пользователем (системным)",
            params = {
                @Param(required = true, name = "Version", desc = "Версия протокола"),
                @Param(required = true, name = "ApiKey", desc = "Ключ API"),
                @Param(required = true, name = "Token", desc = "токен авторизации"),
                @Param(name = "Phone", desc = "Номер мобильного телефона в формате 71234567890"),
                @Param(name = "Email", desc = "Адрес электронной почты"),
                @Param(name = "Password", desc = "Хэш пароля (SHA1)"),
                @Param(name = "FirstName", desc = "Имя"),
                @Param(name = "LastName", desc = "Фамилия"),
                @Param(name = "MiddleName", desc = "Отчество"),
                @Param(name = "Image", desc = "Ссылка на изображение"),
                @Param(name = "NotificationType", desc = "куда получать уведомления (sms,email)"),
                @Param(name = "Birthday", desc = "дата рождения", type = DataColumnType.TIMESTAMP)
            },
            returns = @Return(items = {
                @Param(name = "Id", desc = "Уникальный идентификатор созданного пользователя (guuid)")
            }))
    @Override
    public DataSet getData(HashMap params, JaxisContentType jct) throws
            ConnectionPoolNotFoundException, DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpBadRequestException,
            HttpInternalException, HttpUnauthorizedException, NotConfiguredException, NoSuchAlgorithmException,
            RightServiceException, UserNotAuthorizedException, UserNotFoundException {
        Version version = getParamValidator().getVersion(params, "Version", true);
        String ApiKey = getParamValidator().getString(params, "ApiKey", true);
        String Token = getParamValidator().getString(params, "Token", true);
        String Phone = getParamValidator().getString(params, PARAM_PHONE);
        String Email = getParamValidator().getString(params, PARAM_EMAIL);
        String Passwd = getParamValidator().getString(params, PARAM_PASSWORD);
        String notificationTypeCode = getParamValidator().getString(params, "NotificationType"); // способ оповещения пользователя
        String refTypeStr = getParamValidator().getString(params, PARAM_REF_TYPE); // тип отношений создателя с этим пользователем
        String firstName = getParamValidator().getString(params, "FirstName");
        String lastName = getParamValidator().getString(params, "LastName");
        String middleName = getParamValidator().getString(params, "MiddleName");
        String image = getParamValidator().getString(params, "Image");
        Timestamp birthday = getParamValidator().getTimestamp(params, "Birthday");
        getParamValidator().postValidate();

        // определяем тип отношения пользователя, если нужно
        RefType refType = null;
        if (refTypeStr != null) {
            try {
                refType = RefType.getByCode(refTypeStr);
            } catch (IllegalArgumentException ex) {
                getParamValidator().add(new ValidationErrorItem("UnsupportedRefType", PARAM_REF_TYPE,
                        "Unsupported RefType = " + refTypeStr));
                getParamValidator().postValidate();
            }
        } else {
            // если не указан RefType - должны быть заполнены поля Email, Phone, Password
            Phone = getParamValidator().getString(params, PARAM_PHONE, true);
            Email = getParamValidator().getString(params, PARAM_EMAIL, true);
            Passwd = getParamValidator().getString(params, PARAM_PASSWORD, true);
            getParamValidator().postValidate();
        }

        NotificationTypeCode nType = NotificationTypeCode.sms;
        if ((notificationTypeCode != null) && (!"".equals(notificationTypeCode))) {
            nType = NotificationTypeCode.valueOf(notificationTypeCode);
        }

        // проверяем валидность API-KEY
        ApiKeyHelper.getInstance().check(getDataSource(), ApiKey);

        if (Email != null) {
            Email = Email.toLowerCase();
            Email = Email.trim();
        }
        if (Phone != null) {
            Phone = Phone.toLowerCase();
            Phone = Phone.trim();
        }

        // проверка токена
        User usr = getAuthService().getAuthRepository().getUser(new User(Token, null));

        // если передан телефон - проверим, нет ли пользователя с таким же телефоном
        if (Phone != null) {
            try {
                User u = getAuthService().getAuthRepository().getUserByLogin(Phone);
                throw new HttpForbiddenException("Пользователь уже существует");
            } catch (UserNotFoundException ex) {
                // all right
            }
        }

        // добавим нового пользователя
        String guuid = prepareToken();
        String passwordSalt = guuid;
        User user = new User(null, guuid);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        user.setPasswordSalt(passwordSalt);
        user.setRegistered(true);
        user.setImage(image);
        user.setBirthday(birthday);

        try (Connection conn = getConnection()) {

            try {
                if (refType == RefType.child) {

                    // хэш случайный, юзер отключен
                    user.setPasswordHash(UUID.randomUUID().toString());
                    user.setDisabled(true);

                    // добавляем ребенка без возможности входа в систему
                    user = getAuthService().getAuthRepository().addUser(conn, user);

                    // создаем связь
                    PreparedStatement ps = conn.prepareStatement(
                            "insert into userref (id_sysuser, id_sysuser_ref, id_reftype) values (?,?, (select id_reftype from reftype where code = ?))");
                    ps.setInt(1, usr.getId());
                    ps.setInt(2, user.getId());
                    ps.setString(3, RefType.child.getCode());
                    ps.execute();

                } else if (refType == RefType.slave) {
                    // считаем хэш пароля
                    String passwdHash = User.getPasswdHash(passwordSalt, Passwd);

                    // хэш посчитан, юзер включен
                    user.setPasswordHash(passwdHash);
                    user.setDisabled(false);
                    
                    // добавляем без возможности входа в систему
                    user = getAuthService().getAuthRepository().addUser(conn, user);

                    // создаем связь
                    PreparedStatement ps = conn.prepareStatement(
                            "insert into userref (id_sysuser, id_sysuser_ref, id_reftype) values (?,?, (select id_reftype from reftype where code = ?))");
                    ps.setInt(1, usr.getId());
                    ps.setInt(2, user.getId());
                    ps.setString(3, RefType.slave.getCode());
                    ps.execute();

                } else {
                    // считаем хэш пароля
                    String passwdHash = User.getPasswdHash(passwordSalt, Passwd);

                    // хэш посчитан, юзер включен
                    user.setPasswordHash(passwdHash);
                    user.setDisabled(false);

                    // создаем пользователя
                    user = getAuthService().getAuthRepository().addUser(conn, user);
                }

                // оповещение
                // добавляем контакты для оповещения (телефон если указан)
                if (Phone != null) {
                    Endpoint epSMS = new Endpoint(null, NotificationTypeCode.sms, Phone, null,
                            nType == NotificationTypeCode.sms, true, true);
                    getAuthService().getAuthRepository().upsertEndpoint(conn, user.getId(), epSMS);
                    if (user.getEndpointList() == null) {
                        user.setEndpoints(new ArrayList<Endpoint>());
                    }
                    user.getEndpointList().add(epSMS);
                }

                // добавляем контакты для оповещения (email если указан)
                if (Email != null) {
                    Endpoint epEmail = new Endpoint(null, NotificationTypeCode.email, Email,
                            null,
                            nType == NotificationTypeCode.email, true, true);
                    getAuthService().getAuthRepository().upsertEndpoint(conn, user.getId(), epEmail);
                    if (user.getEndpointList() == null) {
                        user.setEndpoints(new ArrayList<Endpoint>());
                    }
                    user.getEndpointList().add(epEmail);
                }
                conn.commit();
            } catch (SQLException ex) {
                // привет PG JDBC
                conn.rollback();
                throw ex;
            }
        }

        DataSet ds = new DataSet();
        ds.createColumn("Id", DataColumnType.STRING);
        ds.createRecord().setString("Id", guuid);
        return ds;
    }
}
