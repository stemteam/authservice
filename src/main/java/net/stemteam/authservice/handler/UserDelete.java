/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.exception.RightServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import net.stemteam.authservice.api.support.UserRequestPropogation;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.exception.UserNotFoundException;
import net.stemteam.jaxis.annotation.Method;
import net.stemteam.jaxis.annotation.Param;
import net.stemteam.jaxis.annotation.Return;

/**
 * Удаление пользователя из системы системным пользователем
 */
public class UserDelete extends DataSetHandler {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private static final String PARAM_GUUID = "Guuid";

    @Method(desc = "Удаление пользователя из системы системным пользователем",
            params = {
                @Param(required = true, name = "Version", desc = "Версия протокола"),
                @Param(required = true, name = "ApiKey", desc = "Ключ API"),
                @Param(required = true, name = "Token", desc = "токен авторизации"),
                @Param(required = true, name = PARAM_GUUID, desc = "уникальный идентификатор удаляемого пользователя (guuid)")
            },
            returns = @Return(items = {}))
    @Override
    public DataSet getData(HashMap params, JaxisContentType jct) throws
            ConnectionPoolNotFoundException, DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpBadRequestException,
            HttpInternalException, HttpUnauthorizedException, NotConfiguredException, RightServiceException, HttpNotFoundException, UserNotFoundException, UserNotAuthorizedException {
        String Version = getParamValidator().getString(params, "Version", true);
        String ApiKey = getParamValidator().getString(params, "ApiKey", true);
        String Token = getParamValidator().getString(params, "Token", true);
        String guuid = getParamValidator().getString(params, PARAM_GUUID, true);
        getParamValidator().postValidate();

        // проверяем валидность API-KEY
        ApiKeyHelper.getInstance().check(getDataSource(), ApiKey);

        // проверка токена
        User usr = getAuthService().getAuthRepository().getUser(new User(Token, null), UserRequestPropogation.Roles);

        if (!usr.isSystem()) {
            throw new HttpForbiddenException("Действие разрешено только системным пользователям");
        }

        try (Connection conn = getConnection()) {
            PreparedStatement ps1 = conn.prepareStatement(
                    "delete from syslogin where id_sysuser = (select a.id_sysuser from sysuser a where a.guuid = ?::uuid)");
            ps1.setString(1, guuid);
            ps1.execute();

            PreparedStatement ps2 = conn.prepareStatement(
                    "delete from userref "
                    + " where id_sysuser in (select a.id_sysuser from sysuser a where a.guuid = ?::uuid) "
                    + " or id_sysuser_ref in (select a.id_sysuser from sysuser a where a.guuid = ?::uuid) ");
            ps2.setString(1, guuid);
            ps2.setString(2, guuid);
            ps2.execute();

            PreparedStatement ps = conn.prepareStatement("delete from sysuser where guuid = ?::uuid");
            ps.setString(1, guuid);
            try {
                ps.execute();
                conn.commit();

                return null;
            } catch (SQLException ex) {
                // привет PG JDBC
                conn.rollback();
                throw ex;
            }
        }
    }
}
