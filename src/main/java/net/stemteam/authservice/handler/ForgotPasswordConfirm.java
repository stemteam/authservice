/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.common.PasswordValidator;
import net.stemteam.authservice.common.UserRequestHelper;
import net.stemteam.authservice.common.UserRequestType;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.ValidationErrorItem;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.exception.RightServiceException;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Random;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.exception.UserNotFoundException;
import net.stemteam.jaxis.annotation.Method;
import net.stemteam.jaxis.annotation.Param;
import net.stemteam.jaxis.annotation.Return;
import net.stemteam.jaxis.err.HttpUnauthorizedException;

/**
 * Забыл пароль (восстановление на телефон)
 */
public class ForgotPasswordConfirm extends DataSetHandler {

    static final String AB = "0123456789abcdefghijklmnpqrstuvwxyz";
    static Random rnd = new Random();

    public static String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }
        return sb.toString();
    }

    @Method(desc = "Забыл пароль (изменение пароля). Пользователь изменяет свой пароль на основании проверочного кода.",
            params = {
                @Param(required = true, name = "Version", desc = "Версия протокола"),
                @Param(required = true, name = "ApiKey", desc = "Ключ API"),
                @Param(required = true, name = "Login", desc = "номер телефона в формате 7XXXXXXXXXX либо email"),
                @Param(required = true, name = "Code", desc = "Проверочный код"),
                @Param(required = true, name = "NewPassword", desc = "Новый пароль (SHA1)")},
            returns = @Return(items = {}))
    @Override
    public DataSet getData(HashMap params, JaxisContentType jct) throws
            ConnectionPoolNotFoundException, DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpNotFoundException,
            HttpInternalException, NotConfiguredException, NoSuchAlgorithmException, RightServiceException, UserNotAuthorizedException, UserNotFoundException, HttpUnauthorizedException {
        String Version = getParamValidator().getString(params, "Version", true);
        String Login = getParamValidator().getString(params, "Login", true); // номер телефона
        String code = getParamValidator().getString(params, "Code", true); // проверочный код
        String password = getParamValidator().getString(params, "NewPassword", true); // новый пароль
        String ApiKey = getParamValidator().getString(params, "ApiKey", true);
        getParamValidator().postValidate();

        PasswordValidator pv = new PasswordValidator();
        if (!pv.validate(password)) {
            getParamValidator().
                    add(new ValidationErrorItem("BadPassword", "NewPassword", pv.getValidationMessage()));
            getParamValidator().postValidate();
        }

        // проверяем валидность API-KEY
        ApiKeyHelper.getInstance().check(getDataSource(), ApiKey);

        User user = getAuthService().getAuthRepository().getUserByLogin(Login);

        // считаем хэш пароля
        String passwdHash = User.getPasswdHash(user.getPasswordSalt(), password);

        try (Connection conn = getConnection()) {
            // проверка корректности кода запроса пользователя
            if (!UserRequestHelper.codeExists(conn, UserRequestType.PASSRECOVER, user.getGuuid(), code)) {
                getParamValidator().add(new ValidationErrorItem("IncorrectCode", "Code",
                        "Проверочный код указан неверно"));
                getParamValidator().postValidate();
            }
            // обновляем пароль
            PreparedStatement ps = conn.prepareStatement("update sysuser set password = ? where id_sysuser = ?");
            ps.setString(1, passwdHash);
            ps.setInt(2, user.getId());
            ps.execute();
            conn.commit();
        }

        return null;
    }
}
