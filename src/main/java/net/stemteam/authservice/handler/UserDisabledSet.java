/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.entity.RefType;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.exception.RightServiceException;

import java.sql.SQLException;
import java.util.HashMap;
import net.stemteam.authservice.api.support.UserRequestPropogation;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.exception.UserNotFoundException;

/**
 * Включение/отключение пользователя
 */
public class UserDisabledSet extends DataSetHandler {

    private static final String PARAM_TOKEN = "Token";
    private static final String PARAM_GUUID = "Guuid";
    private static final String PARAM_VERSION = "Version";
    private static final String PARAM_APIKEY = "ApiKey";
    private static final String PARAM_DISABLED = "IsDisabled";

    @Override
    public DataSet getData(HashMap params, JaxisContentType jct) throws
            ConnectionPoolNotFoundException, DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpBadRequestException,
            HttpInternalException, HttpUnauthorizedException, NotConfiguredException, HttpNotFoundException,
            RightServiceException, UserNotAuthorizedException, UserNotFoundException {
        String Version = getParamValidator().getString(params, PARAM_VERSION, true);
        String Token = getParamValidator().getString(params, PARAM_TOKEN, true);
        String ApiKey = getParamValidator().getString(params, PARAM_APIKEY, true);
        String guuid = getParamValidator().getString(params, PARAM_GUUID, true);
        Integer isDisabled = getParamValidator().getInt(params, PARAM_DISABLED, true);
        getParamValidator().postValidate();

        ApiKeyHelper.getInstance().check(getDataSource(), ApiKey);

        User user = getAuthService().getAuthRepository().getUser(new User(Token, null), UserRequestPropogation.ReferedUsers,
                UserRequestPropogation.Roles);

        // если указан гуид - проверим что пользователь не супер-админ и не связан с переданным гуидом
        User targetUser = new User(null, guuid);
        if ((!user.isRefered(targetUser, RefType.slave)) && (!user.isRefered(targetUser, RefType.child))
                && (!user.isSystem())) {
            throw new HttpForbiddenException(
                    "Вы не связаны с изменяемым пользователем и/или не являетесь суперпользователем");
        }

        if (user.getGuuid().equals(guuid)) {
            throw new HttpForbiddenException("Запрещено изменение собственного состояния учетной записи");
        }

        getAuthService().getAuthRepository().disableUser(targetUser, isDisabled != 0);

        return null;
    }
}
