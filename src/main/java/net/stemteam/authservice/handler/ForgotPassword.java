/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.handler;

import net.stemteam.authservice.api.support.UserRequestPropogation;
import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.entity.Endpoint;
import net.stemteam.authservice.common.NotificationManager;
import net.stemteam.authservice.common.UserRequestHelper;
import net.stemteam.authservice.common.UserRequestType;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.exception.RightServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.exception.UserNotFoundException;

import static net.stemteam.authservice.notification.handler.NotifyForce.DEFAULT_SMS_SIGN;
import net.stemteam.jaxis.annotation.Method;
import net.stemteam.jaxis.annotation.Param;
import net.stemteam.jaxis.annotation.Return;
import net.stemteam.jaxis.err.HttpUnauthorizedException;

/**
 * Забыл пароль (восстановление на телефон)
 */
public class ForgotPassword extends DataSetHandler {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Method(desc = "Забыл пароль (восстановление на телефон). Шлет СМС-сообщение с проверочным "
            + "кодом на указанный номер телефона. Впоследствии можно будет изменить пароль пользователя, "
            + "воспользовавшись проверочным кодом.",
            params = {
                @Param(required = true, name = "Version", desc = "Версия протокола"),
                @Param(required = true, name = "ApiKey", desc = "Ключ API"),
                @Param(required = true, name = "Login", desc = "номер телефона в формате 7XXXXXXXXXX либо email"),
                @Param(name = "ServiceName", desc = "название сервиса (используется в оповещениях)"),
                @Param(name = "Host", desc = "хост (используется в оповещениях)"),
                @Param(name = "SmsSign", desc
                        = "подпись отправителя в СМС (вместо номера телефона, по договоренности с поставщиком услуги отправки коротких сообщений)")
            },
            returns = @Return(desc = "Отправляет смс с кодом подтверждения на номер пользователя "
                    + "(код используется в ForgotPasswordConfirm)",
                    items = {}))
    @Override
    public DataSet getData(HashMap params, JaxisContentType jct) throws
            ConnectionPoolNotFoundException, DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpNotFoundException,
            HttpInternalException, NotConfiguredException, RightServiceException, UserNotAuthorizedException,
            UserNotFoundException, HttpUnauthorizedException {
        String Version = getParamValidator().getString(params, "Version", true);
        String Login = getParamValidator().getString(params, "Login", true);
        String ApiKey = getParamValidator().getString(params, "ApiKey", true);
        String ServiceName = getParamValidator().getString(params, "ServiceName"); // название сервиса (используется в оповещениях)
        String Host = getParamValidator().getString(params, "Host"); // хост (используется в оповещениях)
        String smsSign = getParamValidator().getString(params, "SmsSign");
        getParamValidator().postValidate();

        if (ServiceName == null) {
            ServiceName = "Сервис";
        }

        if (smsSign == null) {
            smsSign = DEFAULT_SMS_SIGN;
        }

        // проверка ключа API
        ApiKeyHelper.getInstance().check(getDataSource(), ApiKey);

        Login = Login.toLowerCase();
        Login = Login.trim();

        // грузим юзера
        User user = null;
        Endpoint endpoint = null;
        user = getAuthService().getAuthRepository().getUserByLogin(Login, UserRequestPropogation.Endpoints);
        for (Endpoint ep : user.getEndpointList()) {
            if (Login.equals(ep.getNotificationId())) {
                endpoint = ep;
                break;
            }
        }

        try (Connection conn = getConnection()) {
            // поищем, нет ли уже неподтвержденного проверочного кода
            String code = UserRequestHelper.getCode(conn, UserRequestType.PASSRECOVER, user.getGuuid());

            // генерируем проверочный код, пишем его в базу, высылаем пользователю для продолжения
            if (code == null) {
                code = UserRequestHelper.addUserRequest(conn, UserRequestType.PASSRECOVER, user.getGuuid(),
                        300);
            }

            StringBuilder message = new StringBuilder();
            message.append("Доброго времени суток.\n\n");
            message.append("В систему ").append(ServiceName).append(" по адресу http://").append(Host).append(
                    " поступил запрос на восстановление пароля для вашей учетной записи,\n");
            message.append("в котором был указал Ваш электронный адрес (e-mail).\n");
            message.append("\n");
            message.append("Ваш проверочный код для восстановления доступа к Вашему аккаунту: ");
            message.append(code);
            message.append("\n");
            message.append(
                    "Если у Вас возникли проблемы - пожалуйста, свяжитесь со Службой поддержки ").append(ServiceName);
            message.append(" по адресу support@").append(Host).append(".\n");
            message.append("\n");
            message.append("Если Вы не понимаете, о чем идет речь — просто проигнорируйте это сообщение!\n");
            message.append("\n");
            message.append("Служба поддержки ").append(ServiceName).append(".");

            try {
                String emailTitle = ServiceName + " - запрос восстановление пароля";
                String smsMessage = ServiceName + ". Код для смены пароля: " + code;
//                NotificationManager.getInstance().notify(user, smsMessage, null, emailTitle, message.toString(),
//                        null, null, null, null, nType, true, smsSign, true);
                NotificationManager.getInstance().notify(endpoint, user, smsMessage, null, emailTitle, message.
                        toString(),
                        null, null, null, null, smsSign);
            } catch (Exception ex) {
                conn.rollback();
                log.error("Не удалось отправить оповещение");
                log.error("{} {} {}", Login, ex);
                throw new HttpInternalException(
                        "Не удалось отправить SMS подтвержение регистрации. Попробуйте выполнить регистрацию позже.");
            }

            conn.commit();
        }

        return null;
    }
}
