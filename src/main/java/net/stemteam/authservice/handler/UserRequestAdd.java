package net.stemteam.authservice.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.common.UserRequestHelper;
import net.stemteam.authservice.common.UserRequestType;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataColumnType;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.annotation.Method;
import net.stemteam.jaxis.annotation.Param;
import net.stemteam.jaxis.annotation.Return;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.exception.RightServiceException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import net.stemteam.authservice.api.support.UserRequestPropogation;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.exception.UserNotFoundException;

/**
 * Создает пользовательский запрос (сменить пароль, забыл пароль, активация и т.д.), возвращает код. Ограничение:
 * необходимо быть системным пользователем.
 */
public class UserRequestAdd extends DataSetHandler {

    public static final String PARAM_VERSION = "Version";
    public static final String PARAM_TOKEN = "Token";
    public static final String PARAM_APIKEY = "ApiKey";
    public static final String PARAM_LOGIN = "Login";
    public static final String PARAM_TYPE = "Type";
    public static final String PARAM_EXPIRATION_TIME_SEC = "ExpirationTimeSec";

    public static final String FIELD_CODE = "Code";

    @Method(desc = "Создает пользовательский запрос (сменить пароль, забыл пароль, активация и т.д.), возвращает код. "
            + "Ограничение: необходимо быть системным пользователем.",
            params = {
                @Param(required = true, name = PARAM_VERSION, desc = "версия протокола"),
                @Param(required = true, name = PARAM_APIKEY, desc = "ключ API"),
                @Param(required = true, name = PARAM_TOKEN, desc = "токен авторизации"),
                @Param(required = true, name = PARAM_LOGIN, desc = "логин изменяемого пользователя"),
                @Param(required = true, name = PARAM_TYPE, desc
                        = "тип пользовательского запроса (register, passrecover, delete)"),
                @Param(required = true, name = PARAM_EXPIRATION_TIME_SEC, type = DataColumnType.INT32, desc
                        = "время жизни запроса (секунд)")
            },
            returns = @Return(
                    items = {
                        @Param(name = FIELD_CODE, desc = "код запроса")
                    }))
    @Override
    public DataSet getData(HashMap params, JaxisContentType contentType)
            throws HttpUnauthorizedException, HttpForbiddenException,
            HttpInternalException, HttpNotFoundException,
            HttpBadRequestException,
            ConnectionPoolNotFoundException, DataSetException,
            CloseDbConnectionException, SQLException,
            NotConfiguredException, RightServiceException, UserNotAuthorizedException, UserNotFoundException {
        String version = getParamValidator().getString(params, PARAM_VERSION, true);
        String token = getParamValidator().getString(params, PARAM_TOKEN, true);
        String apiKey = getParamValidator().getString(params, PARAM_APIKEY, true);
        String login = getParamValidator().getString(params, PARAM_LOGIN, true);
        String type = getParamValidator().getString(params, PARAM_TYPE, true);
        Integer expirationTimeSec = getParamValidator().getInt(params, PARAM_EXPIRATION_TIME_SEC, true);
        getParamValidator().postValidate();

        login = login.trim().toLowerCase();

        ApiKeyHelper.getInstance().check(getDataSource(), apiKey);

        // проверка токена
        User usr = getAuthService().getAuthRepository().getUser(new User(token, null), UserRequestPropogation.Roles);
        if (!usr.isSystem()) {
            throw new HttpForbiddenException("Действие разрешено только системным пользователям");
        }

        // тащим пользователя
        User requestingUser = getAuthService().getAuthRepository().getUserByLogin(login);

        try (Connection conn = getConnection()) {
            String code = UserRequestHelper.addUserRequest(conn, UserRequestType.getByCode(type), requestingUser.
                    getGuuid(), expirationTimeSec);
            conn.commit();
            DataSet ds = new DataSet();
            ds.createColumn(FIELD_CODE, DataColumnType.STRING);
            ds.createRecord().setString(FIELD_CODE, code);
            return ds;
        }
    }
}
