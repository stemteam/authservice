/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.entity.Endpoint;
import net.stemteam.authservice.common.NotificationManager;
import net.stemteam.authservice.entity.enums.NotificationTypeCode;
import net.stemteam.authservice.common.PasswordValidator;
import net.stemteam.authservice.common.UserRequestHelper;
import net.stemteam.authservice.common.UserRequestType;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataColumnType;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.ValidationErrorItem;
import net.stemteam.jaxis.common.Version;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.NotConfiguredException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static net.stemteam.authservice.entity.User.prepareToken;
import static net.stemteam.authservice.notification.handler.NotifyForce.DEFAULT_SMS_SIGN;
import net.stemteam.jaxis.annotation.Method;
import net.stemteam.jaxis.annotation.Param;
import net.stemteam.jaxis.annotation.Return;

/**
 * Регистрация пользователя в системе
 */
public class Register extends DataSetHandler {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Method(desc = "Регистрация пользователя в системе",
            params = {
                @Param(required = true, name = "Version", desc = "Версия протокола"),
                @Param(required = true, name = "ApiKey", desc = "Ключ API"),
                @Param(required = true, name = "Phone", desc = "Номер мобильного телефона в формате 71234567890"),
                @Param(required = true, name = "Email", desc = "Адрес электронной почты"),
                @Param(required = true, name = "Password", desc = "Хэш пароля (SHA1)"),
                @Param(name = "FirstName", desc = "Имя"),
                @Param(name = "LastName", desc = "Фамилия"),
                @Param(name = "MiddleName", desc = "Отчество"),
                @Param(name = "Image", desc = "Ссылка на изображение"),
                @Param(name = "ServiceName", desc = "название сервиса (используется в оповещениях для формирования сообщения)"),
                @Param(name = "Host", desc = "хост (используется в оповещениях для формирования сообщения)"),
                @Param(name = "SmsSign", desc
                        = "подпись отправителя в СМС (вместо номера телефона, по договоренности с поставщиком услуги отправки коротких сообщений)"),
                @Param(name = "NotificationType", desc = "куда получать уведомления (sms,email)")
            },
            returns = @Return(items = {}))
    @Override
    public DataSet getData(HashMap params, JaxisContentType jct) throws
            ConnectionPoolNotFoundException, DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpBadRequestException,
            HttpInternalException, NotConfiguredException, HttpNotFoundException, NoSuchAlgorithmException {
        Version version = getParamValidator().getVersion(params, "Version", true);
        String ApiKey = getParamValidator().getString(params, "ApiKey", true);
        String Phone = getParamValidator().getString(params, "Phone", true);
        String Email = getParamValidator().getString(params, "Email", true);
        String Passwd = getParamValidator().getString(params, "Password", true);
        String firstName = getParamValidator().getString(params, "FirstName");
        String lastName = getParamValidator().getString(params, "LastName");
        String middleName = getParamValidator().getString(params, "MiddleName");
        String image = getParamValidator().getString(params, "Image");
        String ServiceName = getParamValidator().getString(params, "ServiceName"); // название сервиса (используется в оповещениях)
        String Host = getParamValidator().getString(params, "Host"); // хост (используется в оповещениях)
        String notificationTypeCode = getParamValidator().getString(params, "NotificationType"); // способ оповещения пользователя
        String smsSign = getParamValidator().getString(params, "SmsSign");
        getParamValidator().postValidate();

        PasswordValidator pv = new PasswordValidator();
        if (!pv.validate(Passwd)) {
            getParamValidator().
                    add(new ValidationErrorItem("BadPassword", "Password", pv.getValidationMessage()));
            getParamValidator().postValidate();
        }

        if (ServiceName == null) {
            ServiceName = "Сервис";
        }

        if (smsSign == null) {
            smsSign = DEFAULT_SMS_SIGN;
        }

        NotificationTypeCode nType = NotificationTypeCode.sms;
        if (notificationTypeCode != null && !notificationTypeCode.isEmpty()) {
            nType = NotificationTypeCode.valueOf(notificationTypeCode);
        }

        // проверяем валидность API-KEY
        ApiKeyHelper.getInstance().check(getDataSource(), ApiKey);

        Email = Email.toLowerCase();
        Email = Email.trim();
        Phone = Phone.trim();

        // проверим телефон
        User.checkPhone(Phone);

        // проверим email
        if (!User.isValidEmail(Email)) {
            throw new HttpBadRequestException("Email не корректен");
        }

        // проверим, нет ли в базе пользователя с таким телефоном или email
        if (getAuthService().getAuthRepository().isEmailExists(Email)) {
            getParamValidator().add(new ValidationErrorItem("Forbidden", "Email",
                    "Пользователь с указанным Email уже зарегистрирован в системе"));
            getParamValidator().postValidate();
        }
        if (getAuthService().getAuthRepository().isPhoneExists(Phone)) {
            getParamValidator().add(new ValidationErrorItem("Forbidden", "Phone",
                    "Пользователь с указанным телефоном уже зарегистрирован в системе"));
            getParamValidator().postValidate();
        }

        String guuid = prepareToken();
        String passwordSalt = guuid;

        String passwdHash = User.getPasswdHash(passwordSalt, Passwd);

        try (Connection conn = getConnection()) {
            // чтобы чувак не спамил СМС-ками делаем проверку на создание записи, и если еще не прошло 5 минут - делаем предупреждение
            PreparedStatement ps3 = conn.prepareStatement(
                    "select round(extract(epoch from localtimestamp) - extract(epoch from su.createdate)) as sec "
                    + " from sysuser su"
                    + " join endpoint ep on su.id_sysuser = ep.id_sysuser "
                    + " join notification_type nt on ep.id_notification_type = nt.id_notification_type "
                    + " where su.is_registered = 0 "
                    + "   and ((nt.code = 'email' and ep.notification_id = ?) "
                    + "   or (nt.code = 'sms' and ep.notification_id = ?))");
            ps3.setString(1, Email);
            ps3.setString(2, Phone);
            try (ResultSet rs3 = ps3.executeQuery()) {
                if (rs3.next()) {
                    double sec = rs3.getDouble("SEC");
                    if (sec < 30.0) {
                        int delta = (int) (30 - sec);
                        throw new HttpBadRequestException(
                                "Ваша предыдущая попытка регистрациибыла совсем недавно. Попробуйте через " + String.
                                valueOf(delta) + " секунд.");
                    }
                }
            }

            try {
                // удаляем предыдущие попытки регистрации
                PreparedStatement ps = conn.prepareStatement("delete from sysuser su "
                        + " where su.is_registered = 0 and id_sysuser in "
                        + " (select id_sysuser from endpoint ep "
                        + " join notification_type nt on ep.id_notification_type = nt.id_notification_type "
                        + " where ((nt.code = 'email' and ep.notification_id = ? ) "
                        + "   or (nt.code = 'sms' and ep.notification_id = ?)))");
                ps.setString(1, Email);
                ps.setString(2, Phone);
                ps.execute();

                // добавляем пользователя
                // создаем нового пользователя
                User user = new User(null, guuid);
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setMiddleName(middleName);
                user.setPasswordSalt(passwordSalt);
                user.setPasswordHash(passwdHash);
                user.setRegistered(false);
                user.setDisabled(false);
                user.setImage(image);

                user = getAuthService().getAuthRepository().addUser(conn, user);

                // добавляем запрос
                String activationCode = UserRequestHelper.addUserRequest(conn, UserRequestType.REGISTER, guuid, 300);

                // оповещение
                // добавляем контакты для оповещения
                Endpoint epSMS = new Endpoint(null, NotificationTypeCode.email, Email, null,
                        nType
                        == NotificationTypeCode.email, false, true);
                Endpoint epEmail = new Endpoint(null, NotificationTypeCode.sms, Phone, null,
                        nType
                        == NotificationTypeCode.sms, false, true);

                getAuthService().getAuthRepository().upsertEndpoint(conn, user.getId(), epSMS);
                getAuthService().getAuthRepository().upsertEndpoint(conn, user.getId(), epEmail);

                conn.commit();

                List<Endpoint> endpoints = new ArrayList<>();
                endpoints.add(epSMS);
                endpoints.add(epEmail);
                user.setEndpoints(endpoints);

                StringBuilder message = new StringBuilder();
                message.append("Добрый день.\n\n");
                message.append("В системе").append(ServiceName).append(" по адресу ").append(Host).append(
                        " появилась регистрационная запись,\n");
                message.append("в которой был указал ваш электронный адрес (e-mail).\n");
                message.append("\n");
                message.append("Если вы не понимаете, о чем идет речь — просто проигнорируйте это сообщение!\n");
                message.append("\n");
                message.append("Если же именно вы решили зарегистрироваться в системе ").append(ServiceName).append(
                        " по адресу ").append(Host).append(" \n");
                message.append(
                        "Подтверждение регистрации производится один раз и необходимо для повышения безопасности системы и защиты ее от злоумышленников.\n");
                message.append("Чтобы активировать вашу учетную запись, необходимо перейти по ссылке:\n");
                message.append(Host).append("/#/confirm/registration/").append(activationCode).append("\n");
                message.append("Активация произойдет автоматически.\n");
                message.append("\n");
                message.append(
                        "После активации учетной записи вы сможете войти в систему, используя указанный вами адрес электронной почты и пароль.\n");
                message.append("\n");
                message.append("Благодарим за регистрацию!\n");

                try {
                    String smsMessage = ServiceName + " - код подтверждения регистрации: " + activationCode;
                    String emailTitle = "Регистрация в " + ServiceName;
                    NotificationManager.getInstance().notify(user, smsMessage, null, emailTitle, message.toString(),
                            null, null, null, null, nType, false, smsSign, true);
                } catch (Exception ex) {
                    log.error("Не удалось отправить оповещение");
                    log.error(Phone, ex);
                    throw new HttpInternalException(
                            "Не удалось отправить оповещение для подтвержения регистрации. Попробуйте выполнить регистрацию позже.");
                }

            } catch (SQLException | HttpInternalException ex) {
                conn.rollback();
                throw ex;
            }
        }

        DataSet ds = new DataSet();
        ds.createColumn("Id", DataColumnType.STRING);
        ds.createRecord().setString("Id", guuid);
        return ds;
    }
}
