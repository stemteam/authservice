/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.exception.RightServiceException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.stemteam.authservice.api.support.DataSetProvider;
import net.stemteam.authservice.api.support.ParamHelper;
import net.stemteam.authservice.api.support.UserRequestPropogation;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.exception.UserNotFoundException;
import net.stemteam.jaxis.annotation.Method;
import net.stemteam.jaxis.annotation.Param;
import net.stemteam.jaxis.annotation.Return;

/**
 * Возвращает данные пользователя по его токену/логину/гуиду.
 *
 * Для того чтобы получить свои данные следует передать токен.
 *
 * Для того чтобы получить данные другого пользователя - следует передать свой Token + логин/гуид другого пользователя.
 */
public class UserGet extends DataSetHandler {

    @Method(desc = "Возвращает данные пользователя по его токену/логину/гуиду. Для того чтобы получить свои данные следует передать токен. "
            + "Для того чтобы получить данные другого пользователя - следует быть системным пользователем, "
            + "передать свой Token + логин/гуид другого пользователя.",
            params = {
                @Param(required = true, name = ParamHelper.PARAM_VERSION, desc = "Версия протокола"),
                @Param(required = true, name = ParamHelper.PARAM_APIKEY, desc = "Ключ API"),
                @Param(required = true, name = ParamHelper.PARAM_TOKEN, desc = "токен авторизации"),
                @Param(name = ParamHelper.PARAM_GUUID, desc = "Уникальный идентификатор пользователя (guuid)"),
                @Param(name = ParamHelper.PARAM_LOGIN, desc = "логин пользователя (мобильный телефон или email)"),
                @Param(name = ParamHelper.PARAM_USER_WITH_REFERED_USERS, desc = "вернуть ли связанных пользователей, членов семьи и т.д. (0 - нет, 1 - да)"),
                @Param(name = ParamHelper.PARAM_USER_WITH_ENDPOINTS, desc = "вернуть ли список логинов (0 - нет, 1 - да)"),
                @Param(name = ParamHelper.PARAM_USER_WITH_ROLES, desc = "вернуть ли роли (0 - нет, 1 - да)"),
                @Param(name = ParamHelper.PARAM_USER_WITH_LAST_ACTION, desc = "вернуть ли дату последней активности (0 - нет, 1 - да)"),
                @Param(name = ParamHelper.PARAM_USER_WITH_REQUESTS, desc
                        = "вернуть ли открытые запросы пользователя, на удаление/изменение пароля и т.д. (0 - нет, 1 - да)")
            },
            returns = @Return(items = {}))
    @Override
    public DataSet getData(HashMap params, JaxisContentType jct) throws
            ConnectionPoolNotFoundException, DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpBadRequestException,
            HttpInternalException, HttpUnauthorizedException, NotConfiguredException, HttpNotFoundException,
            RightServiceException, UserNotAuthorizedException, UserNotFoundException {
        String version = getParamValidator().getString(params, ParamHelper.PARAM_VERSION, true);
        String token = getParamValidator().getString(params, ParamHelper.PARAM_TOKEN);
        String login = getParamValidator().getString(params, ParamHelper.PARAM_LOGIN);
        String guuid = getParamValidator().getString(params, ParamHelper.PARAM_GUUID);
        String apiKey = getParamValidator().getString(params, ParamHelper.PARAM_APIKEY, true);
        Integer withReferedUsers = getParamValidator().getInt(params, ParamHelper.PARAM_USER_WITH_REFERED_USERS);
        Integer withEndpoints = getParamValidator().getInt(params, ParamHelper.PARAM_USER_WITH_ENDPOINTS);
        Integer withRoles = getParamValidator().getInt(params, ParamHelper.PARAM_USER_WITH_ROLES);
        Integer withLastAction = getParamValidator().getInt(params, ParamHelper.PARAM_USER_WITH_LAST_ACTION);
        Integer withRequests = getParamValidator().getInt(params, ParamHelper.PARAM_USER_WITH_REQUESTS);

        getParamValidator().postValidate();

        withReferedUsers = withReferedUsers == null ? 0 : withReferedUsers;
        withEndpoints = withEndpoints == null ? 0 : withEndpoints;
        withRoles = withRoles == null ? 0 : withRoles;
        withLastAction = withLastAction == null ? 0 : withLastAction;
        withRequests = withRequests == null ? 0 : withRequests;

        if ((login == null) && (token == null) && (guuid == null)) {
            throw new HttpBadRequestException("Требуется параметр Login или Token или Guuid");
        }

        ApiKeyHelper.getInstance().check(getDataSource(), apiKey);

        List<UserRequestPropogation> props = new ArrayList<>();
        if (withReferedUsers == 1) {
            props.add(UserRequestPropogation.ReferedUsers);
        }
        if (withEndpoints == 1) {
            props.add(UserRequestPropogation.Endpoints);
        }
        if (withRoles == 1) {
            props.add(UserRequestPropogation.Roles);
        }
        if (withLastAction == 1) {
            props.add(UserRequestPropogation.LastAction);
        }
        if (withRequests == 1) {
            props.add(UserRequestPropogation.Requests);
        }

        User user;
        if (login == null && guuid == null) {
            // по токену мы вернем пользователя без проверок, он сам сделал этот запрос
            user = getAuthService().getAuthRepository().getUser(new User(token, null), props.toArray(
                    new UserRequestPropogation[0]));
        } else {
            // здесь нам нужно проверить право на получение этого юзера (по гуиду или логину получать пользователей могут только суперадмины)
            // предполагается что в качестве токена передан токен вызывающего
            User caller = getAuthService().getAuthRepository().getUser(new User(token, null),
                    UserRequestPropogation.Roles);
            if (!caller.isSystem()) {
                throw new HttpForbiddenException("Требуется быть админом для выполнения этого действия");
            }
            if (login != null) {
                user = getAuthService().getAuthRepository().getUserByLogin(login, props.toArray(
                        new UserRequestPropogation[0]));
            } else {
                user = getAuthService().getAuthRepository().getUser(new User(null, guuid), props.toArray(
                        new UserRequestPropogation[0]));
            }
        }

        DataSetProvider prov = new DataSetProvider();
        return prov.getUserDataSet(user);

    }

}
