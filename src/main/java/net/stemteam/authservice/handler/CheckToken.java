/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataColumnType;
import net.stemteam.datatransport.transport.DataRecord;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.exception.RightServiceException;

import java.sql.SQLException;
import java.util.HashMap;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.exception.UserNotFoundException;
import net.stemteam.jaxis.err.StemException;

/**
 * Проверка токена пользователя
 *
 * @deprecated используйте getUser вместо этого метода, потому что метод не возвращает почти никаких данных о
 * пользователе, и по сути дублирует при этом getUser() с ухудшением качества
 */
@Deprecated
public class CheckToken extends DataSetHandler {

    @Override
    protected DataSet getData(HashMap params, JaxisContentType jct) throws
            ConnectionPoolNotFoundException, DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpBadRequestException,
            HttpInternalException, HttpUnauthorizedException, NotConfiguredException, RightServiceException,
            UserNotAuthorizedException, UserNotFoundException, StemException {
        String Version = getParamValidator().getString(params, "Version", true); // версия протокола
        String token = getParamValidator().getString(params, "Token", true); // токен авторизации пользователя
        String ApiKey = getParamValidator().getString(params, "ApiKey", true);
        getParamValidator().postValidate();

        // проверка ключа API
        ApiKeyHelper.getInstance().check(getDataSource(), ApiKey);

        // проверка токена
        User user;
        try {
            user = getAuthService().getUser(token);
        } catch (HttpNotFoundException ex) {
            throw new HttpUnauthorizedException("Пользователь не авторизован");
        }

        DataSet ds = new DataSet();
        ds.createColumn("Token", DataColumnType.STRING);
        ds.createColumn("Id", DataColumnType.STRING);
        DataRecord row = ds.createRecord();

        row.setString("Token", token);
        row.setString("Id", user.getGuuid());
        ds.setAsJson2Array(false);
        return ds;
    }
}
