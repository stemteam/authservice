/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.entity.RefType;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.db.ParamBinder;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.exception.RightServiceException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import net.stemteam.authservice.api.support.UserRequestPropogation;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.exception.UserNotFoundException;
import net.stemteam.datatransport.transport.DataColumnType;
import net.stemteam.jaxis.annotation.Method;
import net.stemteam.jaxis.annotation.Param;
import net.stemteam.jaxis.annotation.Return;

/**
 * Обновление данных пользователя (фио, картинка)
 */
public class UserEdit extends DataSetHandler {

    private static final String PARAM_TOKEN = "Token";
    private static final String PARAM_GUUID = "Guuid";
    private static final String PARAM_VERSION = "Version";
    private static final String PARAM_APIKEY = "ApiKey";
    private static final String PARAM_FIRST_NAME = "FirstName";
    private static final String PARAM_LAST_NAME = "LastName";
    private static final String PARAM_MIDDLE_NAME = "MiddleName";
    private static final String PARAM_IMAGE = "Image";

    @Method(desc = "Обновление данных пользователя (фио, картинка)",
            params = {
                @Param(required = true, name = PARAM_VERSION, desc = "Версия протокола"),
                @Param(required = true, name = PARAM_APIKEY, desc = "Ключ API"),
                @Param(required = true, name = PARAM_TOKEN, desc = "токен авторизации"),
                @Param(name = PARAM_GUUID, desc = "удаляемый пользователь или член его семьи (если не передан - текущий пользователь)"),
                @Param(name = PARAM_FIRST_NAME, desc = "имя"),
                @Param(name = PARAM_LAST_NAME, desc = "фамилия"),
                @Param(name = PARAM_MIDDLE_NAME, desc = "отчество"),
                @Param(name = PARAM_IMAGE, desc = "урл изображения"),
                @Param(name = "Birthday", desc = "дата рождения", type = DataColumnType.TIMESTAMP)
            },
            returns = @Return(items = {}))
    @Override
    public DataSet getData(HashMap params, JaxisContentType jct) throws
            ConnectionPoolNotFoundException, DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpBadRequestException,
            HttpInternalException, HttpUnauthorizedException, NotConfiguredException, HttpNotFoundException,
            RightServiceException, UserNotAuthorizedException, UserNotFoundException {
        String Version = getParamValidator().getString(params, PARAM_VERSION, true);
        String Token = getParamValidator().getString(params, PARAM_TOKEN, true);
        String ApiKey = getParamValidator().getString(params, PARAM_APIKEY, true);
        String guuid = getParamValidator().getString(params, PARAM_GUUID);
        String firstName = getParamValidator().getString(params, PARAM_FIRST_NAME);
        String lastName = getParamValidator().getString(params, PARAM_LAST_NAME);
        String middleName = getParamValidator().getString(params, PARAM_MIDDLE_NAME);
        String image = getParamValidator().getString(params, PARAM_IMAGE);
        Timestamp birthday = getParamValidator().getTimestamp(params, "Birthday");
        getParamValidator().postValidate();

        ApiKeyHelper.getInstance().check(getDataSource(), ApiKey);

        User user = getAuthService().getAuthRepository().getUser(new User(Token, null), UserRequestPropogation.ReferedUsers,
                UserRequestPropogation.Roles);

        // если указан гуид - проверим что пользователь не супер-админ и не связан с переданным гуидом
        if (guuid != null) {
            if (!user.getGuuid().equals(guuid)) {
                User ref = new User(null, guuid);
                if ((!user.isRefered(ref, RefType.slave)) && (!user.isRefered(ref, RefType.child))
                        && (!user.isSystem())) {
                    throw new HttpForbiddenException(
                            "Вы не связаны с изменяемым пользователем и/или не являетесь суперпользователем");
                }
            }
        }

        if (guuid == null) {
            guuid = user.getGuuid();
        }

        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement("update sysuser "
                    + " set first_name = ?, last_name = ?, middle_name = ?, image_url = ?, birthday = ?::timestamp where guuid = ?::uuid");
            int num = 1;
            ParamBinder.setString(ps, num++, firstName);
            ParamBinder.setString(ps, num++, lastName);
            ParamBinder.setString(ps, num++, middleName);
            ParamBinder.setString(ps, num++, image);
            ParamBinder.setTimestamp(ps, num++, birthday);
            ps.setString(num++, guuid);
            ps.execute();
            conn.commit();
            return null;
        }
    }
}
