/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.social.vk;

/**
 * Указывает тип отображения страницы авторизации при авторизации OAuth 2
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public enum VkAuthorizeDisplay {

    /**
     * форма авторизации в отдельном окне
     */
    page,
    /**
     * всплывающее окно
     */
    popup,
    /**
     * авторизация для мобильных устройств (без использования Javascript)
     */
    mobile;
}
