/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.social.vk;

import com.google.gson.Gson;
import net.stemteam.authservice.common.HttpClientHelper;
import net.stemteam.authservice.entity.enums.NotificationTypeCode;
import net.stemteam.authservice.dao.AuthRepository;
import net.stemteam.authservice.entity.NotificationType;
import net.stemteam.authservice.entity.SysLogin;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.oauth.OAuthClient;
import net.stemteam.authservice.oauth.OAuthRequest;
import net.stemteam.authservice.oauth.OAuthSocialApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.UUID;
import net.stemteam.authservice.entity.Endpoint;

/**
 * Клиент соц.сети Вконтакте
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class VkClient extends OAuthSocialApplication implements OAuthClient {
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private String apiVersion = "5.52";
    private VkAuthorizeDisplay oauthDisplay = VkAuthorizeDisplay.popup;
    private String oauthScope = "email";
    private AuthRepository authRepository;

    /**
     * База авторизации
     *
     * @return
     */
    public AuthRepository getAuthRepository() {
        return authRepository;
    }

    /**
     * База авторизации
     *
     * @param authRepository
     */
    public void setAuthRepository(AuthRepository authRepository) {
        this.authRepository = authRepository;
    }

    /**
     * Версия API
     *
     * @return
     */
    public String getApiVersion() {
        return apiVersion;
    }

    /**
     * Версия API
     *
     * @param apiVersion
     */
    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    /**
     * Указывает тип отображения страницы авторизации при авторизации OAuth 2
     *
     * @return
     */
    public VkAuthorizeDisplay getOauthDisplay() {
        return oauthDisplay;
    }

    /**
     * Указывает тип отображения страницы авторизации при авторизации OAuth 2
     *
     * @param oauthDisplay
     */
    public void setOauthDisplay(VkAuthorizeDisplay oauthDisplay) {
        this.oauthDisplay = oauthDisplay;
    }

    /**
     * Права на доступ к чему запрашивать при авторизации посредством OAuth
     *
     * @return
     */
    public String getOauthScope() {
        return oauthScope;
    }

    /**
     * Права на доступ к чему запрашивать при авторизации посредством OAuth
     *
     * @param oauthScope
     */
    public void setOauthScope(String oauthScope) {
        this.oauthScope = oauthScope;
    }

    @Override
    public OAuthRequest prepareRequest() throws Exception {
        String state = UUID.randomUUID().toString();
        String url = "https://oauth.vk.com/authorize"
                + "?client_id=" + getApplicationId()
                + "&display=" + oauthDisplay.toString()
                + "&redirect_uri=" + URLEncoder.encode(getApplicationOAuthRedirectUrl(), "utf-8")
                + "&scope=" + oauthScope
                + "&state=" + URLEncoder.encode(state, "utf-8")
                + "&response_type=code"
                + "&v=" + apiVersion;
        OAuthRequest req = new OAuthRequest();
        req.setState(state);
        req.setUrl(url);
        return req;
    }

    @Override
    public SysLogin authorize(String code, String state) throws Exception {
        // получаем access_token
        String url = "https://oauth.vk.com/access_token"
                + "?client_id=" + getApplicationId()
                + "&client_secret=" + getApplicationSecureKey()
                + "&redirect_uri=" + URLEncoder.encode(getApplicationOAuthRedirectUrl(), "utf-8")
                + "&code=" + URLEncoder.encode(code, "utf-8")
                + "&v=" + apiVersion;
        HttpClientHelper httpHelper = new HttpClientHelper();
        String response = httpHelper.httpGet(url);
        Gson gson = new Gson();
        VkAccessToken accessToken = gson.fromJson(response, VkAccessToken.class);
        logger.debug("VkAccessToken: {}", accessToken);

        // получаем информацию о пользователе VK
        VkUser me = getMe(accessToken);
        logger.debug("VkUser: {}", me);
        
        // проверяем, есть ли такой у нас
        NotificationType notificationType = authRepository.getNotificationType(getApplicationCode());
        Endpoint e = authRepository.getEndpoint(notificationType, String.valueOf(me.getId()));
        User user;
        if (e == null) {
            // создаем нового пользователя
            user = new User(null, UUID.randomUUID().toString());
            user.setFirstName(me.getFirstName());
            user.setLastName(me.getLastName());
            user.setPasswordSalt(user.getGuuid());
            user.setPasswordHash("vk-user-only");
            user.setRegistered(true);
            authRepository.addUser(user);

            // создаем ему точку представления в ВК
            e = new Endpoint();
            e.setEnabled(true);
            e.setVerified(true);
            e.setUserId(user.getId());
            e.setNotificationId(String.valueOf(me.getId()));
            e.setNotificationTypeId(notificationType.getId());
            authRepository.addEndpoint(e);
        } else {
            user = authRepository.getUser(new User(e.getUserId()));
        }
        logger.debug("User.id: {}", user.getId());

        // регистрируем в базе авторизации
        SysLogin login = new SysLogin();
        login.setToken(UUID.randomUUID().toString());
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, accessToken.getExpiresSec());
        login.setExpiredTime(new Timestamp(cal.getTime().getTime()));
        login.setSysUserId(user.getId());
        login.setState(state);
        login.setExtAccessToken(accessToken.getAccessToken());
        login.setEndpoint(e);
        authRepository.addSysLogin(login);
        return login;
    }

    /**
     * Получение информации о текущем пользователе
     *
     * @param token токен доступа
     * @return
     * @throws java.lang.Exception
     */
    public VkUser getMe(VkAccessToken token) throws Exception {
        String url = "https://api.vk.com/method/users.get"
                + "?access_token=" + token.getAccessToken()
                + "&v=" + apiVersion
                + "&fields=city,country,timezone,bdate";
        HttpClientHelper httpHelper = new HttpClientHelper();
        String response = httpHelper.httpGet(url);
        Gson gson = new Gson();
        VkUsersResponse resp = gson.fromJson(response, VkUsersResponse.class);
        return resp.getUsers().get(0);
    }

    @Override
    public NotificationTypeCode getApplicationCode() {
        return NotificationTypeCode.vk;
    }

}
