/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.social.vk;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Ответ запроса АПИ https://api.vk.com/method/users.get
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class VkUsersResponse {

    @SerializedName("response")
    private List<VkUser> users;

    public List<VkUser> getUsers() {
        return users;
    }

    public void setUsers(List<VkUser> users) {
        this.users = users;
    }

}
