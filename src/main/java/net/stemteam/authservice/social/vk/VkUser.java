/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.social.vk;

import com.google.gson.annotations.SerializedName;

/**
 * Пользователь вконтакты
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class VkUser {

    private long id;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("timezone")
    private double timeOffset;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getTimeOffset() {
        return timeOffset;
    }

    public void setTimeOffset(double timeOffset) {
        this.timeOffset = timeOffset;
    }

    @Override
    public String toString() {
        return "id:" + id
                + " firstName:" + firstName
                + " lastName:" + lastName
                + " timeOffset:" + timeOffset;
    }

}
