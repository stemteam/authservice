/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.social.vk;

import com.google.gson.annotations.SerializedName;

public class VkAccessToken {
    
    @SerializedName("access_token")
    private String accessToken;
    
    @SerializedName("expires_in")
    private int expiresSec;
    
    @SerializedName("user_id")
    private long userId;
    
    private String email;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public int getExpiresSec() {
        return expiresSec;
    }

    public void setExpiresSec(int expiresSec) {
        this.expiresSec = expiresSec;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    @Override
    public String toString() {
        return "accessToken:" + accessToken
                + " expiresSec:" + expiresSec
                + " userId:" + userId
                + " email:" + email;
    }
    
    
}
