/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.social.ok;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Пользователь системы Одноклассники
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class OkUser {

    public String uid;
    @SerializedName("first_name")
    public String firstName;
    @SerializedName("last_name")
    public String lastName;
    public String locale;
    public String gender;
    public String online;
    public Date birthday;
    public int age;
    @SerializedName("pic_1")
    public String pic1;
    @SerializedName("pic_2")
    public String pic2;
    @SerializedName("pic_3")
    public String pic3;
    public OkLocation location;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPic1() {
        return pic1;
    }

    public void setPic1(String pic1) {
        this.pic1 = pic1;
    }

    public String getPic2() {
        return pic2;
    }

    public void setPic2(String pic2) {
        this.pic2 = pic2;
    }

    public String getPic3() {
        return pic3;
    }

    public void setPic3(String pic3) {
        this.pic3 = pic3;
    }

    public OkLocation getLocation() {
        return location;
    }

    public void setLocation(OkLocation location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "uid:" + uid
                + " firstName:" + firstName
                + " lastName:" + lastName
                + " locale:" + locale
                + " gender:" + gender
                + " online:" + online
                + " birthday:" + birthday
                + " age:" + age
                + " pic1:" + pic1
                + " pic2:" + pic2
                + " pic3:" + pic3;

    }
}
