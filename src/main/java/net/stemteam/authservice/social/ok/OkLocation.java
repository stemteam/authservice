/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.social.ok;

/**
 * Метосположение (соц.сеть Одноклассники)
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class OkLocation {

    private String city;
    private String country;
    private String countryCode;
    private String countryName;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    @Override
    public String toString() {
        return "city:" + city
                + " country:" + country
                + " countryCode:" + countryCode
                + " countryName:" + countryName;
    }

}
