/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.social.ok;

/**
 * Вид и расположение окна авторизации OAuth
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public enum OkAuthorizeLayout {

    /**
     * стандартное окно для полной версии сайта
     */
    w,
    /**
     * окно для мобильной авторизации
     */
    m,
    /**
     * упрощённое окно для мобильной авторизации без шапки
     */
    a;

}
