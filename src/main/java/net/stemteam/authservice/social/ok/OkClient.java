/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.social.ok;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.stemteam.authservice.common.HttpClientHelper;
import net.stemteam.authservice.entity.enums.NotificationTypeCode;
import net.stemteam.authservice.dao.AuthRepository;
import net.stemteam.authservice.entity.NotificationType;
import net.stemteam.authservice.entity.SysLogin;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.oauth.OAuthClient;
import net.stemteam.authservice.oauth.OAuthRequest;
import net.stemteam.authservice.oauth.OAuthSocialApplication;
import net.stemteam.datatransport.tool.Md5;
import net.stemteam.jaxis.err.HttpInternalException;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import net.stemteam.authservice.entity.Endpoint;

/**
 * Клиент соц.сети Одноклассники
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class OkClient extends OAuthSocialApplication implements OAuthClient {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String oauthScope = "VALUABLE_ACCESS";
    private AuthRepository authRepository;
    private OkAuthorizeLayout authLayout = OkAuthorizeLayout.w;

    /**
     * Вид и расположение окна авторизации OAuth
     *
     * @return
     */
    public OkAuthorizeLayout getAuthLayout() {
        return authLayout;
    }

    /**
     * Вид и расположение окна авторизации OAuth
     *
     * @param authLayout
     */
    public void setAuthLayout(OkAuthorizeLayout authLayout) {
        this.authLayout = authLayout;
    }

    /**
     * База авторизации
     *
     * @return
     */
    public AuthRepository getAuthRepository() {
        return authRepository;
    }

    /**
     * База авторизации
     *
     * @param authRepository
     */
    public void setAuthRepository(AuthRepository authRepository) {
        this.authRepository = authRepository;
    }

    /**
     * Права на доступ к чему запрашивать при авторизации посредством OAuth
     *
     * @return
     */
    public String getOauthScope() {
        return oauthScope;
    }

    /**
     * Права на доступ к чему запрашивать при авторизации посредством OAuth
     *
     * @param oauthScope
     */
    public void setOauthScope(String oauthScope) {
        this.oauthScope = oauthScope;
    }

    @Override
    public OAuthRequest prepareRequest() throws Exception {
        String state = UUID.randomUUID().toString();
        String url = "https://connect.ok.ru/oauth/authorize"
                + "?client_id=" + getApplicationId()
                + "&redirect_uri=" + URLEncoder.encode(getApplicationOAuthRedirectUrl(), "utf-8")
                + "&response_type=code"
                + "&scope=" + oauthScope
                + "&layout=" + authLayout
                + "&state=" + URLEncoder.encode(state, "utf-8");
        OAuthRequest req = new OAuthRequest();
        req.setState(state);
        req.setUrl(url);
        return req;
    }

    @Override
    public SysLogin authorize(String code, String state) throws Exception {
        // получаем access_token
        String url = "https://api.ok.ru/oauth/token.do";
        HttpClientHelper httpHelper = new HttpClientHelper();
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("client_id", getApplicationId()));
        params.add(new BasicNameValuePair("client_secret", getApplicationSecureKey()));
        params.add(new BasicNameValuePair("redirect_uri", getApplicationOAuthRedirectUrl()));
        params.add(new BasicNameValuePair("grant_type", "authorization_code"));
        params.add(new BasicNameValuePair("code", code));
        String response = httpHelper.httpPost(url, params);
        logger.debug("response: {}", response);

        Gson gson = new Gson();
        OkAccessToken accessToken = gson.fromJson(response, OkAccessToken.class);
        logger.debug("accessToken: {}", accessToken);

        // одноклассники столь хороши, что не используют HTTP_CODE, поэтому здесь запросто может быть ошибка
        if ((accessToken.getAccessToken() == null) || (accessToken.getAccessToken().isEmpty())) {
            throw new HttpInternalException(response);
        }

        // получаем информацию о пользователе
        OkUser me = getMe(accessToken);
        logger.debug("user: {}", me);

        // проверяем, есть ли такой у нас
        NotificationType notificationType = authRepository.getNotificationType(getApplicationCode());
        Endpoint e = authRepository.getEndpoint(notificationType, String.valueOf(me.getUid()));
        User user;
        if (e == null) {
            // создаем нового пользователя
            user = new User(null, UUID.randomUUID().toString());
            user.setFirstName(me.getFirstName());
            user.setLastName(me.getLastName());
            user.setPasswordSalt(user.getGuuid());
            user.setPasswordHash("ok-user");
            user.setRegistered(true);
            authRepository.addUser(user);

            // создаем ему точку представления в ВК
            e = new Endpoint();
            e.setEnabled(true);
            e.setVerified(true);
            e.setUserId(user.getId());
            e.setNotificationId(String.valueOf(me.getUid()));
            e.setNotificationTypeId(notificationType.getId());
            authRepository.addEndpoint(e);
        } else {
            user = authRepository.getUser(new User(e.getUserId()));
        }
        logger.debug("User.id: {}", user.getId());

        // регистрируем в базе авторизации
        SysLogin login = new SysLogin();
        login.setToken(UUID.randomUUID().toString());
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, accessToken.getExpiresIn());
        login.setExpiredTime(new Timestamp(cal.getTime().getTime()));
        login.setSysUserId(user.getId());
        login.setState(state);
        login.setExtAccessToken(accessToken.getAccessToken());
        login.setEndpoint(e);
        authRepository.addSysLogin(login);
        return login;
    }

    /**
     * Получение информации о текущем пользователе
     *
     * @param token токен доступа
     * @return
     * @throws java.lang.Exception
     */
    public OkUser getMe(OkAccessToken token) throws Exception {
        // подпись
        String requestPart = "application_key=" + getApplicationPublicKey()
                + "format=JSON"
                + "method=users.getCurrentUser";
        String sig = getRequestSig(requestPart, token);
        // запрос
        String url = "http://api.ok.ru/api/fb.do"
                + "?application_key=" + getApplicationPublicKey()
                + "&format=JSON"
                + "&method=users.getCurrentUser"
                + "&access_token=" + token.getAccessToken() 
                + "&sig=" + sig;

        HttpClientHelper httpHelper = new HttpClientHelper();
        String response = httpHelper.httpGet(url);
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create(); // для понимания дня рождения
        OkUser u = gson.fromJson(response, OkUser.class);
        
        if (u.getUid() == null) {
            throw new HttpInternalException(response);
        }
        
        return u;
    }

    /**
     * Подпись ключем приложения (является частью подписи запроса)
     *
     * @param token
     * @return
     * @throws NoSuchAlgorithmException
     */
    private String getAppSig(OkAccessToken token) throws NoSuchAlgorithmException {
        return Md5.md5ToString(Md5.getMd5((token.getAccessToken() + getApplicationSecureKey()).getBytes()));
    }

    /**
     * Подпись запроса
     *
     * @param requestPart (данные вида ключ1=значение1ключ2=значение2, в алфавитном порядке ключей)
     * @param token
     * @return
     */
    private String getRequestSig(String requestPart, OkAccessToken token) throws NoSuchAlgorithmException {
        return Md5.md5ToString(Md5.getMd5((requestPart + getAppSig(token)).getBytes()));
    }

    @Override
    public NotificationTypeCode getApplicationCode() {
        return NotificationTypeCode.ok;
    }

}
