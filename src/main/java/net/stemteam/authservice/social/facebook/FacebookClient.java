/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.social.facebook;

import com.google.gson.Gson;
import net.stemteam.authservice.common.HttpClientHelper;
import net.stemteam.authservice.entity.enums.NotificationTypeCode;
import net.stemteam.authservice.dao.AuthRepository;
import net.stemteam.authservice.entity.NotificationType;
import net.stemteam.authservice.entity.SysLogin;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.oauth.OAuthClient;
import net.stemteam.authservice.oauth.OAuthRequest;
import net.stemteam.authservice.oauth.OAuthSocialApplication;
import net.stemteam.jaxis.common.HttpHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.UUID;
import net.stemteam.authservice.entity.Endpoint;

/**
 * Клиент соц.сети Facebook
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class FacebookClient extends OAuthSocialApplication implements OAuthClient {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String oauthScope = "email";
    private AuthRepository authRepository;

    /**
     * База авторизации
     *
     * @return
     */
    public AuthRepository getAuthRepository() {
        return authRepository;
    }

    /**
     * База авторизации
     *
     * @param authRepository
     */
    public void setAuthRepository(AuthRepository authRepository) {
        this.authRepository = authRepository;
    }

    /**
     * Права на доступ к чему запрашивать при авторизации посредством OAuth
     *
     * @return
     */
    public String getOauthScope() {
        return oauthScope;
    }

    /**
     * Права на доступ к чему запрашивать при авторизации посредством OAuth
     *
     * @param oauthScope
     */
    public void setOauthScope(String oauthScope) {
        this.oauthScope = oauthScope;
    }

    @Override
    public OAuthRequest prepareRequest() throws Exception {
        String state = UUID.randomUUID().toString();
        String url = "https://www.facebook.com/dialog/oauth"
                + "?client_id=" + getApplicationId()
                + "&redirect_uri=" + URLEncoder.encode(getApplicationOAuthRedirectUrl(), "utf-8")
                + "&scope=" + oauthScope
                + "&state=" + URLEncoder.encode(state, "utf-8");
        OAuthRequest req = new OAuthRequest();
        req.setState(state);
        req.setUrl(url);
        return req;
    }

    @Override
    public SysLogin authorize(String code, String state) throws Exception {
        // получаем access_token
        String url = "https://graph.facebook.com/oauth/access_token"
                + "?client_id=" + getApplicationId()
                + "&client_secret=" + getApplicationSecureKey()
                + "&redirect_uri=" + URLEncoder.encode(getApplicationOAuthRedirectUrl(), "utf-8")
                + "&code=" + URLEncoder.encode(code, "utf-8");
        HttpClientHelper httpHelper = new HttpClientHelper();
        String response = httpHelper.httpGet(url);

        FacebookAccessToken accessToken = parseResponse(response);
        logger.debug("accessToken: {}", accessToken);

        // получаем информацию о пользователе
        FacebookUser me = getMe(accessToken);
        logger.debug("user: {}", me);

        // проверяем, есть ли такой у нас
        NotificationType notificationType = authRepository.getNotificationType(getApplicationCode());
        Endpoint e = authRepository.getEndpoint(notificationType, String.valueOf(me.getId()));
        User user;
        if (e == null) {
            // создаем нового пользователя
            user = new User(null, UUID.randomUUID().toString());
            user.setFirstName(me.getFirstName());
            user.setLastName(me.getLastName());
            user.setPasswordSalt(user.getGuuid());
            user.setPasswordHash("facebook-user");
            user.setRegistered(true);
            authRepository.addUser(user);

            // создаем ему точку представления в ВК
            e = new Endpoint();
            e.setEnabled(true);
            e.setVerified(true);
            e.setUserId(user.getId());
            e.setNotificationId(String.valueOf(me.getId()));
            e.setNotificationTypeId(notificationType.getId());
            authRepository.addEndpoint(e);
        } else {
            user = authRepository.getUser(new User(e.getUserId()));
        }
        logger.debug("User.id: {}", user.getId());

        // регистрируем в базе авторизации
        SysLogin login = new SysLogin();
        login.setToken(UUID.randomUUID().toString());
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, accessToken.getExpires());
        login.setExpiredTime(new Timestamp(cal.getTime().getTime()));
        login.setSysUserId(user.getId());
        login.setState(state);
        login.setExtAccessToken(accessToken.getAccessToken());
        login.setEndpoint(e);
        authRepository.addSysLogin(login);
        return login;
    }

    /**
     * Получение информации о текущем пользователе
     *
     * @param token токен доступа
     * @return
     * @throws java.lang.Exception
     */
    public FacebookUser getMe(FacebookAccessToken token) throws Exception {
        String url = "https://graph.facebook.com/me"
                + "?access_token=" + token.getAccessToken()
                + "&fields=id,name,email";
        HttpClientHelper httpHelper = new HttpClientHelper();
        String response = httpHelper.httpGet(url);
        Gson gson = new Gson();
        FacebookUser u = gson.fromJson(response, FacebookUser.class);
        return u;
    }

    @Override
    public NotificationTypeCode getApplicationCode() {
        return NotificationTypeCode.facebook;
    }

    private FacebookAccessToken parseResponse(String params) throws UnsupportedEncodingException {
        HashMap<String, Object> map = new HashMap<>();
        HttpHelper.parseQuery(params, map);
        FacebookAccessToken token = new FacebookAccessToken();
        token.setAccessToken((String) map.get("access_token"));
        token.setExpires(Integer.valueOf((String) map.get("expires")));
        return token;
    }

}
