/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.social.facebook;

/**
 * Пользователь Facebook
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class FacebookUser {

    private long id;
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getFirstName() {
        return name.substring(0, name.indexOf(" "));
    }
    
    public String getLastName() {
        return name.substring(name.indexOf(" ")+1);
    }
    
    @Override
    public String toString() {
        return "id:" + id
                + " name:" + name;
    }

}
