package net.stemteam.authservice.cron;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import net.stemteam.authservice.service.impl.AuthServiceNative;

/**
 * Удаление устаревших токенов авторизации
 */
@DisallowConcurrentExecution
public class DeleteExpiredLoginsJob implements Job {

    static Logger logger = LoggerFactory.getLogger(DeleteExpiredLoginsJob.class);

    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        logger.debug("{} started ", DeleteExpiredLoginsJob.class.getSimpleName());
        final JobDataMap jobDataMap = jec.getJobDetail().getJobDataMap();
        AuthServiceNative service = (AuthServiceNative) jobDataMap.get("AuthServiceNative");
        try {
            service.getAuthRepository().deleteExpiredLogins();
        } catch (SQLException ex) {
            logger.error("", ex);
            throw new JobExecutionException(ex);
        }
        logger.debug("{} done ", DeleteExpiredLoginsJob.class.getSimpleName());
    }
}
