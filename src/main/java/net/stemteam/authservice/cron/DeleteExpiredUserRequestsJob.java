package net.stemteam.authservice.cron;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Удаление устаревших запросов пользователей
 */
@DisallowConcurrentExecution
public class DeleteExpiredUserRequestsJob implements Job {

    static Logger logger = LoggerFactory.getLogger(DeleteExpiredUserRequestsJob.class);

    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        logger.debug("{} started ", DeleteExpiredUserRequestsJob.class.getSimpleName());
        final JobDataMap jobDataMap = jec.getJobDetail().getJobDataMap();
        DataSource dataSource = (DataSource) jobDataMap.get("DataSource");

        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                    "delete from user_request where expiration_time < current_timestamp");
            ps.execute();
            conn.commit();
        } catch (SQLException ex) {
            logger.error("", ex);
            throw new JobExecutionException(ex);
        }
        logger.debug("{} done ", DeleteExpiredUserRequestsJob.class.getSimpleName());
    }
}
