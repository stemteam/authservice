package net.stemteam.authservice.cron;

import net.stemteam.authservice.entity.enums.NotificationTypeCode;
import net.stemteam.authservice.notification.AzureHub;
import net.stemteam.jaxis.common.SmsAero;
import net.stemteam.jaxis.mail.SmtpMailSender;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import net.stemteam.authservice.service.impl.AuthServiceNative;

/**
 * Отправка уведомлений
 */
@DisallowConcurrentExecution
public class UserNotificationJob implements Job {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public static final String DEV_PREFIX = "dev_";
    public static final String UID_PREFIX = "uid_";
    public static final String APIKEY_PREFIX = "apikey_";

    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        logger.debug("{} started ", UserNotificationJob.class.getSimpleName());

        final JobDataMap jobDataMap = jec.getJobDetail().getJobDataMap();
        DataSource dataSource = (DataSource) jobDataMap.get("DataSource");
        AuthServiceNative service = (AuthServiceNative) jobDataMap.get("AuthServiceNative");

        try (Connection conn = dataSource.getConnection()) {
            // получить сообщения из очереди на отправку
            PreparedStatement getMessages = conn.prepareStatement("select "
                    + "  nt.id_notification, "
                    + "  nt.id_endpoint, "
                    + "  su.guuid, "
                    + "  nt.title, "
                    + "  nt.message, "
                    + "  nt.data, "
                    + "  nt.expirationdate < current_timestamp as is_expired, "
                    + "  t.code, "
                    + "  ep.notification_id,"
                    + "  ep.apikey, "
                    + "  ep.device_id "
                    + " from notification nt "
                    + "  join endpoint ep on ep.id_endpoint = nt.id_endpoint"
                    + "  join notification_type t on ep.id_notification_type = t.id_notification_type "
                    + "  join sysuser su on su.id_sysuser = ep.id_sysuser "
                    + "  where nt.is_notified = 0 "
                    + "    and nt.is_expired = 0 "
                    + " order by ep.id_sysuser");

            try (ResultSet rs = getMessages.executeQuery()) {

                // обновить статус у элемента очереди на отправленный
                PreparedStatement setIsNotified = conn.prepareStatement(
                        "update notification set is_notified = 1 where id_notification = ?");

                // обновить статус у элемента очереди на просроченный
                PreparedStatement setIsExpired = conn.prepareStatement(
                        "update notification set is_expired = 1 where id_notification = ?");

                while (rs.next()) {
                    try {
                        long id = rs.getLong("ID_NOTIFICATION");

                        // если сообщение не актуально - отмечаем и пропускаем
                        if (rs.getBoolean("IS_EXPIRED")) {
                            setIsExpired.setLong(1, id);
                            setIsExpired.execute();
                            conn.commit();
                            continue;
                        }

                        // отправка сообщения
                        String guuid = rs.getString("GUUID");
                        String title = rs.getString("TITLE");
                        String message = rs.getString("MESSAGE");
                        String data = rs.getString("DATA");
                        String notificationId = rs.getString("NOTIFICATION_ID");
                        String apiKey = rs.getString("APIKEY");
                        String deviceId = rs.getString("DEVICE_ID");
                        NotificationTypeCode notificationTypeCode = NotificationTypeCode.valueOf(rs.getString("CODE"));

                        switch (notificationTypeCode) {
                            case sms:
                                if (data != null) { // change signature if need
                                    SmsAero.setSignature(data);
                                }
                                SmsAero.sendSms(notificationId, message);
                                break;
                            case push:
                                AzureHub azure = service.getAzureHubs().get(apiKey);
                                if (azure != null) {
                                    // тэг устройства и пользователя
                                    String expr = DEV_PREFIX + deviceId + " && " + UID_PREFIX + guuid;

                                    // параметры шаблона сообщения
                                    Map<String, String> prop = new HashMap<>();
                                    prop.put("title", title);
                                    prop.put("text", message);
                                    String pushType = "url";
                                    if (data == null) {
                                        pushType = "text";
                                    }
                                    prop.put("type", pushType);
                                    prop.put("value", data == null ? "" : data);
                                    prop.put("click_action", "PUSH_ACTIVITY");

                                    logger.debug(
                                            "send notification: {} with params title: {}, text: {}, type: {}, value: {}",
                                            expr, title, message, pushType, data == null ? "" : data);

                                    azure.sendTemplateNotification(prop, expr);
                                } else {
                                    logger.error("Azure Hub not found for ApiKey {}", apiKey);
                                }
                                break;
                            case email:
                                SmtpMailSender mail = SmtpMailSender.getNewInstance(service.getMailConfig());
                                mail.send(notificationId, null, title, message);
                                break;
                            default:
                                logger.error("Не поддерживаемый тип оповещений: {}", notificationTypeCode.toString());
                                continue;
                        }

                        setIsNotified.setLong(1, id);
                        setIsNotified.execute();
                        conn.commit();
                    } catch (Exception e) {
                        conn.rollback();
                        logger.error("Failed to send notification", e);
                    }
                }
            }
        } catch (SQLException ex) {
            logger.error("", ex);
            throw new JobExecutionException("Не удалось создать соединение с БД", ex);
        }
        logger.debug("{} done ", UserNotificationJob.class.getSimpleName());
    }

}
