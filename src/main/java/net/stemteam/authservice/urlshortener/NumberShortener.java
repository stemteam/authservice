/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.urlshortener;

import java.math.BigInteger;

/**
 * Преобразователь чисел в строку по алфавиту
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class NumberShortener {

    private static final String DEFAULT_ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    private String alphabet;
    private final BigInteger alphabetLen;

    /**
     * Констркутор. Используется алфавит по умолчанию.
     */
    public NumberShortener() {
        alphabet = DEFAULT_ALPHABET;
        alphabetLen = BigInteger.valueOf(alphabet.length());
    }

    /**
     * Конструктор. Создает преобразователь с указанным алфавитом
     *
     * @param alphabet
     */
    public NumberShortener(String alphabet) {
        this.alphabet = alphabet;
        alphabetLen = BigInteger.valueOf(alphabet.length());
    }

    /**
     * Преобразовывает беззнаковое число в строку по алфавиту ALPHABET. Обратное преобразование: decode()
     *
     * @param bi
     * @return
     */
    public String encode(BigInteger bi) {
        StringBuilder sb = new StringBuilder();
        while (bi.compareTo(BigInteger.ZERO) > 0) {
            sb.append(alphabet.charAt(bi.mod(alphabetLen).intValue()));
            bi = bi.divide(alphabetLen);
        }
        return sb.reverse().toString();
    }

    /**
     * Преобразовывает строку по алфавиту ALPHABET в беззнаковое число. Обратное преобразование: encode()
     *
     * @param str
     * @return
     */
    public BigInteger decode(String str) {
        BigInteger bi = BigInteger.ZERO;
        for (int i = 0, len = str.length(); i < len; i++) {
            bi = bi.multiply(alphabetLen).add(BigInteger.valueOf(alphabet.indexOf(str.charAt(i))));
        }
        return bi;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        byte[] bytes = {(byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff,
            (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff};
        BigInteger bi = new BigInteger(1, bytes); // unsigned
        NumberShortener shorterer = new NumberShortener();
        String str = shorterer.encode(bi);
        System.out.println(bi);
        System.out.println(str);
        System.out.println(shorterer.decode(str));
        
        System.out.println(shorterer.encode(BigInteger.valueOf(900000000L)));
        System.out.println(shorterer.encode(BigInteger.valueOf(80000000000L)));
    }

}
