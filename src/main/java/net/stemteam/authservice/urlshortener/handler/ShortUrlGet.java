package net.stemteam.authservice.urlshortener.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.authservice.urlshortener.NumberShortener;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataColumnType;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.annotation.Method;
import net.stemteam.jaxis.annotation.Param;
import net.stemteam.jaxis.annotation.Return;
import net.stemteam.jaxis.common.Version;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Создает короткую ссылку
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class ShortUrlGet extends DataSetHandler {

    public static final String PARAM_VERSION = "Version";
    public static final String PARAM_APIKEY = "ApiKey";
    public static final String PARAM_URL = "Url";

    public static final String FIELD_URL = "ShortUrl";

    @Method(desc
            = "Создает короткую ссылку",
            params = {
                @Param(required = true, name = PARAM_VERSION, desc = "Версия протокола"),
                @Param(required = true, name = PARAM_APIKEY, desc = "Ключ API"),
                @Param(required = true, name = PARAM_URL, desc = "ссылка для сокращения")
            },
            returns = @Return(items = {
                @Param(required = true, name = FIELD_URL, desc = "сокращенная ссылка")
            }))
    @Override
    protected DataSet getData(HashMap params, JaxisContentType jct) throws HttpBadRequestException, SQLException,
            HttpForbiddenException, NotConfiguredException, HttpUnauthorizedException, DataSetException,
            HttpNotFoundException {
        Version version = getParamValidator().getVersion(params, PARAM_VERSION, true);
        String apiKey = getParamValidator().getString(params, PARAM_APIKEY, true);
        String url = getParamValidator().getString(params, PARAM_URL, true);
        getParamValidator().postValidate();

        url = url.trim();

        // проверим ключ апи
        ApiKeyHelper.getInstance().check(getDataSource(), apiKey);

        long id;
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement("select id_short_url from short_url where url = ?");
            ps.setString(1, url);

            PreparedStatement ps2 = conn.prepareStatement("insert into short_url (url) values (?) on conflict do nothing");
            ps2.setString(1, url);
            
            ps2.executeUpdate();
            conn.commit();

            try (ResultSet rs = ps.executeQuery()) {
                rs.next();
                id = rs.getLong("ID_SHORT_URL");
            }
        }

        // декодируем в строку
        NumberShortener ns = new NumberShortener();
        String shortUrl = ns.encode(BigInteger.valueOf(id));
        
        if (getAuthService().getShortUrlGetAddress() != null) {
            shortUrl = getAuthService().getShortUrlGetAddress() + shortUrl;
        }

        DataSet ds = new DataSet();
        ds.createColumn(FIELD_URL, DataColumnType.STRING);
        ds.createRecord().setString(FIELD_URL, shortUrl);
        return ds;
    }
}
