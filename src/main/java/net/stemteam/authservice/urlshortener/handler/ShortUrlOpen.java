package net.stemteam.authservice.urlshortener.handler;

import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.authservice.urlshortener.NumberShortener;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.annotation.Method;
import net.stemteam.jaxis.annotation.Param;
import net.stemteam.jaxis.annotation.Return;
import net.stemteam.jaxis.common.HttpRedirect;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Открывает короткую ссылку, переходит на нужный урл. Формат запроса как и у других сократителей ссылок, т.е.
 * http://yourdomain/LiNkId
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class ShortUrlOpen extends DataSetHandler {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private final String pathPart = "/" + this.getClass().getSimpleName() + "/";

    @Method(desc = "Открывает короткую ссылку, переходит на нужный урл. "
            + " Формат запроса как и у других сократителей ссылок, т.е. http://yourdomain/LiNkId",
            params = {
                @Param(required = true, name = "Url", desc = "ссылка для сокращения")
            },
            returns = @Return(desc = "Редирект на нужный урл или 404",
                    items = {}
            ))
    @Override
    protected DataSet getData(HashMap params, JaxisContentType jct) throws HttpBadRequestException, SQLException,
            HttpForbiddenException, NotConfiguredException, HttpUnauthorizedException, DataSetException,
            HttpNotFoundException, HttpInternalException {

        // парсим айдишку из урла запроса
        String data = getRequestUrl();
        int i = data.indexOf(pathPart);
        if (i == -1) {
            throw new HttpInternalException("Bad java code");
        }
        data = data.substring(i + pathPart.length());

        logger.debug("DATA: {}", data);

        // преобразовываем айдишку к числу
        NumberShortener ns = new NumberShortener();
        long id = ns.decode(data).longValue();

        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement("select url from short_url where id_short_url = ?");
            ps.setLong(1, id);
            try (ResultSet rs = ps.executeQuery()) {
                if (!rs.next()) {
                    throw new HttpNotFoundException("URL: " + getRequestUrl());
                }
                String url = rs.getString("url");

                // TODO redirect to url
                redirect(new HttpRedirect(url));
                return null;

                // TODO remove it
//                DataSet ds = new DataSet();
//                ds.createColumn("Data", DataColumnType.STRING);
//                ds.createRecord().setString("Data", url);
//                return ds;
            }
        }
    }
}
