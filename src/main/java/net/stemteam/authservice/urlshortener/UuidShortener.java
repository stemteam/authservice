/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.urlshortener;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.UUID;

/**
 * Преобразование UUID в строку по алфавиту
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class UuidShortener {

    /**
     * Преобразовывывает UUID к массиву байт
     *
     * @param uuid
     * @return
     */
    public static byte[] toByteArray(UUID uuid) {
        ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
        bb.putLong(uuid.getMostSignificantBits());
        bb.putLong(uuid.getLeastSignificantBits());
        return bb.array();
    }

    /**
     * Преобразовывает массив байт к UUID
     *
     * @param bytes
     * @return
     */
    public static UUID fromByteArray(byte[] bytes) {
        ByteBuffer bb = ByteBuffer.wrap(bytes);
        long high = bb.getLong();
        long low = bb.getLong();
        UUID uuid = new UUID(high, low);
        return uuid;
    }

    /**
     * Кодирует UUID в короткую строку. Раскодировать можно с помощью decode()
     *
     * @param shortener
     * @param uuid
     * @return
     */
    public static String encode(NumberShortener shortener, UUID uuid) {
        BigInteger bi = new BigInteger(1, toByteArray(uuid)); // unsigned big int   
        return shortener.encode(bi);
    }

    /**
     * Декодирует UUID в короткую строку. Обратный метод: encode()
     *
     * @param shortener
     * @param encodedUuid
     * @return
     */
    public static UUID decode(NumberShortener shortener, String encodedUuid) {
        BigInteger bi = shortener.decode(encodedUuid);
        return fromByteArray(bi.toByteArray());
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        UUID uuid = UUID.randomUUID();
        String str = encode(new NumberShortener(), uuid);
        System.out.println(uuid.toString());
        System.out.println(str);
        System.out.println(decode(new NumberShortener(), str).toString());
    }

}
