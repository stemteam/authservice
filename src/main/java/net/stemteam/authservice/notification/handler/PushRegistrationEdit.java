package net.stemteam.authservice.notification.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.entity.Endpoint;
import net.stemteam.authservice.common.NotificationManager;
import net.stemteam.authservice.entity.enums.NotificationTypeCode;
import net.stemteam.authservice.cron.UserNotificationJob;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.ValidationErrorItem;
import net.stemteam.jaxis.annotation.Method;
import net.stemteam.jaxis.annotation.Param;
import net.stemteam.jaxis.annotation.Return;
import net.stemteam.jaxis.common.PNSType;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.exception.RightServiceException;

import java.net.URISyntaxException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.exception.UserNotFoundException;
import net.stemteam.jaxis.err.StemException;

/**
 * Created by warmouse on 17.06.15.
 *
 * Редактирование параметров регистрации устройства
 */
public class PushRegistrationEdit extends DataSetHandler {

    @Method(desc
            = "Регистрация/обновление регистрационных данных мобильного устройства в сервисе отправки сообщений",
            params = {
                @Param(required = true, name = "Version", desc = "Версия протокола"),
                @Param(required = true, name = "ApiKey", desc = "Ключ API"),
                @Param(required = true, name = "Token", desc = "токен авторизации"),
                @Param(required = true, name = "DeviceId", desc = "уникальный id устройства"),
                @Param(required = true, name = "PNSToken", desc = "token полученный от PNS"),
                @Param(required = true, name = "PNSCode", desc = "тип PNS (Platform Notification Service)"),
                @Param(required = true, name = "IsTablet", desc = "является ли устройство планшетом, 1 - является"),
                @Param(required = true, name = "Locale", desc = "локаль устройства"),
                @Param(required = true, name = "Model", desc = "модель устройства"),
                @Param(name = "Name", desc = "настраиваемое имя устройства"),
                @Param(required = true, name = "EndpointApiKey", desc = "ключ апи приложения"),},
            returns = @Return(items = {}))
    @Override
    protected DataSet getData(HashMap params, JaxisContentType jct) throws HttpBadRequestException, SQLException,
            HttpForbiddenException, NotConfiguredException, HttpUnauthorizedException, DataSetException,
            HttpNotFoundException, RightServiceException, UserNotAuthorizedException, UserNotFoundException, StemException {
        String Version = getParamValidator().getString(params, "Version", true); // версия протокола
        String Token = getParamValidator().getString(params, "Token"); // токен авторизации пользователя
        String apiKey = getParamValidator().getString(params, "ApiKey", true); // ключ апи
        String DeviceId = getParamValidator().getString(params, "DeviceId", true); // уникальный id устройства
        String PNSToken = getParamValidator().getString(params, "PNSToken", true); // token полученный от PNS
        String PNSCode = getParamValidator().getString(params, "PNSCode", true); // тип PNS (Platform Notification Service)
        String IsTablet = getParamValidator().getString(params, "IsTablet", true); // является ли устройство планшетом, 1 - является
        String Locale = getParamValidator().getString(params, "Locale", true); // локаль устройства
        String Model = getParamValidator().getString(params, "Model", true); // модель устройства
        String Name = getParamValidator().getString(params, "Name"); // настраиваемое имя устройства
        String endpointApiKey = getParamValidator().getString(params, "EndpointApiKey");
        getParamValidator().postValidate();

        ApiKeyHelper.getInstance().check(getDataSource(), apiKey);

        User user = null;
        if (Token != null) {
            user = getAuthService().getUser(Token);
        }

        PNSType pns = PNSType.getByCode(PNSCode);
        if (pns == null) {
            throw new IllegalArgumentException(String.format("Code %s not exists", PNSCode));
        }

        // определяем тэги для регистрации
        HashSet<String> tags = new HashSet<>();

        // тег для отправки сообщения этому пользователю
        if (user != null) {
            tags.add(UserNotificationJob.UID_PREFIX + user.getGuuid());
        }

        // тег для отправки сообщения пользователю на конкретное устройство
        tags.add(UserNotificationJob.DEV_PREFIX + DeviceId);

        // общий тег для всех наших шаблонных регистраций, для broadcast
        tags.add("all");

        // тег для отправки сообщения на определенную платформу
        tags.add(pns.getCode());

        // тег для отправки сообщения на определенный тип устройства
        Boolean isTablet = IsTablet.equals("1");
        if (isTablet) {
            tags.add("tablet");
        } else {
            tags.add("phone");
        }

        // тег для отправки сообщения определенному приложению
        tags.add(UserNotificationJob.APIKEY_PREFIX + endpointApiKey);

        // сделать запрос azure_id
        try (Connection conn = getConnection()) {
            String azure_id = null;
            Integer id_endpoint = null;
            DeviceId = DeviceId.toLowerCase();

            CallableStatement ps = conn.prepareCall(
                    "select id_endpoint, azure_id from endpoint where device_id = ? and apikey = ?");
            ps.setString(1, DeviceId);
            ps.setString(2, endpointApiKey);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                azure_id = rs.getString("AZURE_ID");
                id_endpoint = rs.getInt("ID_ENDPOINT");
            }
            rs.close();

            // если устройство не зарегистрировано в Azure - получаем новый ID
            if (azure_id == null || azure_id.isEmpty()) {
                azure_id = NotificationManager.getInstance().azureCreateRegistrationId(endpointApiKey);
            }

            // обновляем тэги
            try {
                azure_id = NotificationManager.getInstance().azureUpdateRegistration(endpointApiKey, azure_id, PNSToken,
                        pns, tags);
            } catch (URISyntaxException e) {
                getParamValidator().add(new ValidationErrorItem("NotURI", PNSToken, "\"Не соответствует формату URI\""));
                getParamValidator().postValidate();
            }

            // обновляем информацию в нашей БД
            Endpoint.PushNotificationAttributes pna = new Endpoint.PushNotificationAttributes(
                    azure_id, DeviceId, pns, isTablet, Locale, Name, Model, endpointApiKey);

            Endpoint ep = new Endpoint(
                    id_endpoint, NotificationTypeCode.push, PNSToken, pna, true, true, false);

            Integer userId = (user == null) ? null : user.getId();
            getAuthService().getAuthRepository().upsertEndpoint(conn, userId, ep);

            conn.commit();
        }

        return null;
    }
}
