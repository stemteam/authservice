/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.notification.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.exception.RightServiceException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.exception.UserNotFoundException;
import net.stemteam.jaxis.err.StemException;

/**
 * Возвращает связанных с пользователем пользователей
 */
public class ToggleEndpoint extends DataSetHandler {

    @Override
    protected DataSet getData(HashMap params, JaxisContentType jct) throws ConnectionPoolNotFoundException,
            DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpBadRequestException, HttpInternalException,
            HttpUnauthorizedException, NotConfiguredException, HttpNotFoundException, RightServiceException, UserNotAuthorizedException, UserNotFoundException, StemException {
        String Version = getParamValidator().getString(params, "Version", true); // версия протокола
        String Token = getParamValidator().getString(params, "Token", true); // токен авторизации пользователя
        String ApiKey = getParamValidator().getString(params, "ApiKey", true); // ключ апи
        Integer EndpointId = getParamValidator().getInt(params, "EndpointId", true); // ID контакта оьлзователя
        Integer IsEnabled = getParamValidator().getInt(params, "IsEnabled", true); // использовать контакт для оповещений 0 - нет
        getParamValidator().postValidate();

        // проверка апи-ключа
        ApiKeyHelper.getInstance().check(getDataSource(), ApiKey);

        // получение пользователя по токену + проверка авторизации
        User user = getAuthService().getUser(Token);

        // Определяем флаг
        if (IsEnabled != 0) {
            IsEnabled = 1;
        }

        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement("update endpoint set is_enabled = ? where id_endpoint = ?");
            ps.setInt(1, IsEnabled);
            ps.setInt(2, EndpointId);

            ps.execute();
            conn.commit();

            return null;
        }
    }
}
