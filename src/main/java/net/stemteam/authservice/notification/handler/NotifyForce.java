package net.stemteam.authservice.notification.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.common.NotificationManager;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.StemException;

import java.sql.SQLException;
import java.util.HashMap;
import net.stemteam.authservice.api.support.UserRequestPropogation;

/**
 * Принудительная отправка уведомления пользователям
 */
public class NotifyForce extends DataSetHandler {

    /**
     * Подпись для отправки SMS сообщения по умолчанию
     */
    public static final String DEFAULT_SMS_SIGN = "Telemedic";

    @Override
    protected DataSet getData(HashMap params, JaxisContentType jct) throws StemException, SQLException,
            DataSetException {
        String Version = getParamValidator().getString(params, "Version", true); // версия протокола
        String Token = getParamValidator().getString(params, "Token", true); // токен авторизации пользователя (отправляющего сообщение)
        String GUUID = getParamValidator().getString(params, "Guuid", true); // ID пользователя в системе авторизации
        String ApiKey = getParamValidator().getString(params, "ApiKey", true); // ключ апи
        String smallMessage = getParamValidator().getString(params, "SmallMessage", true); // текст сообщения для SMS/PUSH
        String emailTitle = getParamValidator().getString(params, "EmailTitle", true); // тема сообщения Email
        String emailMessage = getParamValidator().getString(params, "EmailMessage", true); // текст сообщения для Email
        String smsSign = getParamValidator().getString(params, "SmsSign");
        getParamValidator().postValidate();

        if (smsSign == null) {
            smsSign = DEFAULT_SMS_SIGN;
        }

        // проверка ключа апи
        ApiKeyHelper.getInstance().check(getDataSource(), ApiKey);

        // проверка токена отправляющего
        User user = getAuthService().getUser(Token, UserRequestPropogation.Roles);

        if (!user.isSystem()) {
            throw new HttpForbiddenException("Пользователь не имеет права на отправку сообщений");
        }

        // поиск адресата
        User targetUser = getAuthService().getUserByGuuid(Token, GUUID, UserRequestPropogation.Endpoints);
        if (targetUser == null) {
            throw new HttpUnauthorizedException("Получатель не найден");
        }

        try {
            NotificationManager.getInstance().notify(targetUser, smallMessage, null, emailTitle, emailMessage,
                    null, null, null, null, null, true, smsSign, false);
        } catch (Exception e) {
            throw new HttpInternalException(e);
        }

        return null;
    }
}
