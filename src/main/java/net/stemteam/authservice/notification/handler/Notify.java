package net.stemteam.authservice.notification.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.common.NotificationManager;
import net.stemteam.authservice.entity.enums.NotificationTypeCode;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.common.Version;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.StemException;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import net.stemteam.authservice.api.support.UserRequestPropogation;

import static net.stemteam.authservice.notification.handler.NotifyForce.DEFAULT_SMS_SIGN;

/**
 * Отправка уведомления пользователю.
 */
public class Notify extends DataSetHandler {

    @Override
    protected DataSet getData(HashMap params, JaxisContentType jct) throws StemException, SQLException,
            DataSetException {
        Version version = getParamValidator().getVersion(params, "Version", true);
        String Token = getParamValidator().getString(params, "Token", true); // токен авторизации пользователя
        String GUUID = getParamValidator().getString(params, "Guuid", true); // ID пользователя в системе авторизации
        String ApiKey = getParamValidator().getString(params, "ApiKey", true); // ключ апи
        String SmallMessage = getParamValidator().getString(params, "SmallMessage", true); // текст сообщения для SMS/PUSH
        String EmailTitle = getParamValidator().getString(params, "EmailTitle", true); // тема сообщения Email
        String EmailMessage = getParamValidator().getString(params, "EmailMessage", true); // текст сообщения для Email
        String endpointApiKey = getParamValidator().getString(params, "EndpointApiKey", true);
        String notificationTypeCode = getParamValidator().getString(params, "NotificationType"); // способ оповещения пользователя
        Timestamp ExpirationDate = getParamValidator().getTimestamp(params, "ExpirationDate"); // дата после которой сообщение теряет актуальность
        String VerifiedOnly = getParamValidator().getString(params, "VerifiedOnly"); // 1 - отправлять только на проверенные контакты (default = 0)
        String PushTitle = getParamValidator().getString(params, "PushTitle"); // заголовок PUSH
        String PushData = getParamValidator().getString(params, "PushData"); // данные PUSH
        String smsSign = getParamValidator().getString(params, "SmsSign");
        getParamValidator().postValidate();

        if (smsSign == null) {
            smsSign = DEFAULT_SMS_SIGN;
        }

        ApiKeyHelper.getInstance().check(getDataSource(), ApiKey);

        // проверка токена отправляющего
        User user = getAuthService().getUser(Token, UserRequestPropogation.Roles);
        if (!user.isSystem()) {
            throw new HttpForbiddenException("Пользователь не имеет права на отправку сообщений");
        }

        // поиск адресата
        user = getAuthService().getUserByGuuid(Token, GUUID, UserRequestPropogation.Endpoints);

        NotificationTypeCode nType = null;
        if (notificationTypeCode != null && !notificationTypeCode.isEmpty()) {
            nType = NotificationTypeCode.valueOf(notificationTypeCode);
        }

        boolean vOnly = false;
        if (VerifiedOnly != null && VerifiedOnly.equals("1")) {
            vOnly = true;
        }

        try {
            NotificationManager.getInstance().notify(user, SmallMessage, SmallMessage, EmailTitle, EmailMessage,
                    endpointApiKey, PushTitle, PushData, ExpirationDate, nType, vOnly, smsSign, false);
        } catch (Exception e) {
            throw new HttpInternalException(e);
        }

        return null;
    }
}
