/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.notification.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.entity.Endpoint;
import net.stemteam.authservice.entity.enums.NotificationTypeCode;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataColumnType;
import net.stemteam.datatransport.transport.DataRecord;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.exception.RightServiceException;

import java.sql.SQLException;
import java.util.HashMap;
import net.stemteam.authservice.api.support.UserRequestPropogation;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.exception.UserNotFoundException;
import net.stemteam.jaxis.err.StemException;

/**
 * Возвращает связанных с пользователем пользователей
 */
public class EndpointListGet extends DataSetHandler {

    @Override
    protected DataSet getData(HashMap params, JaxisContentType jct) throws
            ConnectionPoolNotFoundException, DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpBadRequestException,
            HttpInternalException, HttpUnauthorizedException, NotConfiguredException, HttpNotFoundException,
            RightServiceException, UserNotAuthorizedException, UserNotFoundException, StemException {
        String Version = getParamValidator().getString(params, "Version", true); // версия протокола
        String Token = getParamValidator().getString(params, "Token", true); // токен авторизации пользователя
        String ApiKey = getParamValidator().getString(params, "ApiKey", true); // ключ апи
        String guuid = getParamValidator().getString(params, "Guuid"); // id пользователя владельца контактов
        getParamValidator().postValidate();

        // проверка апи-ключа
        ApiKeyHelper.getInstance().check(getDataSource(), ApiKey);

        // получение пользователя по токену + проверка авторизации
        User targetUser;
        User usr = getAuthService().getUser(Token, UserRequestPropogation.Roles, UserRequestPropogation.Endpoints,
                UserRequestPropogation.ReferedUsers);

        // если не задан guuid пользователя или он совпадает с получателем
        // т.е. получатель просит свои контакты
        if (guuid == null || (guuid.equals(usr.getGuuid()))) {
            targetUser = usr;
        } else {
            targetUser = getAuthService().getUserByGuuid(Token, guuid, UserRequestPropogation.Endpoints);
            // если пользователь никак не связан с получателем
            if (!usr.isRefered(targetUser, null)) {
                // если получатель не админ - значит недостаточно привелегий
                if (!usr.isSystem()) {
                    throw new HttpForbiddenException("Действие разрешено только системным пользователям");
                }
            }
        }

        // загружаем контакты целевого пользователя
        targetUser = getAuthService().getAuthRepository().loadUserEndpoints(targetUser);

        DataSet ds = new DataSet();
        ds.createColumn("id_endpoint", DataColumnType.INT32);
        ds.createColumn("notification_code", DataColumnType.STRING);
        ds.createColumn("notification_name", DataColumnType.STRING);
        ds.createColumn("notification_id", DataColumnType.STRING);
        ds.createColumn("azure_id", DataColumnType.STRING);
        ds.createColumn("pns_type", DataColumnType.STRING);
        ds.createColumn("is_tablet", DataColumnType.INT32);
        ds.createColumn("model", DataColumnType.STRING);
        ds.createColumn("locale", DataColumnType.STRING);
        ds.createColumn("device_name", DataColumnType.STRING);
        ds.createColumn("apikey", DataColumnType.STRING);
        ds.createColumn("is_verified", DataColumnType.INT32);
        ds.createColumn("is_enabled", DataColumnType.INT32);
        ds.createColumn("is_default", DataColumnType.INT32);

        for (Endpoint ep : targetUser.getEndpointList()) {
            DataRecord rec = ds.createRecord();

            rec.setInt("id_endpoint", ep.getId());
            rec.setString("notification_code", ep.getNotificationTypeCode().toString());
            if (ep.getNotificationTypeCode() == NotificationTypeCode.push) {
                rec.setString("notification_name", ep.getPushAttributes().name);
                rec.setString("model", ep.getPushAttributes().model);
                rec.setString("locale", ep.getPushAttributes().locale);
                rec.setString("device_name", ep.getPushAttributes().deviceId);
                rec.setString("apikey", ep.getPushAttributes().apiKey);
                rec.setString("azure_id", ep.getPushAttributes().azureId);
                rec.setString("pns_type", ep.getPushAttributes().PNSType.getCode());
                rec.setInt("is_tablet", ep.getPushAttributes().isTablet ? 1 : 0);
            } else {
                rec.setString("notification_id", ep.getNotificationId());
            }
            rec.setInt("is_verified", ep.isVerified() ? 1 : 0);
            rec.setInt("is_enabled", ep.isVerified() ? 1 : 0);
            rec.setInt("is_default", ep.isVerified() ? 1 : 0);

        }

        ds.doCamelColumnNames(true);
        return ds;
    }
}
