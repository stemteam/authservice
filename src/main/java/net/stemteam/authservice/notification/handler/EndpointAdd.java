/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.notification.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.entity.Endpoint;
import net.stemteam.authservice.entity.enums.NotificationTypeCode;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataColumnType;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.common.Version;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.exception.RightServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.stemteam.authservice.api.support.UserRequestPropogation;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.exception.UserNotFoundException;
import net.stemteam.jaxis.err.StemException;

/**
 * Добавление пользователя в систему другим пользователем
 */
public class EndpointAdd extends DataSetHandler {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    protected DataSet getData(HashMap params, JaxisContentType jct) throws
            ConnectionPoolNotFoundException, DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpBadRequestException,
            HttpInternalException, HttpUnauthorizedException, NotConfiguredException, NoSuchAlgorithmException,
            RightServiceException, HttpNotFoundException, UserNotAuthorizedException, UserNotFoundException,
            StemException {
        Version version = getParamValidator().getVersion(params, "Version", true);
        String ApiKey = getParamValidator().getString(params, "ApiKey", true);
        String Token = getParamValidator().getString(params, "Token", true);
        String guuid = getParamValidator().getString(params, "Guuid"); // кому добавляем
        String notificationTypeCode = getParamValidator().getString(params,
                "NotificationType", true); // способ оповещения пользователя
        String notificationId = getParamValidator().getString(params,
                "NotificationId", true); // id в системе оповещений
        Integer isEnabled = getParamValidator().getInt(params, "IsEnabled"); // уведомлять по этому контакту?
        Integer isVerified = getParamValidator().getInt(params, "IsVerified"); // контакт является проверенным?
        getParamValidator().postValidate();

        // проверяем валидность API-KEY
        ApiKeyHelper.getInstance().check(getDataSource(), ApiKey);

        if (isEnabled == null) {
            isEnabled = 1;
        }
        if (isVerified == null) {
            isVerified = 1;
        }

        NotificationTypeCode nType = NotificationTypeCode.valueOf(notificationTypeCode);

        if (nType == NotificationTypeCode.push) {
            throw new HttpForbiddenException("Запрещено добавлять контакты для PUSH этим методом.");
        }

        // проверка токена
        User user = getAuthService().getUser(Token, UserRequestPropogation.Endpoints,
                UserRequestPropogation.ReferedUsers, UserRequestPropogation.Roles);
        User targetUser;

        // если не задан guuid пользователя или он совпадает с получателем
        // т.е. получатель меняет свои контакты
        if (guuid == null || (guuid.equals(user.getGuuid()))) {
            targetUser = user;
        } else {
            targetUser = getAuthService().getUser(guuid);
            // если пользователь никак не связан с получателем
            if (!user.isRefered(targetUser, null)) {
                // если получатель не админ - значит недостаточно привелегий
                if (!user.isSystem()) {
                    throw new HttpForbiddenException("Действие разрешено только системным пользователям");
                }
            }
        }

        Endpoint endpoint = new Endpoint(null, nType, notificationId,
                null, isEnabled == 1, isVerified == 1, null);

        try (Connection conn = getConnection()) {
            getAuthService().getAuthRepository().upsertEndpoint(conn, targetUser.getId(), endpoint);
            List<Endpoint> endpoints = new ArrayList<>();
            endpoints.add(endpoint);
            user.setEndpoints(endpoints);

            conn.commit();
        }

        DataSet ds = new DataSet();
        ds.createColumn("Id", DataColumnType.INT32);
        ds.createRecord().setInt("Id", endpoint.getId());
        return ds;
    }
}
