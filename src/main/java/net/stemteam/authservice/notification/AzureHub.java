/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.notification;

import com.windowsazure.messaging.AppleRegistration;
import com.windowsazure.messaging.AppleTemplateRegistration;
import com.windowsazure.messaging.GcmRegistration;
import com.windowsazure.messaging.GcmTemplateRegistration;
import com.windowsazure.messaging.MpnsRegistration;
import com.windowsazure.messaging.MpnsTemplateRegistration;
import com.windowsazure.messaging.Notification;
import com.windowsazure.messaging.NotificationHub;
import com.windowsazure.messaging.Registration;
import com.windowsazure.messaging.WindowsRegistration;
import com.windowsazure.messaging.WindowsTemplateRegistration;
import net.stemteam.jaxis.common.PNSType;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Set;

/**
 * Настройки соединения с хабой оповещений Microsoft Azure
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class AzureHub {

    private String apiKey;
    private String url;
    private String name;

    private NotificationHub hub = null;
    
    /**
     * Настройки соединения с хабой оповещений Microsoft Azure
     *
     * @param apiKey Ключ API для идентификации прилжения связанного с хабом (хабы азуре могут обслуживать только одно
     * приложение)
     * @param url Строка соединения с хабом
     * @param name Название
     */
    public AzureHub(String apiKey, String url, String name) {
        this.apiKey = apiKey;
        this.url = url;
        this.name = name;
        hub = new NotificationHub(url, name);
    }

    /**
     * Ключ API для идентификации прилжения связанного с хабом (хабы азуре могут обслуживать только одно приложение)
     *
     * @return
     */
    public String getApiKey() {
        return apiKey;
    }

    /**
     * Строка соединения с хабом
     *
     * @return
     */
    public String getUrl() {
        return url;
    }

    /**
     * Название
     *
     * @return
     */
    public String getName() {
        return name;
    }
    
    /**
     * Создание нового хаба в Azure
     * Мы используем 1 хаб и создавали его через сайт Azure
     * И этот класс пока не умеет работать с несколькими хабами
     * Поэтому метод пока не используется
     * @param settings
     */
//    public void createNotificationHub(HubSettings settings) {
//        NotificationHubDescription hubDescr = null;
//        hubDescr = new NotificationHubDescription(settings.hubName);
//        if (settings.windowsCredential != null)
//            hubDescr.setWindowsCredential(settings.windowsCredential);
//        if (settings.mpnsCredential != null)
//            hubDescr.setMpnsCredential(settings.mpnsCredential);
//        if (settings.apnsCredential != null)
//            hubDescr.setApnsCredential(settings.apnsCredential);
//        if (settings.gcmCredential != null)
//            hubDescr.setGcmCredential(settings.gcmCredential);
//
//        hubDescr = nmanager.createNotificationHub(hubDescr);
//        hub = new NotificationHub(settings.connectionString, settings.hubName);
//    }

    public static String wrapAPNSMessage(String message) {
        return String.format("{\"aps\": {\"alert\" : \"%s\" }}", message);
    }

    public static String wrapGCMMessage(String message) {
        return String.format("{\"alert\" : \"%s\" }", message);
    }

    public static String wrapWindowsMessage(String message) {
        return String.format("<toast>\n" +
                "<visual>\n" +
                "<binding template=\\\"ToastText01\\\">\n" +
                "<text id=\\\"1\\\">%s</text>\n" +
                "</binding>\n" +
                "</visual>\n" +
                "</toast>", message);
    }

    /**
     * Получение прямого доступа к хабу
     * @return
     */
    public NotificationHub getHub() {
        return hub;
    }

    /**
     * Создать id для новой регистрации устройства в Azure
     * @return - id новой регистрации
     */
    public String createRegistrationId() {
        return hub.createRegistrationId();
    }

    protected Registration createRegistration(Registration reg, Set<String> tags) {
        for (String tag : tags)
            reg.getTags().add(tag);
        return hub.createRegistration(reg);
    }

    protected Registration upsertRegistration(Registration reg, Set<String> tags) {
        for (String tag : tags)
            reg.getTags().add(tag);
        return hub.upsertRegistration(reg);
    }

    /**
     * Создать новую нативную регистрацию устройства
     * @param deviceToken - дескриптор в соответствующей службе PNS
     * @param pns - служба PNS (Platform Notification Service)
     * @param tags - множество тегов для данногой регистрации
     * @return id - новой регистрации
     * @throws URISyntaxException
     */
    public String createRegistration(String deviceToken, PNSType pns, Set<String> tags) throws URISyntaxException {
        Registration reg = null;
        switch (pns) {
            case GCM:
                reg = new GcmRegistration(deviceToken);
                break;
            case APNS:
                reg = new AppleRegistration(deviceToken);
                break;
            case MPNS:
                reg = new MpnsRegistration(new URI(deviceToken));
                break;
            default: // WNS
                reg = new WindowsRegistration(new URI(deviceToken));
        }

        reg = createRegistration(reg, tags);
        return reg.getRegistrationId();
    }

    /**
     * Создать новую или обновить существующую нативную регистрацию устройства в Azure
     * @param regId - id существующей регистрации
     * @param deviceToken - дескриптор в соответствующей службе PNS
     * @param pns - служба PNS (Platform Notification Service)
     * @param tags - множество тегов для данногой регистрации
     * @throws URISyntaxException
     */
    public void upsertRegistration(String regId, String deviceToken, PNSType pns, Set<String> tags) throws URISyntaxException {
        Registration reg = null;
        switch (pns) {
            case GCM:
                reg = new GcmRegistration(regId, deviceToken);
                break;
            case APNS:
                reg = new AppleRegistration(regId, deviceToken);
                break;
            case MPNS:
                reg = new MpnsRegistration(regId, new URI(deviceToken));
                break;
            default: // WNS
                reg = new WindowsRegistration(regId, new URI(deviceToken));
        }
        upsertRegistration(reg, tags);
    }

    /**
     * Создать новую или обновить существующую шаблонную регистрацию устройства в Azure
     * @param deviceToken - дескриптор в соответствующей службе PNS
     * @param pns - служба PNS (Platform Notification Service)
     * @param bodyTemplate - тело шаблона в формате соответствующей PNS
     * @param tags - множество тегов для данногой регистрации
     * @return id - новой регистрации
     * @throws URISyntaxException
     */
    public String createTemplateRegistration(String deviceToken, PNSType pns, String bodyTemplate, Set<String> tags) throws URISyntaxException {
        Registration reg = null;
        switch (pns) {
            case GCM:
                reg = new GcmTemplateRegistration(deviceToken, bodyTemplate);
                break;
            case APNS:
                reg = new AppleTemplateRegistration(deviceToken, bodyTemplate);
                break;
            case MPNS:
                reg = new MpnsTemplateRegistration(new URI(deviceToken), bodyTemplate);
                break;
            default: // WNS
                reg = new WindowsTemplateRegistration(new URI(deviceToken), bodyTemplate);
        }

        reg = createRegistration(reg, tags);
        return reg.getRegistrationId();
    }

    /**
     * Создать новую шаблонную регистрацию устройства
     * @param regId - id существующей регистрации
     * @param deviceToken - дескриптор в соответствующей службе PNS
     * @param pns - служба PNS (Platform Notification Service)
     * @param bodyTemplate - тело шаблона в формате соответствующей PNS
     * @param tags - множество тегов для данногой регистрации
     * @return id - новой регистрации
     * @throws URISyntaxException
     */
    public String upsertTemplateRegistration(String regId, String deviceToken, PNSType pns, String bodyTemplate, Set<String> tags) throws URISyntaxException {
        Registration reg = null;
        switch (pns) {
            case GCM:
                reg = new GcmTemplateRegistration(regId, deviceToken, bodyTemplate);
                break;
            case APNS:
                reg = new AppleTemplateRegistration(regId, deviceToken, bodyTemplate);
                break;
            case MPNS:
                //TODO: надо как-то обновлять windows регистрации
                reg = new MpnsTemplateRegistration(new URI(deviceToken), bodyTemplate);
                break;
            default: // WNS
                reg = new WindowsTemplateRegistration(new URI(deviceToken), bodyTemplate);
        }

        reg = createRegistration(reg, tags);
        return reg.getRegistrationId();
    }

    public void deleteRegistration(String regId) {
        hub.deleteRegistration(regId);
    }

    protected Notification createNotification(PNSType pns, String message) {
        Notification n = null;
        switch (pns) {
            case GCM:
                n = Notification.createGcmNotifiation(message);
                break;
            case APNS:
                n = Notification.createAppleNotifiation(message);
                break;
            case MPNS:
                n = Notification.createMpnsNotifiation(message);
                break;
            default: // WNS
                n = Notification.createWindowsNotification(message);
        }
        return n;
    }

    /**
     * Отправить PUSH уведомление на нативные регистрации с соответствующим тегом
     * @param pns - служба PNS (Platform Notification Service)
     * @param message - сообщение для отправки в формате соответствующей PNS
     * @param tags - множество тегов в регистрациях, которым надо доставить сообщение
     */
    public void sendNotification(PNSType pns, String message, Set<String> tags) {
        Notification n =createNotification(pns, message);

        if (tags == null || tags.isEmpty())
            hub.sendNotification(n);
        else
            hub.sendNotification(n, tags);
    }

    /**
     * Отправить PUSH уведомление на нативные регистрации с соответствующим тегом
     * tagExpression это логическое выражение из тегов для определения кому необходимо отправить сообщение.
     * Это выражение применяется ко всем регистрациям соответствующей платформы
     * @param pns - служба PNS (Platform Notification Service)
     * @param message - сообщение для отправки в формате соответствующей PNS
     * @param tagExpression - логическое выражение из тегов
     */
    public void sendNotification(PNSType pns, String message, String tagExpression) {
        Notification n =createNotification(pns, message);

        if (tagExpression == null || tagExpression.isEmpty())
            hub.sendNotification(n);
        else
            hub.sendNotification(n, tagExpression);
    }

    /**
     * Отправить шаблонное сообщение
     * @param prop - карта параметров шаблона
     * @param tags - множество тегов в регистрациях, которым надо доставить сообщение
     */
    public void sendTemplateNotification(Map<String, String> prop, Set<String> tags) {
        Notification n = Notification.createTemplateNotification(prop);
        hub.sendNotification(n, tags);
    }

    /**
     * Отправить шаблонное сообщение
     * @param prop - карта параметров шаблона
     * @param tagExpression - логическое выражение из тегов
     */
    public void sendTemplateNotification(Map<String, String> prop, String tagExpression) {
        Notification n = Notification.createTemplateNotification(prop);
        hub.sendNotification(n, tagExpression);
    }

    /**
     * Отправить сообщение всем устройствам из данной PNS
     * @param pns - служба PNS (Platform Notification Service)
     * @param message - сообщение для отправки в формате соответствующей PNS
     */
    public void broadcast(PNSType pns, String message) {
        sendNotification(pns, message, "");
    }

}
