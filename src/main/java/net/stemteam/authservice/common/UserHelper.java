/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.common;

import net.stemteam.rightservice.entity.Role;
import net.stemteam.authservice.entity.User;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.NotConfiguredException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Хэлпер по работе с пользователями
 */
public class UserHelper {

    protected UserHelper() {
        // запрещает вызов из подклассов
        throw new UnsupportedOperationException();
    }


    /**
     * Проверяет права пользователя на выполнение метода
     *
     * @param ds базка, откуда тянуть данные
     * @param guuid глобальный идентификатор пользователя
     * @param method урл метода (пример: /meta/FieldListGet)
     * @throws net.stemteam.jaxis.err.NotConfiguredException
     * @throws java.sql.SQLException
     * @throws net.stemteam.jaxis.err.HttpForbiddenException
     */
    public static void checkRight(DataSource ds, String guuid, String method) throws NotConfiguredException,
            SQLException,
            HttpForbiddenException {
        try (Connection conn = ds.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                    " select 1 as permit "
                    + " from STEM_RIGHT_user u "
                    + " join STEM_RIGHT_userrole ur on u.id_STEM_RIGHT_user = ur.id_STEM_RIGHT_user "
                    + " join STEM_RIGHT_roleright r on ur.id_STEM_RIGHT_role = r.id_STEM_RIGHT_role "
                    + " join STEM_RIGHT_right b on b.id_STEM_RIGHT_right = r.id_STEM_RIGHT_right "
                    + " where u.guuid = ? "
                    + "   and b.method = ? ");
            ps.setString(1, guuid);
            ps.setString(2, method);
            try (ResultSet rs = ps.executeQuery()) {
                if (!rs.next()) {
                    throw new HttpForbiddenException("Пользователю запрещено выполнение метода " + method);
                }
            }
        }
    }

    /**
     * Проверяет валидность Email
     *
     * @param email
     * @return
     */
    public static boolean isValidEmail(String email) {
        Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
        Matcher m = p.matcher(email);
        return m.matches();
    }
}
