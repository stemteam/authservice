/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.common;

import net.stemteam.jaxis.err.HttpException;
import net.stemteam.jaxis.err.HttpInternalException;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.List;

/**
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class HttpClientHelper {

    /**
     * Подготавливает билдер с доверительным SSL
     *
     * @return
     * @throws NoSuchAlgorithmException
     * @throws KeyStoreException
     * @throws KeyManagementException
     */
    private HttpClientBuilder getTrustedSslBuilder() throws NoSuchAlgorithmException, KeyStoreException,
            KeyManagementException {
        SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, new TrustStrategy() {
            @Override
            public boolean isTrusted(final X509Certificate[] chain, String authType) {
                return true;
            }
        }).build();
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,
                SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

        RegistryBuilder<ConnectionSocketFactory> connRegistryBuilder = RegistryBuilder.create();
        connRegistryBuilder.register("http", PlainConnectionSocketFactory.INSTANCE);
        connRegistryBuilder.register("https", sslsf);

        Registry<ConnectionSocketFactory> connRegistry = connRegistryBuilder.build();
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(connRegistry);
        connectionManager.setMaxTotal(10);
        connectionManager.setDefaultMaxPerRoute(10);

        RequestConfig requestConfig = RequestConfig
                .custom()
                .setExpectContinueEnabled(false)
                .setCookieSpec(CookieSpecs.DEFAULT)
                .setRedirectsEnabled(false).setSocketTimeout(30000)
                .setConnectTimeout(30000)
                .build();

        HttpClientBuilder clientBuilder = HttpClientBuilder.create();
        clientBuilder.setDefaultRequestConfig(requestConfig);
        clientBuilder.setConnectionManager(connectionManager);
        return clientBuilder;
    }

    /**
     * GET запрос к нужному урлу
     *
     * @param url
     * @return
     * @throws HttpException
     */
    public String httpGet(String url) throws HttpException {
        System.out.println("HTTP GET: " + url);
        HttpClientBuilder clientBuilder;
        try {
            clientBuilder = getTrustedSslBuilder();
        } catch (NoSuchAlgorithmException | KeyStoreException | KeyManagementException ex) {
            throw new HttpInternalException(ex);
        }
        clientBuilder.setUserAgent(DEFAUTL_USER_AGENT);

//        // куки
//        BasicCookieStore cookieStore = new BasicCookieStore();
//        BasicClientCookie c1 = new BasicClientCookie("PHPSESSID", "o8v1ui2pf7bsbvttfv3ukbb030");
//        c1.setDomain("best-restaurant.krd.sobaka.ru");
//        c1.setPath("/");
//        cookieStore.addCookie(c1);
//        clientBuilder.setDefaultCookieStore(cookieStore);
        // clientBuilder.setDefaultHeaders(config.getDefaultHeaders());
        try (CloseableHttpClient httpClient = clientBuilder.build()) {
            HttpGet request = new HttpGet(url);
            try (CloseableHttpResponse response = httpClient.execute(request)) {
                HttpEntity resEntity = response.getEntity();
                if (response.getStatusLine().getStatusCode() == 200) {
                    return EntityUtils.toString(resEntity);
                } else {
                    throw new HttpException(response.getStatusLine().getStatusCode(), EntityUtils.toString(resEntity));
                }
            }
        } catch (IOException ex) {
            throw new HttpInternalException(ex);
        }
    }

    /**
     * POST запрос с типом Multipart
     *
     * @param url
     * @param params
     * @param f файл
     * @param fileParamName имя параметра для передачи файла
     * @return
     * @throws Exception
     */
    public String httpMultipart(String url, List<NameValuePair> params, File f, String fileParamName)
            throws Exception {
        System.out.println("HTTP MULTIPART: " + url);
        HttpClientBuilder clientBuilder = getTrustedSslBuilder();
        clientBuilder.setUserAgent(DEFAUTL_USER_AGENT);

        try (CloseableHttpClient httpClient = clientBuilder.build()) {
            HttpPost post = new HttpPost(url);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            // атач файла
            if (f != null) {
                builder.addBinaryBody(fileParamName, f, ContentType.APPLICATION_OCTET_STREAM, f.getName());
            }

            // параметры
            if (params != null) {
                for (NameValuePair param : params) {
                    builder.addTextBody(param.getName(), param.getValue());
                }
            }

            // поехали
            post.setEntity(builder.build());

            try (CloseableHttpResponse response = httpClient.execute(post)) {
                HttpEntity resEntity = response.getEntity();
                if (response.getStatusLine().getStatusCode() == 200) {
                    return EntityUtils.toString(resEntity);
                } else {
                    System.out.println(EntityUtils.toString(resEntity));
                    throw new Exception(EntityUtils.toString(resEntity));
                }
            }
        } catch (IOException ex) {
            throw new HttpInternalException(ex);
        }
    }

    /**
     * POST запрос
     *
     * @param url
     * @param params
     * @return
     * @throws HttpException
     */
    public String httpPost(String url, List<NameValuePair> params) throws HttpException {
        HttpClientBuilder clientBuilder;
        try {
            clientBuilder = getTrustedSslBuilder();
        } catch (NoSuchAlgorithmException | KeyStoreException | KeyManagementException ex) {
            throw new HttpInternalException(ex);
        }
        clientBuilder.setUserAgent(DEFAUTL_USER_AGENT);

        try (CloseableHttpClient httpClient = clientBuilder.build()) {
            HttpPost post = new HttpPost(url);
            post.setEntity(new UrlEncodedFormEntity(params, "utf-8"));
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                HttpEntity resEntity = response.getEntity();
                if (response.getStatusLine().getStatusCode() == 200) {
                    return EntityUtils.toString(resEntity);
                } else {
                    throw new HttpException(response.getStatusLine().getStatusCode(), EntityUtils.toString(resEntity));
                }
            }
        } catch (IOException ex) {
            throw new HttpInternalException(ex);
        }
    }

    public static final String DEFAUTL_USER_AGENT
            = "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko";
}
