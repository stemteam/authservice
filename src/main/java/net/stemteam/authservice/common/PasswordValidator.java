package net.stemteam.authservice.common;

public class PasswordValidator {

    /**
     * Проверяет пароль на допустимость высоким стандартам.
     * 
     * Длина пароля не менее 6 символов.
     * 
     * Пароль не может содержать пробел.
     * 
     * @param password
     * @return
     */
    public boolean validate(String password) {
        if (password == null) {
            return false;
        }
        if (password.contains(" ")) {
            return false;
        }
        if (password.length() < 6) {
            return false;
        }
        return true;
    }
    
    public String getValidationMessage() {
        return "Длина пароля должна быть не менее 6 символов, пароль не должен содержать пробелы";
    }

}
