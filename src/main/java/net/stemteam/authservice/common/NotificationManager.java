/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.common;

import net.stemteam.authservice.entity.Endpoint;
import net.stemteam.authservice.entity.enums.NotificationTypeCode;
import net.stemteam.authservice.entity.User;
import net.stemteam.datatransport.db.ParamBinder;
import net.stemteam.jaxis.common.PNSType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.sql.DataSource;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Set;
import net.stemteam.authservice.service.impl.AuthServiceNative;

/**
 * Менеджер оповещений
 */
public class NotificationManager {

    private static final Logger logger = LoggerFactory.getLogger(NotificationManager.class);

    static final String GCM_MSG_TMPLT = "{\"notification\":{"
            + "    \"title\": \"$(title)\","
            + "    \"body\": \"$(text)\","
            + "    \"sound\": \"default\","
            + "    \"click_action\": \"$(activity)\""
            + "  },"
            + "  \"data\":{"
            + "    \"message\": \"$(text)\","
            + "    \"type\": \"$(type)\","
            + "    \"value\": \"$(value)\""
            + "  }"
            + "}";

    static final String APNS_MSG_TMPLT = "{\"aps\":{"
            + "  \"alert\":{"
            + "    \"title\": \"$(title)\","
            + "    \"body\": \"$(text)\""
            + "  }},"
            + "  \"sound\": \"default\","
            + "  \"data\":{"
            + "    \"type\": \"$(type)\","
            + "    \"value\": \"$(value)\","
            + "    \"click_action\": \"$(activity)\""
            + "  }"
            + "}";

    //TODO: Добавить шаблон для MPNS
    static final String MPNS_MSG_TMPLT = "";

    //TODO: Разобраться в шаблоне для WNS
    static final String WNS_MSG_TMPLT = "<toast>" + "<visual>" + "<binding template=\\\"ToastText01\\\">"
            + "<text id=\\\"1\\\">$(text)</text>" + "</binding>" + "</visual>" + "</toast>";

    private DataSource dataSource = null;

    // Запрещаем инстансирование
    protected NotificationManager() {
    }

    private AuthServiceNative service;

    public AuthServiceNative getService() {
        return service;
    }

    public void setService(AuthServiceNative service) {
        this.service = service;
    }

    /**
     * Получить шаблон простейшего PUSH уведомления для соответствующей PNS
     *
     * @param pns служба PNS (Platform Notification Service)
     * @return
     */
    protected static String getPNSTemplate(PNSType pns) {
        String tpl = null;
        switch (pns) {
            case GCM:
                tpl = GCM_MSG_TMPLT;
                break;
            case APNS:
                tpl = APNS_MSG_TMPLT;
                break;
            case MPNS:
                tpl = MPNS_MSG_TMPLT;
                break;
            default: // WNS
                tpl = WNS_MSG_TMPLT;
        }
        return tpl;
    }

    /**
     * Получить новый ID регистрации Azure
     *
     * @param apiKey ключ апи приложения, обслуживаемого хабом
     * @return
     */
    public String azureCreateRegistrationId(String apiKey) {
        return service.getAzureHubs().get(apiKey).createRegistrationId();
    }

    /**
     * Добавить или обновить данные регистрации по regId
     *
     * @param apiKey ключ апи приложения, обслуживаемого хабом
     * @param regId id существующей регистрации
     * @param deviceHandle дескриптор в соответствующей службе PNS
     * @param pns служба PNS (Platform Notification Service)
     * @param tags множество тегов для данногой регистрации
     * @return id обновленной регистрации
     * @throws URISyntaxException
     */
    public String azureUpdateRegistration(String apiKey, String regId, String deviceHandle, PNSType pns,
            Set<String> tags) throws URISyntaxException {
        return service.getAzureHubs().get(apiKey).upsertTemplateRegistration(regId, deviceHandle, pns, getPNSTemplate(
                pns), tags);
    }

    /**
     * Оповестить пользователя о чем-либо. Отправка только на проверенные и включенные контакты.
     *
     * @param user описание пользователя
     * @param smsMessage текст короткого сообщения для SMS
     * @param pushMessage текст короткого сообщения для PUSH
     * @param emailTitle тема письма для email
     * @param emailMessage текст сообщения для email
     * @param endpointApiKey апи ключ приложения
     * @param pushTitle заголовок push
     * @param pushData дата для push (url для pushType url)
     * @param expirationDate дата до которой актуальна отправка данного сообщения
     * @param nType тип системы уведомлений (email/sms/push). Если nType == null, отправка на все типы систем
     * уведомления
     * @param verifiedOnly отправлять только на проверенные контакты
     * @param smsSign подпись СМС (отправитель)
     * @param defaultOnly отправлять только на контакты по умолчанию
     * @throws Exception
     */
    public void notify(User user, String smsMessage, String pushMessage, String emailTitle, String emailMessage,
            String endpointApiKey, String pushTitle, String pushData, Timestamp expirationDate,
            NotificationTypeCode nType,
            boolean verifiedOnly, String smsSign, boolean defaultOnly) throws Exception {
        try (Connection conn = dataSource.getConnection()) {
            try {
                PreparedStatement ps = conn.prepareStatement(
                        "insert into notification(id_endpoint, title, message, data, expirationdate) "
                        + "values(?,?,?,?, coalesce(?, current_timestamp + interval '1 hour'))");

                // ставим сообщение в очередь по всем контактам в одной транзакции
                for (Endpoint ep : user.getEndpointList()) {

                    if ((defaultOnly) && (!ep.isDefault())) {
                        logger.debug("notification for user {} endpoint {} skipped", user.getId(), ep.getId());
                        continue;
                    }

                    // если тип уведомления не задан или совпадает
                    if (nType == null || ep.getNotificationTypeCode() == nType) {
                        switch (ep.getNotificationTypeCode()) {
                            case sms:
                                if (ep.isEnabled() && (!verifiedOnly || ep.isVerified())) {
                                    int num = 1;
                                    ParamBinder.setInt(ps, num++, ep.getId());
                                    ParamBinder.setString(ps, num++, null);
                                    ParamBinder.setString(ps, num++, smsMessage);
                                    ParamBinder.setString(ps, num++, smsSign);
                                    ParamBinder.setTimestamp(ps, num++, expirationDate);
                                    ps.execute();
                                    logger.debug("notification for user {} endpoint {}", user.getId(), ep.getId());
                                }
                                break;
                            case push:
                                // если код приложения не задан или код приложения не задан в контакте или они совпадают
                                if ((endpointApiKey == null || endpointApiKey.isEmpty())
                                        || (ep.getPushAttributes().apiKey == null || ep.getPushAttributes().apiKey.
                                        isEmpty()) || (endpointApiKey.equals(ep.getPushAttributes().apiKey))) {
                                    if (ep.isEnabled() && (!verifiedOnly || ep.isVerified())) {
                                        int num = 1;
                                        ParamBinder.setInt(ps, num++, ep.getId());
                                        ParamBinder.setString(ps, num++, pushTitle);
                                        ParamBinder.setString(ps, num++, pushMessage);
                                        ParamBinder.setString(ps, num++, pushData);
                                        ParamBinder.setTimestamp(ps, num++, expirationDate);
                                        ps.execute();
                                        logger.debug("notification for user {} endpoint {}", user.getId(), ep.getId());
                                    }
                                }
                                break;
                            case email:
                                if (ep.isEnabled() && (!verifiedOnly || ep.isVerified())) {
                                    int num = 1;
                                    ParamBinder.setInt(ps, num++, ep.getId());
                                    ParamBinder.setString(ps, num++, emailTitle);
                                    ParamBinder.setString(ps, num++, emailMessage);
                                    ParamBinder.setString(ps, num++, null);
                                    ParamBinder.setTimestamp(ps, num++, expirationDate);
                                    ps.execute();
                                    logger.debug("notification for user {} endpoint {}", user.getId(), ep.getId());
                                }
                            default:
                            // способ отправки не определен
                        }
                    }
                }
                conn.commit();
            } catch (Exception ex) {
                conn.rollback();
                throw new RuntimeException("Failed to queue the message", ex);
            }
        }
    }

    /**
     * Оповестить пользователя о чем-либо. Отправка только на заданный контакт.
     *
     * @param endpoint описание контакта
     * @param user описание пользователя
     * @param smsMessage текст короткого сообщения для SMS
     * @param pushMessage текст короткого сообщения для PUSH
     * @param emailTitle тема письма для email
     * @param emailMessage текст сообщения для email
     * @param endpointApiKey апи ключ приложения
     * @param pushTitle заголовок push
     * @param pushData дата для push (url для pushType url)
     * @param expirationDate дата до которой актуальна отправка данного сообщения
     * @param smsSign подпись СМС (отправитель)
     * @throws Exception
     */
    public void notify(Endpoint endpoint, User user, String smsMessage, String pushMessage,
            String emailTitle,
            String emailMessage, String endpointApiKey, String pushTitle, String pushData,
            Timestamp expirationDate, String smsSign) throws Exception {
        try (Connection conn = dataSource.getConnection()) {
            try {
                PreparedStatement ps = conn.prepareStatement(
                        "insert into notification(id_endpoint, title, message, data, expirationdate) "
                        + "values(?,?,?,?, coalesce(?, current_timestamp + interval '1 hour'))");

                int num = 1;
                switch (endpoint.getNotificationTypeCode()) {
                    case sms:
                        ParamBinder.setInt(ps, num++, endpoint.getId());
                        ParamBinder.setString(ps, num++, null);
                        ParamBinder.setString(ps, num++, smsMessage);
                        ParamBinder.setString(ps, num++, smsSign);
                        ParamBinder.setTimestamp(ps, num++, expirationDate);
                        ps.execute();
                        logger.debug("notification for user {} endpoint {}", user.getId(), endpoint.getId());
                        break;
                    case push:
                        // если код приложения не задан или код приложения не задан в контакте или они совпадают
                        if ((endpointApiKey == null || endpointApiKey.isEmpty())
                                || (endpoint.getPushAttributes().apiKey == null || endpoint.getPushAttributes().apiKey.
                                isEmpty()) || (endpointApiKey.equals(endpoint.getPushAttributes().apiKey))) {

                            ParamBinder.setInt(ps, num++, endpoint.getId());
                            ParamBinder.setString(ps, num++, pushTitle);
                            ParamBinder.setString(ps, num++, pushMessage);
                            ParamBinder.setString(ps, num++, pushData);
                            ParamBinder.setTimestamp(ps, num++, expirationDate);
                            ps.execute();
                            logger.debug("notification for user {} endpoint {}", user.getId(), endpoint.getId());
                        }
                        break;
                    case email:
                        ParamBinder.setInt(ps, num++, endpoint.getId());
                        ParamBinder.setString(ps, num++, emailTitle);
                        ParamBinder.setString(ps, num++, emailMessage);
                        ParamBinder.setString(ps, num++, null);
                        ParamBinder.setTimestamp(ps, num++, expirationDate);
                        ps.execute();
                        logger.debug("notification for user {} endpoint {}", user.getId(), endpoint.getId());
                    default:
                    // способ отправки не определен
                }

                conn.commit();
            } catch (Exception ex) {
                conn.rollback();
                throw new RuntimeException("Failed to queue the message", ex);
            }
        }
    }

    public static NotificationManager getInstance() {
        return NotificationManager.NotificationManagerHolder.INSTANCE;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static class NotificationManagerHolder {

        private static final NotificationManager INSTANCE = new NotificationManager();

        private NotificationManagerHolder() {
        }
    }

}
