/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Хэлпер пользовательских запросов
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class UserRequestHelper {

    protected UserRequestHelper() {
        throw new UnsupportedOperationException("not allowed for tool classes");
    }

    /**
     * Генерирует случайный код активации, 6 цифр
     *
     * @return
     */
    public static String prepareCode() {
        String s = "";
        for (int i = 0; i < 6; i++) {
            s += Math.round(Math.random() * 9.0);
        }
        return s;
    }

    /**
     * Создает запись о пользовательском запросе, возвращает код запроса.
     *
     * @param conn
     * @param type
     * @param guuid
     * @param expirationPeriodSeconds
     * @return сгенерированный код
     * @throws SQLException
     */
    public static String addUserRequest(Connection conn, UserRequestType type, String guuid, int expirationPeriodSeconds)
            throws SQLException {
        String code = prepareCode();
        try (PreparedStatement ps = conn.prepareStatement(
                " insert into user_request (id_sysuser, id_user_request_type, code, expiration_time) "
                + " values ("
                + " (select id_sysuser from sysuser where guuid = ?::uuid), "
                + " (select id_user_request_type from user_request_type where code = ?), "
                + " ?, current_timestamp + cast(? || ' second' as interval))")) {
            ps.setString(1, guuid);
            ps.setString(2, type.getCode());
            ps.setString(3, code);
            ps.setInt(4, expirationPeriodSeconds);
            ps.execute();
        }
        return code;
    }

    /**
     * Проверяет корректность кода запроса пользователя
     * @param conn конект
     * @param type тип запроса
     * @param guuid гуид пользователя
     * @param code код для проверки
     * @return
     * @throws SQLException 
     */
    public static boolean codeExists(Connection conn, UserRequestType type, String guuid, String code)
            throws SQLException {
        try (PreparedStatement ps = conn.prepareStatement(
                " select 1 "
                + " from user_request r "
                + " join user_request_type t on r.id_user_request_type = t.id_user_request_type and t.code = ? "
                + " join sysuser u on r.id_sysuser = u.id_sysuser and u.guuid = ?::uuid "
                + " where r.code = ? and r.expiration_time > current_timestamp ")) {
            ps.setString(1, type.getCode());
            ps.setString(2, guuid);
            ps.setString(3, code);
            try (ResultSet rs = ps.executeQuery()) {
                return rs.next();
            }
        }
    }
    
    /**
     * Получить текущий (актуальный) код по типу запроса пользователя. Если не найден - возвращает null.
     * @param conn конект
     * @param type тип запроса пользователя (код типа)
     * @param guuid идентификатор пользователя
     * @return код
     * @throws SQLException 
     */
    public static String getCode(Connection conn, UserRequestType type, String guuid)
            throws SQLException {
        try (PreparedStatement ps = conn.prepareStatement(
                " select r.code "
                + " from user_request r "
                + " join user_request_type t on r.id_user_request_type = t.id_user_request_type and t.code = ? "
                + " join sysuser u on r.id_sysuser = u.id_sysuser and u.guuid = ?::uuid "
                + " where r.expiration_time > current_timestamp ")) {
            ps.setString(1, type.getCode());
            ps.setString(2, guuid);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs.getString("CODE");
                } else {
                    return null;
                }
            }
        }
    }

}
