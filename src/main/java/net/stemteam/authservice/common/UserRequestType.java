/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.common;

/**
 * Типы запросов пользователей
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public enum UserRequestType {

    REGISTER("register", "Регистрация"),
    PASSRECOVER("passrecover", "Восстановление пароля"),
    DELETE("delete", "Удаление профиля");

    private final String code;
    private final String name;

    UserRequestType(final String code, final String name) {
        this.code = code;
        this.name = name;
    }

    /**
     * Код
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     * Название
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Доступ к элементу по коду
     *
     * @param code код (sms, email, push)
     * @return
     */
    public static UserRequestType getByCode(String code) {
        for (UserRequestType v : values()) {
            if (v.getCode().equals(code)) {
                return v;
            }
        }
        throw new IllegalArgumentException(String.format("Code '%s' not exists", code));
    }
}
