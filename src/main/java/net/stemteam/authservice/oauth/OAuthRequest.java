/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.oauth;

/**
 * Данные для выполнения начального запроса авторизации OAuth
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class OAuthRequest {

    private String url;
    private String state;

    public OAuthRequest() {
    }

    /**
     * Урл запрос во внешнюю систему для авторизации OAuth
     *
     * @return
     */
    public String getUrl() {
        return url;
    }

    /**
     * Урл запрос во внешнюю систему для авторизации OAuth
     *
     * @param url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Уникальная произвольная строка, созданная приложением для защиты от подделки межсайтовых запросов. Обычно в это
     * кладут идентификатор, чтобы затем проверить результаты авторизации.
     *
     * @return
     */
    public String getState() {
        return state;
    }

    /**
     * Уникальная произвольная строка, созданная приложением для защиты от подделки межсайтовых запросов. Обычно в это
     * кладут идентификатор, чтобы затем проверить результаты авторизации.
     *
     * @param state
     */
    public void setState(String state) {
        this.state = state;
    }

}
