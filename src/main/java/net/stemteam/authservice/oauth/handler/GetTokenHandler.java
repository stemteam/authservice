/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.oauth.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.entity.enums.NotificationTypeCode;
import net.stemteam.authservice.entity.NotificationType;
import net.stemteam.authservice.entity.SysLogin;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataColumnType;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;

import java.sql.SQLException;
import java.util.HashMap;

/**
 * Получение токен авторизации по коду (state) переданному в при авторизации во внешней системе посредством OAuth 2
 */
public class GetTokenHandler extends DataSetHandler {

    @Override
    protected DataSet getData(HashMap params, JaxisContentType jct) throws
            ConnectionPoolNotFoundException, DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpBadRequestException,
            HttpInternalException, HttpUnauthorizedException, NotConfiguredException, HttpNotFoundException, Exception {
        String Version = getParamValidator().getString(params, "Version", true);
        String ApiKey = getParamValidator().getString(params, "ApiKey", true);
        String appCode = getParamValidator().getString(params, "appCode", true);
        String state = getParamValidator().getString(params, "state", true);
        getParamValidator().postValidate();

        // проверка апи-ключа
        ApiKeyHelper.getInstance().check(getDataSource(), ApiKey);

        NotificationType nt = getAuthService().getAuthRepository().getNotificationType(NotificationTypeCode.valueOf(
                appCode));
        SysLogin login = getAuthService().getAuthRepository().getSysLogin(nt, state);
        if (login == null) {
            throw new HttpNotFoundException("Токен не найден.");
        }

        DataSet ds = new DataSet();
        ds.createColumn("token", DataColumnType.STRING, login.getId());
        ds.createRecord();
        return ds;
    }
}
