/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.oauth.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.authservice.entity.enums.NotificationTypeCode;
import net.stemteam.authservice.oauth.OAuthClient;
import net.stemteam.authservice.oauth.OAuthRequest;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataColumnType;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;

import java.sql.SQLException;
import java.util.HashMap;

/**
 * Получение ссылки на авторизацию во внешней системе посредством OAuth 2
 */
public class PrepareRequestHandler extends DataSetHandler {

    @Override
    protected DataSet getData(HashMap params, JaxisContentType jct) throws
            ConnectionPoolNotFoundException, DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpBadRequestException,
            HttpInternalException, HttpUnauthorizedException, NotConfiguredException, HttpNotFoundException, Exception {
        String Version = getParamValidator().getString(params, "Version", true); // версия протокола
        String ApiKey = getParamValidator().getString(params, "ApiKey", true); // ключ апи
        String appCode = getParamValidator().getString(params, "appCode", true); // код внешней системы
        getParamValidator().postValidate();

        // проверка апи-ключа
        ApiKeyHelper.getInstance().check(getDataSource(), ApiKey);

        OAuthClient cli = getAuthService().getOAuthClient(NotificationTypeCode.valueOf(appCode));
        OAuthRequest req = cli.prepareRequest();

        DataSet ds = new DataSet();
        ds.createColumn("url", DataColumnType.STRING, req.getUrl());
        ds.createColumn("state", DataColumnType.STRING, req.getState());
        ds.createRecord();
        return ds;
    }
}
