/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.oauth.handler;

import net.stemteam.authservice.entity.enums.NotificationTypeCode;
import net.stemteam.authservice.entity.SysLogin;
import net.stemteam.authservice.oauth.OAuthClient;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;

import java.sql.SQLException;
import java.util.HashMap;

/**
 * Авторизация в Facebook, получение access_token, регистрация и авторизация в хеддо
 */
public class Facebook extends DataSetHandler {

    @Override
    protected DataSet getData(HashMap params, JaxisContentType jct) throws
            ConnectionPoolNotFoundException, DataSetException, CloseDbConnectionException, SQLException,
            HttpForbiddenException, HttpBadRequestException,
            HttpInternalException, HttpUnauthorizedException, NotConfiguredException, HttpNotFoundException, Exception {
        String code = getParamValidator().getString(params, "code", true);
        String state = getParamValidator().getString(params, "state", true);
        getParamValidator().postValidate();

        OAuthClient cli = getAuthService().getOAuthClient(NotificationTypeCode.facebook);
        SysLogin login = cli.authorize(code, state);

        return null;
    }
}
