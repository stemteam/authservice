/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.oauth;

import net.stemteam.authservice.entity.enums.NotificationTypeCode;
import net.stemteam.authservice.entity.SysLogin;

/**
 * Интерфес авторизации посредством OAuth
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public interface OAuthClient {

    /**
     * У каждого приложения есть код, по которому с ним работаем
     *
     * @return
     */
    public NotificationTypeCode getApplicationCode();

    /**
     * Подготовить запрос для выполнения OAuth
     *
     * @return
     * @throws Exception
     */
    public OAuthRequest prepareRequest() throws Exception;

    /**
     * Выполнить полный цикл OAuth авторизации, зарегистрировать при необходимости пользователя в системе, выполнить
     * вход, вернуть данные входа (запись в syslogin)
     *
     * @param code код для получения access_token
     * @param state идентификатор запроса
     * @return
     * @throws Exception
     */
    public SysLogin authorize(String code, String state) throws Exception;

}
