/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.oauth;

/**
 * Приложение соц.сети, с возможностью OAuth
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class OAuthSocialApplication {

    private String applicationId;
    private String applicationPublicKey;
    private String applicationSecureKey;
    private String applicationOAuthRedirectUrl;

    /**
     * Идентификатор приложения
     *
     * @return
     */
    public String getApplicationId() {
        return applicationId;
    }

    /**
     * Идентификатор приложения
     *
     * @param applicationId
     */
    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    /**
     * Публичный ключ приложения
     *
     * @return
     */
    public String getApplicationPublicKey() {
        return applicationPublicKey;
    }

    /**
     * Публичный ключ приложения
     *
     * @param applicationPublicKey
     */
    public void setApplicationPublicKey(String applicationPublicKey) {
        this.applicationPublicKey = applicationPublicKey;
    }

    /**
     * Защищенный ключ приложения
     *
     * @return
     */
    public String getApplicationSecureKey() {
        return applicationSecureKey;
    }

    /**
     * Защищенный ключ приложения
     *
     * @param applicationSecureKey
     */
    public void setApplicationSecureKey(String applicationSecureKey) {
        this.applicationSecureKey = applicationSecureKey;
    }

    /**
     * Урл перенаправления для получения кода
     *
     * @return
     */
    public String getApplicationOAuthRedirectUrl() {
        return applicationOAuthRedirectUrl;
    }

    /**
     * Урл перенаправления для получения кода
     *
     * @param applicationOAuthRedirectUrl
     */
    public void setApplicationOAuthRedirectUrl(String applicationOAuthRedirectUrl) {
        this.applicationOAuthRedirectUrl = applicationOAuthRedirectUrl;
    }
}
