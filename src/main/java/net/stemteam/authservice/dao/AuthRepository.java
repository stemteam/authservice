/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.dao;

import net.stemteam.authservice.entity.Endpoint;
import net.stemteam.authservice.entity.enums.NotificationTypeCode;
import net.stemteam.authservice.entity.NotificationType;
import net.stemteam.authservice.entity.RefType;
import net.stemteam.authservice.entity.SysLogin;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.entity.UserRequest;
import net.stemteam.datatransport.db.ParamBinder;
import net.stemteam.rightservice.service.RightService;
import net.stemteam.rightservice.exception.RightServiceException;
import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import net.stemteam.authservice.api.support.UserRequestPropogation;
import net.stemteam.authservice.entity.UserReference;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.exception.UserNotFoundException;
import net.stemteam.jaxis.common.PNSType;

/**
 * Сервис авторизации
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class AuthRepository {

    private DataSource dataSource;
    private RightService rightService;

    public RightService getRightService() {
        return rightService;
    }

    public void setRightService(RightService rightService) {
        this.rightService = rightService;
    }

    /**
     * Проверяет существования пользователя с указанным email
     *
     * @param email
     * @return
     * @throws java.sql.SQLException
     */
    public boolean isEmailExists(String email) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            CallableStatement ps = conn.prepareCall("select 1 from endpoint ep1 "
                    + " join notification_type nt1 on nt1.id_notification_type = ep1.id_notification_type "
                    + " join sysuser su1 on ep1.id_sysuser = su1.id_sysuser "
                    + " where nt1.code = 'email' "
                    + "   and ep1.notification_id = ? "
                    + "   and su1.is_registered = 1");
            ps.setString(1, email.toLowerCase());
            ResultSet rs = ps.executeQuery();
            boolean result = false;
            if (rs.next()) {
                result = true;
            }
            rs.close();
            return result;
        }
    }

    /**
     * Проверяет существования пользователя с указанным телефоном
     *
     * @param phone
     * @return
     * @throws java.sql.SQLException
     */
    public boolean isPhoneExists(String phone) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            CallableStatement ps = conn.prepareCall("select 1 from endpoint ep1 "
                    + " join notification_type nt1 on nt1.id_notification_type = ep1.id_notification_type "
                    + " join sysuser su1 on ep1.id_sysuser = su1.id_sysuser "
                    + " where nt1.code = 'sms' "
                    + "   and ep1.notification_id = ? "
                    + "   and su1.is_registered = 1");
            ps.setString(1, phone.toLowerCase());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            }
            rs.close();
            return false;
        }
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Возвращает контакт пользователя по идентификатору контакта и его типу
     *
     * @param notificationType тип контакта (null = по всем типам)
     * @param notificationId идентификатор контакта
     * @return
     * @throws SQLException
     */
    public Endpoint getEndpoint(NotificationType notificationType, String notificationId) throws SQLException {
        try (Connection conn = getDataSource().getConnection()) {
            PreparedStatement ps = conn.prepareStatement("select e.* "
                    + " from endpoint e "
                    + " where e.notification_id = ? "
                    + "   and ((? is null) or (e.id_notification_type = ?)) ");
            ps.setString(1, notificationId);
            ParamBinder.setInt(ps, 2, notificationType != null ? notificationType.getId() : null);
            ParamBinder.setInt(ps, 3, notificationType != null ? notificationType.getId() : null);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    Endpoint e = new Endpoint();
                    e.setId(rs.getInt("id_endpoint"));
                    e.setNotificationId(notificationId);
                    e.setUserId(rs.getInt("id_sysuser"));
                    e.setNotificationTypeId(rs.getInt("id_notification_type"));
                    e.setVerified(rs.getInt("is_verified") == 1);
                    e.setEnabled(rs.getInt("is_enabled") == 1);
                    return e;
                } else {
                    return null;
                }
            }
        }
    }

    /**
     * Загружает связи пользователя
     *
     * @param user пользователь, которому требуется загрузить связи
     * @return
     * @throws java.sql.SQLException
     */
    public User loadUserReferences(User user) throws SQLException {
        List<UserReference> refs = new ArrayList<>();
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps3 = conn.prepareStatement("select t.code, t.name, u.guuid, u.is_disabled "
                    + "from userref ur\n"
                    + "join sysuser u on ur.id_sysuser_ref = u.id_sysuser\n"
                    + "join reftype t on ur.id_reftype = t.id_reftype\n"
                    + "where ur.id_sysuser = ? "
                    + " order by t.code ");
            ps3.setInt(1, user.getId());
            try (ResultSet rs = ps3.executeQuery()) {
                while (rs.next()) {
                    User usr = new User(null, rs.getString("GUUID"));
                    usr.setDisabled(rs.getBoolean("IS_DISABLED"));
                    UserReference ref = new UserReference(user, usr, RefType.getByCode(rs.getString("CODE")));
                    refs.add(ref);
                }
            }
        }
        user.setReferences(refs);
        return user;
    }

    /**
     * Загружает телефон родителя пользователя
     *
     * @param user пользователь, которому требуется загрузить телефон родителя
     * @return
     * @throws SQLException
     */
    public User loadUserParentPhone(User user) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement("select ep.notification_id as phone "
                    + "from userref ur "
                    + "join sysuser cc on ur.id_sysuser_ref = cc.id_sysuser "
                    + "  and cc.guuid = ?::uuid "
                    + "join endpoint ep on ep.id_sysuser = ur.id_sysuser "
                    + "  and ep.id_notification_type = 1 "
                    + "  and is_default = 1 ");
            ps.setString(1, user.getGuuid());
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    user.setParentPhone(rs.getString("PHONE"));
                }
            }
        }

        return user;
    }

    /**
     * Загружает контакты пользователя
     *
     * @param user пользователь, которому требуется загрузить контакты
     * @return
     * @throws SQLException
     */
    public User loadUserEndpoints(User user) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            user.setEndpoints(getUserEndpoints(conn, user.getId()));
        }

        List<String> emails = new ArrayList<>(5);
        List<String> phones = new ArrayList<>(5);

        // анализируем контакты и заполняем ими поля
        for (Endpoint ep : user.getEndpointList()) {
            switch (ep.getNotificationTypeCode()) {
                case sms:
                    phones.add(ep.getNotificationId());
                    if (ep.isDefault()) {
                        user.setDefaultPhone(ep.getNotificationId());
                    }
                    break;
                case email:
                    emails.add(ep.getNotificationId());
                    if (ep.isDefault()) {
                        user.setDefaultEmail(ep.getNotificationId());
                    }
                    break;
                default:
                    break;
            }
        }

        user.setEmails(emails);
        user.setPhones(phones);

        return user;
    }

    /**
     * Загружает запросы пользователя
     *
     * @param user пользователь, которому требуется загрузить запросы
     * @return
     * @throws SQLException
     */
    public User loadUserRequests(User user) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps2 = conn.prepareStatement("select t.code as type_code, r.code "
                    + " from user_request r "
                    + " join user_request_type t on r.id_user_request_type = t.id_user_request_type "
                    + " join sysuser u on r.id_sysuser = u.id_sysuser and u.guuid = ?::uuid "
                    + " where r.expiration_time > current_timestamp "
                    + " order by r.createdate ");
            ps2.setString(1, user.getGuuid());
            List<UserRequest> arr = new ArrayList<>();
            try (ResultSet rs = ps2.executeQuery()) {
                while (rs.next()) {
                    UserRequest request = new UserRequest(rs.getString("CODE"), rs.getString("TYPE_CODE"));
                    arr.add(request);
                }
            }
            user.setRequests(arr);
        }

        return user;
    }

    /**
     * Возвращает данные пользователя по Id, либо Guuid, либо Token
     *
     * @param user
     * @param requestPropogations
     * @return
     * @throws SQLException
     * @throws RightServiceException
     * @throws UserNotAuthorizedException
     * @throws UserNotFoundException
     */
    public User getUser(User user, UserRequestPropogation... requestPropogations) throws
            SQLException, RightServiceException,
            UserNotAuthorizedException, UserNotFoundException {
        user = loadUser(user);
        for (UserRequestPropogation prop : requestPropogations) {
            switch (prop) {
                case LastAction:
                    break;
                case Endpoints:
                    user = loadUserEndpoints(user);
                    break;
                case ReferedUsers:
                    user = loadUserReferences(user);
                    break;
                case Roles:
                    user.setRoles(rightService.getUserRoleList(user.getGuuid()));
                    break;
                case Requests:
                    loadUserRequests(user);
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported UserRequestPropogation: " + prop);
            }
        }

        if (user.getToken() != null) {
            updateLoginExpiredTime(new SysLogin(user.getToken()));
            updateLastActionDate(user);
        }

        return user;
    }

    /**
     * Возвращает информацию о пользователе по его логину
     *
     * @param login
     * @param requestPropogations
     * @return
     * @throws java.sql.SQLException
     * @throws net.stemteam.authservice.exception.UserNotAuthorizedException
     * @throws net.stemteam.authservice.exception.UserNotFoundException
     * @throws net.stemteam.rightservice.exception.RightServiceException
     */
    public User getUserByLogin(String login, UserRequestPropogation... requestPropogations) throws SQLException,
            UserNotAuthorizedException, UserNotFoundException, RightServiceException {
        login = login.trim().toLowerCase();
        Endpoint e = getEndpoint(null, login);
        if (e == null) {
            throw new UserNotFoundException("Пользователь не найден");
        }
        return getUser(new User(e.getUserId()), requestPropogations);
    }

    /**
     * Обновление времемени активности - продлеваем активность, чтобы токен не протух
     *
     * @param login
     * @throws SQLException
     */
    private void updateLoginExpiredTime(SysLogin login) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            String sql2
                    = "update syslogin "
                    + "set expired_time = localtimestamp + interval '1 day' "
                    + "where expired_time > localtimestamp "
                    + "  and expired_time < localtimestamp + interval '3 hour' "
                    + "  and id_syslogin = ?::uuid ";
            PreparedStatement ps2 = conn.prepareStatement(sql2);
            ps2.setString(1, login.getId());
            ps2.execute();
            conn.commit();
        }
    }

    private void updateLastActionDate(User user) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            String sql2
                    = " update sysuser "
                    + " set last_action_date = current_timestamp "
                    + " where (last_action_date is null or last_action_date < current_timestamp - interval '5 minute') "
                    + "   and guuid = ?::uuid ";
            try (PreparedStatement ps2 = conn.prepareStatement(sql2)) {
                ps2.setString(1, user.getGuuid());
                ps2.execute();
                conn.commit();
            }
        }
    }

    /**
     * Сервисная процедура, удаляет истекшие токены авторизации
     *
     * @throws SQLException
     */
    public void deleteExpiredLogins() throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            try (PreparedStatement ps = conn.prepareStatement("delete from syslogin where expired_time < localtimestamp ")) {
                ps.execute();
                conn.commit();
            }
        }
    }

    /**
     * Добавляет пользователя
     *
     * @param user
     * @return пользователь с заполненным id
     * @throws SQLException
     */
    // TODO: REFACTOR: Этот метод addUser вызывается в транзакциях, потому сюда передается объект Connection
    // надо как-то описать все логические операции в отдельном сервисе и вызывать его для высокоуровневой логики
    public User addUser(Connection conn, User user) throws SQLException {
        PreparedStatement ps = conn.prepareStatement(
                "insert into sysuser (is_registered, password, guuid, is_disabled, first_name, last_name, middle_name, image_url, password_salt, birthday) "
                + "values (?, ?, ?::uuid, ?, ?, ?, ?, ?,?,?::timestamp) ", Statement.RETURN_GENERATED_KEYS);
        int num = 1;
        ParamBinder.setInt(ps, num++, user.isRegistered() ? 1 : 0);
        ParamBinder.setString(ps, num++, user.getPasswordHash());
        ParamBinder.setString(ps, num++, user.getGuuid());
        ParamBinder.setInt(ps, num++, user.isDisabled() ? 1 : 0);
        ParamBinder.setString(ps, num++, user.getFirstName());
        ParamBinder.setString(ps, num++, user.getLastName());
        ParamBinder.setString(ps, num++, user.getMiddleName());
        ParamBinder.setString(ps, num++, user.getImage());
        ParamBinder.setString(ps, num++, user.getPasswordSalt());
        ParamBinder.setTimestamp(ps, num++, user.getBirthday());
        ps.execute();
        ResultSet keyset = ps.getGeneratedKeys();
        keyset.next();
        int result = keyset.getInt("ID_SYSUSER");
        user.setId(result);
        return user;
    }

    /**
     * Добавляет пользователя
     *
     * @param user
     * @return пользователь с заполненным id
     * @throws SQLException
     */
    public User addUser(User user) throws SQLException {
        try (Connection conn = getDataSource().getConnection()) {
            user = addUser(conn, user);
            conn.commit();
            return user;
        }
    }

    /**
     * Добавляет контакт пользователя
     *
     * @param ep контакт
     * @return контакт с заполненным id
     * @throws SQLException
     */
    public Endpoint addEndpoint(Endpoint ep) throws SQLException {
        try (Connection conn = getDataSource().getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                    "insert into endpoint(id_sysuser, id_notification_type, notification_id, is_verified, is_enabled) "
                    + "values (?, ?, ?, ?, ?) ", Statement.RETURN_GENERATED_KEYS);
            int num = 1;
            ParamBinder.setInt(ps, num++, ep.getUserId());
            ParamBinder.setInt(ps, num++, ep.getNotificationTypeId());
            ParamBinder.setString(ps, num++, ep.getNotificationId());
            ParamBinder.setInt(ps, num++, ep.isVerified() ? 1 : 0);
            ParamBinder.setInt(ps, num++, ep.isEnabled() ? 1 : 0);
            ps.executeUpdate();
            ResultSet keyset = ps.getGeneratedKeys();
            keyset.next();
            int result = keyset.getInt("id_endpoint");
            ep.setId(result);
            conn.commit();
            return ep;
        }
    }

    /**
     * Получить тип системы уведомлений по коду
     *
     * @param typeCode код систумы уведомлений
     * @return
     * @throws SQLException
     */
    public NotificationType getNotificationType(NotificationTypeCode typeCode) throws SQLException {
        try (Connection conn = getDataSource().getConnection()) {
            PreparedStatement ps = conn.prepareStatement("select * from notification_type where code = ?");
            ps.setString(1, typeCode.toString());
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    NotificationType nt = new NotificationType();
                    nt.setCode(typeCode);
                    nt.setId(rs.getInt("id_notification_type"));
                    return nt;
                } else {
                    return null;
                }
            }
        }
    }

    /**
     * Добавляет запись о входе пользователя в систему
     *
     * @param sysLogin пользователь
     * @throws SQLException
     */
    public void addSysLogin(SysLogin sysLogin) throws SQLException {
        try (Connection conn = getDataSource().getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                    "insert into syslogin(id_syslogin, id_sysuser, state, ext_access_token, expired_time, id_endpoint) "
                    + "values (?::uuid, ?, ?, ?, ?, ?) ");
            int num = 1;
            ParamBinder.setString(ps, num++, sysLogin.getId());
            ParamBinder.setInt(ps, num++, sysLogin.getSysUserId());
            ParamBinder.setString(ps, num++, sysLogin.getState());
            ParamBinder.setString(ps, num++, sysLogin.getExtAccessToken());
            ParamBinder.setTimestamp(ps, num++, sysLogin.getExpiredTime());
            ParamBinder.setInt(ps, num++, sysLogin.getEndpoint().getId());
            ps.executeUpdate();
            conn.commit();
        }
    }

    /**
     * Возвращает запись о последнем использовании токена пользователя
     *
     * @param user пользователь для фильтра
     * @return данные последнего входа
     * @throws SQLException
     */
    public SysLogin getUserLastAction(User user) throws SQLException {
        try (Connection conn = getDataSource().getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                    "select s.id_syslogin, s.id_sysuser, s.state, s.ext_access_token, s.expired_time,\n"
                    + "  s.id_endpoint, s.lastdate\n" + "from syslogin s\n" + "where s.id_sysuser = ?\n"
                    + "order by s.lastdate desc limit 1 ");
            ParamBinder.setInt(ps, 1, user.getId());
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    SysLogin login = new SysLogin();
                    login.setToken(rs.getString("id_syslogin"));
                    login.setSysUserId(rs.getInt("id_sysuser"));
                    login.setState(rs.getString("state"));
                    login.setExtAccessToken(rs.getString("ext_access_token"));
                    login.setExpiredTime(rs.getTimestamp("expired_time"));
                    login.setLastDate(rs.getTimestamp("lastdate"));
                    Endpoint ep = new Endpoint();
                    ep.setId(rs.getInt("id_endpoint"));
                    login.setEndpoint(ep);
                    return login;
                } else {
                    return null;
                }
            }
        }
    }

    /**
     * Возвращает запись о входе пользователя в систему
     *
     * @param notificationType
     * @param state
     * @return
     * @throws SQLException
     */
    public SysLogin getSysLogin(NotificationType notificationType, String state) throws SQLException {
        try (Connection conn = getDataSource().getConnection()) {
            PreparedStatement ps = conn.prepareStatement("select s.id_syslogin, s.id_sysuser, s.state,"
                    + "  s.ext_access_token, s.expired_time, s.id_endpoint, s.lastdate\n" + " from syslogin s\n"
                    + " join endpoint e on s.id_endpoint = e.id_endpoint\n"
                    + " where e.id_notification_type = ? and s.state = ?\n");
            int num = 1;
            ParamBinder.setInt(ps, num++, notificationType.getId());
            ParamBinder.setString(ps, num++, state);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    SysLogin login = new SysLogin();
                    login.setToken(rs.getString("id_syslogin"));
                    login.setSysUserId(rs.getInt("id_sysuser"));
                    login.setState(rs.getString("state"));
                    login.setExtAccessToken(rs.getString("ext_access_token"));
                    login.setExpiredTime(rs.getTimestamp("expired_time"));
                    login.setLastDate(rs.getTimestamp("lastdate"));
                    Endpoint ep = new Endpoint();
                    ep.setId(rs.getInt("id_endpoint"));
                    login.setEndpoint(ep);
                    return login;
                } else {
                    return null;
                }
            }
        }
    }

    public void disableUser(User user, boolean isDisabled) throws SQLException {
        try (Connection conn = getDataSource().getConnection()) {
            // переключаем состояние пользователя
            PreparedStatement ps = conn.prepareStatement("update sysuser "
                    + " set is_disabled = ? where guuid = ?::uuid");
            ps.setInt(1, isDisabled ? 1 : 0);
            ps.setString(2, user.getGuuid());

            ps.executeUpdate();

            // а если это запрет, то удалим все его сессии
            if (isDisabled) {
                PreparedStatement ps1 = conn.prepareStatement(
                        "delete from syslogin where id_sysuser = "
                        + " (select a.id_sysuser from sysuser a where a.guuid = ?::uuid)");
                ps1.setString(1, user.getGuuid());
                ps1.executeUpdate();
            }

            conn.commit();
        }
    }

    /**
     * Возвращает пользователя по переданному Id либо Guuid либо Token
     *
     * @param user
     * @return
     * @throws SQLException
     * @throws net.stemteam.authservice.exception.UserNotAuthorizedException
     * @throws net.stemteam.authservice.exception.UserNotFoundException
     */
    private User loadUser(User user) throws SQLException, UserNotAuthorizedException, UserNotFoundException {
        // если передан только токен - определяем айдишку пользователя
        if (user.getId() == null && user.getGuuid() == null) {
            SysLogin login = getSysLogin(user.getToken());
            if (login == null) {
                throw new UserNotAuthorizedException("Пользователь не авторизован");
            }
            user.setId(login.getSysUserId());
        }

        try (Connection conn = getDataSource().getConnection()) {
            PreparedStatement ps = conn.prepareStatement("select u.id_sysuser, u.password, u.guuid,\n"
                    + " u.is_registered, u.is_disabled, u.lastdate, u.first_name, u.last_name,\n"
                    + " u.middle_name, u.image_url, u.password_salt, u.birthday, u.LAST_ACTION_DATE \n"
                    + " from sysuser u "
                    + " where u.id_sysuser = ? or u.guuid = ?::uuid ");
            ParamBinder.setInt(ps, 1, user.getId());
            ParamBinder.setString(ps, 2, user.getGuuid());
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    user.setGuuid(rs.getString("guuid"));
                    user.setId(rs.getInt("ID_SYSUSER"));
                    user.setRegistered(rs.getInt("IS_REGISTERED") == 1);
                    user.setDisabled(rs.getInt("IS_DISABLED") == 1);
                    user.setLastdate(rs.getTimestamp("LASTDATE"));
                    user.setPasswordHash(rs.getString("PASSWORD"));
                    user.setFirstName(rs.getString("FIRST_NAME"));
                    user.setLastName(rs.getString("LAST_NAME"));
                    user.setMiddleName(rs.getString("MIDDLE_NAME"));
                    user.setImage(rs.getString("IMAGE_URL"));
                    user.setPasswordSalt(rs.getString("PASSWORD_SALT"));
                    user.setBirthday(rs.getTimestamp("birthday"));
                    user.setLastActionDate(rs.getTimestamp("LAST_ACTION_DATE"));
                    return user;
                } else {
                    throw new UserNotFoundException("Пользователь не найден");
                }
            }
        }
    }

    /**
     * Возвращает факт входа пользователя в систему по его идентификатору
     *
     * @param token
     * @return
     * @throws SQLException
     */
    public SysLogin getSysLogin(String token) throws SQLException {
        try (Connection conn = getDataSource().getConnection()) {
            PreparedStatement ps = conn.prepareStatement("select s.id_syslogin, s.id_sysuser, s.state,"
                    + "  s.ext_access_token, s.expired_time, s.id_endpoint, s.lastdate\n"
                    + " from syslogin s "
                    + " where id_syslogin = ?::uuid ");
            ps.setString(1, token);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    SysLogin login = new SysLogin();
                    login.setToken(rs.getString("id_syslogin"));
                    login.setSysUserId(rs.getInt("id_sysuser"));
                    login.setState(rs.getString("state"));
                    login.setExtAccessToken(rs.getString("ext_access_token"));
                    login.setExpiredTime(rs.getTimestamp("expired_time"));
                    login.setLastDate(rs.getTimestamp("lastdate"));
                    Endpoint ep = new Endpoint();
                    ep.setId(rs.getInt("id_endpoint"));
                    login.setEndpoint(ep);
                    return login;
                } else {
                    return null;
                }
            }
        }
    }

    /**
     * Обновить информацию о контакте
     *
     * @param conn коннект к БД
     * @param idUser ID пользователя в БД
     * @param endpoint информация о контакте
     * @return ID обновленной записи о контакте
     * @throws SQLException
     */
    public int upsertEndpoint(Connection conn, Integer idUser, Endpoint endpoint) throws SQLException {

        if (endpoint.getNotificationTypeCode() == NotificationTypeCode.push && endpoint.getPushAttributes() == null) {
            throw new IllegalArgumentException("Parameter pushAttributes can't be null for notification type PUSH");
        }

        //conn.prepareStatement("", Statement.RETURN_GENERATED_KEYS);
        CallableStatement call = conn.prepareCall("{ ? = call auth_upsert_endpoint(?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");

        call.registerOutParameter(1, Types.INTEGER);

        // id точки контакта
        ParamBinder.setInt(call, 2, endpoint.getId());

        // id пользователя
        ParamBinder.setInt(call, 3, idUser);

        // тип уведомлений
        call.setString(4, endpoint.getNotificationTypeCode().toString());
        // PNS token
        call.setString(5, endpoint.getNotificationId());
        // параметры push уведомлений
        if (endpoint.getNotificationTypeCode() == NotificationTypeCode.push) {
            // Id устройства
            call.setString(6, endpoint.getPushAttributes().deviceId);
            // Id azure
            call.setString(7, endpoint.getPushAttributes().azureId);
            // PNS Type
            call.setString(8, endpoint.getPushAttributes().PNSType.getCode());
            // IsTablet
            call.setInt(9, endpoint.getPushAttributes().isTablet ? 1 : 0);
            // Locale
            call.setString(10, endpoint.getPushAttributes().locale);
            // Name
            call.setString(11, endpoint.getPushAttributes().name);
            // Model
            call.setString(12, endpoint.getPushAttributes().model);
            // MobileAppCode
            call.setString(13, endpoint.getPushAttributes().apiKey);
        } else {
            // Id устройства
            call.setNull(6, Types.VARCHAR);
            // Id azure
            call.setNull(7, Types.VARCHAR);
            // PNS Type
            call.setNull(8, Types.VARCHAR);
            // IsTablet
            call.setNull(9, Types.SMALLINT);
            // Locale
            call.setNull(10, Types.VARCHAR);
            // Name
            call.setNull(11, Types.VARCHAR);
            // Model
            call.setNull(12, Types.VARCHAR);
            // MobileAppCode
            call.setNull(13, Types.VARCHAR);
        }
        // IsVerified
        call.setInt(14, endpoint.isVerified() ? 1 : 0);
        // IsEnabled
        call.setInt(15, endpoint.isEnabled() ? 1 : 0);

        call.execute();

        endpoint.setId(call.getInt(1));

        return endpoint.getId();
    }

    /**
     * Загрузить список контактов пользователя
     *
     * @param conn коннект к БД
     * @param idUser ID пользователя в БД
     * @return список контактов пользователя
     * @throws java.sql.SQLException
     */
    public List<Endpoint> getUserEndpoints(Connection conn, Integer idUser) throws SQLException {
        List<Endpoint> eplist = new LinkedList<>();

        // загружаем список точек уведомления пользователя
        String sql = "select ep.id_endpoint, nt.code as nt_code, pt.code as pns_code, lc.code as locale, "
                + "       ep.device_id, ep.azure_id, ep.notification_id, ep.name, ep.model, ep.is_tablet,"
                + "       ep.is_verified, ep.is_enabled, ep.is_default, ep.apikey "
                + " from endpoint ep "
                + " join notification_type nt on ep.id_notification_type = nt.id_notification_type "
                + " left join pns_type pt on ep.id_pns_type = pt.id_pns_type "
                + " left join locale lc on ep.id_locale = lc.id_locale "
                + " where ep.id_sysuser = ? "
                + " order by nt.code, ep.is_enabled, ep.is_default";

        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1, idUser);
        try (ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                Endpoint.PushNotificationAttributes pna = null;

                NotificationTypeCode ntype = NotificationTypeCode.valueOf(rs.getString("NT_CODE"));
                if (ntype == NotificationTypeCode.push) {
                    pna = new Endpoint.PushNotificationAttributes(
                            rs.getString("AZURE_ID"),
                            rs.getString("DEVICE_ID"),
                            PNSType.getByCode(rs.getString("PNS_CODE")),
                            rs.getBoolean("IS_TABLET"),
                            rs.getString("LOCALE"),
                            rs.getString("NAME"),
                            rs.getString("MODEL"),
                            rs.getString("APIKEY"));
                }

                Endpoint ep = new Endpoint(
                        rs.getInt("ID_ENDPOINT"),
                        NotificationTypeCode.valueOf(rs.getString("NT_CODE")),
                        rs.getString("NOTIFICATION_ID"),
                        pna,
                        rs.getBoolean("IS_ENABLED"),
                        rs.getBoolean("IS_VERIFIED"),
                        rs.getBoolean("IS_DEFAULT"));

                eplist.add(ep);
            }
        }
        return eplist;
    }
    
    /**
     * Удяляет остальные токены авторизации пользователя, кроме указанного
     * @param token
     * @throws SQLException 
     */
    public void removeAnotherTokens(String token) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            Integer sysUserId = null;
            try (PreparedStatement ps = conn.prepareStatement("select id_sysuser from syslogin where id_syslogin = ?::uuid ")) {
                ps.setString(1, token);
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        sysUserId = rs.getInt("id_sysuser");
                    }
                }
            }
            
            if (sysUserId != null) {
                try (PreparedStatement ps = conn.prepareStatement("delete from syslogin where id_sysuser = ? and id_syslogin <> ?::uuid ")) {
                    ps.setInt(1, sysUserId);
                    ps.setString(2, token);
                    ps.execute();
                    conn.commit();
                }
            }
        }
    }
}
