/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.api.support;

/**
 * Хэлпер параметров
 */
public class ParamHelper {

    /**
     * Лимит по умолчанию
     */
    public static final long PARAM_LIMIT_DEFAULT = 1000000L;

    /**
     * Оффсет по умолчанию
     */
    public static final long PARAM_OFFSET_DEFAULT = 0L;

    /**
     * Версия протокола
     */
    public static final String PARAM_VERSION = "Version";

    /**
     * Ключ АПИ
     */
    public static final String PARAM_APIKEY = "ApiKey";

    /**
     * Токен
     */
    public static final String PARAM_TOKEN = "Token";

    /**
     * Уникальный идентификатор пользователя
     */
    public static final String PARAM_GUUID = "Guuid";
    
    /**
     * Логин
     */
    public static final String PARAM_LOGIN = "Login";
    
    /**
     * дата последнего изменений данных
     */
    public static final String PARAM_LASTDATE = "Lastdate";
    
    public static final String PARAM_LIMIT = "Limit";
    public static final String PARAM_OFFSET = "Offset";

    /**
     * Запрещеный контсруктор, тровает UnsupportedOperationException при вызове
     */
    protected ParamHelper() {
        throw new UnsupportedOperationException("Инициализации классов-утилит запрещена");
    }
    
    public static final String PARAM_USER_WITH_LAST_ACTION = "WithLastAction";
    public static final String PARAM_USER_WITH_ROLES = "WithRoles";
    public static final String PARAM_USER_WITH_ENDPOINTS = "WithEndpoints";
    public static final String PARAM_USER_WITH_REFERED_USERS = "WithReferedUsers";
    public static final String PARAM_USER_WITH_REQUESTS = "WithRequests";

}
