package net.stemteam.authservice.api.support;

import net.stemteam.jaxis.common.Version;

/**
 * Список версий для обратной совместимости
 */
public class BackCompatibility {

    // большой привет Вадиму...
    public static final Version VERSION_100_500 = new Version("100.500");

    public static final Version VERSION_2_13 = new Version("2.13");
}
