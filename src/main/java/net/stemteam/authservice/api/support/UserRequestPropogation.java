/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.api.support;

/**
 * Параметры запроска данных пользователя (что и насколько глубоко возвращать)
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public enum UserRequestPropogation {
    
    @Deprecated // возвращается вместе с пользователем, нет смысла вызывать отдельно
    LastAction,
    Endpoints,
    ReferedUsers,
    Roles,
    Requests;
}
