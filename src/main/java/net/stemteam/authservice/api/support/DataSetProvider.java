/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.api.support;

import java.util.ArrayList;
import java.util.List;
import net.stemteam.authservice.entity.Endpoint;
import net.stemteam.authservice.entity.enums.NotificationTypeCode;
import net.stemteam.authservice.entity.RefType;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.entity.UserReference;
import net.stemteam.authservice.entity.UserRequest;
import net.stemteam.datatransport.exception.ColumnNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataColumnType;
import net.stemteam.datatransport.transport.DataRecord;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.rightservice.entity.Role;

/**
 * Провайдырь аще
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class DataSetProvider {

    public DataSet getRoleListDataSet(List<Role> roles) throws DataSetException {
        DataSet ds = new DataSet();
        ds.createColumn("Id", DataColumnType.INT32);
        ds.createColumn("Name", DataColumnType.STRING);
        ds.createColumn("Code", DataColumnType.STRING);
        ds.createColumn("IsAdmin", DataColumnType.INT32);
        ds.createColumn("Description", DataColumnType.STRING);
        for (Role role : roles) {
            DataRecord rec = ds.createRecord();
            rec.setInt("Id", role.getId());
            rec.setString("Name", role.getName());
            rec.setString("Code", role.getCode());
            rec.setInt("IsAdmin", role.isAdmin() ? 1 : 0);
            rec.setString("Description", role.getDescription());
        }
        return ds;
    }

    public DataSet getReferedUsersDataSet(List<UserReference> refs) throws DataSetException {
        DataSet ds = new DataSet();
        ds.createColumn("RefType", DataColumnType.STRING);
        ds.createColumn("RefTypeName", DataColumnType.STRING);
        ds.createColumn("Guuid", DataColumnType.STRING);
        for (UserReference ref : refs) {
            DataRecord rec = ds.createRecord();
            rec.setString("RefType", ref.getRefType().getCode());
            rec.setString("RefTypeName", ref.getRefType().getName());
            rec.setString("Guuid", ref.getTargetUser().getGuuid());
        }
        return ds;
    }

    public DataSet getEndpointsDataSet(List<Endpoint> eps) throws DataSetException {
        DataSet ds = new DataSet();
        ds.createColumn("Id", DataColumnType.INT32);
        ds.createColumn("NotificationTypeCode", DataColumnType.STRING);
        ds.createColumn("NotificationId", DataColumnType.STRING);
        ds.createColumn("IsDefault", DataColumnType.INT32);
        ds.createColumn("IsEnabled", DataColumnType.INT32);
        for (Endpoint e : eps) {
            DataRecord rec = ds.createRecord();
            rec.setInt("Id", e.getId());
            rec.setString("NotificationTypeCode", e.getNotificationTypeCode().toString());
            rec.setString("NotificationId", e.getNotificationId());
            rec.setInt("IsDefault", e.isDefault() ? 1 : 0);
            rec.setInt("IsEnabled", e.isEnabled() ? 1 : 0);
        }
        return ds;
    }
    
    public DataSet getRequestsDataSet(List<UserRequest> requests) throws DataSetException {
        DataSet ds = new DataSet();
        ds.createColumn("TypeCode", DataColumnType.STRING);
        ds.createColumn("Code", DataColumnType.STRING);
        for (UserRequest r : requests) {
            DataRecord rec = ds.createRecord();
            rec.setString("TypeCode", r.getType());
            rec.setString("Code", r.getCode());
        }
        return ds;
    }
    
    private static final String FIELD_TOKEN = "Token";
    private static final String FIELD_GUUID = "Id";
    private static final String FIELD_EMAIL = "Email";
    private static final String FIELD_PHONE = "Phone";
    private static final String FIELD_FIRST_NAME = "FirstName";
    private static final String FIELD_LAST_NAME = "LastName";
    private static final String FIELD_MIDDLE_NAME = "MiddleName";
    private static final String FIELD_IMAGE = "Image";

    public DataSet getUserDataSet(User user) throws DataSetException {
        DataSet ds = new DataSet();
        ds.createColumn(FIELD_TOKEN, DataColumnType.STRING);
        ds.createColumn(FIELD_GUUID, DataColumnType.STRING);
        ds.createColumn(FIELD_EMAIL, DataColumnType.STRING);
        ds.createColumn(FIELD_PHONE, DataColumnType.STRING);
        ds.createColumn(FIELD_FIRST_NAME, DataColumnType.STRING);
        ds.createColumn(FIELD_LAST_NAME, DataColumnType.STRING);
        ds.createColumn(FIELD_MIDDLE_NAME, DataColumnType.STRING);
        ds.createColumn(FIELD_IMAGE, DataColumnType.STRING);
        ds.createColumn("Roles", DataColumnType.DATASET);
        ds.createColumn("ReferedUsers", DataColumnType.DATASET);
        ds.createColumn("Endpoints", DataColumnType.DATASET);
        ds.createColumn("LastActionDate", DataColumnType.TIMESTAMP);
        ds.createColumn("Requests", DataColumnType.DATASET);
        ds.createColumn("Birthday", DataColumnType.TIMESTAMP);
        DataRecord row = ds.createRecord();

        row.setString(FIELD_TOKEN, user.getToken());
        row.setString(FIELD_GUUID, user.getGuuid());
        row.setString(FIELD_EMAIL, user.getDefaultEmail());
        row.setString(FIELD_PHONE, user.getDefaultPhone());
        row.setString(FIELD_FIRST_NAME, user.getFirstName());
        row.setString(FIELD_LAST_NAME, user.getLastName());
        row.setString(FIELD_MIDDLE_NAME, user.getMiddleName());
        row.setString(FIELD_IMAGE, user.getImage());
        row.setTimestamp("Birthday", user.getBirthday());
        ds.setAsJson2Array(false);

        // роли, права и прочее
        if (user.getReferences() != null) {
            row.setDataSet("ReferedUsers", getReferedUsersDataSet(user.getReferences()));
        }
        if (user.getEndpointList() != null) {
            row.setDataSet("Endpoints", getEndpointsDataSet(user.getEndpointList()));
        }
        if (user.getRoles() != null) {
            row.setDataSet("Roles", getRoleListDataSet(user.getRoles()));
        }
        if (user.getLastActionDate() != null) {
            row.setTimestamp("LastActionDate", user.getLastActionDate());
        }
        if (user.getRequests() != null) {
            row.setDataSet("Requests", getRequestsDataSet(user.getRequests()));
        }
        return ds;
    }

    public User getUserFromDataRecord(DataRecord row) throws ColumnNotFoundException, DataSetException {
        User u = new User();
        u.setToken(row.getString(FIELD_TOKEN));
        u.setGuuid(row.getString(FIELD_GUUID));
        u.setFirstName(row.getString(FIELD_FIRST_NAME));
        u.setLastName(row.getString(FIELD_LAST_NAME));
        u.setMiddleName(row.getString(FIELD_MIDDLE_NAME));
        u.setImage(row.getString(FIELD_IMAGE));

        u.setDefaultEmail(row.getString(FIELD_EMAIL)); // TODO какая-то муть, это есть в эндпоинтах
        u.setDefaultPhone(row.getString(FIELD_PHONE)); // TODO какая-то муть

        u.setLastActionDate(row.getTimestamp("LastActionDate"));
        u.setBirthday(row.getTimestamp("Birthday"));

        DataSet roles = row.getDataSet("Roles");
        DataSet endpoints = row.getDataSet("Endpoints");
        DataSet references = row.getDataSet("ReferedUsers");
        DataSet requests = row.getDataSet("Requests");

        if (roles != null) {
            u.setRoles(getRolesFromDataSet(roles));
        }

        if (endpoints != null) {
            u.setEndpoints(getEndpointsFromDataSet(endpoints));
        }
        
        if (references != null) {
            u.setReferences(getReferedUsersFromDataSet(u, references));
        }
        
        if (requests != null) {
            u.setRequests(getRequestsFromDataSet(requests));
        }

        return u;
    }

    public List<Role> getRolesFromDataSet(DataSet ds) throws ColumnNotFoundException {
        List<Role> roles = new ArrayList<>();
        for (DataRecord row : ds.getRecords()) {
            Role r = new Role();
            r.setId(row.getInteger("Id"));
            r.setName(row.getString("Name"));
            r.setCode(row.getString("Code"));
            r.setDescription(row.getString("Description"));
            r.setAdmin(row.getInteger("IsAdmin") == 1);
            roles.add(r);
        }
        return roles;
    }

    public List<Endpoint> getEndpointsFromDataSet(DataSet ds) throws ColumnNotFoundException {
        List<Endpoint> items = new ArrayList<>();
        for (DataRecord row : ds.getRecords()) {
            Endpoint ep = new Endpoint(row.getInteger("Id"), NotificationTypeCode.valueOf(row.
                    getString("NotificationTypeCode")), row.getString("NotificationId"));
            ep.setDefault(row.getInteger("IsDefault") == 1);
            ep.setEnabled(row.getInteger("IsEnabled") == 1);
            items.add(ep);
        }
        return items;
    }
    
    public List<UserReference> getReferedUsersFromDataSet(User u, DataSet ds) throws DataSetException {
        List<UserReference> refs = new ArrayList<>();
        for (DataRecord row : ds.getRecords()) {
            UserReference ref = new UserReference(u, new User(row.getString("Guuid")), null);
            if (row.getString("RefType") != null) {
                ref.setRefType(RefType.valueOf(row.getString("RefType")));
            }
            refs.add(ref);
        }
        return refs;
    }
    
    public List<UserRequest> getRequestsFromDataSet(DataSet ds) throws DataSetException {
        List<UserRequest> res = new ArrayList<>();
        for (DataRecord row : ds.getRecords()) {
            UserRequest req = new UserRequest(row.getString("Code"),row.getString("TypeCode"));
            res.add(req);
        }
        return res;
    }

}
