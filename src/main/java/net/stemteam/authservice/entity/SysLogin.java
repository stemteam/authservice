/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.entity;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Факт авторизации пользователя
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class SysLogin {

    private String id;
    private int sysUserId;
    private Timestamp expiredTime;
    private Timestamp lastDate;
    private String state;
    private String extAccessToken;
    private Endpoint endpoint;

    public SysLogin() {
    }
    
    public SysLogin(String id) {
        this.id = id;
    }
    
    /**
     * Токен авторизации (идентификатор)
     *
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * Токен авторизации (идентификатор)
     *
     * @param id
     */
    public void setToken(String id) {
        this.id = id;
    }

    /**
     * Ссылка на пользователя (id)
     *
     * @return
     */
    public int getSysUserId() {
        return sysUserId;
    }

    /**
     * Ссылка на пользователя (id)
     *
     * @param sysUserId
     */
    public void setSysUserId(int sysUserId) {
        this.sysUserId = sysUserId;
    }

    /**
     * Дата и время, начиная с которого токен авторизации будет считаться устаревшим
     *
     * @return
     */
    public Timestamp getExpiredTime() {
        return expiredTime;
    }

    /**
     * Дата и время, начиная с которого токен авторизации будет считаться устаревшим
     *
     * @param expiredTime
     */
    public void setExpiredTime(Timestamp expiredTime) {
        this.expiredTime = expiredTime;
    }

    /**
     * Получить дату и время последнего использования токена
     *
     * @return дата и время последнего использования токена
     */
    public Timestamp getLastDate() {
        return lastDate;
    }

    /**
     * Установить дату и время последнего использования токена
     *
     * @param lastDate дата и время последнего использования токена
     */
    public void setLastDate(Timestamp lastDate) {
        this.lastDate = lastDate;
    }

    /**
     * Идентификатор запроса для внешней авторизации посредством OAuth
     *
     * @return
     */
    public String getState() {
        return state;
    }

    /**
     * Идентификатор запроса для внешней авторизации посредством OAuth
     *
     * @param state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * Связанный с фактом авторизации ключ для выполнения API внешней системы (при авторизации через внешнюю систему)
     *
     * @return
     */
    public String getExtAccessToken() {
        return extAccessToken;
    }

    /**
     * Связанный с фактом авторизации ключ для выполнения API внешней системы (при авторизации через внешнюю систему)
     *
     * @param extAccessToken
     */
    public void setExtAccessToken(String extAccessToken) {
        this.extAccessToken = extAccessToken;
    }

    @Override
    public String toString() {
        return "id:" + id
                + " state:" + state
                + " extAccessToken:" + extAccessToken
                + " expiredTime:" + expiredTime
                + " lastDate:" + lastDate;
    }

    /**
     * Конечная точка
     *
     * @return
     */
    public Endpoint getEndpoint() {
        return endpoint;
    }

    /**
     * Конечная точка
     *
     * @param endpoint
     */
    public void setEndpoint(Endpoint endpoint) {
        this.endpoint = endpoint;
    }

}
