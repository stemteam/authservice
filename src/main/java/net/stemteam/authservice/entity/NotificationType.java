/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.entity;

import net.stemteam.authservice.entity.enums.NotificationTypeCode;

/**
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class NotificationType {
    
    private int id;
    private NotificationTypeCode code;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public NotificationTypeCode getCode() {
        return code;
    }

    public void setCode(NotificationTypeCode code) {
        this.code = code;
    }
    
    
}
