/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.entity.enums;

/**
 * Коды известных типов нотификаций
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public enum NotificationTypeCode {

    sms,
    email,
    push,
    vk,
    ok,
    facebook,
    esia;

}
