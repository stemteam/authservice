/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.entity;

/**
 * Запрос пользователя с кодом подтверждения
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class UserRequest {

    private final String code;
    private final String type;

    /**
     * Конструктор
     *
     * @param code код подтверждения
     * @param type тип запроса (код типа)
     */
    public UserRequest(String code, String type) {
        this.code = code;
        this.type = type;
    }

    /**
     * Код подтверждения
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     * Тип запроса (код типа)
     *
     * @return
     */
    public String getType() {
        return type;
    }

}
