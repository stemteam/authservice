/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.entity;

import net.stemteam.datatransport.tool.Md5;
import net.stemteam.jaxis.common.ExceptionHelper;
import net.stemteam.rightservice.entity.Role;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static net.stemteam.authservice.common.ApiKeyHelper.bytesToHex;

/**
 * Пользователь системы
 */
public class User {

    // соль1
    private static String PASSWORD_SALT1 = "big bada bum";
    // соль2
    private static String PASSWORD_SALT2 = "big bada bum";

    private Integer id;
    private boolean registered;
    private boolean disabled;
    private String passwordHash;

    private String guuid;
    private String token;
    private String defaultPhone;
    private String defaultEmail;
    private String activationCode;
    private String parentPhone;
    private Timestamp lastdate;
    private Timestamp lastActionDate;
    private Timestamp birthday;

    private List<Role> roles;

    private List<UserReference> references;
    private List<UserRequest> requests;
    private List<Endpoint> endpoints;

    private String firstName;
    private String lastName;
    private String middleName;
    private String image;

    private List<String> emails;
    private List<String> phones;

    public User(Integer id) {
        this.id = id;
    }

    public User(String guuid) {
        this.guuid = guuid;
    }

    /**
     * Конструктор копирования. Используется наследниками для быстрой инициализации данными, полученными из ответов
     * системы авторизации.
     *
     * @param user
     */
    public User(User user) {
        super();

        this.id = user.id;
        this.registered = user.registered;
        this.disabled = user.disabled;
        this.passwordHash = user.passwordHash;
        this.guuid = user.guuid;
        this.token = user.token;
        this.defaultPhone = user.defaultPhone;
        this.defaultEmail = user.defaultEmail;
        this.activationCode = user.activationCode;
        this.parentPhone = user.parentPhone;
        this.lastdate = user.lastdate;
        this.lastActionDate = user.lastActionDate;
        this.roles = user.roles;
        this.references = user.references;
        this.requests = user.requests;
        this.endpoints = user.endpoints;
        this.firstName = user.firstName;
        this.lastName = user.lastName;
        this.middleName = user.middleName;
        this.image = user.image;
        this.emails = user.emails;
        this.phones = user.phones;
        this.birthday = user.birthday;
    }

    /**
     * Адреса электронной почты
     *
     * @return
     */
    public List<String> getEmails() {
        return emails;
    }

    /**
     * Адреса электронной почты
     *
     * @param emails
     */
    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    /**
     * Телефоны
     *
     * @return
     */
    public List<String> getPhones() {
        return phones;
    }

    /**
     * Телефоны
     *
     * @param phones
     */
    public void setPhones(List<String> phones) {
        this.phones = phones;
    }

    /**
     * Дата последних изменений
     *
     * @return
     */
    public Timestamp getLastdate() {
        return lastdate;
    }

    /**
     * Дата последних изменений
     *
     * @param lastdate
     */
    public void setLastdate(Timestamp lastdate) {
        this.lastdate = lastdate;
    }

    /**
     * Аватар пользователя
     *
     * @return
     */
    public String getImage() {
        return image;
    }

    /**
     * Аватар пользователя
     *
     * @param image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * Имя пользователя
     *
     * @return
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Имя пользователя
     *
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Фамилия пользователя
     *
     * @return
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Фамилия пользователя
     *
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Отчество пользователя
     *
     * @return
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Отчество пользователя
     *
     * @param middleName
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Возвращает глобальный идентификатор пользователя (Global Unique User ID)
     *
     * @return
     */
    public String getGuuid() {
        return guuid;
    }

    /**
     * Устанавливает глобальный идентификатор пользователя (Global Unique User ID)
     *
     * @param guuid
     */
    public void setGuuid(String guuid) {
        this.guuid = guuid;
    }

    /**
     * Возвращает токен авторизации пользователя
     *
     * @return
     */
    public String getToken() {
        return token;
    }

    /**
     * Устанавливает токен авторизации пользователя
     *
     * @param token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * Установка номера телефона пользователя по умолчанию
     *
     * @param defaultPhone
     */
    public void setDefaultPhone(String defaultPhone) {
        this.defaultPhone = defaultPhone;
    }

    /**
     * Установка емейла пользователя по умолчанию
     *
     * @param defaultEmail
     */
    public void setDefaultEmail(String defaultEmail) {
        this.defaultEmail = defaultEmail;
    }

    /**
     * Возвращает код активации
     *
     * @return
     */
    public String getActivationCode() {
        return activationCode;
    }

    /**
     * Устанавливает код активации (временный код, используемый для различный целей, отправляемый пользователю для
     * подтверждения его намерений)
     *
     * @param activationCode
     */
    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    /**
     * Конструктор без параметров
     *
     */
    public User() {
    }

    /**
     * Конструктор обыкновенный
     *
     * @param token токен авторизаци пользователя
     * @param guuid глобальный идентификатор пользователя
     */
    public User(String token, String guuid) {
        this.guuid = guuid;
        this.token = token;
    }

    /**
     * Возвращает отношения (связи с другими пользователями)
     *
     * @return
     */
    public List<UserReference> getReferences() {
        return references;
    }

    /**
     * Возвращает отношения (связи с другими пользователями)
     *
     * @param refType тип отношения
     * @return
     */
    public ArrayList<UserReference> getReferences(RefType refType) {
        ArrayList<UserReference> ar = new ArrayList();
        for (UserReference ref : references) {
            if (ref.getRefType().equals(refType)) {
                ar.add(ref);
            }
        }
        return ar;
    }

    /**
     * Получить дату последей зарегистрированной проверки токена пользователя
     *
     * @return дата
     */
    public Timestamp getLastActionDate() {
        return lastActionDate;
    }

    /**
     * Установить дату последей зарегистрированной проверки токена пользователя
     *
     * @return дата
     */
    public void setLastActionDate(Timestamp lastActionDate) {
        this.lastActionDate = lastActionDate;
    }

    /**
     * Проверяет связан ли текущий пользователь с указанным по заданному типу отношений
     *
     * @param user
     * @param refType
     * @return
     */
    public boolean isRefered(User user, RefType refType) {
        for (UserReference ref : references) {
            if (ref.getRefType().equals(refType)) {
                if (ref.getTargetUser().getGuuid().equals(user.getGuuid())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Добавление связи
     *
     * @param target
     * @param refType
     * @return
     */
    public User addReference(User target, RefType refType) {
        UserReference ur = new UserReference(this, target, refType);
        if (references == null) {
            references = new ArrayList<>();
        }
        references.add(ur);
        return this;
    }

    /**
     * роли пользователя
     *
     * @return
     */
    public List<Role> getRoles() {
        return roles;
    }

    /**
     * роли пользователя
     *
     * @param roles
     */
    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public void setReferences(List<UserReference> references) {
        this.references = references;
    }

    public void setRequests(List<UserRequest> requests) {
        this.requests = requests;
    }

    /**
     * Проверяет, имеет ли пользователь указанную роль. Поиск по коду.
     *
     * @param roleCode уникальный код роли
     * @return
     */
    public boolean isRole(final String roleCode) {
        for (Role r : roles) {
            if (r.getCode().equals(roleCode)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Добавляет роль
     *
     * @param role
     * @return
     */
    public boolean addRole(Role role) {
        if (roles == null) {
            roles = new ArrayList<>();
        }
        return roles.add(role);
    }

    /**
     * Удаляет роль
     *
     * @param role
     * @return
     */
    public boolean removeRole(Role role) {
        return roles.remove(role);
    }

    /**
     * Устанавливает телефон родителя
     *
     * @param parentPhone
     */
    public void setParentPhone(String parentPhone) {
        this.parentPhone = parentPhone;
    }

    /**
     * Возвращает телефон родителя
     *
     * @return
     */
    public String getParentPhone() {
        return parentPhone;
    }

    /**
     * Запросы пользователя с кодами подтверждения
     *
     * @return
     */
    public List<UserRequest> getRequests() {
        return requests;
    }

    /**
     * Отключен ли
     *
     * @return
     */
    public boolean isDisabled() {
        return disabled;
    }

    /**
     * Отключен ли
     *
     * @param disabled
     */
    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    /**
     * Отсыпать соли 1
     *
     * @return
     */
    public static String getSalt1() {
        return PASSWORD_SALT1;
    }

    /**
     * Отсыпать соли 2
     *
     * @return
     */
    public static String getSalt2() {
        return PASSWORD_SALT2;
    }

    /**
     * Насыпать соли 1
     *
     * @param salt
     */
    public static void setSalt1(String salt) {
        PASSWORD_SALT1 = salt;
    }

    /**
     * Насыпать соли 2
     *
     * @param salt
     */
    public static void setSalt2(String salt) {
        PASSWORD_SALT2 = salt;
    }

    /**
     * Проверяет корректность номера телефона и соответствие формату 7123456789
     *
     * @param phone
     */
    public static void checkPhone(String phone) {
        String digits = "0123456789";
        if (phone == null) {
            throw new SecurityException(ExceptionHelper.prepareMessage("Не указан номер телефона"));
        }
        if (phone.length() != 11) {
            throw new SecurityException(ExceptionHelper.prepareMessage("Номер телефона не содержит 11 символов"));
        }
        if (!"7".equals(phone.substring(0, 1))) {
            throw new SecurityException(ExceptionHelper.prepareMessage("Номер телефона должен начинаться с 7-ки"));
        }
        for (int i = 0; i < 11; i++) {
            String s = phone.substring(i, i + 1);
            if (!digits.contains(s)) {
                throw new SecurityException(ExceptionHelper.prepareMessage(
                        "Номер телефона должен содержать только цифры"));
            }
        }
    }

    /**
     * Проверяет валидность Email
     *
     * @param email
     * @return
     */
    public static boolean isValidEmail(String email) {
        Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
        Matcher m = p.matcher(email);
        return m.matches();
    }
    private String passwordSalt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    /**
     * Возвращает хэш пароля пользователя (преобразование на сервере)
     *
     * @param login
     * @param passwd
     * @return
     * @throws java.security.NoSuchAlgorithmException
     */
    public static String getPasswdHash(String login, String passwd) throws NoSuchAlgorithmException {
        // приводим логин к нижнему регистру (чтобы не было разночтений)
        login = login.toLowerCase();
        return Md5.md5ToString(Md5.getMd5((passwd + PASSWORD_SALT1 + login + PASSWORD_SALT2).getBytes()));
    }

    /**
     * Возвращает хэш пароля (как это делается на клиенте)
     *
     * @param phone
     * @param passwd
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String getPasswdHashAsClient(String phone, String passwd) throws NoSuchAlgorithmException {
        String sha = "*-*" + passwd + "o_O x_x [o]_[o]";
        MessageDigest md = MessageDigest.getInstance("SHA");
        String s = bytesToHex(md.digest(sha.getBytes()));
        return s.toLowerCase();
    }

    /**
     * Генерирует уникальный идентификатор сеанса пользователя. Используется как временный идентификатор авторизованного
     * птользователя.
     *
     * @return
     */
    public static String prepareToken() {
        return UUID.randomUUID().toString();
    }

    /**
     * Получить email пользователя по умолчанию
     *
     * @return email пользователя
     */
    public String getDefaultEmail() {
        return defaultEmail;
    }

    /**
     * Получить телефон пользователя по умолчанию
     *
     * @return телефон пользователя
     */
    public String getDefaultPhone() {
        return defaultPhone;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public List<Endpoint> getEndpointList() {
        return endpoints;
    }

    public void setEndpoints(List<Endpoint> endpoints) {
        this.endpoints = endpoints;
    }

    /**
     * Индивидуальная соль пароля
     *
     * @param passwordSalt
     */
    public void setPasswordSalt(String passwordSalt) {
        this.passwordSalt = passwordSalt;
    }

    /**
     * Индивидуальная соль пароля
     *
     * @return
     */
    public String getPasswordSalt() {
        return passwordSalt;
    }

    /**
     * Проверка пароля
     *
     * @param password
     * @return
     * @throws NoSuchAlgorithmException
     */
    public boolean checkPassword(String password) throws NoSuchAlgorithmException {
        return getPasswordHash().equals(getPasswdHash(getPasswordSalt(), password));
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", registered=" + registered + ", disabled=" + disabled + ", passwordHash='"
                + passwordHash + '\'' + ", guuid='" + guuid + '\'' + ", token='" + token + '\'' + ", defaultPhone='"
                + defaultPhone + '\'' + ", defaultEmail='" + defaultEmail + '\'' + ", activationCode='" + activationCode
                + '\'' + ", parentPhone='" + parentPhone + '\'' + ", lastdate=" + lastdate + ", roles=" + roles
                + ", references=" + references + ", requests=" + requests + ", endpoints=" + endpoints + ", firstName='"
                + firstName + '\'' + ", lastName='" + lastName + '\'' + ", middleName='" + middleName + '\''
                + ", image='" + image + '\'' + ", emails=" + emails + ", phones=" + phones + ", passwordSalt='"
                + passwordSalt + '\'' + '}';
    }

    /**
     * Определяет наличие признак системного пользователя у ролей пользователя. Пользователь с таким признаком может
     * делать все
     *
     * @return
     */
    public boolean isSystem() {
        if (roles != null) {
            for (Role r : roles) {
                if (r.isAdmin()) {
                    return true;
                }
            }
        } else {
            throw new UnsupportedOperationException(
                    "Роли пользователя не загружены, проверка невозможна. См: UserRequestPropogation.Roles");
        }
        return false;
    }

    /**
     * дата рождения
     * @return 
     */
    public Timestamp getBirthday() {
        return birthday;
    }

    /**
     * дата рождения
     * @param birthday 
     */
    public void setBirthday(Timestamp birthday) {
        this.birthday = birthday;
    }
    
    
}
