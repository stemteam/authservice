package net.stemteam.authservice.entity;

import net.stemteam.authservice.entity.enums.NotificationTypeCode;
import net.stemteam.datatransport.db.ParamBinder;
import net.stemteam.jaxis.common.PNSType;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by warmouse on 06.07.15.
 *
 * Структура с описанием параметров необходимых для отправки уведомления пользователю
 */
public class Endpoint {

    private Integer id;
    private NotificationTypeCode notificationTypeCode;
    private String notificationId;
    private PushNotificationAttributes pushAttributes;
    private boolean enabled;
    private boolean verified;
    private boolean _default;
    private Integer notificationTypeId;
    private Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public boolean isDefault() {
        return _default;
    }

    public void setDefault(boolean _default) {
        this._default = _default;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getNotificationTypeId() {
        return notificationTypeId;
    }

    public void setNotificationTypeId(Integer notificationTypeId) {
        this.notificationTypeId = notificationTypeId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setNotificationTypeCode(NotificationTypeCode notificationTypeCode) {
        this.notificationTypeCode = notificationTypeCode;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public void setPushAttributes(PushNotificationAttributes pushAttributes) {
        this.pushAttributes = pushAttributes;
    }

    public Endpoint() {
    }

    public Endpoint(Integer idEndpoint, NotificationTypeCode notificationTypeCode, String notificationId) {
        this.id = idEndpoint;
        this.notificationTypeCode = notificationTypeCode;
        this.notificationId = notificationId;
    }

    /**
     * Дополнительные атрибуты для PUSH уведомлений Используется как простая структура, без внутренней логики
     */
    public static class PushNotificationAttributes {

        public String azureId = null;
        public String deviceId = null;
        public PNSType PNSType = null;
        public Boolean isTablet = null;
        public String locale = null;
        public String name = null;
        public String model = null;
        public String apiKey = null;

        /**
         * Конструктор. Дополнительные атрибуты системы PUSH уведомлений
         *
         * @param azureId id полученный от MS Azure
         * @param deviceId уникальный ID устройства полученный с устройства
         * @param PNSType типа центра уведомлений
         * @param isTablet это планшет? true - да, false - нет, null - desktop
         * @param locale локаль устройства
         * @param name настраиваемое имя устройства (если есть)
         * @param model модель устройства
         * @param apiKey код типа мобильного приложения
         */
        public PushNotificationAttributes(String azureId, String deviceId, net.stemteam.jaxis.common.PNSType PNSType,
                Boolean isTablet, String locale, String name, String model, String apiKey) {
            this.azureId = azureId;
            this.deviceId = deviceId;
            this.PNSType = PNSType;
            this.isTablet = isTablet;
            this.locale = locale;
            this.name = name;
            this.model = model;
            this.apiKey = apiKey;
        }

        /**
         * Конструктор копирования
         *
         * @param pa
         */
        public PushNotificationAttributes(PushNotificationAttributes pa) {
            this.azureId = pa.azureId;
            this.deviceId = pa.deviceId;
            this.PNSType = pa.PNSType;
            this.isTablet = pa.isTablet;
            this.locale = pa.locale;
            this.name = pa.name;
            this.model = pa.model;
            this.apiKey = pa.apiKey;
        }
    }

    /**
     * Конструктор копирования
     *
     * @param ep
     */
    public Endpoint(Endpoint ep) {
        id = ep.id;
        notificationTypeCode = ep.notificationTypeCode;
        notificationId = ep.notificationId;
        enabled = ep.enabled;
        verified = ep.verified;
        _default = ep._default;
        if (ep.pushAttributes != null) {
            pushAttributes = new PushNotificationAttributes(ep.pushAttributes);
        }
    }

    /**
     * Конструктор
     *
     * @param idEndpoint id в базе данных
     * @param notificationTypeCode тип системы доставки уведомления
     * @param notificationId ID устройства в системе уведомлений (email, phone number, PUSH token)
     * @param pushAttributes дополнительные атрибуты для доставки PUSH уведомлений
     * @param enabled использовать этот канал для доставки уведомлений
     * @param verified точка доставки проверена
     * @param isDefault точка доставки по умолчанию (указана при регистрации)
     */
    public Endpoint(Integer idEndpoint, NotificationTypeCode notificationTypeCode, String notificationId,
            PushNotificationAttributes pushAttributes, Boolean enabled, Boolean verified, Boolean isDefault) {
        if (notificationTypeCode == NotificationTypeCode.push && pushAttributes == null) {
            throw new IllegalArgumentException("Parameter pushAttributes can't be null for notification type PUSH");
        }

        this.id = idEndpoint;
        this.notificationTypeCode = notificationTypeCode;
        this.notificationId = notificationId;
        if (notificationTypeCode == NotificationTypeCode.push) {
            this.pushAttributes = pushAttributes;
        } else {
            this.pushAttributes = null;
        }
        this.enabled = enabled;
        this.verified = verified;
        this._default = isDefault;
    }

    public NotificationTypeCode getNotificationTypeCode() {
        return notificationTypeCode;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public PushNotificationAttributes getPushAttributes() {
        return pushAttributes;
    }

    public Integer getId() {
        return id;
    }

    

}
