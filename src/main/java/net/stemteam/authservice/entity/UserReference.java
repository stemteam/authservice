/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.entity;

/**
 * Отношение пользователей
 */
public class UserReference {

    private final User user;
    private User targetUser;
    private RefType refType;

    public void setTargetUser(User targetUser) {
        this.targetUser = targetUser;
    }

    public void setRefType(RefType refType) {
        this.refType = refType;
    }
    
    public UserReference(User user, User targetUser, RefType refType) {
        this.user = user;
        this.targetUser = targetUser;
        this.refType = refType;
    }

    public User getUser() {
        return user;
    }

    public User getTargetUser() {
        return targetUser;
    }

    public RefType getRefType() {
        return refType;
    }

}
