/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.entity;

/**
 * Тип отношений пользователей
 */
public enum RefType {

    child("child", "Ребенок"),
    slave("slave", "Подчиненный. Владелец волен делать с ним что хочет.");

    private final String code;
    private final String name;

    RefType(String code, String name) {
        this.code = code;
        this.name = name;
    }

    /**
     * Возвращает уникальный код отношения пользователей
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     * Возвращает название отношения пользователей
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Доступ к элементу по коду
     *
     * @param code код (sms, email, push)
     * @return
     */
    public static RefType getByCode(String code) {
        for (RefType v : values()) {
            if (v.getCode().equals(code)) {
                return v;
            }
        }
        throw new IllegalArgumentException("Code " + code + " not exists");
    }
}
