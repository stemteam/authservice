/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.template.handler;

import net.stemteam.authservice.service.impl.AuthServiceNative;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.jaxis.handler.AbstractHandler;

import java.sql.SQLException;
import java.util.HashMap;
import net.stemteam.authservice.exception.UserNotAuthorizedException;
import net.stemteam.authservice.exception.UserNotFoundException;

/**
 * Хэндлер возвращающий DataSet
 */
public abstract class DataSetHandler extends AbstractHandler {

    private AuthServiceNative authService;

    public AuthServiceNative getAuthService() {
        return authService;
    }

    public void setAuthService(AuthServiceNative authService) {
        this.authService = authService;
    }

    @Override
    public String prepareResponce(HashMap params, JaxisContentType contentType) throws HttpUnauthorizedException,
            HttpForbiddenException, HttpInternalException, HttpNotFoundException, HttpBadRequestException, HttpException {
        // запрашиваем данные, получаем дата-сет
        DataSet dataSet = null;
        try {
            dataSet = getData(params, contentType);
        } catch (SecurityException ex) {
            throw new HttpForbiddenException(ex);
        } catch (ConnectionPoolNotFoundException ex) {
            throw new HttpInternalException(ex);
        } catch (DataSetException ex) {
            throw new HttpInternalException(ex);
        } catch (CloseDbConnectionException ex) {
            throw new HttpInternalException(ex);
        } catch (SQLException ex) {
            throw new HttpInternalException(ex);
        } catch (IllegalArgumentException ex) {
            throw new HttpBadRequestException(ex);
        } catch (NotConfiguredException ex) {
            throw new HttpInternalException(ex);
        } catch (UserNotAuthorizedException ex) {
            throw new HttpUnauthorizedException("Пользователь не авторизован");
        } catch (UserNotFoundException ex) {
            throw new HttpNotFoundException("Пользователь не найден");
        } catch (HttpException ex) { // это важно, чтобы не отконвертровать все HTTP исключения в Internal
            throw ex;
        } catch (Exception ex) {
            throw new HttpInternalException(ex);
        }

        if (dataSet == null) {
            return null;
        }

        return getResponceAsString(dataSet, contentType);
    }

    abstract protected DataSet getData(HashMap params, JaxisContentType contentType) throws HttpUnauthorizedException,
            HttpForbiddenException, HttpInternalException, HttpNotFoundException, HttpBadRequestException,
            ConnectionPoolNotFoundException, DataSetException, CloseDbConnectionException, SQLException,
            NotConfiguredException, Exception;
}
