/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.authservice.exception;

import net.stemteam.jaxis.err.StemException;

/**
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class UserNotAuthorizedException extends StemException {

    public UserNotAuthorizedException(String userMessage) {
        super(userMessage);
    }
    
}
