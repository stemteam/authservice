package net.stemteam.dao.common;

import java.io.Serializable;
import java.util.List;

/**
 * IBaseDao interface
 *
 * Created by warmouse, Vadim Zavgorodniy <iwarmouse@gmail.com> on 17.07.16.
 */
public interface IGenericDao<T extends BaseEntity<PK>, PK extends Serializable> {
    T getByPK(PK key) throws DaoException;
    T create(T object) throws DaoException;
    T update(T object) throws DaoException;
    void delete(T object) throws DaoException;
    List<T> getAll() throws DaoException;
}
