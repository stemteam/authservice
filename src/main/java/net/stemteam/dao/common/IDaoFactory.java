package net.stemteam.dao.common;

/**
 * IDaoFactory base DAO factory interface
 *
 * Created by warmouse, Vadim Zavgorodniy <iwarmouse@gmail.com> on 17.07.16.
 */
public interface IDaoFactory<Context> {
    Context getContext() throws DaoException;
    IGenericDao getDao(Context context, Class dtoClass) throws DaoException;
}

//public interface IDaoFactory<Context> {
//    Context getContext() throws DaoException;
//    IGenericDao getDao(Context context, Class dtoClass) throws DaoException;
//}
