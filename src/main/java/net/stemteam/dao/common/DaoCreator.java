package net.stemteam.dao.common;

/**
 * DaoCreator interface
 *
 * Created by warmouse, Vadim Zavgorodniy <iwarmouse@gmail.com> on 17.07.16.
 */
public interface DaoCreator<Context> {
    IGenericDao create(Context context);
}
