package net.stemteam.dao.common;

import java.io.Serializable;

/**
 * BaseEntity class
 * Created by warmouse, Vadim Zavgorodniy <iwarmouse@gmail.com> on 17.07.16.
 */
public abstract class BaseEntity<PK extends Serializable> {
    public abstract PK getId();
    public abstract void setId(PK key);
}
