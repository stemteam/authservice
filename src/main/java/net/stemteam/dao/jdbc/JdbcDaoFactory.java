package net.stemteam.dao.jdbc;

import net.stemteam.dao.common.DaoCreator;
import net.stemteam.dao.common.DaoException;
import net.stemteam.dao.common.IDaoFactory;
import net.stemteam.dao.common.IGenericDao;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Фабрика создания DAO классов работающих через JDBC
 *
 * Created by warmouse, Vadim Zavgorodniy <iwarmouse@gmail.com> on 22.07.16.
 */
public abstract class JdbcDaoFactory implements IDaoFactory<Connection> {
    private DataSource dataSource;

    protected Map<Class, DaoCreator<Connection>> creators = new HashMap<>();

    public DataSource getDataSource() {
        return dataSource;
    }

    @Override
    public Connection getContext() throws DaoException {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public IGenericDao getDao(Connection context, Class dtoClass) throws DaoException {
        DaoCreator<Connection> creator = creators.get(dtoClass);
        if (creator == null) {
            throw new DaoException("Dao object for " + dtoClass + " not found.");
        }

        return creator.create(context);
    }

    /**
     * В реализации этот класс должен быть перекрыт и в нем заполнена карта creators
     *
     * @param dataSource источник данных для DAO
     */
    public JdbcDaoFactory(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
