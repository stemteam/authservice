package net.stemteam.dao.jdbc;

import net.stemteam.dao.common.BaseEntity;
import net.stemteam.dao.common.DaoException;
import net.stemteam.dao.common.IGenericDao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

/**
 * Базовая реализация DAO классов на основе JDBC
 *
 * Created by warmouse, Vadim Zavgorodniy <iwarmouse@gmail.com> on 22.07.16.
 */
public abstract class GenericDaoJdbc<T extends BaseEntity<PK>, PK extends Serializable>
        implements IGenericDao<T, PK> {
    protected Connection connection;

    protected Class<T> type;

    public GenericDaoJdbc(Connection connection, Class<T> type) {
        this.connection = connection;
        this.type = type;
    }

    public Connection getConnection() {
        return connection;
    }

    @Override
    public T getByPK(PK key) throws DaoException {
        throw new DaoException("This method is not implemented yet.");
    }

    @Override
    public T create(T object) throws DaoException {
        throw new DaoException("This method is not implemented yet.");
    }

    @Override
    public T update(T object) throws DaoException {
        throw new DaoException("This method is not implemented yet.");
    }

    @Override
    public void delete(T object) throws DaoException {
        throw new DaoException("This method is not implemented yet.");
    }

    @Override
    public List<T> getAll() throws DaoException {
        throw new DaoException("This method is not implemented yet.");
    }
}
