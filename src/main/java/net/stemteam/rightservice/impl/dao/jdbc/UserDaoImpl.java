package net.stemteam.rightservice.impl.dao.jdbc;

import net.stemteam.dao.common.DaoException;
import net.stemteam.dao.jdbc.GenericDaoJdbc;
import net.stemteam.rightservice.dao.UserDao;
import net.stemteam.rightservice.entity.Role;
import net.stemteam.rightservice.entity.RightUser;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * UserDao implementation
 *
 * Created by Vadim Zavgorodniy <iwarmouse@gmail.com> on 22.07.16.
 */
public class UserDaoImpl extends GenericDaoJdbc<RightUser, Integer> implements UserDao {

    public UserDaoImpl(Connection connection) {
        super(connection, RightUser.class);
    }

    @Override
    public RightUser getByPK(Integer key) throws DaoException {

        RightUser user = null;
        try {
            PreparedStatement ps1 = connection.prepareStatement(
                    "select id_stem_right_user as id, guuid from stem_right_user where id_stem_right_user = ? ");
            ps1.setInt(1, key);

            try (ResultSet rs1 = ps1.executeQuery()) {
                if (rs1.next()) {
                    user = new RightUser();
                    user.setId(rs1.getInt("ID"));
                    user.setGuuid(rs1.getString("GUUID"));
                }
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }

        return user;
    }

    @Override
    public RightUser create(RightUser object) throws DaoException {
        try {
            PreparedStatement ps = connection.prepareStatement(
                    "insert into stem_right_user (guuid) values (?) ",
                    Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, object.getGuuid());
            ps.executeUpdate();
            try (ResultSet keyset = ps.getGeneratedKeys()) {
                keyset.next();

                object.setId(keyset.getInt(1));
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }

        return object;
    }

    @Override
    public RightUser update(RightUser object) throws DaoException {
        throw new DaoException("This method is not implemented yet.");
    }

    @Override
    public void delete(RightUser object) throws DaoException {
        try {
            PreparedStatement ps = connection.prepareStatement("delete from stem_right_user u\n" +
                    " where u.id_stem_right_user = ?");
            ps.setInt(1, object.getId());
            ps.executeUpdate();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    public RightUser getByGuuid(String guuid) throws DaoException {
        RightUser user = null;

        try {
            PreparedStatement ps1 = connection.prepareStatement(
                    "select id_stem_right_user as id, guuid from stem_right_user where guuid = ? ");
            ps1.setString(1, guuid);

            try (ResultSet rs1 = ps1.executeQuery()) {
                if (rs1.next()) {
                    user = new RightUser();
                    user.setId(rs1.getInt("ID"));
                    user.setGuuid(rs1.getString("GUUID"));
                }
            }

        } catch (Exception e) {
            throw new DaoException(e);
        }

        return user;
    }

    @Override
    public void addRole(RightUser user, Role role) throws DaoException {
        try {
            PreparedStatement ps = connection.prepareStatement("insert into\n" +
                    "  stem_right_userrole (id_stem_right_user, id_stem_right_role)\n" +
                    "values (?, ?)");
            ps.setInt(1, user.getId());
            ps.setInt(2, role.getId());
            ps.executeUpdate();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void deleteRole(RightUser user, Role role) throws DaoException {
        try {
            PreparedStatement ps = connection.prepareStatement("delete from stem_right_userrole\n" +
                    "where id_stem_right_user = ? and id_stem_right_role = ?");
            ps.setInt(1, user.getId());
            ps.setInt(2, role.getId());
            ps.executeUpdate();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    public boolean isAdmin(RightUser user) throws DaoException {
        boolean res = false;

        try {
            PreparedStatement ps = connection.prepareStatement("select count(1) > 0 as is_admin\n" +
                    "from stem_right_user u\n" +
                    "  join stem_right_userrole ur on ur.id_stem_right_user = u.id_stem_right_user\n" +
                    "  join  stem_right_role s on s.id_stem_right_role = ur.id_stem_right_role\n" +
                    "where\n" +
                    "  u.id_stem_right_user = ?\n" +
                    "  and s.is_admin = 1");
            ps.setInt(1, user.getId());

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    res = rs.getBoolean("IS_ADMIN");
                }
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }

        return res;
    }
}
