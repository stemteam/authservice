package net.stemteam.rightservice.impl.dao.jdbc;

import net.stemteam.dao.common.DaoCreator;
import net.stemteam.dao.common.IGenericDao;
import net.stemteam.dao.jdbc.JdbcDaoFactory;
import net.stemteam.rightservice.entity.Role;
import net.stemteam.rightservice.entity.RightUser;

import javax.sql.DataSource;
import java.sql.Connection;

/**
 * Реализация фабрики для создания классов RightService
 *
 * Created by warmouse, Vadim Zavgorodniy <iwarmouse@gmail.com> on 23.07.16.
 */
public class JdbcDaoFactoryImpl extends JdbcDaoFactory {
    public JdbcDaoFactoryImpl(DataSource dataSource) {
        super(dataSource);

        creators.put(RightUser.class, new DaoCreator<Connection>() {
            @Override
            public IGenericDao create(Connection connection) {
                return new UserDaoImpl(connection) {
                };
            }
        });

        creators.put(Role.class, new DaoCreator<Connection>() {
            @Override
            public IGenericDao create(Connection connection) {
                return new RoleDaoImpl(connection) {
                };
            }
        });
    }
}
