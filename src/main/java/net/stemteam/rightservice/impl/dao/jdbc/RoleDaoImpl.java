package net.stemteam.rightservice.impl.dao.jdbc;

import net.stemteam.dao.common.DaoException;
import net.stemteam.dao.jdbc.GenericDaoJdbc;
import net.stemteam.rightservice.dao.RoleDao;
import net.stemteam.rightservice.entity.Role;
import net.stemteam.rightservice.entity.RightUser;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

/**
 * RoleDao implementation
 *
 * Created by Vadim Zavgorodniy <iwarmouse@gmail.com> on 22.07.16.
 */
public class RoleDaoImpl extends GenericDaoJdbc<Role, Integer> implements RoleDao {
    public RoleDaoImpl(Connection connection) {
        super(connection, Role.class);
    }

    @Override
    public Role getByPK(Integer key) throws DaoException {
        Role role = null;

        try {
            PreparedStatement ps = connection.prepareStatement(
                    " select s.id_STEM_RIGHT_role as role_id, s.name, s.description, s.code, is_admin\n" +
                            " from STEM_RIGHT_role s\n" +
                            " where s.id_STEM_RIGHT_role = ?" +
                            " order by s.code");
            ps.setInt(1, key);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    role = new Role();
                    role.setId(rs.getInt("ROLE_ID"));
                    role.setCode(rs.getString("CODE"));
                    role.setName(rs.getString("NAME"));
                    role.setDescription(rs.getString("DESCRIPTION"));
                    role.setAdmin(rs.getInt("IS_ADMIN") == 1);
                }
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }
        return role;
    }

    @Override
    public Role create(Role object) throws DaoException {
        try {
            PreparedStatement ps = connection.prepareStatement(
                    "insert into stem_right_role (name, description, code, is_admin) values (?,?,?,?) ",
                    Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, object.getName());
            ps.setString(2, object.getDescription());
            ps.setString(3, object.getCode());
            ps.setInt(4, object.isAdmin() ? 1 : 0);
            ps.executeUpdate();
            try (ResultSet keyset = ps.getGeneratedKeys()) {
                keyset.next();

                object.setId(keyset.getInt(1));
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }

        return object;
    }

    @Override
    public Role update(Role object) throws DaoException {
        try {
            PreparedStatement ps = connection.prepareStatement("update stem_right_role set " +
                            "name = ?, description = ?, code = ?, is_admin = ? where id_stem_right_role = ?");
            ps.setString(1, object.getName());
            ps.setString(2, object.getDescription());
            ps.setString(3, object.getCode());
            ps.setInt(4, object.isAdmin() ? 1 : 0);
            ps.setInt(5, object.getId());

            ps.executeUpdate();
        } catch (Exception e) {
            throw new DaoException(e);
        }

        return object;
    }

    @Override
    public void delete(Role object) throws DaoException {
        try {
            PreparedStatement ps = connection.prepareStatement("delete from stem_right_role\n" +
                    "where id_stem_right_role = ?");
            ps.setInt(1, object.getId());
            ps.executeUpdate();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    public List<Role> getAll() throws DaoException {
        List<Role> res = new LinkedList<>();

        try {
            PreparedStatement ps = connection.prepareStatement(
                    " select s.id_STEM_RIGHT_role as role_id, s.name, s.description, s.code, s.is_admin\n" +
                            " from STEM_RIGHT_role s\n" +
                            " order by s.code");
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Role role = new Role();
                    role.setId(rs.getInt("ROLE_ID"));
                    role.setCode(rs.getString("CODE"));
                    role.setName(rs.getString("NAME"));
                    role.setDescription(rs.getString("DESCRIPTION"));
                    role.setAdmin(rs.getInt("IS_ADMIN") == 1);
                    res.add(role);
                }
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }
        return res;
    }

    @Override
    public Role getRoleByCode(String code) throws DaoException {
        Role role = null;

        try {
            PreparedStatement ps = connection.prepareStatement(
                    " select s.id_STEM_RIGHT_role as role_id, s.name, s.description, s.code, is_admin\n" +
                            " from STEM_RIGHT_role s\n" +
                            " where s.code = ?");
            ps.setString(1, code);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    role = new Role();
                    role.setId(rs.getInt("ROLE_ID"));
                    role.setCode(rs.getString("CODE"));
                    role.setName(rs.getString("NAME"));
                    role.setDescription(rs.getString("DESCRIPTION"));
                    role.setAdmin(rs.getInt("IS_ADMIN") == 1);
                }
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }
        return role;
    }

    @Override
    public List<Role> getRoleByUser(RightUser user) throws DaoException {
        List<Role> res = new LinkedList<>();

        try {
            PreparedStatement ps = connection.prepareStatement("select s.id_STEM_RIGHT_role as role_id,\n" +
                    " s.name, s.description, s.code, s.is_admin\n" +
                    " from STEM_RIGHT_role s\n" +
                    "   join STEM_RIGHT_userrole ur on s.id_STEM_RIGHT_role = ur.id_STEM_RIGHT_role\n" +
                    "   join STEM_RIGHT_user u on ur.id_STEM_RIGHT_user = u.id_STEM_RIGHT_user\n" +
                    " where u.id_STEM_RIGHT_user = ?\n" +
                    " order by s.code");
            ps.setInt(1, user.getId());
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Role role = new Role();
                    role.setId(rs.getInt("ROLE_ID"));
                    role.setCode(rs.getString("CODE"));
                    role.setName(rs.getString("NAME"));
                    role.setDescription(rs.getString("DESCRIPTION"));
                    role.setAdmin(rs.getInt("IS_ADMIN") == 1);
                    res.add(role);
                }
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }
        return res;
    }

    @Override
    public void deleteUserRoles(RightUser user) throws DaoException {
        try {
            PreparedStatement ps = connection.prepareStatement("delete from stem_right_userrole ur\n" +
                    "where id_stem_right_userrole in (\n" +
                    " select id_stem_right_userrole from stem_right_userrole ur\n" +
                    "  join stem_right_user u on u.id_stem_right_user = ur.id_stem_right_user\n" +
                    "where u.guuid = ?)");
            ps.setString(1, user.getGuuid());
            ps.execute();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }
}
