package net.stemteam.rightservice.impl.client;

import net.stemteam.dao.common.DaoException;
import net.stemteam.dao.common.IDaoFactory;
import net.stemteam.rightservice.dao.RoleDao;
import net.stemteam.rightservice.dao.UserDao;
import net.stemteam.rightservice.entity.Role;
import net.stemteam.rightservice.entity.RightUser;
import net.stemteam.rightservice.exception.RightServiceException;
import net.stemteam.rightservice.service.RightService;

import java.sql.Connection;
import java.util.LinkedList;
import java.util.List;

/**
 * Класс реализующий интерфейс RightClient для прямой работы с системой прав
 *
 * Created by Vadim Zavgorodniy <iwarmouse@gmail.com> on 06.07.16.
 */
public class RightClientJava implements RightService {
    private IDaoFactory<Connection> factory;

    public RightClientJava(IDaoFactory<Connection> factory) {
        this.factory = factory;
    }

    @Override
    public List<Role> getRoleList() throws RightServiceException {
        try (Connection context = factory.getContext()) {
            RoleDao roleDao = (RoleDao)factory.getDao(context, Role.class);
            return roleDao.getAll();
        } catch (Exception e) {
            throw new RightServiceException(e);
        }
    }

    @Override
    public Role getRoleByCode(String roleCode) throws RightServiceException {
        try (Connection context = factory.getContext()) {
            RoleDao roleDao = (RoleDao)factory.getDao(context, Role.class);
            return roleDao.getRoleByCode(roleCode);
        } catch (Exception e) {
            throw new RightServiceException(e);
        }
    }

    @Override
    public Role getRoleById(Integer id) throws RightServiceException {
        try (Connection context = factory.getContext()) {
            RoleDao roleDao = (RoleDao)factory.getDao(context, Role.class);

            Role role = roleDao.getByPK(id);
            if (role == null) {
                throw new RightServiceException("Role not found");
            }

            return role;
        } catch (Exception e) {
            throw new RightServiceException(e);
        }
    }

    @Override
    public boolean getIsAdmin(String guuid) throws RightServiceException {
        try (Connection context = factory.getContext()) {
            UserDao userDao = (UserDao)factory.getDao(context, RightUser.class);
            RightUser user = userDao.getByGuuid(guuid);

            return  user != null && userDao.isAdmin(user);
        } catch (Exception e) {
            throw new RightServiceException(e);
        }
    }

    @Override
    public List<Role> getUserRoleList(String guuid) throws RightServiceException {
        try (Connection context = factory.getContext()) {
            RoleDao roleDao = (RoleDao)factory.getDao(context, Role.class);
            UserDao userDao = (UserDao)factory.getDao(context, RightUser.class);
            RightUser user = userDao.getByGuuid(guuid);
            if (user == null) {
                return new LinkedList<>();
            }

            return roleDao.getRoleByUser(user);
        } catch (Exception e) {
            throw new RightServiceException(e);
        }
    }

    @Override
    public void addUserRole(String guuid, int roleId) throws RightServiceException {
        try (Connection context = factory.getContext()) {
            UserDao userDao = (UserDao)factory.getDao(context, RightUser.class);
            RoleDao roleDao = (RoleDao)factory.getDao(context, Role.class);
            try {
                RightUser user = userDao.getByGuuid(guuid);
                if (user == null) {
                    user = new RightUser();
                    user.setGuuid(guuid);
                    user = userDao.create(user);
                }

                Role role = roleDao.getByPK(roleId);
                if (role == null) {
                    throw new RightServiceException("Role not found");
                }
                userDao.addRole(user, role);
                context.commit();
            } catch (DaoException e) {
                context.rollback();
            }
        } catch (Exception e) {
            throw new RightServiceException(e);
        }
    }

    @Override
    public void deleteUserRole(String guuid, int roleId) throws RightServiceException {
        try (Connection context = factory.getContext()) {
            UserDao userDao = (UserDao)factory.getDao(context, RightUser.class);
            RoleDao roleDao = (RoleDao)factory.getDao(context, Role.class);
            try {
                RightUser user = userDao.getByGuuid(guuid);
                Role role = roleDao.getByPK(roleId);
                if (user != null && role != null) {
                    userDao.deleteRole(user, role);
                    context.commit();
                }
            } catch (DaoException e) {
                context.rollback();
            }
        } catch (Exception e) {
            throw new RightServiceException(e);
        }
    }

    @Override
    public void deleteRightUser(String guuid) throws RightServiceException {
        try (Connection context = factory.getContext()) {
            UserDao userDao = (UserDao)factory.getDao(context, RightUser.class);
            RoleDao roleDao = (RoleDao)factory.getDao(context, Role.class);
            try {
                RightUser user = userDao.getByGuuid(guuid);
                if (user != null) {
                    roleDao.deleteUserRoles(user);
                    userDao.delete(user);

                    context.commit();
                }
            } catch (DaoException e) {
                context.rollback();
            }
        } catch (Exception e) {
            throw new RightServiceException(e);
        }
    }

    @Override
    public RightUser getRightUserByGuuid(String guuid) throws RightServiceException {
        try (Connection context = factory.getContext()) {
            UserDao userDao = (UserDao)factory.getDao(context, RightUser.class);
            return userDao.getByGuuid(guuid);
        } catch (Exception e) {
            throw new RightServiceException(e);
        }
    }
}
