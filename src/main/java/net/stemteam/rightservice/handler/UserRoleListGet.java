/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.rightservice.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataColumnType;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.annotation.Method;
import net.stemteam.jaxis.annotation.Param;
import net.stemteam.jaxis.annotation.Return;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.entity.Role;
import net.stemteam.rightservice.exception.RightServiceException;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import net.stemteam.authservice.api.support.DataSetProvider;
import net.stemteam.authservice.entity.User;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.jaxis.err.StemException;

/**
 * Получить список ролей пользователя
 */
public class UserRoleListGet extends DataSetHandler {

    @Method(desc = "Получить список ролей пользователя",
            params = {
                @Param(required = true, name = "Version", desc = "Версия протокола"),
                @Param(required = true, name = PARAM_TOKEN, desc = "токен авторизации"),
                @Param(required = true, name = "ApiKey", desc = "Ключ API"),
                @Param(required = true, name = "Guuid", desc = "Идентификатор пользователя"),},
            returns = @Return(
                    items = {
                        @Param(name = "Id", desc = "Идентификатор роли", type = DataColumnType.INT32),
                        @Param(name = "Name", desc = "Название роли"),
                        @Param(name = "Code", desc = "Код роли"),
                        @Param(name = "Description", desc = "Описание роли")
                    }))
    @Override
    public DataSet getData(HashMap params, JaxisContentType jct) throws ConnectionPoolNotFoundException,
            DataSetException, CloseDbConnectionException, SQLException, HttpForbiddenException, NotConfiguredException,
            HttpInternalException, HttpUnauthorizedException, RightServiceException, StemException {
        String Version = getParamValidator().getString(params, PARAM_VERSION, true);
        String ApiKey = getParamValidator().getString(params, PARAM_API_KEY, true);
        String guuid = getParamValidator().getString(params, PARAM_GUUID, true);
        String token = getParamValidator().getString(params, PARAM_TOKEN, true);
        getParamValidator().postValidate();

        // проверка API-ключа
        ApiKeyHelper.getInstance().check(getDataSource(), ApiKey);

        List<Role> roles = getAuthService().getUserRoleList(token, guuid);
        DataSetProvider prov = new DataSetProvider();
        DataSet ds = prov.getRoleListDataSet(roles);

//        DataSet ds = POJO2DataSet.convertCollection(roles);
//        if (ds != null) {
//            ds.doCamelColumnNames(true);
//        }
        return ds;
    }
    public static final String PARAM_TOKEN = "Token";
    public static final String PARAM_GUUID = "Guuid";
    public static final String PARAM_API_KEY = "ApiKey";
    public static final String PARAM_VERSION = "Version";

}
