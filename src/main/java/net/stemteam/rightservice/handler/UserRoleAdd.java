/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.rightservice.handler;

import net.stemteam.authservice.common.ApiKeyHelper;
import net.stemteam.datatransport.exception.CloseDbConnectionException;
import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataColumnType;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.annotation.Method;
import net.stemteam.jaxis.annotation.Param;
import net.stemteam.jaxis.annotation.Return;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.rightservice.exception.RightServiceException;

import java.sql.SQLException;
import java.util.HashMap;
import net.stemteam.authservice.template.handler.DataSetHandler;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.StemException;

/**
 * Добавить роль пользователю
 */
public class UserRoleAdd extends DataSetHandler {
    public static String PARAM_ROLE_CODE = "RoleCode";

    @Method(desc = "Добавить роль пользователю",
            params = {
                @Param(required = true, name = "Version", desc = "Версия протокола"),
                @Param(required = true, name = PARAM_TOKEN, desc = "токен авторизации"),
                @Param(required = true, name = "ApiKey", desc = "Ключ API"),
                @Param(required = true, name = "Guuid", desc = "Идентификатор пользователя"),
                @Param(required = true, name = "RoleId", desc = "Идентификатор роли",
                        type = DataColumnType.INT32),},
            returns = @Return(desc = "Возвращает: HTTP OK", items = {}))
    @Override
    public DataSet getData(HashMap params, JaxisContentType jct) throws ConnectionPoolNotFoundException,
            DataSetException, CloseDbConnectionException, SQLException, HttpForbiddenException, NotConfiguredException,
            HttpInternalException, HttpUnauthorizedException, RightServiceException, StemException {
        String Version = getParamValidator().getString(params, PARAM_VERSION, true);
        String ApiKey = getParamValidator().getString(params, PARAM_API_KEY, true);
        String guuid = getParamValidator().getString(params, PARAM_GUUID, true);
        Integer roleId = getParamValidator().getInt(params, PARAM_ROLE_ID);
        String roleCode = getParamValidator().getString(params, PARAM_ROLE_CODE);
        String token = getParamValidator().getString(params, PARAM_TOKEN, true);
        getParamValidator().postValidate();
        
        if ((roleCode == null) && (roleId == null)) {
            throw new HttpBadRequestException("Должен быть передан хотя бы один из параметров {roleId, roleCode}");
        }

        // проверка API-ключа
        ApiKeyHelper.getInstance().check(getDataSource(), ApiKey);

        if (roleId != null) {
            getAuthService().addUserRole(token, guuid, roleId);
        } else {
            getAuthService().addUserRole(token, guuid, roleCode);
        }

        return null;
    }
    public static final String PARAM_TOKEN = "Token";
    public static final String PARAM_ROLE_ID = "RoleId";
    public static final String PARAM_GUUID = "Guuid";
    public static final String PARAM_API_KEY = "ApiKey";
    public static final String PARAM_VERSION = "Version";

}
