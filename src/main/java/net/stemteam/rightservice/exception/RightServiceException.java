package net.stemteam.rightservice.exception;

import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.err.StemException;

/**
 * Общее исключение системы работы с правами
 *
 * Created by Vadim Zavgorodniy <iwarmouse@gmail.com> on 14.07.16.
 */
public class RightServiceException extends StemException {
    public RightServiceException(Exception ex, String userMessage) {
        super(ex, userMessage);
    }

    public RightServiceException(String userMessage) {
        super(userMessage);
    }

    public RightServiceException(DataSet errorDataSet) {
        super(errorDataSet);
    }

    public RightServiceException(Exception ex) {
        super(ex);
    }
}
