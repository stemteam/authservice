package net.stemteam.rightservice.entity;

import net.stemteam.dao.common.BaseEntity;

/**
 * Сущность представляющая роль пользователя в системе
 *
 * Created by Vadim Zavgorodniy <iwarmouse@gmail.com> on 06.07.16.
 */
public class Role extends BaseEntity<Integer> {
    Integer id;
    String name;
    String description;
    String code;
    boolean isAdmin = false;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean isAdimn) {
        this.isAdmin = isAdimn;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", code='" + code + '\'' +
                ", isAdmin=" + isAdmin +
                '}';
    }
}
