package net.stemteam.rightservice.entity;

import net.stemteam.dao.common.BaseEntity;

/**
 * Пользователь системы прав
 *
 * Created by Vadim Zavgorodniy <iwarmouse@gmail.com> on 06.07.16.
 */
public class RightUser extends BaseEntity<Integer> {
    Integer id;
    String guuid;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getGuuid() {
        return guuid;
    }

    public void setGuuid(String guuid) {
        this.guuid = guuid;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", guuid='" + guuid + '\'' +
                '}';
    }
}
