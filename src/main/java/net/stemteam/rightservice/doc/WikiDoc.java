/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.rightservice.doc;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import net.stemteam.authservice.service.AuthServiceFactory;

/**
 * Класс для генерации wiki-документации на основе аннотаций к методам сервиса
 */
public class WikiDoc {

    protected WikiDoc() {
        throw new UnsupportedOperationException();
    }

    public static void main(String[] args) throws FileNotFoundException {
        // путь к папке с доками
        String fileName = args.length > 0 ? args[0] : "auth.txt";
        String header = "";
        String footer = "";
        if (args.length > 1) {
            header = args[1] + "\n\n";
        }
        if (args.length > 2) {
            footer = args[2] + "\n\n";
        }

        File f = new File(fileName);
        if (f.getParentFile() != null) {
            f.getParentFile().mkdirs();
        }

        // записываем в файл
        try (PrintWriter out = new PrintWriter(fileName)) {

            out.write(header);

            ArrayList<Class> classes = new ArrayList<>(Arrays.asList(AuthServiceFactory.HANDLER_CLASSES));
            classes.addAll(Arrays.asList(AuthServiceFactory.RIGHT_HANDLER_CLASSES));
            
            printClassesDoc(out, classes);

            out.write(footer);

            out.flush();
        }
    }

    protected static void printClassesDoc(PrintWriter out, ArrayList<Class> classes) {
        Collections.sort(classes, new Comparator<Class>() {
            @Override
            public int compare(Class o1, Class o2) {
                return o1.getSimpleName().compareTo(o2.getSimpleName());
            }
        });

        for (Class cls : classes) {

            // генерация документации для каждого файла
            System.out.println("[DOC-WIKI] generating doc for " + cls.getSimpleName());
            String s = net.stemteam.jaxis.annotation.TranslatorWiki.getDoc(cls, cls.getSimpleName());

            out.write(s);
        }
    }
}
