package net.stemteam.rightservice.dao;

import net.stemteam.dao.common.DaoException;
import net.stemteam.dao.common.IGenericDao;
import net.stemteam.rightservice.entity.Role;
import net.stemteam.rightservice.entity.RightUser;

/**
 * RightUser DAO interface

 Created by Vadim Zavgorodniy <iwarmouse@gmail.com> on 22.07.16.
 */
public interface UserDao extends IGenericDao<RightUser, Integer> {
    /**
     * Найти пользователя по его guuid
     *
     * @param guuid идентификатор пользователя
     * @return RightUser или null если не найден
     * @throws DaoException
     */
    RightUser getByGuuid(String guuid) throws DaoException;

    /**
     * Добавить роль пользователю
     *
     * @param user изменяемый пользователь
     * @param role роль для добавления
     */
    void addRole(RightUser user, Role role) throws DaoException;

    /**
     * Удалить роль пользователю
     *
     * @param user изменяемый пользователь
     * @param role роль для удаления
     * @throws DaoException
     */
    void deleteRole(RightUser user, Role role) throws DaoException;

    /**
     * Получить признак наличия роли с правами администратора
     *
     * @param user пользователь для поиска
     * @return true - если есть права администратора, иначе false
     * @throws DaoException
     */
    boolean isAdmin(RightUser user) throws DaoException;
}
