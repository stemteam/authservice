package net.stemteam.rightservice.dao;

import net.stemteam.dao.common.DaoException;
import net.stemteam.dao.common.IGenericDao;
import net.stemteam.rightservice.entity.Role;
import net.stemteam.rightservice.entity.RightUser;

import java.util.List;

/**
 * Role DAO interface
 *
 * Created by Vadim Zavgorodniy <iwarmouse@gmail.com> on 22.07.16.
 */
public interface RoleDao extends IGenericDao<Role, Integer> {

    /**
     * Получить роль по коду роли
     *
     * @param code код роли
     * @return роль
     * @throws DaoException
     */
    Role getRoleByCode(String code) throws DaoException;

    /**
     * Получить список ролей пользователя
     *
     * @param user
     * @return список ролей
     * @throws DaoException
     */
    List<Role> getRoleByUser(RightUser user) throws DaoException;

    /**
     * Удалить все роли пользователя
     *
     * @param user пользователь
     * @throws DaoException
     */
    void deleteUserRoles(RightUser user) throws DaoException;
}
