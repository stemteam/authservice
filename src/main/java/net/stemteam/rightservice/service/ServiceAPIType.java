package net.stemteam.rightservice.service;

/**
 * Типы доступных клиентов
 *
 * Created by Vadim Zavgorodniy <iwarmouse@gmail.com> on 07.07.16.
 */
public enum ServiceAPIType {
    JAVA_NATIVE,
    HTTP_REQUEST
}
