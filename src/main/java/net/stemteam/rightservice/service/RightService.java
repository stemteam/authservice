package net.stemteam.rightservice.service;

import net.stemteam.rightservice.entity.Role;
import net.stemteam.rightservice.entity.RightUser;
import net.stemteam.rightservice.exception.RightServiceException;

import java.util.List;

/**
 * Интерфейс для доступа к сервису работы с ролями и правами
 *
 * Created by Vadim Zavgorodniy <iwarmouse@gmail.com> on 06.07.16.
 */
public interface RightService {
    /**
     * Получить список доступных ролей
     *
     * @return
     */
    List<Role> getRoleList() throws RightServiceException;

    /**
     * Получить описание роли по коду
     *
     * @param roleCode код роли
     * @return
     * @throws RightServiceException
     */
    Role getRoleByCode(String roleCode) throws RightServiceException;

    /**
     * Получить описание роли по id
     *
     * @param id идентификатор роли
     * @return
     * @throws RightServiceException
     */
    Role getRoleById(Integer id) throws RightServiceException;

    /**
     * Получить признак наличия флага администратора у пользователя
     *
     * @param guuid идентификатор пользователя
     * @return true - если у пользователя есть права на управление другими пользователями, иначе false
     * @throws RightServiceException
     */
    boolean getIsAdmin(String guuid) throws RightServiceException;

    /**
     * Получить список ролей пользователя
     *
     * @param guuid идентификатор пользователя
     * @return
     */
    List<Role> getUserRoleList(String guuid) throws RightServiceException;

    /**
     * Добавить пользователю роль с заданным идентификатором
     *
     * @param guuid идентификатор пользователя
     * @param roleId идентификатор добавляемой роли
     */
    void addUserRole(String guuid, int roleId) throws RightServiceException;

    /**
     * Забрать у пользователя роль с заданным идентификатором
     *
     * @param guuid идентификатор пользователя
     * @param roleId идентификатор забираемой роли
     */
    void deleteUserRole(String guuid, int roleId) throws RightServiceException;

    /**
     * Удалить пользователя из системы
     *
     * @param guuid идентификатор пользователя
     * @throws RightServiceException
     */
    void deleteRightUser(String guuid) throws RightServiceException;

    /**
     * Получить пользователя по guuid
     *
     * @param guuid идентификатор пользователя
     * @return
     * @throws RightServiceException
     */
    RightUser getRightUserByGuuid(String guuid) throws RightServiceException;
}

